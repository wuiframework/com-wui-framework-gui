/** WARNING: this file has been automatically generated from typescript interfaces, which exist in this package. */

/* tslint:disable: variable-name no-use-before-declare only-arrow-functions */
namespace Com.Wui.Framework.Gui.HttpProcessor.Resolvers {
    "use strict";
    export let IViewerTestEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnSuiteStart",
                "setOnSuiteEnd",
                "setOnTestCaseStart",
                "setOnTestCaseEnd",
                "setOnAssert"
            ]);
        }();
}

namespace Com.Wui.Framework.Gui.HttpProcessor.Resolvers {
    "use strict";
    export let IViewerTestPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ]);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    export let IGuiCommons : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "getGuiOptions",
                "getEvents",
                "getGuiElementClass",
                "Visible",
                "Enabled",
                "StyleClassName",
                "Id",
                "Parent",
                "InstanceOwner",
                "InstancePath",
                "getChildElements",
                "getScreenPosition",
                "setPosition",
                "getSize",
                "Width",
                "Height",
                "FitToParent",
                "getFitToParent",
                "IsCached",
                "IsPrepared",
                "IsLoaded",
                "IsCompleted",
                "DisableAsynchronousDraw",
                "Draw",
                "getArgs",
                "setArg",
                "getInnerHtmlMap",
                "getWrappingElement",
                "setWrappingElement",
                "IsPreventingScroll"
            ], Com.Wui.Framework.Commons.Interfaces.IBaseObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Components {
    "use strict";
    export let ICropBox : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GuiType",
                "setDimensions",
                "setSize",
                "EnvelopOwner",
                "getEvents"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Components {
    "use strict";
    export let IDragBar : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "getEvents"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Components {
    "use strict";
    export let IFileUpload : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "getEvents",
                "setOpenElement",
                "setDropZone",
                "getStream",
                "Value",
                "Upload",
                "Aboard",
                "MultipleSelectEnabled",
                "Filter",
                "MaxFileSize",
                "MaxChunkSize"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Components {
    "use strict";
    export let INotification : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GuiType",
                "Width",
                "Text"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Components {
    "use strict";
    export let IResizeBar : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "ResizeableType",
                "getEvents"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Components {
    "use strict";
    export let IScrollBar : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GuiType",
                "OrientationType",
                "Size",
                "getEvents"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Components {
    "use strict";
    export let ISelectBox : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GuiType",
                "Value",
                "Width",
                "Height",
                "MaxVisibleItemsCount",
                "Add",
                "Select",
                "Clear",
                "OptionsCount",
                "DisableCharacterNavigation",
                "DisableAdvancedSelection",
                "setInitSize",
                "setOpenDirection",
                "getEvents"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Components {
    "use strict";
    export let IToolTip : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GuiType",
                "Text"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IAsyncRequestEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IGuiCommonsEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "getOwner",
                "Subscriber",
                "Exists",
                "getAll",
                "setEvent",
                "FireAsynchronousMethod",
                "RemoveHandler",
                "Clear",
                "Subscribe",
                "setOnStart",
                "setBeforeLoad",
                "setOnLoad",
                "setOnComplete",
                "setOnShow",
                "setOnHide",
                "setOnMouseOver",
                "setOnMouseOut",
                "setOnMouseMove",
                "setOnMouseDown",
                "setOnMouseUp",
                "setOnClick",
                "setOnDoubleClick"
            ], Com.Wui.Framework.Commons.Interfaces.IBaseObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IFormsObjectEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnFocus",
                "setOnBlur",
                "setOnChange"
            ], IGuiCommonsEvents);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IBasePanelEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnScroll",
                "setBeforeResize",
                "setOnResize",
                "setOnResizeComplete"
            ], IFormsObjectEvents);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IBasePanelHolderEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnOpen",
                "setOnClose",
                "setBeforeOpen",
                "setBeforeClose"
            ], IBasePanelEvents);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IButtonEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnResize"
            ], IFormsObjectEvents);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let ICropBoxEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnResize",
                "setOnResizeStart",
                "setOnResizeChange",
                "setOnResizeComplete"
            ], IGuiCommonsEvents);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let ICropBoxEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IDialogEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnOpen",
                "setOnClose",
                "setOnDrag",
                "setBeforeResize",
                "setOnResize"
            ], IFormsObjectEvents);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IDirectoryBrowserEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnChange",
                "setOnPathRequest",
                "setOnDirectoryRequest",
                "setOnCreateDirectoryRequest",
                "setOnRenameRequest",
                "setOnDeleteRequest"
            ], IBasePanelEvents);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IDirectoryBrowserEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IDragBarEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnDragStart",
                "setOnDragChange",
                "setOnDragComplete"
            ], IGuiCommonsEvents);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IDragBarEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IDropDownListEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnResize",
                "setOnScroll",
                "setOnSelect"
            ], IFormsObjectEvents);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IErrorEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IFileUploadEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnError",
                "setOnChange",
                "setOnAboard",
                "setOnUploadStart",
                "setOnUploadChange",
                "setOnUploadComplete"
            ], IGuiCommonsEvents);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IFileUploadEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IHttpRequestEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IKeyEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let ILabelEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnChange"
            ], IGuiCommonsEvents);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IMessageEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IMouseEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IMoveEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IProgressBarEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnChange"
            ], IGuiCommonsEvents);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IResizeBarEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnResize",
                "setOnResizeStart",
                "setOnResizeChange",
                "setOnResizeComplete"
            ], IGuiCommonsEvents);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IResizeBarEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IResizeEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IScrollBarEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnArrow",
                "setOnButton",
                "setOnTracker",
                "setOnScroll",
                "setOnChange"
            ], IGuiCommonsEvents);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let IScrollEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let ISelectBoxEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnResize",
                "setOnScroll",
                "setOnChange",
                "setOnSelect"
            ], IGuiCommonsEvents);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let ITextAreaEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setBeforeResize",
                "setOnResize",
                "setOnScroll"
            ], IFormsObjectEvents);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    export let ITouchEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces {
    "use strict";
    export let IEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], Com.Wui.Framework.Commons.Interfaces.IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces {
    "use strict";
    export let IEventsManager : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "getAll",
                "setEventArgs",
                "setEvent",
                "FireEvent",
                "RemoveHandler",
                "Exists"
            ], Com.Wui.Framework.Commons.Interfaces.IEventsManager);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    export let IBaseGuiObject : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Title",
                "Changed",
                "Value"
            ], IGuiCommons);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    export let IAbstractGuiObject : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Notification",
                "setSize",
                "StyleAttributes",
                "AppendStyleAttributes"
            ], IBaseGuiObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    export let IFormsObject : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Notification",
                "Error",
                "TabIndex",
                "getSelectorEvents",
                "getEvents",
                "IsPersistent"
            ], IBaseGuiObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    export let IBaseGuiGroupObject : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Configuration",
                "getWidth"
            ], IFormsObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    export let IBaseGuiGroupObjectArgs : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Visible",
                "ForceSetValue",
                "Value",
                "Enabled",
                "Error",
                "AddGuiOption",
                "getGuiOptionsList",
                "TitleText",
                "Width",
                "Resize"
            ], Com.Wui.Framework.Commons.Interfaces.IBaseObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    export let IBasePanel : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "loaderIcon",
                "loaderText",
                "AddChild",
                "getChildPanelList",
                "setChildPanelArgs",
                "ContentType",
                "Scrollable",
                "Width",
                "Height",
                "getScrollTop",
                "getScrollLeft",
                "getEvents",
                "Value"
            ], IFormsObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    export let IBasePanelHolder : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GuiType",
                "IsOpened",
                "getBody",
                "getEvents"
            ], IBasePanel);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    export let IBaseViewerArgs : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Visible"
            ], Com.Wui.Framework.Commons.Interfaces.IBaseObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    export let IBasePanelViewerArgs : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "AsyncEnabled"
            ], IBaseViewerArgs);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    export let IBasePanelHolderViewerArgs : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "HeaderText",
                "DescriptionText",
                "IsOpened",
                "BodyArgs",
                "HolderViewerClass",
                "BodyViewerClass"
            ], IBasePanelViewerArgs);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    export let IBaseViewer : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "InstanceOwner",
                "IsCached",
                "IsPrinted",
                "TestModeEnabled",
                "ViewerArgs",
                "getInstance",
                "setLayer",
                "getLayersType",
                "PrepareImplementation",
                "Show"
            ], Com.Wui.Framework.Commons.Interfaces.IBaseObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    export let IBasePanelViewer : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "ViewerArgs"
            ], IBaseViewer);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    export let IGuiElement : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Id",
                "getId",
                "GuiTypeTag",
                "StyleClassName",
                "Visible",
                "getVisible",
                "Width",
                "getWidth",
                "Height",
                "getHeight",
                "setAttribute",
                "Add",
                "addChildElement",
                "getChildElements",
                "getChildElement",
                "getGuiChildElements",
                "getGuiTypeTag",
                "getAttributes",
                "Draw",
                "ToDOMElement",
                "getWrappingElement",
                "setWrappingElement"
            ], Com.Wui.Framework.Commons.Interfaces.IBaseObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    export let IResponsiveElement : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "StyleClassName",
                "WidthOfColumn",
                "HeightOfRow",
                "getWidthOfColumn",
                "getHeightOfRow",
                "Alignment",
                "getAlignment",
                "FitToParent",
                "getFitToParent",
                "VisibilityStrategy",
                "getVisibilityStrategy",
                "Add"
            ], Com.Wui.Framework.Commons.Interfaces.IBaseObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let IAccordion : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GuiType",
                "setPanelHoldersArgs",
                "Clear",
                "Collapse",
                "Expand",
                "ExpandSingle",
                "getPanel"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IBasePanel);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let IImageButton : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GuiType",
                "IconName",
                "IsSelected"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let IButton : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GuiType",
                "Text",
                "Width",
                "getEvents"
            ], IImageButton);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let ICheckBox : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Text",
                "Checked"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let IDialog : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GuiType",
                "TopOffset",
                "MaxWidth",
                "MaxHeight",
                "AutoResize",
                "AutoCenter",
                "ResizeableType",
                "Modal",
                "Draggable",
                "PanelViewer",
                "PanelViewerArgs",
                "Width",
                "Height",
                "getEvents"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let IDirectoryBrowser : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Path",
                "Filter",
                "setStructure",
                "Clear",
                "getEvents",
                "Value"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let IDropDownList : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GuiType",
                "Hint",
                "Value",
                "Width",
                "Height",
                "MaxVisibleItemsCount",
                "Add",
                "Select",
                "Clear",
                "ShowField",
                "ShowButton",
                "getItemsCount",
                "getEvents"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let IFormInput : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Clear"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseGuiGroupObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let IIcon : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "IconType"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let IImage : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Source",
                "GuiType",
                "Link",
                "LoadingSpinnerEnabled",
                "OpacityShowEnabled",
                "setSize",
                "getWidth",
                "getHeight",
                "IsSelected",
                "TabIndex",
                "getStream",
                "OutputArgs",
                "Version"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseGuiObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let ILabel : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Text",
                "getEvents"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseGuiObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let IInputLabel : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GuiType",
                "Width",
                "Error"
            ], ILabel);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let ILabelList : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "IconName",
                "Add",
                "Clear"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseGuiObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let ILink : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "ReloadTo",
                "OpenInNewWindow",
                "TabIndex"
            ], ILabel);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let INumberPicker : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GuiType",
                "Value",
                "ValueStep",
                "RangeStart",
                "RangeEnd",
                "Width",
                "Text",
                "TabIndex",
                "DecimalPlaces"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let IProgressBar : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GuiType",
                "Value",
                "RangeStart",
                "RangeEnd",
                "Width",
                "getEvents"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseGuiObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let IRadioBox : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GroupName",
                "getGroup"
            ], ICheckBox);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let ITab : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IButton);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let ITabs : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Add",
                "Clear",
                "Select",
                "getItem"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseGuiObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let ITextArea : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GuiType",
                "Value",
                "Width",
                "MaxWidth",
                "Height",
                "MaxHeight",
                "ResizeableType",
                "ReadOnly",
                "LengthLimit",
                "Hint",
                "getEvents"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject);
        }();
}

namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    export let ITextField : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "GuiType",
                "Value",
                "Width",
                "setAutocompleteDisable",
                "ReadOnly",
                "setPasswordEnabled",
                "setOnlyNumbersAllowed",
                "IconName",
                "Hint",
                "LengthLimit",
                "setAutocompleteData"
            ], Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject);
        }();
}

/* tslint:enable */
