/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.ErrorPages {
    "use strict";

    export class Http403ForbiddenPage extends BaseErrorPage {

        protected getFaviconSource() : string {
            return "resource/graphics/Com/Wui/Framework/Gui/ForbiddenIcon.ico";
        }

        protected getPageTitle() : string {
            return "WUI - HTTP 403";
        }

        protected getMessageHeader() : string {
            return "HTTP status 403";
        }

        protected getMessageBody() : string {
            return "Access denied.";
        }
    }
}
