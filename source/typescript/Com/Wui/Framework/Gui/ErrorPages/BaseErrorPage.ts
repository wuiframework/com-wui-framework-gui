/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.ErrorPages {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StaticPageContentManager = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;

    export abstract class BaseErrorPage extends Com.Wui.Framework.Commons.ErrorPages.BaseErrorPage {

        protected getFaviconSource() : string {
            return "resource/graphics/Com/Wui/Framework/Gui/ErrorIcon.ico";
        }

        protected getPageTitle() : string {
            return "WUI - Error";
        }

        protected getMessageHeader() : string {
            return "Error";
        }

        protected getMessageBody() : string {
            return "Something has went wrong";
        }

        protected getPageBody() : string {
            const EOL : string = StringUtils.NewLine(false);
            let output : string = "";
            output +=
                "<div class=\"Exception\">" + EOL +
                "   <h1>" + this.getMessageHeader() + "</h1>" + EOL +
                "   <div class=\"Message\">" + EOL +
                this.getMessageBody() + EOL +
                "   </div>" + EOL +
                "</div>" + EOL +
                "<div class=\"Logo\">" + EOL +
                "   <div class=\"WUI\"></div>" + EOL +
                "</div>";

            return output;
        }

        protected resolver() : void {
            const title : string = this.getPageTitle();
            const body : string = this.getPageBody();

            try {
                ExceptionsManager.ThrowExit();
            } catch (ex) {
                // stop all background execution, but continue in execution of current resolver
            }
            Echo.ClearAll();
            StaticPageContentManager.Clear();
            if (!ObjectValidator.IsEmptyOrNull(this.getFaviconSource())) {
                StaticPageContentManager.FaviconSource(this.getFaviconSource());
            }
            StaticPageContentManager.Title(title);
            StaticPageContentManager.BodyAppend(body);
            StaticPageContentManager.Draw();
        }
    }
}
