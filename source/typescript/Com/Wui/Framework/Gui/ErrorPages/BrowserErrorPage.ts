/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.ErrorPages {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class BrowserErrorPage extends BaseErrorPage {

        protected getPageTitle() : string {
            return "WUI - Browser Error";
        }

        protected getMessageHeader() : string {
            return "Unsupported browser";
        }

        protected getMessageBody() : string {
            return "You are using unsupported type or version of the browser. " +
                "Please, choose one of the supported browser from the list below:" + StringUtils.NewLine() +
                "<a href=\"http://windows.microsoft.com/en-us/internet-explorer/download-ie\" " +
                "target=\"_blank\">Internet Explorer 5+</a>" + StringUtils.NewLine() +
                "<a href=\"https://www.mozilla.org/en-US/firefox/new/\" target=\"_blank\">Firefox</a>" + StringUtils.NewLine() +
                "<a href=\"http://www.google.com/chrome/\" target=\"_blank\">Google Chrome</a>" + StringUtils.NewLine() +
                "<a href=\"http://www.opera.com/\" target=\"_blank\">Opera</a>" + StringUtils.NewLine() +
                "<a href=\"https://www.apple.com/safari/\" target=\"_blank\">Safari</a>";
        }
    }
}
