/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.ErrorPages {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export class Http404NotFoundPage extends BaseErrorPage {
        private filePath : string;

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            this.filePath = "";
            if ($POST.KeyExists(HttpRequestConstants.HTTP404_FILE_PATH)) {
                this.filePath = $POST.getItem(HttpRequestConstants.HTTP404_FILE_PATH);
            }
        }

        protected getPageTitle() : string {
            return "WUI - HTTP 404";
        }

        protected getMessageHeader() : string {
            return "HTTP status 404";
        }

        protected getMessageBody() : string {
            if (!ObjectValidator.IsEmptyOrNull(this.filePath)) {
                return "File has not been found. Required file path is:" + StringUtils.NewLine() +
                    "<a href=\"" + this.filePath + "\">" + this.filePath + "</a>";
            }
            return "File has not been found.";
        }
    }
}
