/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.ErrorPages {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import Exception = Com.Wui.Framework.Commons.Exceptions.Type.Exception;
    import ExceptionCode = Com.Wui.Framework.Commons.Enums.ExceptionCode;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import ErrorPageException = Com.Wui.Framework.Commons.Exceptions.Type.ErrorPageException;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class ExceptionErrorPage extends BaseErrorPage {
        private fatalErrorExists : boolean;
        private exceptionsList : ArrayList<Exception>;

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            if (ObjectValidator.IsEmptyOrNull(this.fatalErrorExists)) {
                if ($GET.KeyExists(HttpRequestConstants.EXCEPTION_TYPE)) {
                    this.fatalErrorExists =
                        StringUtils.ToInteger($GET.getItem(HttpRequestConstants.EXCEPTION_TYPE)) === ExceptionCode.FATAL_ERROR;
                } else {
                    this.fatalErrorExists = false;
                }
            }
            if (ObjectValidator.IsEmptyOrNull(this.exceptionsList)) {
                if ($POST.KeyExists(HttpRequestConstants.EXCEPTIONS_LIST)) {
                    this.exceptionsList = $POST.getItem(HttpRequestConstants.EXCEPTIONS_LIST);
                } else {
                    this.exceptionsList = new ArrayList<Exception>();
                }
            }
        }

        protected getExceptionsList() : ArrayList<Exception> {
            return this.exceptionsList;
        }

        protected isFatalError() : boolean {
            return this.fatalErrorExists;
        }

        protected getPageTitle() : string {
            if (this.isFatalError()) {
                return "WUI - Fatal Error";
            } else {
                return "WUI - Exception";
            }
        }

        protected getMessageHeader() : string {
            if (this.isFatalError()) {
                return "FATAL Error!";
            } else {
                return "Oops, something went wrong...";
            }
        }

        protected getMessageBody() : string {
            LogIt.Debug("executed GUI error page");
            let errorsOutput : string = "";
            const EOL : string = StringUtils.NewLine(false);

            this.getExceptionsList().foreach(($exception : Exception) : void => {
                errorsOutput += "thrown by: ";
                errorsOutput += "<b>" + $exception.Owner() + "</b>: ";
                errorsOutput += StringUtils.NewLine();
                errorsOutput += $exception.ToString() + StringUtils.NewLine();
                LogIt.Error("thrown by: " + $exception.Owner() + ", message: " + $exception.ToString("", false));
            });
            const echo : string = this.getEchoOutput();
            LogIt.Debug("echo before exception: " + StringUtils.NewLine(false) +
                StringUtils.StripTags(StringUtils.Replace(echo, StringUtils.NewLine(), StringUtils.NewLine(false))));

            return errorsOutput + EOL +
                "       <div class=\"Echo\">" + EOL +
                "           <span onclick=\"" +
                "document.getElementById('exceptionEcho').style.display=" +
                "document.getElementById('exceptionEcho').style.display===" +
                "'block'?'none':'block';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">" +
                "Echo output before exception" +
                "           </span>" + EOL +
                "           <div id=\"exceptionEcho\" class=\"Text\" style=\"border: 0 solid black; display: none;\">" + EOL +
                echo + EOL +
                "           </div>" + StringUtils.NewLine() +
                "           <a style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif; " +
                "text-decoration: none;\" " +
                "href=\"#" + this.createLink("/about/Cache") + "\">Cache info</a>" + EOL +
                "       </div>";
        }

        protected resolver() : void {
            try {
                super.resolver();
            } catch (ex) {
                try {
                    ExceptionsManager.Throw(this.getClassName(), ex);
                } catch (ex) {
                    // register error page self-error and continue with processing of general exception manager
                }
                try {
                    ExceptionsManager.Throw(this.getClassName(), new ErrorPageException(this.getClassName() + " self error."));
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            }
        }
    }
}
