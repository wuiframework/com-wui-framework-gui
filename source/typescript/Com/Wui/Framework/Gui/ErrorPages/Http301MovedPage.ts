/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.ErrorPages {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export class Http301MovedPage extends BaseErrorPage {
        private link : string;

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            this.link = "";
            if ($POST.KeyExists(HttpRequestConstants.HTTP301_LINK)) {
                this.link = $POST.getItem(HttpRequestConstants.HTTP301_LINK);
            }
        }

        protected getFaviconSource() : string {
            return "resource/graphics/Com/Wui/Framework/Gui/MovedIcon.ico";
        }

        protected getPageTitle() : string {
            return "WUI - HTTP 301";
        }

        protected getMessageHeader() : string {
            return "HTTP status 301";
        }

        protected getMessageBody() : string {

            return "File has been moved. New address is:" + StringUtils.NewLine() +
                "<a href=\"#" + this.link + "\">" + this.getRequest().getHostUrl() + "#" + this.link + "</a>";
        }
    }
}
