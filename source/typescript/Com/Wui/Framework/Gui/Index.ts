/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import RuntimeTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner;
    import StaticPageContentManger = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;
    import BaseViewer = Com.Wui.Framework.Gui.Primitives.BaseViewer;

    export class Index extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver {

        protected resolver() : void {
            let output : string =
                "<div class=\"GuiInterface\">" +
                "<h1>WUI Framework base GUI Library</h1>" +
                "<h3>Common and abstract classes or interfaces connected with creation and handling of GUI.</h3>" +
                "<div class=\"Index\">";

            /* dev:start */
            output +=
                "<H3>Runtime tests</H3>" +

                "<H4>Page content</H4>" +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.Utils.WindowManagerTest) + "\">Window Manager</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.Utils.StaticPageContentManagerTest) + "\">" +
                "Static Page Content Manager</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.Events.EventsManagerTest) + "\">Global Events Factory</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.ErrorPagesTest) + "\">Error Pages</a>" +
                StringUtils.NewLine() +

                "<H4>GUI elements</H4>" +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.GuiObjectManagerTest) + "\">GUI Object Manager</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.Events.ElementEventsManagerTest) + "\">" +
                "Element Events Manager</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.Utils.ElementManagerTest) + "\">Element Manager</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.Primitives.PrimitivesTest) + "\">GUI Primitives</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.SerializationTest) + "\">" +
                "GUI Primitives Serialization</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.Utils.ScreenPositionTest.CallbackLink() + "\">Screen position test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.PerformanceTest.CallbackLink() + "\">Performance test</a>" +
                StringUtils.NewLine() +

                "<H4>GUI utils</H4>" +
                "<a href=\"" + RuntimeTestRunner.CreateLink(Com.Wui.Framework.Commons.RuntimeTests.HttpRequestParserTest) + "\">" +
                "Http Request Parser Test</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + this.createLink("/web/PersistenceManagerTest") + "\">Persistence Manager Test</a>" +
                StringUtils.NewLine() +

                "<H4>Draw GUI resolvers</H4>" +
                "<a href=\"" + RuntimeTests.Primitives.BasePanelViewerTest.CallbackLink() + "\">Base Panel Viewer Test</a>" +
                StringUtils.NewLine() +
                "<a href=\"#" + this.createLink("/web/" + StringUtils.Replace(this.getClassName(), ".", "/")) + "\">" +
                "Missing Viewer Test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + BaseViewer.CallbackLink() + "\">Viewer Bad Implementation Test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.Primitives.AsyncBasePanelViewerTest.CallbackLink() + "\">Asynchronous Loader Test</a>" +
                StringUtils.NewLine() +

                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.Utils.TextSelectionManagerTest) + "\">" +
                "TextSelectionManager test</a>" +
                StringUtils.NewLine() +

                "<H4>Com.Wui.Framework.Gui.ImageProcessor</H4>" +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.ImageProcessor.ImageTransformTest) + "\">ImageTransform test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.ImageProcessor.ImageFiltersTest) + "\">ImageFilters test</a>" +
                StringUtils.NewLine() +

                "<H4>Com.Wui.Framework.Gui.Primitives</H4>" +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.Primitives.BaseGuiObjectTest) + "\">BaseGuiObject test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.Primitives.FormsObjectTest) + "\">FormsObject test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.Primitives.BasePanelTest) + "\">BasePanel test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.Primitives.AbstractGuiObjectTest) + "\">" +
                "AbstractGuiObject test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.Primitives.GuiElementTest) + "\">GuiElement test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTestRunner.CreateLink(RuntimeTests.Primitives.GuiCommonsTest) + "\">GuiCommons test</a>" +
                StringUtils.NewLine();
            /* dev:end */

            output +=
                "</div>" +
                "</div>";

            output +=
                "<div class=\"Note\">" +
                "version: " + this.getEnvironmentArgs().getProjectVersion() +
                ", build: " + this.getEnvironmentArgs().getBuildTime() +
                "</div>" + StringUtils.NewLine(false) +
                "<div class=\"Logo\">" + StringUtils.NewLine(false) +
                "   <div class=\"WUI\"></div>" + StringUtils.NewLine(false) +
                "</div>";

            StaticPageContentManger.Clear();
            StaticPageContentManger.Title("WUI - GUI Index");
            StaticPageContentManger.BodyAppend(output);
            StaticPageContentManger.Draw();
        }
    }
}
