/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui {
    "use strict";
    import Parent = Com.Wui.Framework.Commons.PersistenceApi.PersistenceFactory;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class PersistenceFactory extends Com.Wui.Framework.Commons.PersistenceApi.PersistenceFactory {
        private static globalOwner : string;

        /**
         * @param {any} $persistenceType , which should be validated.
         * @param {string} $owner , which should be validated.
         * @return {string} Returns generated session Id.
         */
        public static sessionIdGenerator($persistenceType : any, $owner : string) : string {
            if ($persistenceType === Enums.PersistenceType.AUTOFILL ||
                $persistenceType === Enums.PersistenceType.FORM_VALUES ||
                $persistenceType === Enums.PersistenceType.ERROR_FLAGS) {
                if (ObjectValidator.IsEmptyOrNull(PersistenceFactory.globalOwner)) {
                    $owner = PersistenceFactory.globalOwner;
                } else {
                    $owner = $persistenceType;
                    $persistenceType = Enums.PersistenceType.BROWSER;
                }
            }

            return Parent.sessionIdGenerator($persistenceType, $owner);
        }

        /**
         * @param {string} $value , which should be validated.
         * @return {string} Returns global Owner.
         */
        public static GlobalOwner($value? : string) : string {
            if (ObjectValidator.IsSet($value)) {
                PersistenceFactory.globalOwner = $value;
            }

            return PersistenceFactory.globalOwner;
        }
    }
}
