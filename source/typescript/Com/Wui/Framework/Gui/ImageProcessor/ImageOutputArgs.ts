/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.ImageProcessor {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import ImageCorners = Com.Wui.Framework.Gui.Structures.ImageCorners;
    import ImageCropDimensions = Com.Wui.Framework.Gui.Structures.ImageCropDimensions;
    import ImageDataSourceType = Com.Wui.Framework.Gui.Enums.ImageDataSourceType;

    /**
     * ImageOutputArgs is structure handling configuration of image output format.
     */
    export class ImageOutputArgs extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private width : number;
        private height : number;
        private fillEnabled : boolean;
        private readonly corners : ImageCorners;
        private cornerOnEnvelop : boolean;
        private zoom : number;
        private readonly cropDimension : ImageCropDimensions;
        private rotate : number;
        private blurEnabled : boolean;
        private grayscaleEnabled : boolean;
        private invertEnabled : boolean;
        private brightness : number;
        private contrast : number;
        private waterMarkSrc : string;
        private waterMarkEnabled : boolean;
        private quality : number;
        private frontEndCacheEnabled : boolean;
        private backEndCacheEnabled : boolean;
        private expireTime : string;
        private canvasEnabled : boolean;

        constructor() {
            super();
            this.width = 0;
            this.height = 0;
            this.fillEnabled = true;
            this.corners = new ImageCorners();
            this.cornerOnEnvelop = true;
            this.zoom = 100;
            this.cropDimension = new ImageCropDimensions();
            this.rotate = 0;
            this.blurEnabled = false;
            this.grayscaleEnabled = false;
            this.invertEnabled = false;
            this.brightness = 0;
            this.contrast = 0;
            this.waterMarkEnabled = false;
            this.quality = 100;
            this.frontEndCacheEnabled = true;
            this.backEndCacheEnabled = false;
            this.expireTime = "+10 days";
            this.canvasEnabled = true;
        }

        /**
         * @param {number} $width Specify image output width value.
         * @param {number} $height Specify image output height value.
         * @return {void}
         */
        public setSize($width : number, $height : number) : void {
            this.width = Property.Integer(this.width, $width);
            this.height = Property.Integer(this.height, $height);
        }

        /**
         * @return {number} Returns image output width value.
         */
        public getWidth() : number {
            return this.width;
        }

        /**
         * @return {number} Returns image output height value.
         */
        public getHeight() : number {
            return this.height;
        }

        /**
         * @param {boolean} [$value] Specify, if image size should be scaled exactly to required width and height or,
         * if original scale should be preserved.
         * @return {boolean} Returns true, if image original scale should be modified, otherwise false.
         */
        public FillEnabled($value? : boolean) : boolean {
            return this.fillEnabled = Property.Boolean(this.fillEnabled, $value);
        }

        /**
         * @param {number} [$topLeft] Specify top left corner radius.
         * @param {number} [$topRight] Specify top right corner radius.
         * @param {number} [$bottomLeft] Specify bottom left corner radius.
         * @param {number} [$bottomRight] Specify bottom right corner radius.
         * @return {ImageCorners} Returns structure handling image corners radius.
         */
        public Corners($topLeft? : number, $topRight? : number, $bottomLeft? : number, $bottomRight? : number) : ImageCorners {
            this.corners.setCorners($topLeft, $topRight, $bottomLeft, $bottomRight);
            return this.corners;
        }

        /**
         * @param {boolean} [$value] Specify if image corner, should be applied on image envelop.
         * @return {boolean} Returns true, if image corner should be applied on envelop,
         * otherwise false, if corner should be applied on image itself.
         */
        public CornerOnEnvelop($value? : boolean) : boolean {
            return this.cornerOnEnvelop = Property.Boolean(this.cornerOnEnvelop, $value);
        }

        /**
         * @param {number} [$topX] Specify crop top X dimension in pixel format.
         * @param {number} [$topY] Specify crop top Y dimension in pixel format.
         * @param {number} [$bottomX] Specify crop bottom X dimension in pixel format.
         * @param {number} [$bottomY] Specify crop bottom Y dimension in pixel format.
         * @return {ImageCropDimensions} Returns structure handling image crop dimensions.
         */
        public CropDimension($topX? : number, $topY? : number, $bottomX? : number, $bottomY? : number) : ImageCropDimensions {
            this.cropDimension.setDimensions($topX, $topY, $bottomX, $bottomY);
            return this.cropDimension;
        }

        /**
         * @param {number} [$value] Specify zoom scale in percentage format.
         * @return {number} Returns zoom scale in percentage format.
         */
        public Zoom($value? : number) : number {
            if (ObjectValidator.IsDouble($value) && $value <= 1.0 && $value >= 0.0) {
                $value = Math.ceil($value * 100) + 100;
            }
            this.zoom = Property.PositiveInteger(this.zoom, $value, 0, 200);
            if (this.zoom < 100) {
                this.zoom += 100;
            }
            return this.zoom;
        }

        /**
         * @param {number} [$value] Specify image rotation in degrees format.
         * @param {boolean} [$clockwise=true] Specify if rotation value should be interpreted in clockwise direction
         * independently on value sign.
         * @return {number} Returns image rotation in degrees format.
         */
        public Rotation($value? : number, $clockwise : boolean = true) : number {
            if (ObjectValidator.IsInteger($value) || ObjectValidator.IsDouble($value)) {
                if (ObjectValidator.IsDouble($value)) {
                    $value = Convert.ToFixed($value, 0);
                }
                let negativeRotate : boolean = false;
                let absoluteRotate : number = $value;
                if (!$clockwise) {
                    absoluteRotate *= (-1);
                }
                if (absoluteRotate < 0) {
                    absoluteRotate *= -1;
                    negativeRotate = true;
                }
                while (absoluteRotate / 90 > 4) {
                    absoluteRotate -= 360;
                }
                if (negativeRotate) {
                    absoluteRotate = 360 - absoluteRotate;
                }
                this.rotate = absoluteRotate;
            }
            return this.rotate;
        }

        /**
         * @param {boolean} [$value] Specify, if blur effect should be applied on image output.
         * @return {boolean} Returns true, if blur effect should be applied on image output, otherwise false.
         */
        public BlurEnabled($value? : boolean) : boolean {
            return this.blurEnabled = Property.Boolean(this.blurEnabled, $value);
        }

        /**
         * @param {boolean} [$value] Specify, if only gray scale should be applied on image output.
         * @return {boolean} Returns true, if only gray scale should be applied on image output, otherwise false.
         */
        public GrayscaleEnabled($value? : boolean) : boolean {
            return this.grayscaleEnabled = Property.Boolean(this.grayscaleEnabled, $value);
        }

        /**
         * @param {boolean} [$value] Specify, if image output colors should be inverted.
         * @return {boolean} Returns true, if image output colors should be inverted, otherwise false.
         */
        public InvertEnabled($value? : boolean) : boolean {
            return this.invertEnabled = Property.Boolean(this.invertEnabled, $value);
        }

        /**
         * @param {number} [$value] Specify output brightness in 8bit format with range <-255;255>.
         * @return {number} Returns output brightness in 8bit format.
         */
        public Brightness($value? : number) : number {
            return this.brightness = Property.Integer(this.brightness, $value, -255, 255);
        }

        /**
         * @param {number} [$value] Specify output contrast in 8bit format with range <-255;255>.
         * @return {number} Returns output contrast in 8bit format.
         */
        public Contrast($value? : number) : number {
            return this.contrast = Property.Integer(this.contrast, $value, -255, 255);
        }

        /**
         * @param {boolean} [$value] Specify, if watermark effect should be applied on image output.
         * @return {boolean} Returns true, if watermark effect should be applied on image output, otherwise false.
         */
        public WaterMarkEnabled($value? : boolean) : boolean {
            return this.waterMarkEnabled = Property.Boolean(this.waterMarkEnabled, $value);
        }

        /**
         * @param {string} [$value] Specify path to watermark data source.
         * @return {string} Returns path to watermark data source.
         */
        public WaterMarkSource($value? : string) : string {
            return this.waterMarkSrc = Property.String(this.waterMarkSrc, $value);
        }

        /**
         * @param {boolean} [$value] Specify, if image output should be loaded from browser cache.
         * @return {boolean} Returns true, if image output should be loaded from browser cache, otherwise false.
         */
        public FrontEndCacheEnabled($value? : boolean) : boolean {
            return this.frontEndCacheEnabled = Property.Boolean(this.frontEndCacheEnabled, $value);
        }

        /**
         * @param {boolean} [$value] Specify, if image output should be cached on server side.
         * @return {boolean} Returns true, if image output should be cached on server side, otherwise false.
         */
        public BackEndCacheEnabled($value? : boolean) : boolean {
            return this.backEndCacheEnabled = Property.Boolean(this.backEndCacheEnabled, $value);
        }

        /**
         * @param {string} [$value] Specify, if image cache expire time.
         * @return {string} Returns image cache expire time.
         */
        public ExpireTime($value? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                const expireTime : number = Property.Time(null, $value);
                return this.expireTime = Convert.TimeToGMTformat(expireTime);
            }
            return this.expireTime;
        }

        /**
         * @param {number} [$value] Specify output quality in percentage format.
         * @return {number} Returns output quality in percentage format.
         */
        public Quality($value? : number) : number {
            return this.quality = Property.PositiveInteger(this.quality, $value, 0, 100);
        }

        /**
         * @return {string} Returns string in base64 format, which is representing image output configuration
         * suitable for server side processing.
         */
        public ToUrlData() : string {
            let output : string = "";
            if (this.width !== 0) {
                output += "&w=" + this.width;
            }
            if (this.height !== 0) {
                output += "&h=" + this.height;
            }
            if (this.fillEnabled !== true) {
                output += "&f=" + Convert.BooleanToString(this.fillEnabled);
            }
            if (this.corners.TopLeft() !== 0) {
                output += "&ctl=" + this.corners.TopLeft();
            }
            if (this.corners.TopRight() !== 0) {
                output += "&ctr=" + this.corners.TopRight();
            }
            if (this.corners.BottomLeft() !== 0) {
                output += "&cbl=" + this.corners.BottomLeft();
            }
            if (this.corners.BottomRight() !== 0) {
                output += "&cbr=" + this.corners.BottomRight();
            }
            if (this.cornerOnEnvelop !== true) {
                output += "&ce=" + Convert.BooleanToString(this.cornerOnEnvelop);
            }

            if (this.cropDimension.TopX() !== 0) {
                output += "&ctx=" + this.cropDimension.TopX();
            }
            if (this.cropDimension.TopY() !== 0) {
                output += "&cty=" + this.cropDimension.TopY();
            }
            if (this.cropDimension.BottomX() !== 0) {
                output += "&cbx=" + this.cropDimension.BottomX();
            }
            if (this.cropDimension.BottomY() !== 0) {
                output += "&cby=" + this.cropDimension.BottomY();
            }

            if (this.zoom !== 100) {
                output += "&z=" + this.zoom;
            }

            if (this.rotate !== 0) {
                output += "&r=" + this.rotate;
            }
            if (this.blurEnabled !== false) {
                output += "&b=" + Convert.BooleanToString(this.blurEnabled);
            }
            if (this.grayscaleEnabled !== false) {
                output += "&g=" + Convert.BooleanToString(this.grayscaleEnabled);
            }
            if (this.invertEnabled !== false) {
                output += "&i=" + Convert.BooleanToString(this.invertEnabled);
            }
            if (this.brightness !== 0) {
                output += "&br=" + this.brightness;
            }
            if (this.contrast !== 0) {
                output += "&ct=" + this.contrast;
            }

            if (this.waterMarkEnabled !== false) {
                output += "&we=" + Convert.BooleanToString(this.waterMarkEnabled);
            }
            if (ObjectValidator.IsEmptyOrNull(this.waterMarkSrc)) {
                output += "&wt=" + ImageDataSourceType.FILE_SYSTEM;
                output += "&ws=" + this.waterMarkSrc;
            }

            if (this.quality !== 100) {
                output += "&q=" + this.quality;
            }
            if (this.frontEndCacheEnabled !== true) {
                output += "&fec=" + Convert.BooleanToString(this.frontEndCacheEnabled);
            }
            if (this.backEndCacheEnabled !== true) {
                output += "&bec=" + Convert.BooleanToString(this.backEndCacheEnabled);
            }
            if (this.expireTime !== "+10 days") {
                output += "&e=" + this.expireTime;
            }

            return ObjectEncoder.Base64(output);
        }

        /**
         * @param {boolean} [$value] Specify, if HTML5 canvas can be used for load of the image.
         * @return {boolean} Returns true, if image can be loaded by HTML5 canvas, otherwise false.
         */
        public CanvasEnabled($value? : boolean) : boolean {
            return this.canvasEnabled = Property.Boolean(this.canvasEnabled, $value);
        }
    }
}
