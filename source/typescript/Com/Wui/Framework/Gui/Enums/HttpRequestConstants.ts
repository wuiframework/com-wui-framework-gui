/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Enums {
    "use strict";

    export class HttpRequestConstants extends Com.Wui.Framework.Commons.Enums.HttpRequestConstants {
        public static readonly RUNTIME_TEST : string = "RuntimeTest";
        public static readonly TEST_MODE : string = "TestMode";
        public static readonly FORCE_REFRESH : string = "ForceRefresh";
        public static readonly HIDE_CHILDREN : string = "HideChildren";
        public static readonly ELEMENT_INSTANCE : string = "Element";
    }
}
