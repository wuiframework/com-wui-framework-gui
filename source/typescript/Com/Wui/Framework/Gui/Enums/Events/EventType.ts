/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Enums.Events {
    "use strict";

    export class EventType extends Com.Wui.Framework.Commons.Enums.Events.EventType {
        public static readonly ON_SHOW : string = "onshow";
        public static readonly ON_HIDE : string = "onhide";

        public static readonly ON_FOCUS : string = "onfocus";
        public static readonly ON_BLUR : string = "onblur";

        public static readonly ON_KEY_PRESS : string = "onkeypress";
        public static readonly ON_KEY_DOWN : string = "onkeydown";
        public static readonly ON_KEY_UP : string = "onkeyup";

        public static readonly ON_CLICK : string = "onclick";
        public static readonly ON_DOUBLE_CLICK : string = "ondblclick";
        public static readonly ON_RIGHT_CLICK : string = "onrightclick";
        public static readonly ON_MOUSE_OVER : string = "onmouseover";
        public static readonly ON_MOUSE_OUT : string = "onmouseout";
        public static readonly ON_MOUSE_DOWN : string = "onmousedown";
        public static readonly ON_MOUSE_UP : string = "onmouseup";
        public static readonly ON_MOUSE_MOVE : string = "onmousemove";

        public static readonly ON_TOUCH_START : string = "ontouchstart";
        public static readonly ON_TOUCH_END : string = "ontouchend";
        public static readonly ON_TOUCH_MOVE : string = "ontouchmove";

        public static readonly BEFORE_RESIZE : string = "beforeResize";
        public static readonly ON_RESIZE : string = "onresize";
        public static readonly ON_RESIZE_START : string = "onresizeStart";
        public static readonly ON_RESIZE_CHANGE : string = "onresizeChange";
        public static readonly ON_RESIZE_COMPLETE : string = "onresizeComplete";

        public static readonly ON_DRAG : string = "ondrag";
        public static readonly ON_DRAG_START : string = "ondragStart";
        public static readonly ON_DRAG_CHANGE : string = "ondragChange";
        public static readonly ON_DRAG_COMPLETE : string = "ondragComplete";
        public static readonly ON_DRAG_OVER : string = "ondragover";

        public static readonly ON_SCROLL : string = "onscroll";
        public static readonly ON_VERTICAL_SCROLL : string = "onverticalscroll";
        public static readonly ON_HORIZONTAL_SCROLL : string = "onhorizontalscroll";

        public static readonly ON_DROP : string = "ondrop";

        public static readonly ON_SELECT : string = "onselect";
    }
}
