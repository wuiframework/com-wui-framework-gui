/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Enums.Events {
    "use strict";

    export class OpacityEventType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly START : string = "onopacitystart";
        public static readonly CHANGE : string = "onopacitychange";
        public static readonly COMPLETE : string = "onopacitycomplete";
    }
}
