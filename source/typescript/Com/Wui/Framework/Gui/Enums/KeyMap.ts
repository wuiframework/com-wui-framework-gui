/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Enums {
    "use strict";

    export enum KeyMap {
        F2 = 113,
        F4 = 115,
        F5 = 116,
        TAB = 9,
        BACKSPACE = 8,
        LEFT_ARROW = 37,
        RIGHT_ARROW = 39,
        DOWN_ARROW = 40,
        UP_ARROW = 38,
        DELETE = 46,
        A = 97,
        C = 99,
        F = 70,
        V = 118,
        X = 120,
        ZERO = 48,
        NINE = 57,
        COMMA = 44,
        DOT = 46,
        PLUS = 43,
        MINUS = 45,
        SPACE = 32,
        ESC = 27,
        ENTER = 13,
        END = 35,
        HOME = 36
    }
}
