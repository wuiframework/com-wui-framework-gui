/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Enums {
    "use strict";

    export class DesignerProtocolConstants extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly ROOT_ELEMENT_VARIABLE : string = "!__ROOT_ELEMENT__";
        public static readonly HTML_ELEMENT_VARIABLE : string = "!__HTML_ELEMENT__";
        public static readonly VARIABLE_NAME : string = "__VariableName";
        public static readonly DESIGNER_ELEMENT : string = "DesignerElement";
        public static readonly HTML_ELEMENT : string = "HtmlElement";
        public static readonly GUI_ELEMENT : string = "GuiElement";
        public static readonly GUI_OBJECT : string = "GuiObject";
    }
}
