/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Enums {
    "use strict";

    export class CursorType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly NS_RESIZE : string = "ns-resize";
        public static readonly EW_RESIZE : string = "ew-resize";
        public static readonly NESW_RESIZE : string = "nesw-resize";
        public static readonly NWSE_RESIZE : string = "nwse-resize";
    }
}
