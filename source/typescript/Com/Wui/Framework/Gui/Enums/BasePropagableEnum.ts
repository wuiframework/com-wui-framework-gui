/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Enums {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;

    export abstract class BasePropagableEnum extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        /**
         * @param {BasePropagableEnum} $parentValue Specify parent value.
         * @param {BasePropagableEnum} $childValue Specify child value.
         * @return {BasePropagableEnum} Returns propagated value, if propagation has been allowed, otherwise default value is returned.
         */
        public static PriorityValue<T extends BasePropagableEnum>($parentValue : T, $childValue? : T) : T {
            const thisClass : any = Reflection.getInstance().getClass(this.ClassName());
            if (!ObjectValidator.IsEmptyOrNull($childValue)) {
                return $childValue;
            } else if (!ObjectValidator.IsEmptyOrNull($parentValue) && thisClass.IsPropagated($parentValue)) {
                return $parentValue;
            }
            return thisClass.getDefaultValue();
        }

        /**
         * @param {BasePropagableEnum} $value Specify value, which should be normalized.
         * @return {string} Returns normalized BasePropagableEnum value.
         */
        public static Normalize($value : BasePropagableEnum) : string {
            const thisClass : any = Reflection.getInstance().getClass(this.ClassName());
            if (thisClass.Contains($value)) {
                return StringUtils.Remove($value.toString(), "Propagated");
            }
            return thisClass.getDefaultValue();
        }

        /**
         * @param {BasePropagableEnum} $value Specify value for, which should be generated CSS style class.
         * @param {string} $initialClass Specify initial class which should be modified.
         * @return {string} Returns CSS style class for specified BasePropagableEnum value.
         */
        public static StyleClassName($value : BasePropagableEnum, $initialClass? : string) : string {
            const thisClass : any = Reflection.getInstance().getClass(this.ClassName());
            const classPrefix : string = ObjectValidator.IsEmptyOrNull($initialClass) ? "" : $initialClass
                .replace(new RegExp(`\\s*${thisClass.getPrefix()}([a-zA-Z])*`, "g"), "");
            const normalizedPrefix : string = ObjectValidator.IsEmptyOrNull(classPrefix) ? "" : classPrefix + " ";
            if (thisClass.Contains($value)) {
                return normalizedPrefix + thisClass.getPrefix() + thisClass.Normalize($value);
            }
            return normalizedPrefix + thisClass.getDefaultValue();
        }

        /**
         * @param {BasePropagableEnum} $value Specify value, which should be validated.
         * @return {boolean} Returns true if BasePropagableEnum value is propagated, otherwise false.
         */
        public static IsPropagated($value : BasePropagableEnum) : boolean {
            const thisClass : any = Reflection.getInstance().getClass(this.ClassName());
            if (thisClass.Contains($value)) {
                return StringUtils.Contains($value.toString(), "Propagated");
            }
            return false;
        }

        /**
         * @return {string} Returns prefix for BasePropagableEnum enum value.
         */
        protected static getPrefix() : string {
            return "";
        }

        /**
         * @return {string} Returns default BasePropagableEnum enum value.
         */
        protected static getDefaultValue() : string {
            return "";
        }
    }
}
