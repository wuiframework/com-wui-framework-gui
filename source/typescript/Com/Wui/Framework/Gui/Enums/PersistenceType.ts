/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Enums {
    "use strict";

    export class PersistenceType extends Com.Wui.Framework.Commons.Enums.PersistenceType {
        public static readonly GUI_COMMONS : string = "guicommons";
        public static readonly VIEWER : string = "viewer";
        public static readonly AUTOFILL : string = "autofill";
        public static readonly FORM_VALUES : string = "formvalues";
        public static readonly ERROR_FLAGS : string = "errorflags";
    }
}
