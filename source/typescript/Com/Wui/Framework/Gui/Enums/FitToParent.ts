/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Enums {
    "use strict";

    export class FitToParent extends BasePropagableEnum {
        public static readonly NONE : string = "None";
        public static readonly FULL : string = "Full";
        public static readonly VERTICAL : string = "Vertical";
        public static readonly HORIZONTAL : string = "Horizontal";

        /**
         * @return {string} Returns prefix for BasePropagableEnum enum value.
         */
        public static getPrefix() : string {
            return "FitToParent";
        }

        /**
         * @return {string} Returns default BasePropagableEnum enum value.
         */
        protected static getDefaultValue() : string {
            return FitToParent.NONE;
        }
    }
}
