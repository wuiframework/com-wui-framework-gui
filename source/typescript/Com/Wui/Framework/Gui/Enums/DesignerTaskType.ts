/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Enums {
    "use strict";

    export class DesignerTaskType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly RELOAD : string = "Reload";
        public static readonly BUILD : string = "Build";
        public static readonly ADD_ITEM : string = "AddItem";
        public static readonly DELETE_ITEM : string = "DeleteItem";
        public static readonly ITEM_EXISTS : string = "ItemExists";
        public static readonly SELECT_ROOT_ITEM : string = "SelectRootItem";
        public static readonly SELECT_ITEM : string = "SelectItem";
        public static readonly MOVE_ITEM : string = "MoveItem";
        public static readonly SET_SIZE : string = "SetSize";
        public static readonly SET_PROPERTY : string = "SetProperty";
        public static readonly SHOW_PROPERTIES : string = "ShowProperties";
        public static readonly MARK_ITEM : string = "MarkItem";
        public static readonly GET_PROJECT_MAP : string = "GetProjectMap";
        public static readonly GET_INSTANCE_MAP : string = "GetInstanceMap";
        public static readonly ECHO : string = "Echo";
        public static readonly EVENT : string = "Event";
        public static readonly FAIL : string = "Fail";
        public static readonly GET_FILESYSTEM_PATH : string = "GetFileSystemPath";
        public static readonly GET_FILESYSTEM_DIRECTORY : string = "GetFileSystemDirectory";
    }
}
