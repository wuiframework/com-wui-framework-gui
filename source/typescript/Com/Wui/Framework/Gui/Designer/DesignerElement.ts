/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Designer {
    "use strict";
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiElement = Com.Wui.Framework.Gui.Primitives.GuiElement;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import DesignerProtocolConstants = Com.Wui.Framework.Gui.Enums.DesignerProtocolConstants;
    import IDesignerElementMap = Com.Wui.Framework.Gui.Interfaces.Designer.IDesignerElementMap;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import IBaseViewer = Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseViewer;

    /**
     * DesignerElement class provides structure for handling of element rendering focused on GUI Designer needs.
     */

    export class DesignerElement extends GuiElement {
        private static elementIndex : number = 0;
        private readonly designerId : string;

        constructor() {
            super();
            DesignerElement.elementIndex++;
            this.designerId = DesignerProtocolConstants.DESIGNER_ELEMENT + DesignerElement.elementIndex;
        }

        /**
         * @param {string|IGuiElement|IGuiCommons|IBaseViewer|HTMLElement} $value Specify element's content.
         * @return {IGuiElement} Returns reference to current GuiElement instance.
         */
        public Add($value : string | IGuiElement | IGuiCommons | IBaseViewer | HTMLElement) : IGuiElement {
            DesignerElement.elementIndex++;
            return super.Add($value);
        }

        /**
         * @return {IDesignerElementMap[]} Returns object's map, which describes content of the GUI Element.
         */
        public getMap() : IDesignerElementMap[] {
            const map : IDesignerElementMap[] = [];
            const reflection : Reflection = Reflection.getInstance();
            this.getChildElements().foreach(($element : any, $key : number) : void => {
                const item : IDesignerElementMap = <IDesignerElementMap>{};
                if (reflection.IsMemberOf($element, GuiElement)) {
                    item.id = $element.designerId;
                    item.type = DesignerProtocolConstants.GUI_ELEMENT;
                    item.map = $element.getMap();
                } else if (reflection.IsMemberOf($element, GuiCommons)) {
                    const element : GuiCommons = <GuiCommons>$element;
                    const parent : GuiCommons = element.Parent();
                    item.id = element.Id();
                    item.type = DesignerProtocolConstants.GUI_OBJECT;
                    if (!ObjectValidator.IsEmptyOrNull(parent)) {
                        parent.getProperties().forEach(($value : string) : void => {
                            if (parent.hasOwnProperty($value) && parent[$value] === element) {
                                item.name = $value;
                            }
                        });
                    }
                } else {
                    item.id = this.designerId + "_" + $key;
                    item.type = DesignerProtocolConstants.HTML_ELEMENT;
                }
                map.push(item);
            });
            return map;
        }

        protected itemToString($item : string | IGuiElement | IGuiCommons | IBaseViewer | HTMLElement, $EOL : string,
                               $key? : number) : string {
            if (ObjectValidator.IsEmptyOrNull($item)) {
                return "";
            }
            if (ObjectValidator.IsString($item) || ObjectValidator.IsSet((<HTMLElement>$item).outerHTML)) {
                return $EOL + "<span id=\"" + this.designerId + "_" + $key + "\">" + super.itemToString($item, $EOL + "    ") + "</span>";
            }
            return super.itemToString($item, $EOL);
        }
    }
}
