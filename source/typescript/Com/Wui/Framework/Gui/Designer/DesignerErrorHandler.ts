/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Designer {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import Exception = Com.Wui.Framework.Commons.Exceptions.Type.Exception;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import AsyncHttpResolver = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.AsyncHttpResolver;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IDesignerProtocol = Com.Wui.Framework.Gui.Interfaces.Designer.IDesignerProtocol;
    import DesignerTaskType = Com.Wui.Framework.Gui.Enums.DesignerTaskType;

    /**
     * DesignerErrorHandler class provides redirection of exceptions handling to the GUI Designer layer.
     */
    export class DesignerErrorHandler extends AsyncHttpResolver {
        private exceptionsList : ArrayList<Exception>;

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            if (ObjectValidator.IsEmptyOrNull(this.exceptionsList)) {
                if ($POST.KeyExists(HttpRequestConstants.EXCEPTIONS_LIST)) {
                    this.exceptionsList = $POST.getItem(HttpRequestConstants.EXCEPTIONS_LIST);
                } else {
                    this.exceptionsList = new ArrayList<Exception>();
                }
            }
        }

        protected getExceptionsList() : ArrayList<Exception> {
            return this.exceptionsList;
        }

        protected resolver() : void {
            try {
                Echo.ClearAll();
                this.sendData(<IDesignerProtocol>{
                    data: Echo.getStream(),
                    task: DesignerTaskType.ECHO
                });
                this.sendData(<IDesignerProtocol>{
                    data: this.getExceptionsList().ToString(),
                    task: DesignerTaskType.FAIL
                });
            } catch (ex) {
                try {
                    this.sendData(<IDesignerProtocol>{
                        data: ex.message,
                        task: DesignerTaskType.FAIL
                    });
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            }
        }

        private sendData($data : IDesignerProtocol) : void {
            try {
                window.parent.postMessage($data, "*");
            } catch (ex) {
                window.parent.postMessage(<IDesignerProtocol>{
                    data: "Failed to send designer data: " + ex.message,
                    task: DesignerTaskType.FAIL
                }, "*");
            }
        }
    }
}
