/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Designer {
    "use strict";

    /**
     * BlankPanelViewer class provides implementation of empty panel viewer suitable for on-the-fly development of the GUI.
     */
    export class BlankPanelViewer extends Com.Wui.Framework.Gui.Primitives.BaseViewer {

        protected testImplementation() : string {
            return " ";
        }
    }
}
