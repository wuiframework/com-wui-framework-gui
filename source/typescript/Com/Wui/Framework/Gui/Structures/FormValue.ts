/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Structures {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * FormValue class provides structure for HTML input element.
     */
    export class FormValue extends Com.Wui.Framework.Commons.Primitives.BaseArgs {
        private readonly id : string;
        private readonly value : any;
        private readonly errorFlag : boolean;

        /**
         * @param {string} $id Set element id value.
         * @param {any} $value Set element value.
         * @param {boolean} [$errorFlag] Specify, if element is in error status or not.
         */
        constructor($id : string, $value : any, $errorFlag? : boolean) {
            super();
            this.id = $id;
            this.value = $value;
            if (ObjectValidator.IsSet($errorFlag)) {
                this.errorFlag = $errorFlag;
            } else {
                this.errorFlag = false;
            }
        }

        /**
         * @return {string} Returns element's id.
         */
        public Id() : string {
            return this.id;
        }

        /**
         * @return {any} Returns element's value.
         */
        public Value() : any {
            return this.value;
        }

        /**
         * @return {boolean} Returns true, if element is in error state, otherwise false.
         */
        public ErrorFlag() : boolean {
            return this.errorFlag;
        }
    }
}
