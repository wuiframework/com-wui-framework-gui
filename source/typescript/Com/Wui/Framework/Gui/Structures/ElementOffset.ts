/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Structures {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    /**
     * ElementOffset class provides structure for element offset parameters.
     */
    export class ElementOffset extends Com.Wui.Framework.Commons.Primitives.BaseArgs {
        private top : number;
        private left : number;

        /**
         * @param {number} [$top] Set top offset value.
         * @param {number} [$left] Set left offset value.
         */
        constructor($top? : number, $left? : number) {
            super();
            this.top = 0;
            this.left = 0;
            this.Top($top);
            this.Left($left);
        }

        /**
         * @param {number} [$value] Set top offset value.
         * @return {number} Returns top offset value.
         */
        public Top($value? : number) : number {
            return this.top = Property.Integer(this.top, $value);
        }

        /**
         * @param {number} [$value] Set left offset value.
         * @return {number} Returns left offset value.
         */
        public Left($value? : number) : number {
            return this.left = Property.Integer(this.left, $value);
        }
    }
}
