/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Structures {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    /**
     * ImageCorners class provides structure for handling of image corners radius.
     */
    export class ImageCorners extends Com.Wui.Framework.Commons.Primitives.BaseArgs {
        private topLeft : number;
        private topRight : number;
        private bottomLeft : number;
        private bottomRight : number;

        /**
         * @param {number} [$topLeft] Specify top left radius value in pixels.
         * @param {number} [$topRight] Specify top right radius value in pixels.
         * @param {number} [$bottomLeft] Specify bottom left radius value in pixels.
         * @param {number} [$bottomRight] Specify bottom right radius value in pixels.
         */
        constructor($topLeft? : number, $topRight? : number, $bottomLeft? : number, $bottomRight? : number) {
            super();
            this.topLeft = 0;
            this.topRight = 0;
            this.bottomLeft = 0;
            this.bottomRight = 0;
            this.setCorners($topLeft, $topRight, $bottomLeft, $bottomRight);
        }

        /**
         * @param {number} $topLeft Specify top left radius value in pixels.
         * @param {number} $topRight Specify top right radius value in pixels.
         * @param {number} $bottomLeft Specify bottom left radius value in pixels.
         * @param {number} $bottomRight Specify bottom right radius value in pixels.
         * @return {void}
         */
        public setCorners($topLeft : number, $topRight : number, $bottomLeft : number, $bottomRight : number) : void {
            this.TopLeft($topLeft);
            this.TopRight($topRight);
            this.BottomLeft($bottomLeft);
            this.BottomRight($bottomRight);
        }

        /**
         * @param {number} [$value] Set top left radius value in pixels.
         * @return {number} Returns top left radius value in pixels.
         */
        public TopLeft($value? : number) : number {
            return this.topLeft = Property.PositiveInteger(this.topLeft, $value);
        }

        /**
         * @param {number} [$value] Set top right radius value in pixels.
         * @return {number} Returns top right radius value in pixels.
         */
        public TopRight($value? : number) : number {
            return this.topRight = Property.PositiveInteger(this.topRight, $value);
        }

        /**
         * @param {number} [$value] Set bottom left radius value in pixels.
         * @return {number} Returns bottom left radius value in pixels.
         */
        public BottomLeft($value? : number) : number {
            return this.bottomLeft = Property.PositiveInteger(this.bottomLeft, $value);
        }

        /**
         * @param {number} [$value] Set bottom right radius value in pixels.
         * @return {number} Returns bottom right radius value in pixels.
         */
        public BottomRight($value? : number) : number {
            return this.bottomRight = Property.PositiveInteger(this.bottomRight, $value);
        }
    }
}
