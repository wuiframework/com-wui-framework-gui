/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Structures {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;

    /**
     * Borders class provides structure for element borders parameters.
     */
    export class Borders extends ElementOffset {
        private bottom : number;
        private right : number;

        /**
         * @param {string} [$elementId] Specify elementId, whose size should be parsed.
         */
        constructor($elementId? : string) {
            super();
            if (!ObjectValidator.IsEmptyOrNull($elementId) && ElementManager.Exists($elementId)) {
                this.Left(ElementManager.getCssIntegerValue($elementId, "border-left-width"));
                this.Top(ElementManager.getCssIntegerValue($elementId, "border-top-width"));
                this.Right(ElementManager.getCssIntegerValue($elementId, "border-right-width"));
                this.Bottom(ElementManager.getCssIntegerValue($elementId, "border-bottom-width"));
            } else {
                this.bottom = 0;
                this.right = 0;
            }
        }

        /**
         * @param {number} [$value] Set border top value.
         * @return {number} Returns border top value.
         */
        public Top($value? : number) : number {
            return super.Top($value);
        }

        /**
         * @param {number} [$value] Set border left value.
         * @return {number} Returns border left value.
         */
        public Left($value? : number) : number {
            return super.Left($value);
        }

        /**
         * @param {number} [$value] Set border bottom value.
         * @return {number} Returns border bottom value.
         */
        public Bottom($value? : number) : number {
            return this.bottom = Property.Integer(this.bottom, $value);
        }

        /**
         * @param {number} [$value] Set border right offset value.
         * @return {number} Returns border right offset value.
         */
        public Right($value? : number) : number {
            return this.right = Property.Integer(this.right, $value);
        }

        /**
         * @return {number} Returns cumulative borders width value.
         */
        public getWidth() : number {
            return this.Left() + this.Right();
        }

        /**
         * @return {number} Returns cumulative borders height value.
         */
        public getHeight() : number {
            return this.Top() + this.Bottom();
        }

        /**
         * @return {Size} Returns cumulative borders size value.
         */
        public getSize() : Size {
            const size : Size = new Size();
            size.Width(this.getWidth());
            size.Height(this.getHeight());
            return size;
        }
    }
}
