/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Structures {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    /**
     * ImageCropDimensions class provides structure for handling of image crop dimensions.
     */
    export class ImageCropDimensions extends Com.Wui.Framework.Commons.Primitives.BaseArgs {
        private topX : number;
        private topY : number;
        private bottomX : number;
        private bottomY : number;

        /**
         * @param {number} [$topX] Specify top X position value.
         * @param {number} [$topY] Specify top Y position value.
         * @param {number} [$bottomX] Specify bottom X position value.
         * @param {number} [$bottomY] Specify bottom Y position value.
         */
        constructor($topX? : number, $topY? : number, $bottomX? : number, $bottomY? : number) {
            super();
            this.topX = 0;
            this.topY = 0;
            this.bottomX = 0;
            this.bottomY = 0;
            this.setDimensions($topX, $topY, $bottomX, $bottomY);
        }

        /**
         * @param {number} $topX Specify top X position value.
         * @param {number} $topY Specify top Y position value.
         * @param {number} $bottomX Specify bottom X position value.
         * @param {number} $bottomY Specify bottom Y position value.
         * @return {void}
         */
        public setDimensions($topX : number, $topY : number, $bottomX : number, $bottomY : number) : void {
            this.TopX($topX);
            this.TopY($topY);
            this.BottomX($bottomX);
            this.BottomY($bottomY);
        }

        /**
         * @param {number} [$value] Set top X position value.
         * @return {number} Returns top X position value.
         */
        public TopX($value? : number) : number {
            return this.topX = Property.PositiveInteger(this.topX, $value);
        }

        /**
         * @param {number} [$value] Set top Y position value.
         * @return {number} Returns top Y position value.
         */
        public TopY($value? : number) : number {
            return this.topY = Property.PositiveInteger(this.topY, $value);
        }

        /**
         * @param {number} [$value] Set bottom X position value.
         * @return {number} Returns bottom X position value.
         */
        public BottomX($value? : number) : number {
            return this.bottomX = Property.PositiveInteger(this.bottomX, $value);
        }

        /**
         * @param {number} [$value] Set bottom Y position value.
         * @return {number} Returns bottom Y position value.
         */
        public BottomY($value? : number) : number {
            return this.bottomY = Property.PositiveInteger(this.bottomY, $value);
        }
    }
}
