/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Structures {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;

    /**
     * Size class provides structure for width and height values.
     */
    export class Size extends Com.Wui.Framework.Commons.Primitives.BaseArgs {
        private width : number;
        private height : number;

        /**
         * @param {string} [$elementId] Specify elementId, whose size should be parsed.
         * @param {boolean} [$force] Specify if returned values should be up-to-date (slower - needs layout recalculation).
         * @param {boolean} [$ignoreHidden] Specify if returned values should be 0 if element is hidden.
         */
        constructor($elementId? : string, $force : boolean = false, $ignoreHidden : boolean = true) {
            super();
            this.width = 0;
            this.height = 0;
            if (!ObjectValidator.IsEmptyOrNull($elementId)) {
                this.width = ElementManager.getOffsetWidth($elementId, $force, $ignoreHidden);
                this.height = ElementManager.getOffsetHeight($elementId, $force, $ignoreHidden);
            }
        }

        /**
         * @param {number} [$value] Set width value.
         * @return {number} Returns width value.
         */
        public Width($value? : number) : number {
            this.width = Property.PositiveInteger(this.width, $value);
            return this.width;
        }

        /**
         * @param {number} [$value] Set height value.
         * @return {number} Returns height value.
         */
        public Height($value? : number) : number {
            this.height = Property.PositiveInteger(this.height, $value);
            return this.height;
        }
    }
}
