/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Structures {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import DisplayUnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import Exception = Com.Wui.Framework.Commons.Exceptions.Type.Exception;

    /**
     * PropagableNumber class provides structure for handling of propagable number values.
     */
    export class PropagableNumber extends Com.Wui.Framework.Commons.Primitives.BaseArgs {
        private readonly propagated : boolean;
        private readonly values : IPropagableNumberValue[];

        /**
         * @param {PropagableNumber} $parentValue Specify parent value.
         * @param {PropagableNumber} $childValue Specify child value.
         * @return {PropagableNumber} Returns propagated value, if propagation has been allowed, otherwise null.
         */
        public static PriorityValue($parentValue : PropagableNumber, $childValue : PropagableNumber) : PropagableNumber {
            if (!ObjectValidator.IsEmptyOrNull($childValue)) {
                return $childValue;
            } else if (!ObjectValidator.IsEmptyOrNull($parentValue) && $parentValue.IsPropagated()) {
                return $parentValue;
            }
            return null;
        }

        /**
         * @param {string|string[]|IPropagableNumberValue|IPropagableNumberValue[]} $value Specify value/values.
         * @param {boolean} [$propagated] Specify propagable flag.
         */
        constructor($value : string | string[] | IPropagableNumberValue | IPropagableNumberValue[] | PropagableNumber,
                    $propagated? : boolean) {
            super();
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                if (ObjectValidator.IsSet((<any>$value).IsPropagated)) {
                    const copyFrom : PropagableNumber = <PropagableNumber>$value;
                    this.values = JSON.parse(JSON.stringify(copyFrom.values));
                    this.propagated = copyFrom.propagated;
                } else {
                    const valArray : any[] =
                        ObjectValidator.IsArray($value) ? <string[] | IPropagableNumberValue[]>$value : [<IPropagableNumberValue>$value];
                    this.values = [];
                    valArray.forEach(($value : string | IPropagableNumberValue) : void => {
                        if (ObjectValidator.IsString($value)) {
                            const value : number = parseFloat($value.toString());
                            if (ObjectValidator.IsSet(value)) {
                                if (StringUtils.Contains($value.toString(), "px")) {
                                    this.values.push({number: value, unitType: UnitType.PX});
                                } else if (StringUtils.Contains($value.toString(), "%")) {
                                    this.values.push({number: value, unitType: UnitType.PCT});
                                } else {
                                    throw new Exception("Unit type of value not supported : " + $value);
                                }
                            } else {
                                throw new Exception("Format of value not supported : " + $value);
                            }
                        } else {
                            this.values.push(<IPropagableNumberValue>$value);
                        }
                    });
                    this.propagated = ObjectValidator.IsSet($propagated) ? $propagated : false;
                }
            } else {
                this.values = [];
            }
        }

        /**
         * @return {boolean} Returns propagable flag value.
         */
        public IsPropagated() : boolean {
            return this.propagated;
        }

        /**
         * @param {number} $parentPixelSize Specify value, which should be normalized.
         * @param {DisplayUnitType} $targetUnitType Specify targeted unit type for normalization.
         * @return {number} Returns normalized value.
         */
        public Normalize($parentPixelSize : number, $targetUnitType : DisplayUnitType) : number {
            if (ObjectValidator.IsEmptyOrNull(this.values)) {
                return -1;
            }
            let returnVal : number = 0;
            this.values.forEach(($value : IPropagableNumberValue) : void => {
                let normalizedNumber : number = $value.number;
                if ($parentPixelSize > 0) {
                    if ($value.unitType === DisplayUnitType.PX && $targetUnitType === DisplayUnitType.PCT) {
                        normalizedNumber = $value.number / $parentPixelSize * 100;
                    } else if ($value.unitType === DisplayUnitType.PCT && $targetUnitType === DisplayUnitType.PX) {
                        normalizedNumber = $parentPixelSize * $value.number / 100;
                    }
                }
                returnVal += normalizedNumber;
            });
            if ($targetUnitType === DisplayUnitType.PX) {
                return Math.round(returnVal);
            } else {
                return returnVal;
            }
        }

    }

    export class IPropagableNumberValue {
        public number : number;
        public unitType : DisplayUnitType;
    }
}
