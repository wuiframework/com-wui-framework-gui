/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Gui.RuntimeTests.Utils {
    "use strict";
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import AbsoluteOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import BrowserType = Com.Wui.Framework.Commons.Enums.BrowserType;

    export class ElementManagerTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        public testExists() : void {
            Echo.Println("<div id=\"testId0\">test element</div>");
            Echo.Println("<div id=\"testId4\">new element</div>");
            this.assertEquals(ElementManager.Exists("testId0"), true, "validate if element has been rendered");
            this.assertEquals(ElementManager.Exists("testId#"), false, "validate that element has not been rendered");
            this.assertEquals(ElementManager.Exists("testId4"), true, "validate if element has been rendered");
        }

        public testGetClassName() : void {
            Echo.Println("<div id=\"darla_csc_holder\" class=\"darla\">...</div>");
            Echo.Println("<div id=\"testId1\" class=\"testClass\">test element</div>");
            this.assertEquals(ElementManager.getClassName("testId1"), "testClass", "get test element class name");
            this.assertEquals(ElementManager.getClassName("testId#"), null, "try to get class name of undefined element");
            this.assertEquals(ElementManager.getClassName("darla_csc_holder"), "darla", "get test element class name");
        }

        public testSetClassName() : void {
            Echo.Println("<div id=\"testId2\" class=\"testClass\">test element</div>");
            ElementManager.setClassName("testId2", "superTestClass");
            ElementManager.setClassName("testId#", "superUndefinedClass");
            this.assertEquals(ElementManager.getClassName("testId2"), "superTestClass", "get test element modified class name");
        }

        public testGetCssValue() : void {
            Echo.Println("<div id=\"testId3\" style=\"color: red;\">test element</div>");
            Echo.Println("<div id=\"testId5\" style=\"display: none;\">test display</div>");
            this.assertEquals(ElementManager.getCssValue("testId3", "color") === "rgb(255, 0, 0)" ||
                ElementManager.getCssValue("testId3", "color") === "red", true, "get test element css value");
            this.assertEquals(ElementManager.getCssValue("testId5", "display") === "none", true, "get test element css value");

            if (this.getRequest().IsOnServer() ||
                this.getRequest().getBrowserType() === BrowserType.INTERNET_EXPLORER ||
                this.getRequest().getBrowserType() === BrowserType.FIREFOX) {
                /**
                 * @description This use case is applicable only at IE or FF browsers or, if test case is running at server,
                 * because browsers with gecko core are not able to get css rules, if test case is running from the file
                 */
                if (this.getRequest().getBrowserType() === BrowserType.SAFARI) {
                    /**
                     * @description SAFARI compatibility test case, because SAFARI does not support global overflow attribute
                     */
                    this.assertEquals(ElementManager.getCssValue("Content", "overflow-x") === "visible" &&
                        ElementManager.getCssValue("Content", "overflow-y") === "visible", true, "get style sheet css value by id");
                } else {
                    this.assertEquals(ElementManager.getCssValue("Content", "overflow") === "visible" ||
                        ElementManager.getCssValue("Content", "overflow") === "auto", true, "get style sheet css value by id");
                }
                this.assertEquals(ElementManager.getCssValue("Note", "margin-top"), "20px", "get style sheet css value by class name");
            }
            this.assertEquals(ElementManager.getCssValue("testId#", "width"), null, "try to get css value for undefined element");
        }

        public testGetCssIntValue() : void {
            Echo.Println("<div id=\"testId4\" style=\"width: 100px; border: 1px solid red; background-color: blue;\">test element</div>");
            Echo.Println("<div id=\"testId6\" style=\"display: none; position: absolute; z-index: 4; right: 52px;" +
                " height: 44px;\">...element...</div>");

            this.assertEquals(ElementManager.getCssIntegerValue("testId6", "z-index"), 4, "get test element css integer value");
            this.assertEquals(ElementManager.getCssIntegerValue("testId4", "width"), 100, "get test element css integer value");
            this.assertEquals(ElementManager.getCssIntegerValue("testId4", "color"), null, "try to get not defined css integer value");
            this.assertEquals(ElementManager.getCssIntegerValue("testId4", "background-color"), null,
                "try to get not integer value as integer value");
            this.assertEquals(ElementManager.getCssIntegerValue("testId6", "z-index"), 4, "get test element css integer value");

            let padding : number = ElementManager.getCssIntegerValue("Content", "padding");
            if (this.getRequest().IsOnServer() ||
                this.getRequest().getBrowserType() === BrowserType.INTERNET_EXPLORER ||
                this.getRequest().getBrowserType() === BrowserType.FIREFOX) {
                /**
                 * @description This use case is applicable only at IE or FF browsers or, if test case is running at server,
                 * because browsers with gecko core are not able to get css rules, if test case is running from the file
                 */
                if (this.getRequest().getBrowserType() === BrowserType.INTERNET_EXPLORER &&
                    this.getRequest().getBrowserVersion() <= 8
                    || this.getRequest().getBrowserType() === BrowserType.SAFARI) {
                    /**
                     * @description IE8< and SAFARI compatibility test case,
                     * because IE8< and SAFARI does not support global padding attribute
                     */
                    padding = ElementManager.getCssIntegerValue("Content", "padding-top");
                    this.assertEquals(padding === 10 &&
                        padding === ElementManager.getCssIntegerValue("Content", "padding-bottom") &&
                        padding === ElementManager.getCssIntegerValue("Content", "padding-left") &&
                        padding === ElementManager.getCssIntegerValue("Content", "padding-right"), true,
                        "get style sheet css integer value");
                } else {
                    this.assertEquals(padding, 10, "get style sheet css integer value");
                }
            }
        }

        public testSetCssProperty() : void {
            Echo.Println("<div id=\"testId5\">test element</div>");
            ElementManager.setCssProperty("testId5", "border", "1px solid green");
            let border : string = ElementManager.getCssValue("testId5", "border");
            if (this.getRequest().getBrowserType() === BrowserType.INTERNET_EXPLORER &&
                this.getRequest().getBrowserVersion() <= 8) {
                /**
                 * @description IE8< compatibility test case, because IE8< does not support global border attribute
                 * and it has different values ordering
                 */
                border = ElementManager.getCssValue("testId5", "border-top");
                this.assertEquals(border === "green 1px solid" &&
                    border === ElementManager.getCssValue("testId5", "border-bottom") &&
                    border === ElementManager.getCssValue("testId5", "border-left") &&
                    border === ElementManager.getCssValue("testId5", "border-right"), true,
                    "validate if element css property has been set");
            } else if (this.getRequest().getBrowserType() === BrowserType.SAFARI) {
                /**
                 * @description SAFARI compatibility test case, because SAFARI does not support global border attribute
                 */
                border = ElementManager.getCssValue("testId5", "border-top-width");
                this.assertEquals(border === "1px" &&
                    border === ElementManager.getCssValue("testId5", "border-bottom-width") &&
                    border === ElementManager.getCssValue("testId5", "border-left-width") &&
                    border === ElementManager.getCssValue("testId5", "border-right-width"), true,
                    "validate if element border width has been set");
                border = ElementManager.getCssValue("testId5", "border-top-style");
                this.assertEquals(border === "solid" &&
                    border === ElementManager.getCssValue("testId5", "border-bottom-style") &&
                    border === ElementManager.getCssValue("testId5", "border-left-style") &&
                    border === ElementManager.getCssValue("testId5", "border-right-style"), true,
                    "validate if element border style has been set");
                border = ElementManager.getCssValue("testId5", "border-top-color");
                this.assertEquals(border === "green" &&
                    border === ElementManager.getCssValue("testId5", "border-bottom-color") &&
                    border === ElementManager.getCssValue("testId5", "border-left-color") &&
                    border === ElementManager.getCssValue("testId5", "border-right-color"), true,
                    "validate if element border color has been set");
            } else {
                this.assertEquals(ElementManager.getCssValue("testId5", "border"), "1px solid green",
                    "validate if element css property has been set");
            }
        }

        public testClearCssProperty() : void {
            Echo.Println("<div id=\"testId6\">test element</div>");
            ElementManager.setCssProperty("testId6", "border", "1px solid red");
            let border : string = ElementManager.getCssValue("testId6", "border");
            if (this.getRequest().getBrowserType() === BrowserType.INTERNET_EXPLORER &&
                this.getRequest().getBrowserVersion() <= 8) {
                /**
                 * @description IE8< compatibility test case, because IE8< does not support global border attribute
                 * and it has different values ordering
                 */
                border = ElementManager.getCssValue("testId6", "border-top");
                this.assertEquals(border === "red 1px solid" &&
                    border === ElementManager.getCssValue("testId6", "border-bottom") &&
                    border === ElementManager.getCssValue("testId6", "border-left") &&
                    border === ElementManager.getCssValue("testId6", "border-right"), true,
                    "validate if element css property has been set");

                ElementManager.ClearCssProperty("testId6", "border-top");
                ElementManager.ClearCssProperty("testId6", "border-bottom");
                ElementManager.ClearCssProperty("testId6", "border-left");
                ElementManager.ClearCssProperty("testId6", "border-right");

                this.assertEquals(
                    ElementManager.getCssValue("testId6", "border-top") === null &&
                    ElementManager.getCssValue("testId6", "border-bottom") === null &&
                    ElementManager.getCssValue("testId6", "border-left") === null &&
                    ElementManager.getCssValue("testId6", "border-right") === null, true,
                    "validate if element css property has been cleaned");
            } else if (this.getRequest().getBrowserType() === BrowserType.SAFARI) {
                /**
                 * @description SAFARI compatibility test case, because SAFARI does not support global border attribute
                 */
                border = ElementManager.getCssValue("testId6", "border-top-width");
                this.assertEquals(border === "1px" &&
                    border === ElementManager.getCssValue("testId6", "border-bottom-width") &&
                    border === ElementManager.getCssValue("testId6", "border-left-width") &&
                    border === ElementManager.getCssValue("testId6", "border-right-width"), true,
                    "validate if element border width has been set");
                border = ElementManager.getCssValue("testId6", "border-top-style");
                this.assertEquals(border === "solid" &&
                    border === ElementManager.getCssValue("testId6", "border-bottom-style") &&
                    border === ElementManager.getCssValue("testId6", "border-left-style") &&
                    border === ElementManager.getCssValue("testId6", "border-right-style"), true,
                    "validate if element border style has been set");
                border = ElementManager.getCssValue("testId6", "border-top-color");
                this.assertEquals(border === "red" &&
                    border === ElementManager.getCssValue("testId6", "border-bottom-color") &&
                    border === ElementManager.getCssValue("testId6", "border-left-color") &&
                    border === ElementManager.getCssValue("testId6", "border-right-color"), true,
                    "validate if element border color has been set");

                ElementManager.ClearCssProperty("testId6", "border-top-width");
                ElementManager.ClearCssProperty("testId6", "border-bottom-width");
                ElementManager.ClearCssProperty("testId6", "border-left-width");
                ElementManager.ClearCssProperty("testId6", "border-right-width");
                ElementManager.ClearCssProperty("testId6", "border-top-style");
                ElementManager.ClearCssProperty("testId6", "border-bottom-style");
                ElementManager.ClearCssProperty("testId6", "border-left-style");
                ElementManager.ClearCssProperty("testId6", "border-right-style");
                ElementManager.ClearCssProperty("testId6", "border-top-color");
                ElementManager.ClearCssProperty("testId6", "border-bottom-color");
                ElementManager.ClearCssProperty("testId6", "border-left-color");
                ElementManager.ClearCssProperty("testId6", "border-right-color");

                this.assertEquals(
                    ElementManager.getCssValue("testId6", "border-top-width") === null &&
                    ElementManager.getCssValue("testId6", "border-bottom-width") === null &&
                    ElementManager.getCssValue("testId6", "border-left-width") === null &&
                    ElementManager.getCssValue("testId6", "border-right-width") === null &&
                    ElementManager.getCssValue("testId6", "border-top-style") === null &&
                    ElementManager.getCssValue("testId6", "border-bottom-style") === null &&
                    ElementManager.getCssValue("testId6", "border-left-style") === null &&
                    ElementManager.getCssValue("testId6", "border-right-style") === null &&
                    ElementManager.getCssValue("testId6", "border-top-color") === null &&
                    ElementManager.getCssValue("testId6", "border-bottom-color") === null &&
                    ElementManager.getCssValue("testId6", "border-left-color") === null &&
                    ElementManager.getCssValue("testId6", "border-right-color") === null, true,
                    "validate if element css property has been cleaned");
            } else {
                this.assertEquals(ElementManager.getCssValue("testId6", "border"), "1px solid red",
                    "validate if element css property has been set");

                ElementManager.ClearCssProperty("testId6", "border");

                this.assertEquals(ElementManager.getCssValue("testId6", "border") === null, true,
                    "validate if element css property has been cleaned");
            }
        }

        public testGetInnerHtml() : void {
            Echo.Println("<div id=\"testId7\"><span class=\"innerClass\">test element</span></div>");
            const innerHtml : string = ElementManager.getInnerHtml("testId7");
            if (this.getRequest().getBrowserType() === BrowserType.INTERNET_EXPLORER &&
                this.getRequest().getBrowserVersion() <= 8) {
                /**
                 * @description IE8< compatibility test case, because IE8< has different style rules standard
                 */
                this.assertEquals(innerHtml === "<SPAN class=innerClass>test element</SPAN>", true,
                    "get innerHTML of the element");
            } else {
                this.assertEquals(innerHtml === "<span class=\"innerClass\">test element</span>", true,
                    "get innerHTML of the element");
            }
        }

        public testSetInnerHtml() : void {
            Echo.Println("<div id=\"testId8\"><span class=\"innerClass\">test element</span></div>");
            ElementManager.setInnerHtml("testId8", "this is inner HTML set by element manager");
            this.assertEquals(ElementManager.getInnerHtml("testId8"), "this is inner HTML set by element manager",
                "set innerHTML to the element");
        }

        public testAppendHtml() : void {
            Echo.Println("<div id=\"testId81\"><span class=\"innerClass\">test element</span></div>");
            ElementManager.AppendHtml("testId81", " this is appended HTML set by element manager");
            const innerHtml : string = ElementManager.getInnerHtml("testId81");
            if (this.getRequest().getBrowserType() === BrowserType.INTERNET_EXPLORER &&
                this.getRequest().getBrowserVersion() <= 8) {
                /**
                 * @description IE8< compatibility test case, because IE8< has different style rules standard
                 */
                this.assertEquals(
                    innerHtml === "<SPAN class=innerClass>test element</SPAN><SPAN guiType=\"HtmlAppender\">" +
                    "&nbsp;this is appended HTML set by element manager</SPAN>", true,
                    "validate that innerHTML of the element has been correctly appended");
            } else {
                this.assertEquals(
                    innerHtml === "<span class=\"innerClass\">test element</span><span guitype=\"HtmlAppender\">" +
                    "&nbsp;this is appended HTML set by element manager</span>" ||
                    innerHtml === "<span class=\"innerClass\">test element</span><span guiType=\"HtmlAppender\">" +
                    "&nbsp;this is appended HTML set by element manager</span>", true,
                    "validate that innerHTML of the element has been correctly appended");
            }
        }

        public testIsVisible() : void {
            Echo.Println("<div id=\"testId9\">test element</div>");
            this.assertEquals(ElementManager.IsVisible("testId9"), true, "validate if element is visible");
            ElementManager.setCssProperty("testId9", "display", "none");
            this.assertEquals(ElementManager.IsVisible("testId9"), false,
                "validate that element is not visible due to css property");
            this.assertEquals(ElementManager.IsVisible("testId#"), false,
                "validate that element is not visible because it does not exists");
        }

        public testShow() : void {
            Echo.Println("<div id=\"testId10\">test element</div>");
            ElementManager.Show("testId10");
            this.assertEquals(ElementManager.getCssValue("testId10", "display"), "block", "validate that element is visible");
        }

        public testHide() : void {
            Echo.Println("<div id=\"testId11\">test element</div>");
            ElementManager.Hide("testId11");
            this.assertEquals(ElementManager.getCssValue("testId11", "display"), "none", "validate that element is not visible");
        }

        public testToggleVisibility() : void {
            Echo.Println("<div id=\"testId12\">test element</div>");
            ElementManager.ToggleVisibility("testId12");
            this.assertEquals(ElementManager.IsVisible("testId12"), false, "validate that element is not visible");
            ElementManager.ToggleVisibility("testId12");
            this.assertEquals(ElementManager.IsVisible("testId12"), true, "validate that element is visible");
        }

        public testTurnOn() : void {
            Echo.Println("<div id=\"testId13\" class=\"testClass\">test element</div>");
            ElementManager.TurnOn("testId13");
            this.assertEquals(ElementManager.getClassName("testId13"), GeneralCssNames.ON,
                "validate that element has been turned on");
        }

        public testTurnOff() : void {
            Echo.Println("<div id=\"testId14\" class=\"testClass\">test element</div>");
            ElementManager.TurnOff("testId14");
            this.assertEquals(ElementManager.getClassName("testId14"), GeneralCssNames.OFF,
                "validate that element has been turned off");
        }

        public testToggleOnOff() : void {
            Echo.Println("<div id=\"testId15\" class=\"testClass\">test element</div>");
            ElementManager.ToggleOnOff("testId15");
            this.assertEquals(ElementManager.getClassName("testId15"), GeneralCssNames.ON,
                "validate that element has been turned on");
            ElementManager.ToggleOnOff("testId15");
            this.assertEquals(ElementManager.getClassName("testId15"), GeneralCssNames.OFF,
                "validate that element has been turned off");
        }

        public testTurnActive() : void {
            Echo.Println("<div id=\"testId151\" class=\"testClass\">test element</div>");
            ElementManager.TurnActive("testId151");
            this.assertEquals(ElementManager.getClassName("testId151"), GeneralCssNames.ACTIVE,
                "validate that element has been activated");
        }

        public testBringToFront() : void {
            Echo.Println("<div id=\"testId16\" class=\"testClass\">test element</div>");
            ElementManager.BringToFront("testId16");
            this.assertEquals(StringUtils.ToInteger(ElementManager.getCssValue("testId16", "z-index")), 1000,
                "validate that element has been \"bring to front\"");
        }

        public testSendToBack() : void {
            Echo.Println("<div id=\"testId17\" class=\"testClass\">test element</div>");
            ElementManager.SendToBack("testId17");
            this.assertEquals(StringUtils.ToInteger(ElementManager.getCssValue("testId17", "z-index")), 0,
                "validate that element has been \"send to back\"");
        }

        public testSetOpacity() : void {
            Echo.Println("<div id=\"testId18\"><div class=\"Logo\">test element</div></div>");
            Echo.Println("<div id=\"testId182\" style=\"background-color: red;\">test element 2</div>");

            ElementManager.setOpacity("testId18", -10);
            this.assertEquals(ElementManager.getCssValue("testId18", "opacity") === "0" ||
                ElementManager.getCssValue("testId18", "opacity") === "0.00", true,
                "validate opacity range correction under");
            ElementManager.setOpacity("testId18", 110);
            this.assertEquals(ElementManager.getCssValue("testId18", "opacity") === "1" ||
                ElementManager.getCssValue("testId18", "opacity") === "1.00", true,
                "validate opacity range correction above");
            ElementManager.setOpacity("testId18", 5);
            this.assertEquals(ElementManager.getCssValue("testId18", "opacity"), "0.05",
                "validate element opacity value set by integer");
            ElementManager.setOpacity("testId18", 0.1);
            this.assertEquals(ElementManager.getCssValue("testId18", "opacity") === "0.1" ||
                ElementManager.getCssValue("testId18", "opacity") === "0.10", true,
                "validate element opacity value set by float");
            ElementManager.setOpacity("testId182", 0.3);
        }

        public testChangeOpacity() : void {
            Echo.Println("<div id=\"testId19\"><div class=\"Logo\">test element</div></div>");
            ElementManager.setOpacity("testId19", 0);
            ElementManager.ChangeOpacity("testId19", DirectionType.UP, 80);
        }

        public testEnabled() : void {
            Echo.Println("<div id=\"testId20\">" +
                "<div id=\"testId20_Enabled\" style=\"display: block; background-color: green;\">enabled test element</div>" +
                "<div id=\"testId20_Disabled\" style=\"display: none; background-color: gray;\">disabled test element</div>" +
                "</div>");
            ElementManager.Enabled("testId20", true);
            this.assertEquals(ElementManager.IsEnabled("testId20"), true,
                "validate that element is enabled");
            ElementManager.Enabled("testId20", false);
            this.assertEquals(ElementManager.IsEnabled("testId20"), false,
                "validate that element is disabled");
        }

        public testSetWidth() : void {
            Echo.Println("<div id=\"testId21\" style=\"border: 1px solid red;\">test element</div>");
            ElementManager.setWidth("testId21", 100);
            this.assertEquals(ElementManager.getCssValue("testId21", "width"), "100px",
                "validate element width");
        }

        public testSetHeight() : void {
            Echo.Println("<div id=\"testId22\" style=\"border: 1px solid red;\">test element</div>");
            ElementManager.setHeight("testId22", 100);
            this.assertEquals(ElementManager.getCssValue("testId22", "height"), "100px",
                "validate element height");
        }

        public testSetSize() : void {
            Echo.Println("<div id=\"testId23\" style=\"border: 1px solid red;\">test element</div>");
            ElementManager.setSize("testId23", 100, 100);
            this.assertEquals(ElementManager.getCssValue("testId23", "width"), "100px",
                "validate element width");
            this.assertEquals(ElementManager.getCssValue("testId23", "height"), "100px",
                "validate element height");
        }

        public testGetAbsoluteOffset() : void {
            Echo.Println("<div id=\"testId24\">test element</div>");
            let offset : AbsoluteOffset = ElementManager.getAbsoluteOffset("testId24");

            this.assertEquals(offset.Left(), 20, "validate element's absolute left offset");
            Echo.Println("element's absolute top offset: " + offset.Top());

            Echo.Println("<div id=\"testId25\" style=\"position: absolute; top: 100px; left: 100px;\">" +
                "test element with absolute position</div>");
            offset = ElementManager.getAbsoluteOffset("testId25");
            this.assertEquals(offset.Left(), 100, "validate absolute left offset of element with absolute position");
            this.assertEquals(offset.Top(), 100, "validate absolute top offset of element with absolute position");

            Echo.Println("<div style=\"position: relative; top: 10px; left: 10px;\">" +
                "<div style=\"position: absolute; top: 100px; left: 100px; padding: 10px; margin: 10px;\">" +
                "<div id=\"testId26\" style=\"position: relative; top: 10px; left: 10px;\">test element with relative envelop</div>" +
                "</div>" +
                "</div>");
            offset = ElementManager.getAbsoluteOffset("testId26");
            if (this.getRequest().getBrowserType() === BrowserType.INTERNET_EXPLORER &&
                this.getRequest().getBrowserVersion() <= 7) {
                /**
                 * @description IE7< compatibility test case, because IE7< has wire behaviour of offset calculation
                 */
                this.assertEquals(offset.Left(), 180, "validate absolute left offset of element with mixed position");
            } else {
                this.assertEquals(offset.Left(), 160, "validate absolute left offset of element with mixed position");
            }
            Echo.Println("absolute top offset of element with mixed postion: " + offset.Top());
        }
    }
}
/* dev:end */
