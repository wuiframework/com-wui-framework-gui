/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Gui.RuntimeTests.Utils {
    "use strict";
    import StaticPageContentManager = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import BrowserType = Com.Wui.Framework.Commons.Enums.BrowserType;

    export class StaticPageContentManagerTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        public integralTest() : void {
            Echo.Println("this text has been printed before tested page manager initialization");

            StaticPageContentManager.Clear();
            StaticPageContentManager.Title("Test title");
            StaticPageContentManager.Charset("UTF-32");
            StaticPageContentManager.Language(LanguageType.CZ);
            const link : HTMLLinkElement = document.createElement("link");
            link.rel = "stylesheet";
            link.href = "appended link";
            link.type = "text/css";
            StaticPageContentManager.HeadLinkAppend(link);
            StaticPageContentManager.HeadLinkAppend("test link 2");
            const script : HTMLScriptElement = document.createElement("script");
            script.src = "test script";
            script.type = "text/javascript";
            StaticPageContentManager.HeadScriptAppend(script);
            const meta : HTMLMetaElement = document.createElement("meta");
            meta.httpEquiv = "Author";
            meta.content = "test meta";
            StaticPageContentManager.HeadMetaDataAppend(meta);

            StaticPageContentManager.BodyAppend("this is new body");
            StaticPageContentManager.Draw();
            Echo.Print(WindowManager.ViewHTMLCode());
        }

        public testLanguage() : void {
            StaticPageContentManager.Language(LanguageType.CZ);
            this.assertEquals(StaticPageContentManager.Language(), LanguageType.CZ, "validate LanguageType");
        }

        public testTitle() : void {
            StaticPageContentManager.Title("Test title");
            this.assertEquals(StaticPageContentManager.Title(), "Test title", "validate title");
        }

        public testFaviconSource() : void {
            StaticPageContentManager.FaviconSource("http://www.WuiFramework.com/favicon.ico");
            this.assertEquals(StaticPageContentManager.FaviconSource(), "http://www.WuiFramework.com/favicon.ico",
                "validate favicon source");
        }

        public testCharset() : void {
            StaticPageContentManager.Charset("base64");
            this.assertEquals(StaticPageContentManager.Charset(), "base64", "validate content transfer encoding");
        }

        public testHeadLinkAppend() : void {
            Echo.Println("<link rel=\"stylesheet\" type=\"text/css\" href=\"theme.css\">");
            const linkElement : HTMLLinkElement = document.createElement("link");
            StaticPageContentManager.HeadLinkAppend(linkElement, "stylesheet", "text/css");
            this.assertEquals(StringUtils.PatternMatched("" +
                "<!DOCTYPE html>\r\n" +
                "\r\n" +
                "<head>\r\n" +
                "<title>Test title</title>\r\n" +
                "\r\n" +
                "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\r\n" +
                "<meta http-equiv=\"Author\" content=\"test meta\">\r\n" +
                "\r\n" +
                "<link href=\"http://www.WuiFramework.com/favicon.ico\" rel=\"shortcut icon\" type=\"text/css\">\r\n" +
                "<link href=\"resource/css/com-wui-framework-gui-*.min.css\" rel=\"stylesheet\" type=\"text/css\">\r\n" +
                "<link rel=\"stylesheet\" href=\"appended link\" type=\"text/css\">\r\n" +
                "<link href=\"test link 2\" rel=\"stylesheet\" type=\"text/css\">\r\n" +
                "<link>\r\n" +
                "\r\n" +
                "<script src=\"test script\" type=\"text/javascript\"></script>\r\n" +
                "</head>\r\n" +
                "\r\n" +
                "<body " +
                "onfocus=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
                "onblur=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyBlurEventHandler();\">\r\n" +
                "<div id=\"Browser\" class=\"" + BrowserType[this.getRequest().getBrowserType()] + "\">\r\n" +
                "<div id=\"Language\" class=\"Cz\">\r\n" +
                "<div id=\"Content\" class=\"Content\" guiType=\"PageContent\">\r\n" +
                "this is new body\r\n" +
                "\r\n" +
                "</div>\r\n" +
                "</div>\r\n" +
                "</div>\r\n" +
                "</body>",
                StaticPageContentManager.ToString()), true);
        }

        public testHeadMetaDataAppend() : void {
            Echo.Println("<meta charset=\"utf-8\">");
            const metaElement : HTMLMetaElement = document.createElement("meta");
            StaticPageContentManager.HeadMetaDataAppend(metaElement, "http response header");
            this.assertEquals(StringUtils.PatternMatched("" +
                "<!DOCTYPE html>\r\n" +
                "\r\n" +
                "<head>\r\n" +
                "<title>Test title</title>\r\n" +
                "\r\n" +
                "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\r\n" +
                "<meta http-equiv=\"Author\" content=\"test meta\">\r\n" +
                "<meta>\r\n" +
                "\r\n" +
                "<link href=\"http://www.WuiFramework.com/favicon.ico\" rel=\"shortcut icon\" type=\"text/css\">\r\n" +
                "<link href=\"resource/css/com-wui-framework-gui-*.min.css\" rel=\"stylesheet\" type=\"text/css\">\r\n" +
                "<link rel=\"stylesheet\" href=\"appended link\" type=\"text/css\">\r\n" +
                "<link href=\"test link 2\" rel=\"stylesheet\" type=\"text/css\">\r\n" +
                "<link>\r\n" +
                "\r\n" +
                "<script src=\"test script\" type=\"text/javascript\"></script>\r\n" +
                "</head>\r\n" +
                "\r\n" +
                "<body " +
                "onfocus=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
                "onblur=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyBlurEventHandler();\">\r\n" +
                "<div id=\"Browser\" class=\"" + BrowserType[this.getRequest().getBrowserType()] + "\">\r\n" +
                "<div id=\"Language\" class=\"Cz\">\r\n" +
                "<div id=\"Content\" class=\"Content\" guiType=\"PageContent\">\r\n" +
                "this is new body\r\n" +
                "\r\n" +
                "</div>\r\n" +
                "</div>\r\n" +
                "</div>\r\n" +
                "</body>",
                StaticPageContentManager.ToString()), true);
        }

        public testscriptElemet() : void {
            Echo.Println("<script src=\"test script\" type=\"text/javascript\"></script>");
            const scriptElement : HTMLScriptElement = document.createElement("script");
            StaticPageContentManager.HeadScriptAppend(scriptElement, "text/javascript");
            this.assertEquals(StringUtils.PatternMatched("" +
                "<!DOCTYPE html>\r\n" +
                "\r\n" +
                "<head>\r\n" +
                "<title>Test title</title>\r\n" +
                "\r\n" +
                "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\r\n" +
                "<meta http-equiv=\"Author\" content=\"test meta\">\r\n" +
                "<meta>\r\n" +
                "\r\n" +
                "<link href=\"http://www.WuiFramework.com/favicon.ico\" rel=\"shortcut icon\" type=\"text/css\">\r\n" +
                "<link href=\"resource/css/com-wui-framework-gui-*.min.css\" rel=\"stylesheet\" type=\"text/css\">\r\n" +
                "<link rel=\"stylesheet\" href=\"appended link\" type=\"text/css\">\r\n" +
                "<link href=\"test link 2\" rel=\"stylesheet\" type=\"text/css\">\r\n" +
                "<link>\r\n" +
                "\r\n" +
                "<script src=\"test script\" type=\"text/javascript\"></script>\r\n" +
                "<script></script>\r\n" +
                "</head>\r\n" +
                "\r\n" +
                "<body " +
                "onfocus=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
                "onblur=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyBlurEventHandler();\">\r\n" +
                "<div id=\"Browser\" class=\"" + BrowserType[this.getRequest().getBrowserType()] + "\">\r\n" +
                "<div id=\"Language\" class=\"Cz\">\r\n" +
                "<div id=\"Content\" class=\"Content\" guiType=\"PageContent\">\r\nt" +
                "his is new body\r\n" +
                "\r\n" +
                "</div>\r\n" +
                "</div>\r\n" +
                "</div>\r\n" +
                "</body>",
                StaticPageContentManager.ToString()), true);
        }

        public testBodyAppend() : void {
            StaticPageContentManager.BodyAppend("bodyElement");
            this.assertEquals(StaticPageContentManager.getBody(), "this is new body\r\nbodyElement\r\n", "");
        }

        public testToString() : void {
            Echo.PrintCode(StaticPageContentManager.ToString());
        }

        public Process() : void {
            Echo.Println("print before resolver process");
            super.Process();
        }

        protected resolver() : void {
            Echo.Println("print before runtime test");
            super.resolver();
        }
    }
}
/* dev:end */
