/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Gui.RuntimeTests.Utils {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import RuntimeTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner;
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;

    class MockGuiCommons extends GuiCommons {
    }

    export class ScreenPositionTest extends RuntimeTestRunner {
        private element : GuiCommons;

        public testObject() : void {
            this.element.DisableAsynchronousDraw();
            this.element.StyleClassName("TestCss");

            Echo.Println("<style>" +
                ".TestCss {" +
                "position: relative; top: 250px; left: 50px; " +
                "width: 100px; height: 100px; padding: 20px; margin: 30px; " +
                "background-color: red; border: 1px solid black;}" +
                "</style>");
            Echo.Println(
                "<div style=\"position: relative; top: 100px; left: 100px; " +
                "width: 400px; height: 400px; padding: 20px; margin: 30px;" +
                "background-color: yellow; border: 1px solid black;" +
                "overflow: scroll;\">" +
                "<div style=\"position: absolute; top: 100px; left: 100px; " +
                "width: 300px; height: 300px; padding: 20px; margin: 30px;" +
                "background-color: green; border: 1px solid black;" +
                "overflow: scroll;\">" +
                this.element.Draw() +
                "</div>" +
                "</div>");
            Echo.Print("<div id=\"positionMarker\" " +
                "style=\"background: blue; width: 5px; height: 5px; position: fixed; top: 0; left: 0; z-index: 1000000;\"></div>");
            Echo.Println("<div style=\"position: relative; top: 2500px; left: 2500px; float: right;\">ScreenPadding</div>");
        }

        public testPosition() : void {
            this.addButton("getPosition", () : void => {
                const position : ElementOffset = this.element.getScreenPosition();
                Echo.Printf("X: {0}, Y: {1}", position.Left(), position.Top());
                ElementManager.setPosition("positionMarker", position);
            });
        }

        protected before() : void {
            this.element = new MockGuiCommons();
        }
    }
}
/* dev:end */
