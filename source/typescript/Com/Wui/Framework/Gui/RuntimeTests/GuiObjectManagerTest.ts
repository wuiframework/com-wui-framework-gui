/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Gui.RuntimeTests {
    "use strict";
    import RuntimeTestRunner = Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import Primitives = Com.Wui.Framework.Gui.Primitives;
    import OrientationType = Com.Wui.Framework.Gui.Enums.OrientationType;
    import IScrollBar = Com.Wui.Framework.Gui.Interfaces.Components.IScrollBar;

    class TestScrollBar extends Com.Wui.Framework.Gui.Primitives.GuiCommons implements IScrollBar {

        public static ClassName() : string {
            return "Com.Wui.Framework.Gui.RuntimeTests.TestScrollBar";
        }

        constructor($orientationType : OrientationType, $id? : string) {
            super();
        }

        public GuiType($orientationType? : OrientationType) : OrientationType {
            return undefined;
        }

        public OrientationType($orientationType? : OrientationType) : OrientationType {
            return undefined;
        }

        public Size($value? : number) : number {
            return -1;
        }

        public getEvents() : Com.Wui.Framework.Gui.Interfaces.Events.IScrollBarEvents {
            return undefined;
        }
    }

    class MockGuiCommons extends Primitives.GuiCommons {
    }

    class MockBaseGuiObject extends Primitives.BaseGuiObject {
    }

    class MockFormsObject extends Primitives.FormsObject {
    }

    export class GuiObjectManagerTest extends RuntimeTestRunner {

        public testAdd() : void {
            let passed : boolean = true;
            try {
                const manager : GuiObjectManager = new GuiObjectManager();
                manager.Add(new MockGuiCommons());
                manager.Add(new MockBaseGuiObject());
                manager.Add(new MockFormsObject());
                manager.Add(new Primitives.AbstractGuiObject());
            } catch (ex) {
                Echo.Println(ex);
                LogIt.Error("runtime test - GuiObjectManager.Add", ex);
                passed = false;
            }
            this.assertEquals(passed, true, "all element types can be added");
        }

        public testExists() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            const element : Primitives.AbstractGuiObject = new Primitives.AbstractGuiObject();
            manager.Add(element);
            const unregisterElement : Primitives.AbstractGuiObject = new Primitives.AbstractGuiObject();
            this.assertEquals(manager.Exists(element), true, "validate that element Id exist");
            this.assertEquals(manager.Exists(unregisterElement), false, "validate that element does not exist");
        }

        public testGetType() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            const element : Primitives.AbstractGuiObject = new Primitives.AbstractGuiObject();
            manager.Add(element);
            this.assertEquals(manager.getType(Primitives.AbstractGuiObject).IsEmpty(), false,
                "validate that element can be get by type");
            this.assertEquals(manager.getType(TestScrollBar).IsEmpty(), true,
                "validate that not registered element type can not be get by type");
            manager.Add(element);
            this.assertEquals(manager.getType(Primitives.AbstractGuiObject).Length(), 1,
                "validate that element can not be duplicated");
            let undefinedElement : Primitives.GuiCommons; // tslint:disable-line
            this.assertEquals(ObjectValidator.IsEmptyOrNull(manager.getType(undefinedElement)), true,
                "try to get undefined element");
        }

        public testGetAll() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            manager.Add(new Primitives.AbstractGuiObject());
            manager.Add(new MockGuiCommons());
            this.assertEquals(manager.getAll().Length(), 2, "validate that all elements can be get");
        }

        public testSetActive() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            const element : Primitives.AbstractGuiObject = new Primitives.AbstractGuiObject();
            const unregisteredElement : Primitives.AbstractGuiObject = new Primitives.AbstractGuiObject();
            let passed : boolean = true;
            try {
                manager.Add(element);
                manager.setActive(element, true);
                manager.setActive(element, false);
                manager.setActive(unregisteredElement, true);
                manager.setActive(unregisteredElement, false);
            } catch (ex) {
                Echo.Println(ex);
                LogIt.Error("runtime test - GuiObjectManager.setActive", ex);
                passed = false;
            }
            this.assertEquals(passed, true, "validate element's activation handling");
        }

        public testIsActive() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            const element : Primitives.AbstractGuiObject = new Primitives.AbstractGuiObject();
            const unregisteredElement : Primitives.AbstractGuiObject = new Primitives.AbstractGuiObject();
            let undefinedElement : Primitives.GuiCommons; // tslint:disable-line
            manager.Add(element);

            manager.setActive(element, true);
            this.assertEquals(manager.IsActive(element), true,
                "validate that element id is active");
            manager.setActive(element, false);
            this.assertEquals(manager.IsActive(element), false,
                "validate that element id is not active");
            manager.setActive(undefinedElement, true);
            this.assertEquals(manager.IsActive(undefinedElement), false,
                "validate that undefined element id can not be active");
            manager.setActive(unregisteredElement, true);
            this.assertEquals(manager.IsActive(unregisteredElement), true,
                "validate that unregistered element can be active");
            manager.setActive(unregisteredElement, false);
            this.assertEquals(manager.IsActive(unregisteredElement), false,
                "validate that unregistered element id is not active");
            manager.setActive(element, true);
            this.assertEquals(manager.IsActive(<ClassName>Primitives.AbstractGuiObject), true,
                "validate that element type is active");
            this.assertEquals(manager.IsActive(<ClassName>Primitives.FormsObject), false,
                "validate that element type is not active");
        }

        public testGetActive() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            const element : Primitives.AbstractGuiObject = new Primitives.AbstractGuiObject();
            manager.Add(element);

            manager.setActive(element, true);
            this.assertEquals(manager.getActive(<ClassName>Primitives.AbstractGuiObject).IsEmpty(), false,
                "validate that element type is active");
            manager.setActive(element, false);
            this.assertEquals(manager.getActive(<ClassName>Primitives.AbstractGuiObject).IsEmpty(), true,
                "validate that element type is not active");
            this.assertEquals(manager.getActive(<ClassName>Primitives.FormsObject).IsEmpty(), true,
                "validate that not registered element type is not active");
        }

        public testClear() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            const element : Primitives.AbstractGuiObject = new Primitives.AbstractGuiObject();
            manager.Add(element);
            manager.setActive(element, true);
            Echo.Println("<i>before clear</i>");
            Echo.Println(manager.ToString());
            this.assertEquals(manager.Exists(element), true, "validate that element exists");
            Echo.Println("<i>after clear</i>");
            manager.Clear();
            Echo.Println(manager.ToString());
            this.assertEquals(manager.Exists(element), false, "validate that element does not exists");
        }

        public GlobalManagerTest() : void {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            Echo.Println("<i>before registration</i>");
            Echo.Println(manager.ToString());
            const element : Primitives.FormsObject = new MockFormsObject();
            manager.Add(new MockGuiCommons());
            manager.Add(new MockBaseGuiObject());
            manager.Add(element);
            Echo.Println("<i>after registration</i>");
            Echo.Println(manager.ToString());
            manager.setActive(element, true);
            Echo.Println("<i>after activation</i>");
            Echo.Println(manager.ToString());

            this.assertEquals(manager.Exists(element), true, "validate existence of element");
        }
    }
}
/* dev:end */
