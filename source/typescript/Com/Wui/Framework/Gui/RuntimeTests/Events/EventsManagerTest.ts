/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Gui.RuntimeTests.Events {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import GeneralEventOwner = Com.Wui.Framework.Gui.Enums.Events.GeneralEventOwner;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import KeyEventHandler = Com.Wui.Framework.Gui.Utils.KeyEventHandler;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import MoveEventArgs = Com.Wui.Framework.Gui.Events.Args.MoveEventArgs;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;

    export class EventsManagerTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        public IntegralTest() : void {
            this.getEventsManager().setEvent(GeneralEventOwner.WINDOW, EventType.ON_LOAD,
                () : void => {
                    Echo.Println("Window has been loaded");
                });
            this.getEventsManager().setEvent(GeneralEventOwner.BODY, EventType.ON_LOAD,
                () : void => {
                    Echo.Println("Body has been loaded");
                });

            let undloadFired : boolean = false;
            this.getEventsManager().setEvent(GeneralEventOwner.WINDOW, EventType.BEFORE_REFRESH,
                ($args : EventArgs) : void => {
                    if (!undloadFired) {
                        undloadFired = true;
                        $args.PreventDefault();
                        Echo.Println("Window is undloading" + StringUtils.NewLine());
                    }
                });

            this.getEventsManager().setEvent(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST,
                () : void => {
                    Echo.Println("Request has been changed");
                });

            this.getEventsManager().setEvent(GeneralEventOwner.WINDOW, EventType.ON_RESIZE,
                () : void => {
                    Echo.Println("Window has been resized");
                });

            this.getEventsManager().setEvent(GeneralEventOwner.BODY, EventType.ON_CLICK,
                () : void => {
                    Echo.Println("Body has been clicked");
                });

            this.getEventsManager().setEvent(GeneralEventOwner.BODY, EventType.ON_KEY_DOWN,
                ($args : KeyEventArgs) : void => {
                    Echo.Println("Body key down - " + $args.getKeyCode() + " (" + $args.NativeEventArgs().key + ")");
                });
            this.getEventsManager().setEvent(GeneralEventOwner.BODY, EventType.ON_KEY_PRESS,
                ($args : KeyEventArgs) : void => {
                    Echo.Println("Body key press - " + $args.getKeyCode() + " (" + $args.NativeEventArgs().key + ")");

                    Echo.Println("Is navigation key: ");
                    Echo.Printf(KeyEventHandler.IsNavigate($args.NativeEventArgs()));

                    Echo.Println("Is integer or navigate: ");
                    Echo.Printf(KeyEventHandler.IsInteger($args.NativeEventArgs()));
                });
            this.getEventsManager().setEvent(GeneralEventOwner.BODY, EventType.ON_KEY_UP,
                ($args : KeyEventArgs) : void => {
                    Echo.Println("Body key up - " + $args.getKeyCode() + " (" + $args.NativeEventArgs().key + ")");
                });

            this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE,
                ($args : MoveEventArgs) : void => {
                    Echo.Println("distance X: " + $args.getDistanceX());
                    Echo.Println("distance Y: " + $args.getDistanceY());
                });
        }
    }
}
/* dev:end */
