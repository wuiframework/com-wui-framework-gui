/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Gui.RuntimeTests.Primitives {
    "use strict";
    import AbstractGuiObject = Com.Wui.Framework.Gui.Primitives.AbstractGuiObject;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class AbstractGuiObjectTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        public testConstructor() : void {
            const guiobject : AbstractGuiObject = new AbstractGuiObject("id_333");
            this.assertEquals(guiobject.Id(), "id_333", "validate Id string");
        }

        public testNotification() : void {
            const guiobject : AbstractGuiObject = new AbstractGuiObject("id_333");
            this.assertEquals(guiobject.Notification(), (<any>AbstractGuiObject).notification);
        }

        public testsetSize() : void {
            const guiobject : AbstractGuiObject = new AbstractGuiObject("id_333");
            guiobject.setSize(30, 60);
            this.assertEquals(guiobject.getSize().Height(60), 60);
            this.assertEquals(guiobject.getSize().Width(30), 30);
        }

        public testStyleAttributes() : void {
            const guiobject : AbstractGuiObject = new AbstractGuiObject("id2");
            guiobject.DisableAsynchronousDraw();
            guiobject.StyleAttributes("border-radius: 2px");
            this.assertEquals(guiobject.StyleAttributes(), "border-radius: 2px", "validate attributes");
            Echo.PrintCode(guiobject.Draw());
            Echo.Println("<div style=\"width: 60px; height: 60px; border-radius: 2px;" +
                " position: relative;\">" + guiobject.Draw() + "</div>");
        }

        public testAppendStyleAttributes() : void {
            const guiobject : AbstractGuiObject = new AbstractGuiObject("abstractGuiId");
            guiobject.DisableAsynchronousDraw();
            guiobject.AppendStyleAttributes("background-color: green");
            this.assertEquals(guiobject.Draw(),
                "\r\n<div class=\"ComWuiFrameworkGuiPrimitives\">" +
                "\r\n   <div id=\"abstractGuiId_GuiWrapper\" guiType=\"GuiWrapper\">" +
                "\r\n      <div id=\"abstractGuiId\" class=\"AbstractGuiObject\" style=\"display: block;\">" +
                "\r\n         <div style=\"background-color: green; border: 1px solid red; color: red; height: 20px; overflow-x: hidden;" +
                " overflow-y: hidden; width: 100px;\">abstractGuiId</div>" +
                "\r\n      </div>" +
                "\r\n   </div>" +
                "\r\n</div>");
        }
    }
}
/* dev:end */
