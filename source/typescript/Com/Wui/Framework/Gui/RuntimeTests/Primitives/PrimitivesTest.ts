/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Gui.RuntimeTests.Primitives {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import Primitives = Com.Wui.Framework.Gui.Primitives;
    import PanelContentType = Com.Wui.Framework.Gui.Enums.PanelContentType;

    class MockGuiCommons extends Primitives.GuiCommons {
    }

    class MockBaseGuiObject extends Primitives.BaseGuiObject {
    }

    class MockFormsObject extends Primitives.FormsObject {
    }

    export class PrimitivesTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        public testGuiCommons() : void {
            const element : Primitives.GuiCommons = new MockGuiCommons();
            this.assertEquals(ObjectValidator.IsEmptyOrNull(element.StyleClassName()), true,
                "validate that element does not have wrapper class name");
            element.StyleClassName("testClassName");
            this.assertEquals(ObjectValidator.IsEmptyOrNull(element.Parent()), true,
                "validate that element does not have parent");
            this.assertEquals(element.StyleClassName(), "testClassName", "validate wrapper class name");
            this.assertEquals(element.Visible(), true, "validate that element is visible");
            this.assertEquals(element.Enabled(), true, "validate that element is enabled");

            Echo.Println("generated id: " + element.Id());
            Echo.PrintCode(element.Draw());
            Echo.Println("<i>After set element properties</i>");
            element.Visible(false);
            element.Enabled(false);
            this.assertEquals(element.Visible(), false,
                "validate that element is not visible");
            this.assertEquals(element.Enabled(), false,
                "validate that element is not enabled");
            this.assertEquals(element.getChildElements().IsEmpty(), true,
                "validate that element does not have any child elements");
            Echo.PrintCode(element.Draw());
            const element2 : Primitives.GuiCommons = new MockGuiCommons("testElementId");
            this.assertEquals(element2.Id(), "testElementId", "validate force set of element id");
        }

        public testBaseGuiObject() : void {
            const element : Primitives.BaseGuiObject = new MockBaseGuiObject();
            Echo.PrintCode(element.Draw());
            element.getEvents().setEvent(EventType.ON_CLICK, () : void => {
                Echo.Println("element has been clicked");
            });
            Echo.Println(element.getEvents().ToString());

            this.assertEquals(element.Changed(), false,
                "validate that element has not been interacted by user");
            this.assertEquals(ObjectValidator.IsEmptyOrNull(element.Value()), true,
                "validate that element value is null");
        }

        public testFormsObject() : void {
            const element : Primitives.FormsObject = new MockFormsObject();
            Echo.PrintCode(element.Draw());
            this.assertEquals(element.Error(), false,
                "validate that element is not in error state");
            element.Error(true);
            this.assertEquals(element.Error(), true,
                "validate that element is in error state");
            this.assertEquals(ObjectValidator.IsEmptyOrNull(element.TabIndex()), true,
                "validate that element does not have specified tabindex");
            element.TabIndex(1);
            this.assertEquals(element.TabIndex(), 1,
                "validate that element tab index has been specified");

            Echo.Printf(GuiObjectManager.getInstanceSingleton());
            this.assertEquals(Primitives.FormsObject.CollectValues().IsEmpty(), true,
                "validate that any of form element has not been interacted user");
            this.assertEquals(Primitives.FormsObject.CollectValues(element.Id()).IsEmpty(), true,
                "validate that specific element has not been interacted by user");
        }

        public testAbstractGuiObject() : void {
            const element : Primitives.AbstractGuiObject = new Primitives.AbstractGuiObject();
            element.DisableAsynchronousDraw();
            Echo.PrintCode(element.Draw());
            element.setSize(200, 200);
            element.StyleClassName("TestClassName");
            element.StyleAttributes("position: absolute;");
            element.AppendStyleAttributes("left: 100px;");
            Echo.Println("<i>After set element properties</i>");
            Echo.PrintCode(element.Draw());
            Echo.Println("<div style=\"width: 200px; height: 200px; position: relative;\">" + element.Draw() + "</div>");
        }

        public testBasePanel() : void {
            const element : Primitives.BasePanel = new Primitives.BasePanel();
            element.ContentType(PanelContentType.WITH_ELEMENT_WRAPPER);
            Echo.PrintCode(element.Draw());
            element.ContentType(PanelContentType.WITHOUT_ELEMENT_WRAPPER);
            Echo.Println("<i>Panel with out element wrapper</i>");
            Echo.PrintCode(element.Draw());
            element.ContentType(PanelContentType.HIDDEN);
            Echo.Println("<i>Hidden Panel</i>");
            Echo.PrintCode(element.Draw());
            Echo.Println("<div style=\"clear: both;\">" + element.Draw() + "</div>");
            element.ContentType(PanelContentType.ASYNC_LOADER);
            Echo.Println("<i>Panel with async loading</i>");
            Echo.PrintCode(element.Draw());
            Echo.Println("<div style=\"clear: both;\">" + element.Draw() + "</div>");

            this.assertEquals(element.getChildPanelList().IsEmpty(), true, "validate that panel does not contains any child");
            this.assertEquals(element.Scrollable(), false, "validate that panel content can not be scrolled");
            element.Scrollable(true);
            this.assertEquals(element.Scrollable(), true, "validate that panel content can be scrolled");
            element.Width(200);
            this.assertEquals(element.Width(), 200, "validate that panel width");
            element.Height(200);
            this.assertEquals(element.Height(), 200, "validate that panel height");
        }
    }
}
/* dev:end */
