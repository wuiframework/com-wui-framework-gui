/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Gui.RuntimeTests.Primitives {
    "use strict";
    import BasePanel = Com.Wui.Framework.Gui.Primitives.BasePanel;
    import PanelContentType = Com.Wui.Framework.Gui.Enums.PanelContentType;
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export class BasePanelTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        public testgetEvents() : void {
            const basepanel : BasePanel = new BasePanel();
            const handler : any = () : void => {
                // test event handler
            };
            basepanel.getEvents().setEvent("test", handler);
            this.assertEquals(basepanel.getEvents().Exists("test"), true);
        }

        public testContentType() : void {
            const basepanel : BasePanel = new BasePanel();
            Echo.PrintCode(JSON.stringify(basepanel));
            this.assertEquals(basepanel.ContentType(PanelContentType.WITH_ELEMENT_WRAPPER),
                PanelContentType.WITH_ELEMENT_WRAPPER);
            Echo.Println("<i>Panel with out element wrapper</i>");
        }

        public __IgnoretestAddChild() : void {
            const basepanel : BasePanel = new BasePanel();
            const basepanel2 : BasePanel = new BasePanel();
            const basepanel3 : BasePanel = new BasePanel();
            basepanel.AddChild(basepanel2, "test");
            basepanel.AddChild(basepanel3, "test2");
            this.assertEquals(basepanel.getChildElements(), new ArrayList());
        }

        public testValue() : void {
            const basepanel : BasePanel = new BasePanel();
            const viewerargs : BasePanelViewerArgs = new BasePanelViewerArgs();
            this.assertEquals(basepanel.Value("viewerargs"), null);
        }

        public testScrollable() : void {
            const basepanel : BasePanel = new BasePanel();
            this.assertEquals(basepanel.Scrollable(true), true);
        }

        public testWidth() : void {
            const basepanel : BasePanel = new BasePanel();
            this.assertEquals(basepanel.Width(8), 8);
            Echo.PrintCode(JSON.stringify(basepanel.Draw()));
        }

        public testHeight() : void {
            const basepanel : BasePanel = new BasePanel();
            basepanel.Height(16);
            this.assertEquals(basepanel.Height(), 16);
        }

        public testGetScrollTop() : void {
            const basepanel : BasePanel = new BasePanel();
            this.assertEquals(basepanel.getScrollTop(), -1);
        }

        public testGetScrollLeft() : void {
            const basepanel : BasePanel = new BasePanel();
            this.assertEquals(basepanel.getScrollLeft(), -1);
        }

        public testDraw() : void {
            const basepanel : BasePanel = new BasePanel("basePanelId");
            basepanel.DisableAsynchronousDraw();
            Echo.Printf(JSON.stringify(basepanel.Draw()));
            this.assertEquals(basepanel.Draw(),
                "\r\n<div id=\"basePanelId_PanelEnvelop\" class=\"Panel\" style=\"display: block;\">" +
                "\r\n    <div class=\"ComWuiFrameworkGuiPrimitives\">" +
                "\r\n       <div id=\"basePanelId_GuiWrapper\" guiType=\"GuiWrapper\">" +
                "\r\n          <div id=\"basePanelId\" class=\"BasePanel\" style=\"display: block;\">" +
                "\r\n             <div guiType=\"Panel\">" +
                "\r\n                <div id=\"basePanelId_PanelContentEnvelop\" class=\"Envelop\" style=\"display: none;\">" +
                "\r\n                   <div id=\"basePanelId_PanelContent\" class=\"Content\">" +
                "\r\n                      <div style=\"clear: both;\"></div>" +
                "\r\n                   </div>" +
                "\r\n                </div>" +
                "\r\n             </div>" +
                "\r\n          </div>" +
                "\r\n       </div>" +
                "\r\n    </div>" +
                "\r\n</div>");
        }
    }
}
/* dev:end */
