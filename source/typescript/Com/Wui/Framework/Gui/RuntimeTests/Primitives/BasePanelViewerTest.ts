/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Gui.RuntimeTests.Primitives {
    "use strict";
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import BasePanel = Com.Wui.Framework.Gui.Primitives.BasePanel;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class BasePanelViewerTest extends Com.Wui.Framework.Gui.Primitives.BasePanelViewer {

        constructor($args? : BasePanelViewerArgs) {
            super($args);
            this.setInstance(new BasePanel());
        }

        public getInstance() : BasePanel {
            return <BasePanel>super.getInstance();
        }

        protected normalImplementation() : void {
            const args : BasePanelViewerArgs = this.ViewerArgs();
            if (!ObjectValidator.IsEmptyOrNull(args) && !ObjectValidator.IsEmptyOrNull(this.getInstance())) {
                // handle viewer args
            }
        }

        protected testImplementation() : string {
            const args : BasePanelViewerArgs = new BasePanelViewerArgs();
            args.Visible(true);
            args.AsyncEnabled(false);
            this.ViewerArgs(args);
            this.normalImplementation();

            return "test append";
        }
    }
}
/* dev:end */
