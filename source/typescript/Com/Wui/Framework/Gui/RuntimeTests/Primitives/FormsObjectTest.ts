/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Gui.RuntimeTests.Primitives {
    "use strict";
    import FormsObject = Com.Wui.Framework.Gui.Primitives.FormsObject;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    class MockFormsObject extends FormsObject {
    }

    export class FormsObjectTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        public testgetEvents() : void {
            const formsobject : FormsObject = new MockFormsObject();
            const handler : any = () : void => {
                // test event handler
            };
            formsobject.getEvents().setEvent("test", handler);
            this.assertEquals(formsobject.getEvents().Exists("test"), true);
        }

        public testNotification() : void {
            const formsobject : FormsObject = new MockFormsObject();
            this.assertEquals(formsobject.Notification(), (<any>FormsObject).notification);
        }

        public testEnabled() : void {
            const formsobject : FormsObject = new MockFormsObject();
            this.assertEquals(formsobject.Enabled(), true, "validate enabled");
            const formsobject2 : FormsObject = new MockFormsObject();
            this.assertEquals(formsobject2.Enabled(false), false);
        }

        public testError() : void {
            const formsobject : FormsObject = new MockFormsObject();
            this.assertEquals(formsobject.Error(true), true);
            const formsobject2 : FormsObject = new MockFormsObject();
            this.assertEquals(formsobject2.Error(false), false);
        }

        public testTabIndex() : void {
            const formsobject : FormsObject = new MockFormsObject();
            this.assertEquals(formsobject.TabIndex(5), 5);
        }

        public testgetSelectorEvents() : void {
            const formsobject : FormsObject = new MockFormsObject();
            Echo.PrintCode(JSON.stringify(formsobject));
            const handler : any = () : void => {
                // test event handler
            };
            formsobject.getEvents().setEvent("test", handler);
            this.assertEquals(formsobject.getEvents().Exists("test"), true);
        }
    }
}
/* dev:end */
