/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Gui.RuntimeTests.Primitives {
    "use strict";
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;

    export class AsyncBasePanelViewerTest extends BasePanelViewerTest {

        protected normalImplementation() : void {
            if (!this.getInstance().IsLoaded()) {
                const args : BasePanelViewerArgs = new BasePanelViewerArgs();
                args.Visible(true);
                args.AsyncEnabled(true);
                this.ViewerArgs(args);
            }
            super.normalImplementation();
        }

        protected testImplementation() : string {
            this.normalImplementation();
            return "async test append";
        }
    }
}
/* dev:end */
