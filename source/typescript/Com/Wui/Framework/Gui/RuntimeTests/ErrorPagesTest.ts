/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Gui.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import FatalError = Com.Wui.Framework.Commons.Exceptions.Type.FatalError;

    export class ErrorPagesTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        public testExceptionsManager() : void {
            this.addButton("native exception", () : void => {
                throw new Error("native error");
            });
            this.addButton("custom exception", () : void => {
                ExceptionsManager.Throw(ErrorPagesTest.ClassName(), "my error is here");
            });
            this.addButton("fatal error", () : void => {
                ExceptionsManager.Throw(ErrorPagesTest.ClassName(), new FatalError("my fatal error is here"));
            });
        }

        public testHttpManager() : void {
            this.addButton("http 301", () : void => {
                this.getHttpManager().Return301Moved("/testFile/location");
            });
            this.addButton("http 403", () : void => {
                this.getHttpManager().Return403Forbidden();
            });
            this.addButton("http 404", () : void => {
                this.getHttpManager().Return404NotFound();
            });
        }
    }
}
/* dev:end */
