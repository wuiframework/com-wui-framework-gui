/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Gui.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import FormsObject = Com.Wui.Framework.Gui.Primitives.FormsObject;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import GeneralEventOwner = Com.Wui.Framework.Commons.Enums.Events.GeneralEventOwner;

    export class PersistenceManagerTest extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver {
        protected resolver() : void {
            this.getEventsManager().setEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_SUCCESS,
                () : void => {
                    Echo.Println("<b>Collected data:</b>");
                    Echo.Printf(FormsObject.CollectValues());
                });
            this.getHttpManager().ReloadTo("/PersistenceManager", null, true);
        }
    }
}
/* dev:end */
