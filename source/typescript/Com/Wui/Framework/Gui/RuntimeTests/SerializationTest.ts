/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Gui.RuntimeTests {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import Primitives = Com.Wui.Framework.Gui.Primitives;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import BasePanelViewerTest = Com.Wui.Framework.Gui.RuntimeTests.Primitives.BasePanelViewerTest;

    class MockGuiCommons extends Primitives.GuiCommons {
    }

    class MockBaseGuiObject extends Primitives.BaseGuiObject {
    }

    class MockFormsObject extends Primitives.FormsObject {
    }

    export class SerializationTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        public testGuiCommons() : void {
            const serialized : string = ObjectEncoder.Serialize(new MockGuiCommons());
            Echo.Println(StringUtils.HardWrap(serialized, 150));
            Echo.PrintCode(ObjectDecoder.Unserialize(serialized).Draw());
        }

        public testBaseGuiObject() : void {
            const serialized : string = ObjectEncoder.Serialize(new MockBaseGuiObject());
            Echo.Println(StringUtils.HardWrap(serialized, 150));
            Echo.PrintCode(ObjectDecoder.Unserialize(serialized).Draw());
        }

        public testAbstractGuiObject() : void {
            const object : Primitives.AbstractGuiObject = new Primitives.AbstractGuiObject();
            object.getEvents().setEvent(EventType.ON_CLICK, () : void => {
                Echo.Println("deserialized onlick");
            });
            const serialized : string = ObjectEncoder.Serialize(object);
            Echo.Println(StringUtils.HardWrap(serialized, 150));
            const deserialized : Primitives.AbstractGuiObject = ObjectDecoder.Unserialize(serialized);
            deserialized.getEvents().setEvent(EventType.ON_MOUSE_OVER, ($args : EventArgs) : void => {
                ElementManager.setCssProperty($args.Owner(), "border", "1px solid green");
            });
            deserialized.getEvents().setEvent(EventType.ON_CLICK, () : void => {
                Echo.Println("not desirialized onlick");
            });
            deserialized.getEvents().setEvent(EventType.ON_MOUSE_OUT, ($args : EventArgs) : void => {
                ElementManager.setCssProperty($args.Owner(), "border", "0px solid red");
            });

            Echo.PrintCode(deserialized.Draw());
            Echo.Println(deserialized.Draw());
        }

        public testFormsObject() : void {
            const serialized : string = ObjectEncoder.Serialize(new MockFormsObject());
            Echo.Println(StringUtils.HardWrap(serialized, 150));
            Echo.PrintCode(ObjectDecoder.Unserialize(serialized).Draw());
        }

        public testBasePanel() : void {
            const serialized : string = ObjectEncoder.Serialize(new Primitives.BasePanel());
            Echo.Println(StringUtils.HardWrap(serialized, 150));
            Echo.PrintCode(ObjectDecoder.Unserialize(serialized).Draw());
        }

        public testBasePanelViewer() : void {
            const serialized : string = ObjectEncoder.Serialize(new BasePanelViewerTest());
            Echo.Println(StringUtils.HardWrap(serialized, 150));
            Echo.PrintCode(ObjectDecoder.Unserialize(serialized).Show());
        }

        public testCrcCheck() : void {
            const testArray : ArrayList<any> = new ArrayList<any>();
            testArray.Add(null, "crc");
            testArray.Add(new ArrayList<any>(), "variables");
            testArray.getItem("variables").Add("string");
            testArray.getItem("variables").Add(true);
            testArray.getItem("variables").Add(123);
            testArray.getItem("variables").Add(new MockBaseGuiObject());
            testArray.getItem("variables").Add(($arg : string) : string => {
                return $arg;
            });
            testArray.Add(StringUtils.getCrc(ObjectEncoder.Serialize(testArray.getItem("variables"))), "crc");

            Echo.Println("<i>Serialized object</i>");
            let serialized : string = ObjectEncoder.Serialize(testArray);
            Echo.Println(StringUtils.HardWrap(serialized, 150) + StringUtils.NewLine());

            Echo.Println("<i>After deserialization</i>");
            const deserialized : ArrayList<any> = ObjectDecoder.Unserialize(serialized);
            Echo.Printf(deserialized);
            serialized = ObjectEncoder.Serialize(deserialized.getItem("variables"));

            Echo.Println("<i>Serialized variables after deserialization</i>");
            Echo.Println(StringUtils.NewLine() + StringUtils.HardWrap(serialized, 150));
            this.assertEquals(deserialized.getItem("crc"), StringUtils.getCrc(serialized));
        }

        public testAsyncSerialization() : void {
            const testArray : ArrayList<any> = new ArrayList<any>();
            testArray.Add(null, "crc");
            testArray.Add(new ArrayList<any>(), "variables");
            testArray.getItem("variables").Add("string");
            testArray.getItem("variables").Add(true);
            testArray.getItem("variables").Add(123);
            testArray.getItem("variables").Add(new MockBaseGuiObject());
            testArray.getItem("variables").Add(($arg : string) : string => {
                return $arg;
            });
            testArray.Add(StringUtils.getCrc(ObjectEncoder.Serialize(testArray.getItem("variables"))), "crc");

            let testStart : number = new Date().getTime();
            ObjectEncoder.Serialize(testArray, ($data : string) : void => {
                Echo.Printf(ObjectDecoder.Unserialize($data));
                Echo.Printf("process time: " + (new Date().getTime() - testStart) / 1000);

                testStart = new Date().getTime();
                ObjectDecoder.Unserialize($data, ($output : any) : void => {
                    Echo.Printf("process time: " + (new Date().getTime() - testStart) / 1000);
                    Echo.Printf($output);
                });
            });
        }
    }
}
/* dev:end */
