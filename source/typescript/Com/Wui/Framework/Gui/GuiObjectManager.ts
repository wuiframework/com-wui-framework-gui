/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiObjectsMap = Com.Wui.Framework.Gui.Structures.GuiObjectsMap;

    /**
     * GuiObjectManager class provides register, filtering and handling of all GUI objects.
     */
    export class GuiObjectManager extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private map : GuiObjectsMap;
        private hoveredElement : IGuiCommons;
        private activeObjectsMap : GuiObjectsMap;

        /**
         * @return {GuiObjectManager} Returns singleton instance of GuiObjectManager class,
         * which should be used as global GUI objects register.
         */
        public static getInstanceSingleton() : GuiObjectManager {
            return Loader.getInstance().getHttpResolver().getGuiRegister();
        }

        constructor() {
            super();
            this.map = new GuiObjectsMap();
            this.hoveredElement = null;
            this.activeObjectsMap = new GuiObjectsMap();
        }

        /**
         * @param {IGuiCommons|ClassName} $value Element object or class name, which should be validated.
         * @return {boolean} Returns true, if element object or group of elements with desired class name has been registered,
         * otherwise false.
         */
        public Exists($value : IGuiCommons | ClassName) : boolean {
            if (ObjectValidator.IsEmptyOrNull($value)) {
                return false;
            }
            if (ObjectValidator.IsSet((<any>$value).ClassName)) {
                return !ObjectValidator.IsEmptyOrNull(this.map.getType($value));
            }
            return this.map.Exists(<IGuiCommons>$value);
        }

        /**
         * Add element to the GUI objects register, if element is not already in register.
         * @param {IGuiCommons} [$objectOrMap] Element, which should be registered.
         * @return {void}
         */
        public Add($objectOrMap : IGuiCommons) : void {
            this.map.Add($objectOrMap);
        }

        /**
         * @param {IGuiCommons} $element Element, which should be handled.
         * @param {boolean} $status Specify element's focus or activity status.
         * @return {void}
         */
        public setActive($element : IGuiCommons, $status : boolean) : void {
            if (!ObjectValidator.IsEmptyOrNull($element)) {
                if ($status) {
                    this.activeObjectsMap.Add($element);
                } else {
                    this.activeObjectsMap.Clear($element);
                }
            }
        }

        /**
         * @param {IGuiCommons} $element Element, which should be handled.
         * @param {boolean} $status Specify element's on mouse over status.
         * @return {void}
         */
        public setHovered($element : IGuiCommons, $status : boolean) : void {
            if ($status) {
                if (!ObjectValidator.IsEmptyOrNull($element) && this.Exists($element)) {
                    this.hoveredElement = $element;
                }
            } else if (ObjectValidator.IsEmptyOrNull(this.hoveredElement) || this.hoveredElement === $element) {
                this.hoveredElement = null;
            }
        }

        /**
         * @param {IGuiCommons|ClassName} $className Element class name, which should be searched.
         * @return {ArrayList<IGuiCommons>} Returns array list of elements with desired element type.
         */
        public getType($className : IGuiCommons | ClassName) : ArrayList<IGuiCommons> {
            return this.map.getType($className);
        }

        /**
         * @param {IGuiCommons|ClassName} [$type] Element type, which should be searched.
         * @return {ArrayList<IGuiCommons>} Returns array list of all active elements with desired element type,
         * if type has been specified, otherwise returns array list of all active elements.
         */
        public getActive($type? : IGuiCommons | ClassName) : ArrayList<IGuiCommons> {
            if (!ObjectValidator.IsSet($type)) {
                return this.activeObjectsMap.getAll();
            }
            return this.activeObjectsMap.getType($type);
        }

        /**
         * @return {IGuiCommons} Returns element instance, if some of element has been hovered by the mouse, otherwise null.
         */
        public getHovered() : IGuiCommons {
            return this.hoveredElement;
        }

        /**
         * @param {IGuiCommons|ClassName} $elementOrType Element or class name, which should be validated.
         * @return {boolean} Returns true, if desired element or group of elements with desired class
         * is hovered, otherwise false.
         */
        public IsHovered($elementOrType : IGuiCommons | ClassName) : boolean {
            if (ObjectValidator.IsSet($elementOrType)) {
                if (ObjectValidator.IsEmptyOrNull(this.getHovered()) || ObjectValidator.IsEmptyOrNull($elementOrType)) {
                    return false;
                }
                if (ObjectValidator.IsSet((<any>$elementOrType).ClassName)) {
                    return this.getHovered().IsMemberOf($elementOrType);
                }
                return this.getHovered() === $elementOrType;
            }
            return false;
        }

        /**
         * @return {ArrayList<IGuiCommons>} Returns array list of all registered GUI objects.
         */
        public getAll() : ArrayList<IGuiCommons> {
            return this.map.getAll();
        }

        /**
         * @param {IGuiCommons|ClassName} $elementOrType Element or class name, which should be validated.
         * @return {boolean} Returns true, if desired element or group of elements with desired class
         * is focused or in active state, otherwise false.
         */
        public IsActive($elementOrType : IGuiCommons | ClassName) : boolean {
            if (ObjectValidator.IsEmptyOrNull($elementOrType)) {
                return false;
            }
            if (ObjectValidator.IsSet((<any>$elementOrType).ClassName)) {
                return !ObjectValidator.IsEmptyOrNull(this.activeObjectsMap.getType($elementOrType));
            }
            return this.activeObjectsMap.Exists(<IGuiCommons>$elementOrType);
        }

        /**
         * Clean up all registered elements, if $element has not been specified.
         * Clean up element from the registered GUI objects, if $element has been specified and is registered.
         * @param {IGuiCommons} [$element] Element, which should be removed.
         * @return {void}
         */
        public Clear($element? : IGuiCommons) : void {
            if (ObjectValidator.IsSet($element)) {
                if (this.Exists($element)) {
                    this.setHovered($element, false);
                    this.activeObjectsMap.Clear($element);
                    if (this.Exists($element.Parent())) {
                        const parent : IGuiCommons = $element.Parent();
                        parent.getChildElements().RemoveAt(parent.getChildElements().IndexOf($element));
                    }
                    this.map.Clear($element);
                }
            } else {
                this.hoveredElement = null;
                this.activeObjectsMap.Clear();
                this.map.Clear();
            }
        }

        public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
            let output : string = "All elements:";
            if ($htmlTag) {
                output = "<b>" + output + "</b>";
            }
            output = $prefix + output + StringUtils.NewLine($htmlTag);

            if (this.map.IsEmpty()) {
                output += $prefix + "none of GUI element has been registered yet" + StringUtils.NewLine($htmlTag);
            } else {
                output += this.map.ToString($prefix, $htmlTag);
            }
            let active : string = "Active elements:";
            if ($htmlTag) {
                active = "<b>" + active + "</b>";
            }
            output += $prefix + active + StringUtils.NewLine($htmlTag);
            if (this.activeObjectsMap.IsEmpty()) {
                output += $prefix + "no active elements has been found" + StringUtils.NewLine($htmlTag);
            } else {
                output += this.activeObjectsMap.ToString($prefix, $htmlTag);
            }
            return output;
        }
    }
}
