/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * GuiOptionsManager class provides register of all allowed GUI objects options.
     */
    export class GuiOptionsManager extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private optionsList : ArrayList<Enums.GuiOptionType>;
        private readonly availableOptionsList : ArrayList<Enums.GuiOptionType>;

        constructor($availableList : ArrayList<Enums.GuiOptionType>) {
            super();
            this.optionsList = new ArrayList<Enums.GuiOptionType>();
            this.availableOptionsList = $availableList;
        }

        /**
         * @param {GuiOptionType} $guiOptionType Option type, which should be allowed at current GUI object.
         * @return {void}
         */
        public Add($guiOptionType : Enums.GuiOptionType) : void {
            if (!ObjectValidator.IsEmptyOrNull($guiOptionType)) {
                if (this.availableOptionsList.Length() === 0) {
                    ExceptionsManager.Throw(this.getClassName(),
                        "No available gui option has been set to current GUI object.");
                }

                if (!this.optionsList.Contains($guiOptionType)) {
                    if ($guiOptionType === Enums.GuiOptionType.ALL) {
                        this.optionsList = this.availableOptionsList;
                    } else {
                        if (this.availableOptionsList.Contains($guiOptionType)) {
                            this.optionsList.Add($guiOptionType);
                        } else {
                            ExceptionsManager.Throw(this.getClassName(),
                                "Option '" + Enums.GuiOptionType[$guiOptionType] + "' is not allowed on this object.");
                        }
                    }
                }
            }
        }

        /**
         * @param {...GuiOptionType} $guiOptionType Open-ended options type, which should be validated.
         * @return {boolean} Returns true, if GUI object has allowed desired option type, otherwise false.
         */
        public Contains(...$guiOptionType : Enums.GuiOptionType[]) : boolean {
            let index : number;
            for (index = 0; index < $guiOptionType.length; index++) {
                if (this.optionsList.Contains($guiOptionType[index])) {
                    return true;
                }
            }
            return false;
        }
    }
}
