/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.HttpProcessor {
    "use strict";
    import ErrorPages = Com.Wui.Framework.Gui.ErrorPages;
    import HttpRequestConstants = Com.Wui.Framework.Commons.Enums.HttpRequestConstants;

    /**
     * HttpManager class provides handling of current http request
     */
    export class HttpManager extends Com.Wui.Framework.Commons.HttpProcessor.HttpManager {
        protected registerGeneralResolvers() : void {
            super.registerGeneralResolvers();
            this.OverrideResolver("/ServerError/Cookies", ErrorPages.CookiesErrorPage);
            this.OverrideResolver("/ServerError/Browser", ErrorPages.BrowserErrorPage);
            this.OverrideResolver("/ServerError/Http/Moved", ErrorPages.Http301MovedPage);
            this.OverrideResolver("/ServerError/Http/Forbidden", ErrorPages.Http403ForbiddenPage);
            this.OverrideResolver("/ServerError/Http/NotFound", ErrorPages.Http404NotFoundPage);
            this.OverrideResolver("/ServerError/Exception/{" + HttpRequestConstants.EXCEPTION_TYPE + "}", ErrorPages.ExceptionErrorPage);
            this.RegisterResolver("/PersistenceManager", Resolvers.PersistenceManager);
        }
    }
}
