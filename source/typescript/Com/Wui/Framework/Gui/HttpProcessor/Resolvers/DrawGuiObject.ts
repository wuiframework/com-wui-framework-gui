/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.HttpProcessor.Resolvers {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ElementEventsManager = Com.Wui.Framework.Gui.Events.ElementEventsManager;
    import ViewerManager = Com.Wui.Framework.Gui.ViewerManager;
    import BaseViewer = Com.Wui.Framework.Gui.Primitives.BaseViewer;
    import StaticPageContentManger = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;
    import TextSelectionManager = Com.Wui.Framework.Gui.Utils.TextSelectionManager;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import ViewerManagerEventArgs = Com.Wui.Framework.Gui.Events.Args.ViewerManagerEventArgs;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import IEventsHandler = Com.Wui.Framework.Commons.Interfaces.IEventsHandler;
    import GeneralEventOwner = Com.Wui.Framework.Gui.Enums.Events.GeneralEventOwner;
    import ViewerCacheManager = Com.Wui.Framework.Gui.ViewerCacheManager;
    import WuiBuilderConnector = Com.Wui.Framework.Commons.Connectors.WuiBuilderConnector;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import IWebServiceResponseHandler = Com.Wui.Framework.Commons.Interfaces.IWebServiceResponseHandler;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;

    /**
     * DrawGuiObject class provides handling of requests for rendering of GUI objects.
     */
    export class DrawGuiObject extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver {
        protected forceRefreshLinkEvents : ElementEventsManager;
        protected viewCodeLinkEvents : ElementEventsManager;
        protected echoOutputLinkEvents : ElementEventsManager;
        protected echoClearLinkEvents : ElementEventsManager;
        protected cacheLinkEvents : ElementEventsManager;
        protected backToIndexLinkEvents : ElementEventsManager;
        protected cacheGeneratorEvents : ElementEventsManager;
        protected objectName : string;
        protected guiManager : GuiObjectManager;

        private testModeEnabled : boolean;
        private forceRefresh : boolean;
        private readonly echoOutputId : string;
        private viewerManager : ViewerManager;

        constructor() {
            super();
            this.forceRefreshLinkEvents = new ElementEventsManager("ForceRefreshLink");
            this.viewCodeLinkEvents = new ElementEventsManager("ViewCodeLink");
            this.echoOutputLinkEvents = new ElementEventsManager("EchoOutputLink");
            this.cacheLinkEvents = new ElementEventsManager("CacheLink");
            this.cacheGeneratorEvents = new ElementEventsManager("CacheGeneratorLink");
            this.backToIndexLinkEvents = new ElementEventsManager("BackToIndexLink");
            this.echoOutputId = "DeveloperCorner_EchoOutput";
            this.guiManager = GuiObjectManager.getInstanceSingleton();
        }

        /**
         * @param {string} [$value] Object class name in string format, which should be used by the resolver.
         * @return {string} Returns class name of requested object for rendering.
         */
        public ObjectClassName($value? : string) : string {
            if (ObjectValidator.IsEmptyOrNull($value)) {
                if (!ObjectValidator.IsSet(this.objectName)) {
                    this.objectName = this.getRequest().getScriptPath();
                    this.objectName = StringUtils.Remove(this.objectName, "/" + HttpRequestConstants.TEST_MODE);
                    this.objectName = StringUtils.Remove(this.objectName, "/" + HttpRequestConstants.FORCE_REFRESH);
                    this.objectName = StringUtils.Substring(this.objectName,
                        StringUtils.IndexOf(this.objectName, "web/") + 4, StringUtils.Length(this.objectName));
                    this.objectName = StringUtils.Replace(this.objectName, "/", ".");
                }
            } else {
                this.objectName = Property.String(this.objectName, $value);
            }

            return this.objectName;
        }

        /**
         * Execute resolver implementation.
         * @return {void}
         */
        public Process() : void {
            Echo.ClearAll();
            this.resolver();
        }

        protected testMode() : boolean {
            if (!ObjectValidator.IsSet(this.testModeEnabled)) {
                this.testModeEnabled = StringUtils.Contains(this.getRequest().getScriptPath(), HttpRequestConstants.TEST_MODE);
            }

            return this.testModeEnabled;
        }

        protected refresh() : boolean {
            if (!ObjectValidator.IsSet(this.forceRefresh)) {
                this.forceRefresh = StringUtils.Contains(this.getRequest().getScriptPath(), HttpRequestConstants.FORCE_REFRESH);
            }

            return this.forceRefresh;
        }

        protected includeChildren() : boolean {
            return true;
        }

        protected navigator() : string {
            let url : string;
            let output : string = "               ";
            const separator : string = StringUtils.NewLine(false) + "               " + " | ";

            if (!this.testMode()) {
                output +=
                    "<a href=\"#" + this.createLink("") + "/" + HttpRequestConstants.TEST_MODE + "\">Test mode</a>";
            } else {
                url = this.createLink("");
                url = StringUtils.Remove(url, "/" + HttpRequestConstants.TEST_MODE);
                output += "<a href=\"#" + url + "\">Normal mode</a>";
            }

            output +=

                separator +
                "<a id=\"" + this.viewCodeLinkEvents.getOwner() + "\" " +
                "href=\"#" + this.createLink("") + "\">View HTML Code</a>" +

                separator +
                "<a id=\"" + this.echoOutputLinkEvents.getOwner() + "\" " +
                "href=\"#" + this.createLink("") + "\">Echo output</a>" +

                separator +
                "<a id=\"" + this.cacheLinkEvents.getOwner() + "\" " +
                "href=\"#" + this.createLink("") + "\">Cache</a>" +

                separator +
                "<a id=\"" + this.forceRefreshLinkEvents.getOwner() + "\" " +
                "href=\"#" + this.createLink("") + "\">Force refresh</a>" +

                separator +
                "<a id=\"" + this.backToIndexLinkEvents.getOwner() + "\" " +
                "href=\"#" + this.createLink("") + "\">Back to index</a>";

            return output;
        }

        protected developerCorner() : string {
            const id : string = "DeveloperCorner";
            const distanceId : string = "DeveloperCorner_Distance";
            const contentId : string = "DeveloperCorner_Content";
            const bodyId : string = "DeveloperCorner_Body";
            const viewCodeId : string = "DeveloperCorner_ViewCode";
            const bodyEvents : ElementEventsManager = new ElementEventsManager(id);
            const contentEvents : ElementEventsManager = new ElementEventsManager(contentId);
            const navigator : string = this.navigator();
            const EOL : string = StringUtils.NewLine(false);

            if (!ObjectValidator.IsEmptyOrNull(navigator)) {
                bodyEvents.setOnMouseOver(() : void => {
                    ElementManager.Show(distanceId);
                    ElementManager.Show(contentId);
                });
                contentEvents.setOnDoubleClick(() : void => {
                    TextSelectionManager.Clear();
                    ElementManager.setInnerHtml(viewCodeId, "");
                    ElementManager.Hide(bodyId);
                    ElementManager.Hide(distanceId);
                    ElementManager.Hide(contentId);
                });

                this.forceRefreshLinkEvents.setOnClick(($args : EventArgs) : void => {
                    $args.PreventDefault();
                    this.loadGuiObject().ClearCache();
                    this.getHttpManager().Refresh();
                });

                this.viewCodeLinkEvents.setOnClick(($args : EventArgs, $manager : GuiObjectManager) : void => {
                    $args.PreventDefault();
                    const hovered : IGuiCommons = $manager.getHovered();
                    if (!ObjectValidator.IsEmptyOrNull(hovered)) {
                        $manager.setHovered($manager.getHovered(), false);
                    }

                    ElementManager.Show(distanceId);
                    ElementManager.Show(contentId);
                    ElementManager.setInnerHtml(viewCodeId, "");
                    ElementManager.Hide(this.echoOutputId + "_Envelop");
                    ElementManager.Show(viewCodeId);
                    ElementManager.setInnerHtml(viewCodeId, WindowManager.ViewHTMLCode());
                    ElementManager.Show(bodyId);

                    if (ElementManager.Exists(viewCodeId)) {
                        const codes : any = document.getElementById(viewCodeId).getElementsByTagName("pre");
                        if (codes.length !== 0) {
                            const htmlCode : HTMLPreElement = <HTMLPreElement>codes[0];
                            if (document.getElementById(contentId).offsetWidth > htmlCode.offsetWidth) {
                                ElementManager.setWidth(htmlCode, htmlCode.parentElement.offsetWidth);
                            }
                            const contentHeight : number = htmlCode.parentElement.offsetHeight;
                            const contentOffset : number = ElementManager.getAbsoluteOffset(htmlCode.parentElement).Top() + 50;
                            const windowHeight : number = WindowManager.getSize().Height();
                            if (contentHeight + contentOffset > windowHeight) {
                                ElementManager.setHeight(htmlCode, windowHeight - contentOffset);
                            }
                        }
                    }
                });

                this.echoOutputLinkEvents.setOnClick(($args : EventArgs, $manager : GuiObjectManager) : void => {
                    $args.PreventDefault();
                    const hovered : IGuiCommons = $manager.getHovered();
                    if (!ObjectValidator.IsEmptyOrNull(hovered)) {
                        $manager.setHovered($manager.getHovered(), false);
                    }

                    ElementManager.Show(distanceId);
                    ElementManager.Show(contentId);
                    ElementManager.Hide(viewCodeId);
                    ElementManager.Show(this.echoOutputId + "_Envelop");
                    ElementManager.Show(bodyId);

                    const contentHeight : number = ElementManager.getElement(this.echoOutputId).offsetHeight;
                    const contentOffset : number = ElementManager.getAbsoluteOffset(this.echoOutputId).Top() + 35;
                    const windowHeight : number = WindowManager.getSize().Height();
                    if (contentHeight + contentOffset > windowHeight) {
                        ElementManager.setHeight(this.echoOutputId, windowHeight - contentOffset);
                    }
                });

                this.echoClearLinkEvents = new ElementEventsManager(this.echoOutputId + "_Clear");
                this.echoClearLinkEvents.setOnClick(($args : EventArgs) : void => {
                    $args.PreventDefault();
                    if (!ObjectValidator.IsEmptyOrNull(Echo.getStream())) {
                        ElementManager.setInnerHtml(this.echoOutputId, "");
                        ElementManager.ClearCssProperty(this.echoOutputId, "height");
                        Echo.ClearAll();
                    }
                });

                this.backToIndexLinkEvents.setOnClick(($args : EventArgs) : void => {
                    $args.PreventDefault();
                    this.getHttpManager().ReloadTo("/web/");
                });

                this.getEventsManager().setEvent(ViewerManager.ClassName(), EventType.BEFORE_LOAD,
                    () : void => {
                        this.forceRefreshLinkEvents.Subscribe();
                        this.viewCodeLinkEvents.Subscribe();
                        this.echoOutputLinkEvents.Subscribe();
                        this.echoClearLinkEvents.Subscribe();
                        this.backToIndexLinkEvents.Subscribe();
                        bodyEvents.Subscribe();
                        contentEvents.Subscribe();
                        ElementManager.setHeight(distanceId, ElementManager.getElement(id).offsetHeight);
                    });

                const visibility : string = this.testMode() ? "block" : "none";
                return "<div guiType=\"DeveloperCorner\" class=\"DeveloperCorner\">" + EOL +
                    "   <div id=\"" + distanceId + "\" class=\"Distance\" " +
                    "style=\"display: " + visibility + "\"></div>" + EOL +
                    "   <div id=\"" + id + "\" class=\"Envelop\">" + EOL +
                    "       <div id=\"" + contentId + "\" class=\"Content\" style=\"display: " + visibility + "\">" + EOL +
                    "           <div class=\"Links\">" + EOL + navigator + EOL +
                    "           </div>" + EOL +
                    "           <div class=\"Info\">" + EOL +
                    "               <div class=\"Viewer\">" + this.ObjectClassName() + "</div>" + EOL +
                    StringUtils.Format(
                        "               <div class=\"Process\">Page was generated in " +
                        "<span id=\"DeveloperCorner_ProcessTime\">{0}</span> seconds.</div>" + EOL,
                        Convert.TimeToSeconds(this.getHttpManager().getProcessTime())) +
                    "           <div style=\"clear: both;\"></div>" + EOL +
                    "           </div>" + EOL +
                    "           <div id=\"" + bodyId + "\" class=\"Debug\">" + EOL +
                    "           <div id=\"" + viewCodeId + "\" class=\"ViewCode\"></div>" + EOL +
                    "           <div id=\"" + this.echoOutputId + "_Envelop\">" + EOL +
                    "               <div id=\"" + this.echoOutputId + "_Clear\" class=\"Link\">Clear output</div>" + EOL +
                    "               <div id=\"" + this.echoOutputId + "\" class=\"Echo\">" +
                    "Nothing has been printed by Echo yet.</div>" + EOL +
                    "           </div>" + EOL +
                    "           </div>" + EOL +
                    "       </div>" + EOL +
                    "   </div>" + EOL +
                    "</div>";
            } else {
                return "<div guiType=\"DeveloperCorner\" class=\"DeveloperCorner\">" + EOL +
                    "    <div id=\"" + bodyId + "\" class=\"Debug\">" + EOL +
                    "       <div id=\"" + this.echoOutputId + "\" class=\"Echo\"></div>" + EOL +
                    "    </div>" + EOL +
                    "</div>";
            }
        }

        protected contextMenu() : string {
            this.getEventsManager().setEvent(ViewerManager.ClassName(), EventType.BEFORE_LOAD, () : void => {
                this.cacheGeneratorEvents.Subscribe("DeveloperCorner_ContextMenu_GenerateCache");
                this.viewCodeLinkEvents.Subscribe("DeveloperCorner_ContextMenu_ViewCode");
                this.backToIndexLinkEvents.Subscribe("DeveloperCorner_ContextMenu_BackToIndex");
            });

            const EOL : string = StringUtils.NewLine(false);
            return "<div guiType=\"DeveloperCorner\" class=\"DeveloperCorner\">" + EOL +
                "   <div id=\"DeveloperCorner_ContextMenu\" class=\"ContextMenu\">" + EOL +
                "       <div id=\"DeveloperCorner_ContextMenu_ViewCode\" class=\"Link\">View HTML Code</div>" + EOL +
                "       <div id=\"DeveloperCorner_ContextMenu_GenerateCache\" class=\"Link\">Generate Cache</div>" + EOL +
                "       <div id=\"DeveloperCorner_ContextMenu_BackToIndex\" class=\"Link\">Back to index</div>" + EOL +
                "   </div>" + EOL +
                "</div>";
        }

        protected wuiBuilderContent() : string {
            if (this.getRequest().IsWuiPlugin() && !this.getRequest().getUrlArgs().KeyExists(HttpRequestConstants.WUI_DESIGNER)) {
                const backgroundId : string = "DeveloperCorner_WuiBuilder_Background";
                const progressId : string = "DeveloperCorner_WuiBuilder_Progress";

                WindowManager.getEvents().setOnLoad(() : void => {
                    const connector : WuiBuilderConnector = WuiBuilderConnector.Connect();
                    connector.getEvents().OnError(($args : ErrorEventArgs) : void => {
                        document.getElementById(this.echoOutputLinkEvents.getOwner()).click();
                        Echo.Println($args.Message());
                    });

                    const updateMessagePosition : IEventsHandler = () : void => {
                        if (ElementManager.IsVisible(backgroundId)) {
                            const size : Size = WindowManager.getSize();
                            ElementManager.setSize(backgroundId, size.Width(), size.Height());
                            const messageSize : Size = new Size(progressId, true);
                            const position : ElementOffset = new ElementOffset(
                                (size.Height() - messageSize.Height()) / 2,
                                (size.Width() - messageSize.Width()) / 2
                            );
                            ElementManager.setPosition(progressId, position);
                        }
                    };

                    WindowManager.getEvents().setOnResize(updateMessagePosition);

                    const errorHandler : IWebServiceResponseHandler = ($message : string) : void => {
                        ElementManager.Hide(backgroundId);
                        ElementManager.Hide(progressId);
                        document.getElementById(this.echoOutputLinkEvents.getOwner()).click();
                        Echo.Println($message);
                    };

                    connector.getEvents().OnStart(() : void => {
                        connector.getEvents().OnBuildStart(() : void => {
                            ElementManager.Show(backgroundId);
                            ElementManager.Show(progressId);
                            ElementManager.setInnerHtml(progressId, "Build in progress, please wait ...");
                            updateMessagePosition();
                        });

                        connector.getEvents().OnFail(errorHandler);
                        connector.getEvents().OnWarning(errorHandler);
                    });
                });

                const EOL : string = StringUtils.NewLine(false);
                return "<div guiType=\"DeveloperCorner\" class=\"DeveloperCorner\">" + EOL +
                    "   <div id=\"DeveloperCorner_WuiBuilder\" class=\"WuiBuilder\">" + EOL +
                    "       <div id=\"" + backgroundId + "\" class=\"Background\"></div>" + EOL +
                    "       <div id=\"" + progressId + "\" class=\"Progress\"></div>" + EOL +
                    "   </div>" + EOL +
                    "</div>";
            }
            return "";
        }

        protected loadGuiObject() : ViewerManager {
            if (!ObjectValidator.IsSet(this.viewerManager)) {
                this.viewerManager = new ViewerManager(this.ObjectClassName());
                this.viewerManager.ReloadCacheEnabled(this.refresh());
                this.viewerManager.TestModeEnabled(this.testMode());
                this.viewerManager.IncludeChildren(this.includeChildren());
            }
            return this.viewerManager;
        }

        protected resolver() : void {
            WindowManager.getEvents().setOnRightClick(($eventArgs : EventArgs) : void => {
                $eventArgs.PreventDefault();
                let top : number = WindowManager.getMouseY(<MouseEvent>$eventArgs.NativeEventArgs(), true) + 15;
                let left : number = WindowManager.getMouseX(<MouseEvent>$eventArgs.NativeEventArgs(), true) + 15;
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    ElementManager.Show("DeveloperCorner_ContextMenu");

                    const menuSize : Size = new Size("DeveloperCorner_ContextMenu");
                    const windowSize : Size = WindowManager.getSize();
                    if (top + menuSize.Height() > windowSize.Height()) {
                        top = windowSize.Height() - menuSize.Height() - 20;
                    }
                    if (left + menuSize.Width() > windowSize.Width()) {
                        left = windowSize.Width() - menuSize.Width() - 20;
                    }
                    ElementManager.setCssProperty("DeveloperCorner_ContextMenu", "top", top);
                    ElementManager.setCssProperty("DeveloperCorner_ContextMenu", "left", left);
                }, 100);
            });
            WindowManager.getEvents().setOnClick(() : void => {
                if (ElementManager.IsVisible("DeveloperCorner_ContextMenu")) {
                    ElementManager.Hide("DeveloperCorner_ContextMenu");
                }
            });
            WindowManager.getEvents().setOnRequest(() : void => {
                this.getHttpManager().Refresh();
            });
            if (ObjectValidator.IsSet(window.onhashchange)) {
                window.onhashchange = () : void => {
                    this.getHttpManager().Refresh();
                };
            }
            WindowManager.getEvents().setOnKeyDown(($eventArgs : KeyEventArgs, $manager : GuiObjectManager) : void => {
                if ($eventArgs.getKeyCode() === KeyMap.BACKSPACE && $manager.getActive().IsEmpty()) {
                    $eventArgs.PreventDefault();
                    this.getHttpManager().ReloadTo("/web/");
                }
            });

            let progressIndex : number = 0;
            const loadingProgressHandler : IEventsHandler = () : void => {
                if (progressIndex < 100) {
                    progressIndex++;
                    ElementManager.setInnerHtml("DrawGuiContentLoader_Progress", "Progress: " + progressIndex + "%");
                }
            };
            this.getEventsManager().setEvent(ViewerManager.ClassName(), EventType.ON_CHANGE, loadingProgressHandler);
            this.getEventsManager().setEvent(BaseViewer.ClassName(), EventType.ON_CHANGE, loadingProgressHandler);

            StaticPageContentManger.Clear();
            StaticPageContentManger.Title("WUI - Developer Corner");
            StaticPageContentManger.BodyAppend(this.developerCorner() + StringUtils.NewLine(false));
            StaticPageContentManger.BodyAppend(this.wuiBuilderContent() + StringUtils.NewLine(false));
            StaticPageContentManger.BodyAppend(this.contextMenu() + StringUtils.NewLine(false) + StringUtils.NewLine(false));
            StaticPageContentManger.BodyAppend(
                "<div id=\"DrawGuiContentLoader\" " +
                "style=\"display: block; position: relative; top: 100px; left: 100px; float: left;\">" + StringUtils.NewLine(false) +
                "   <h1>Loading, please wait ...</h1>" + StringUtils.NewLine(false) +
                "   <div id=\"DrawGuiContentLoader_Progress\"></div>" + StringUtils.NewLine(false) +
                "</div>");
            StaticPageContentManger.Draw();
            ElementManager.setClassName(ElementManager.getElement("Content", true), "Content DrawGuiObject");
            this.getEventsManager().FireEvent(ViewerManager.ClassName(), EventType.BEFORE_LOAD, false);

            const pageSize : Size = WindowManager.getSize();
            const loaderSize : Size = new Size("DrawGuiContentLoader", true);
            ElementManager.setCssProperty("DrawGuiContentLoader", "top", (pageSize.Height() - loaderSize.Height()) / 2);
            ElementManager.setCssProperty("DrawGuiContentLoader", "left", (pageSize.Width() - loaderSize.Width()) / 2);

            this.cacheGeneratorEvents.setOnClick(($args : EventArgs) : void => {
                $args.PreventDefault();
                document.getElementById(this.echoOutputLinkEvents.getOwner()).click();
                document.getElementById(this.echoClearLinkEvents.getOwner()).click();
                Echo.Println("Generating cache, please wait...");
                const startTime : number = new Date().getTime();
                this.getEventsManager().setEvent(ViewerCacheManager.ClassName(), EventType.ON_CHANGE, () : void => {
                    if ((new Date().getTime() - startTime) % 100 === 0) {
                        Echo.Print(".");
                    }
                });
                this.loadGuiObject().getCache(() : void => {
                    const cacheReport : string =
                        "Process time: " + Convert.TimeToSeconds(new Date().getTime() - startTime) + " seconds" +
                        StringUtils.NewLine() +
                        this.loadGuiObject().ToString();

                    this.loadGuiObject().getCacheRawData();

                    Echo.Println("Cache has been created. " +
                        "<span class=\"Link\" style=\"float: none; clear: none;\" onclick=\"" +
                        "document.getElementById('GeneratedCacheDetails').style.display==='block'?" +
                        "document.getElementById('GeneratedCacheDetails').style.display='none':" +
                        "document.getElementById('GeneratedCacheDetails').style.display='block';\">" +
                        "Show/Hide details</span>" +
                        "<div id=\"GeneratedCacheDetails\" style=\"display: none;\">" + cacheReport + "</div>");
                });
            });

            const cleanupPageContent : IEventsHandler = () : void => {
                if (!ObjectValidator.IsEmptyOrNull(this.echoOutputId) && ElementManager.Exists(this.echoOutputId)) {
                    const stream : HTMLSpanElement = document.createElement("span");
                    stream.innerHTML = Echo.getStream();
                    const nodes : any = document.body.getElementsByTagName("span");
                    let child : any;
                    let attributeValue : string;
                    const attributeKey : string = "guitype";
                    let index : number;
                    for (index = 0; index < nodes.length; index++) {
                        child = nodes[index];
                        if (child.nodeType === 1 && ObjectValidator.IsSet(child.attributes[attributeKey])) {
                            attributeValue = ObjectValidator.IsSet(child.attributes[attributeKey].value) ?
                                child.attributes[attributeKey].value : child.attributes[attributeKey].nodeValue;
                            if (attributeValue === "HtmlAppender" &&
                                StringUtils.Contains(stream.innerHTML, child.innerHTML) &&
                                child.parentElement.id !== this.echoOutputId) {
                                child.parentElement.removeChild(child);
                            }
                        }
                    }
                }
            };

            this.getEventsManager().setEvent(ViewerManager.ClassName(), EventType.ON_COMPLETE,
                ($eventArgs : ViewerManagerEventArgs) : void => {
                    if (!$eventArgs.AsyncCallback()) {
                        ElementManager.Hide("DrawGuiContentLoader");
                        if (!ObjectValidator.IsEmptyOrNull($eventArgs.Result())) {
                            if ((<ViewerCacheManager>$eventArgs.Result()).IsTypeOf(ViewerCacheManager)) {
                                StaticPageContentManger.BodyAppend($eventArgs.Result().Show() + StringUtils.NewLine(false));
                            } else {
                                const view : BaseViewer = <BaseViewer>$eventArgs.Result();
                                if (view.IsCached()) {
                                    ElementManager.setClassName("DeveloperCorner", "Envelop Cached");
                                } else {
                                    if (!ObjectValidator.IsEmptyOrNull(view.getInstance())) {
                                        view.getInstance().getEvents().setOnComplete(cleanupPageContent);
                                    }
                                    StaticPageContentManger.BodyAppend(view.Show() + StringUtils.NewLine(false));
                                }

                                this.cacheLinkEvents.setOnClick(($args : EventArgs) : void => {
                                    $args.PreventDefault();
                                    document.getElementById(this.echoOutputLinkEvents.getOwner()).click();
                                    document.getElementById(this.echoClearLinkEvents.getOwner()).click();

                                    const cacheInfoEvents : ElementEventsManager = new ElementEventsManager("CacheInfoLink");
                                    Echo.Println("<a id=\"" + cacheInfoEvents.getOwner() + "\" " +
                                        "href=\"#" + this.createLink("") + "\">Show cache info</a>");
                                    Echo.Println("<a id=\"" + this.cacheGeneratorEvents.getOwner() + "\" " +
                                        "href=\"#" + this.createLink("") + "\">Generate cache</a>");

                                    cacheInfoEvents.setOnClick(($args : EventArgs) : void => {
                                        $args.PreventDefault();
                                        document.getElementById(this.echoClearLinkEvents.getOwner()).click();
                                        Echo.Printf(this.loadGuiObject());
                                    });
                                    cacheInfoEvents.Subscribe();
                                    this.cacheGeneratorEvents.Subscribe();
                                });
                                this.cacheLinkEvents.Subscribe();
                            }
                        } else {
                            StaticPageContentManger.BodyAppend(
                                "<h1>Viewer \"" + this.ObjectClassName() + "\" source file has not been found " +
                                "or it does not implement IBaseViewer interface!</h1>");
                        }

                        if (!ObjectValidator.IsEmptyOrNull(this.echoOutputId) && ElementManager.Exists(this.echoOutputId)) {
                            cleanupPageContent();
                            const stream : string = Echo.getStream();
                            if (!ObjectValidator.IsEmptyOrNull(stream)) {
                                ElementManager.setInnerHtml(this.echoOutputId, "");
                            }
                            Echo.Init(this.echoOutputId, true);
                            Echo.Print(stream);
                        }
                        ElementManager.setInnerHtml("DeveloperCorner_ProcessTime",
                            Convert.TimeToSeconds(this.getHttpManager().getProcessTime()).toString());

                        this.getEventsManager().FireEvent(GeneralEventOwner.BODY, EventType.ON_LOAD);
                    }
                });
            this.loadGuiObject().Process();
        }
    }
}
