/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.HttpProcessor.Resolvers {
    "use strict";
    import Parent = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.RuntimeTestRunner;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import StaticPageContentManger = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import GeneralEventOwner = Com.Wui.Framework.Commons.Enums.Events.GeneralEventOwner;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import IEventsManager = Com.Wui.Framework.Gui.Interfaces.IEventsManager;
    import BaseViewer = Com.Wui.Framework.Gui.Primitives.BaseViewer;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;

    /**
     * RuntimeTestRunner class provides handling of unit tests at runtime.
     */
    export abstract class RuntimeTestRunner extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.RuntimeTestRunner {

        /**
         * @param {ClassName} [$className] Specify class name from, which should be created the runtime unit test link.
         * @return {string} Returns link in string format, which can be used for resolver registration or as runtime callback.
         */
        public static CreateLink($className? : ClassName) : string {
            if (!ObjectValidator.IsSet($className)) {
                $className = this;
            }
            if (ObjectValidator.IsSet((<any>$className).ClassName)) {
                return "#" + Loader.getInstance().getHttpManager().CreateLink(
                    "/" + HttpRequestConstants.RUNTIME_TEST + "/" + StringUtils.Replace((<any>$className).ClassName(), ".", "/"));
            }
            return Loader.getInstance().getHttpManager().CreateLink("");
        }

        /**
         * @return {string} Returns link, which can be used for resolver registration or invoke by link element.
         */
        public static CallbackLink() : string {
            return this.CreateLink();
        }

        /**
         * Execute resolver implementation.
         * @return {void}
         */
        public Process() : void {
            Echo.ClearAll();
            this.resolver();
        }

        protected resolver() : void {
            StaticPageContentManger.Clear();
            StaticPageContentManger.Title("WUI - Unit Test");
            StaticPageContentManger.Draw();
            this.getEvents().setOnSuiteStart(() : void => {
                ElementManager.setClassName(document.body, "RuntimeTest");
            });
            const autoScrollHandler : any = () : void => {
                WindowManager.ScrollToBottom();
            };
            this.getEvents().setOnTestCaseEnd(autoScrollHandler);
            this.getEvents().setOnSuiteEnd(autoScrollHandler);

            let testName : string = this.getRequest().getScriptPath();
            let executeParent : boolean = true;
            if (StringUtils.Contains(testName, HttpRequestConstants.RUNTIME_TEST)) {
                testName = StringUtils.Substring(testName,
                    StringUtils.IndexOf(testName, HttpRequestConstants.RUNTIME_TEST + "/") + 12, StringUtils.Length(testName));
                testName = StringUtils.Replace(testName, "/", ".");
                if (testName !== this.getClassName()) {
                    const reflect : Reflection = Reflection.getInstance();
                    const testClass : any = reflect.getClass(testName);
                    if (!ObjectValidator.IsEmptyOrNull(testClass)) {
                        const testObject : Parent = new testClass();
                        if (reflect.IsMemberOf(testObject, Parent)) {
                            testObject.Process();
                            const testViewer : BaseViewer = new BaseViewer();
                            const events : IEventsManager = this.getEventsManager();
                            events.setEvent(GeneralEventOwner.BODY, EventType.ON_LOAD, () : void => {
                                const elements : ArrayList<IGuiCommons> = GuiObjectManager.getInstanceSingleton().getAll();
                                elements.foreach(($element : IGuiCommons) : void => {
                                    if (!$element.IsCached() && !$element.IsLoaded() &&
                                        ObjectValidator.IsEmptyOrNull($element.Parent()) &&
                                        ElementManager.Exists($element.Id())) {
                                        if (ObjectValidator.IsEmptyOrNull($element.InstanceOwner())) {
                                            $element.InstanceOwner(testViewer);
                                        }
                                        if ($element.Visible()) {
                                            if ((<any>$element).hasAsynchronousDraw()) {
                                                $element.Visible(true);
                                            } else {
                                                events.FireEvent($element, EventType.ON_START);
                                                if (ElementManager.IsVisible($element.Id())) {
                                                    events.FireEvent($element, EventType.BEFORE_LOAD, false);
                                                }
                                            }
                                        }
                                    }
                                });
                            });
                            events.FireEvent(GeneralEventOwner.BODY, EventType.ON_LOAD);
                            executeParent = false;
                        }
                    }
                }
            }
            if (executeParent) {
                super.resolver();
            }
        }
    }
}
