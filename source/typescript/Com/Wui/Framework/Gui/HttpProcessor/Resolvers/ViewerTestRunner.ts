/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.HttpProcessor.Resolvers {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import BaseViewer = Com.Wui.Framework.Gui.Primitives.BaseViewer;
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import HttpRequestParser = Com.Wui.Framework.Commons.HttpProcessor.HttpRequestParser;
    import EventsManager = Com.Wui.Framework.Gui.Events.EventsManager;
    import EnvironmentArgs = Com.Wui.Framework.Commons.EnvironmentArgs;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import StaticPageContentManager = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IEventsHandler = Com.Wui.Framework.Gui.Interfaces.IEventsHandler;
    import IArrayList = Com.Wui.Framework.Commons.Interfaces.IArrayList;
    import FormsObject = Com.Wui.Framework.Gui.Primitives.FormsObject;

    /**
     * ViewerTestRunner class provides structure for functional tests focused on handling of Viewer instance at runtime.
     */
    export class ViewerTestRunner<TGuiCommons extends GuiCommons> extends BaseObject {
        private readonly events : EventsManager;
        private readonly environment : EnvironmentArgs;
        private readonly httpManager : HttpManager;
        private readonly request : HttpRequestParser;
        private owner : BaseViewer;
        private instance : TGuiCommons;
        private passingCount : number = 0;
        private failingCount : number = 0;
        private skippedCount : number = 0;
        private assertionsCount : number = 0;
        private eventHandlers : any;
        private methodFilter : string[];
        private defaultTimeout : number = 10000; // ms
        private currentTimeout : number;

        constructor() {
            super();

            this.environment = Loader.getInstance().getEnvironmentArgs();
            this.httpManager = Loader.getInstance().getHttpManager();
            this.request = this.httpManager.getRequest();
            this.events = Loader.getInstance().getHttpResolver().getEvents();

            this.eventHandlers = {
                onSuiteStart() : void {
                    // default event handler
                },
                onSuiteEnd($passing : number, $failing : number) : void {
                    // default event handler
                },
                onTestCaseStart($testName : string) : void {
                    // default event handler
                },
                onTestCaseEnd($testName : string) : void {
                    // default event handler
                },
                onAssert($status : boolean) : void {
                    // default event handler
                }
            };
            this.methodFilter = [];
            this.currentTimeout = this.defaultTimeout;
        }

        /**
         * @param $owner {BaseViewer} Specify owner of this class, which should be targeted by implemented tests.
         * @return {void}
         */
        public setOwner($owner : BaseViewer) : void {
            this.owner = $owner;
            this.instance = <any>this.owner.getInstance();
        }

        /**
         * Execute tests implementation.
         * @return {void}
         */
        public Process() : string {
            return this.resolver();
        }

        protected timeoutLimit($value? : number) : number {
            return this.currentTimeout = Property.Integer(this.currentTimeout, $value);
        }

        protected setMethodFilter(...$names : string[]) : void {
            $names.forEach(($name : string) : void => {
                this.methodFilter.push(StringUtils.ToLowerCase($name));
            });
        }

        protected getInstance() : TGuiCommons {
            return this.instance;
        }

        protected removeInstance() : void {
            (<any>this).owner.setInstance(null);
        }

        protected getEvents() : IViewerTestEvents {
            return {
                setOnAssert       : ($callback? : any) : void => {
                    this.eventHandlers.onAssert = $callback;
                },
                setOnSuiteEnd     : ($callback? : any) : void => {
                    this.eventHandlers.onSuiteEnd = $callback;
                },
                setOnSuiteStart   : ($callback? : any) : void => {
                    this.eventHandlers.onSuiteStart = $callback;
                },
                setOnTestCaseEnd  : ($callback? : any) : void => {
                    this.eventHandlers.onTestCaseEnd = $callback;
                },
                setOnTestCaseStart: ($callback? : any) : void => {
                    this.eventHandlers.onTestCaseStart = $callback;
                }
            };
        }

        protected getAbsoluteRoot() : string {
            return StringUtils.Remove(ObjectDecoder.Url(this.getRequest().getHostUrl()), "file:///", "file://", "/index.html");
        }

        protected resolver() : string {
            const progress : HTMLElement = document.createElement("div");
            progress.id = this.getUID();
            progress.className = "WuiViewerTestRunnerProgress";
            progress.innerText = "Running automated functional tests...";

            const tests : ArrayList<string> = new ArrayList<string>();
            const methods : string[] = this.getMethods();
            let output : string = "";
            let index : number;
            for (index = 0; index < methods.length; index++) {
                if (StringUtils.Contains(StringUtils.ToLowerCase(methods[index]), "test")) {
                    tests.Add(methods[index]);
                }
            }

            let timeoutId : number = null;
            const stopTimeout : any = () : void => {
                if (timeoutId !== null) {
                    clearTimeout(timeoutId);
                }
            };
            const createTimeout : any = ($handler : any) : void => {
                stopTimeout();
                if (this.currentTimeout > -1) {
                    timeoutId = this.getEventsManager().FireAsynchronousMethod(() : void => {
                        this.failingCount++;
                        Echo.Println("<i id=\"" + this.getClassNameWithoutNamespace() + "_Assert_Fail_" + this.failingCount + "\" " +
                            "style=\"color: red;\">Test case/suite has reached timeout (" + this.currentTimeout + " ms)</i>");
                        stopTimeout();
                        $handler();
                    }, this.currentTimeout);
                }
            };
            const handlePromiseCall : any = ($promise : IViewerTestPromise, $handler : () => void) : void => {
                if (ObjectValidator.IsFunction($promise)) {
                    createTimeout($handler);
                    $promise(() : void => {
                        stopTimeout();
                        $handler();
                    });
                } else {
                    $handler();
                }
            };

            const getNextTest : any = ($index : number, $callback : () => void) : void => {
                if ($index < tests.Length()) {
                    const method : string = tests.getItem($index);
                    const testName : string = StringUtils.Remove(method, "test", "__Ignore");
                    Echo.Print("<h3>" + this.getClassName() + " - " + testName + "</h3>");

                    if (this.methodFilter.length === 0 && StringUtils.StartsWith(method, "__Ignore")
                        || this.methodFilter.length !== 0 && (
                            this.methodFilter.indexOf(StringUtils.ToLowerCase(testName)) === -1 &&
                            this.methodFilter.indexOf(StringUtils.ToLowerCase("test" + testName)) === -1 &&
                            this.methodFilter.indexOf(StringUtils.ToLowerCase(method)) === -1)) {
                        this.skippedCount++;
                        output =
                            "<h4><i style=\"color: orange;\">skipped</i></h4>" +
                            "<hr>";
                        Echo.Print(output);
                        getNextTest($index + 1, $callback);
                    } else {
                        ElementManager.getElement(progress.id).innerText = "Running automated functional test: " + testName;

                        const tearDownHandler : any = () : void => {
                            Echo.Print("<hr>");
                            this.eventHandlers.onTestCaseEnd(method);
                            getNextTest($index + 1, $callback);
                        };

                        const testCaseHandler : any = () : void => {
                            this.currentTimeout = this.defaultTimeout;
                            handlePromiseCall(this.tearDown(), tearDownHandler);
                        };

                        const setUpHandler : any = () : void => {
                            this.eventHandlers.onTestCaseStart(method);
                            handlePromiseCall(this[method](), testCaseHandler);
                        };

                        handlePromiseCall(this.setUp(), setUpHandler);
                    }
                } else {
                    $callback();
                }
            };

            this.getInstance().getEvents().setOnComplete(() : void => {
                getNextTest(0, () : void => {
                    const afterHandler : any = () : void => {
                        output = "<span class=\"Result\">";
                        if (this.failingCount === 0) {
                            output += "SUCCESS" + StringUtils.NewLine();
                        } else {
                            output += "FAILURES!" + StringUtils.NewLine();
                        }
                        output += "</span>";
                        if (this.skippedCount !== 0) {
                            output += "Skipped tests: " + this.skippedCount + StringUtils.NewLine();
                        }
                        output +=
                            "Tests: " + tests.Length() + ", Assertions: " + this.assertionsCount + ", Failures: " + this.failingCount + ".";
                        Echo.Println(output);

                        this.getEventsManager().FireAsynchronousMethod(() : void => {
                            ElementManager.Hide(progress.id);
                            if (this.failingCount !== 0) {
                                ElementManager.setCssProperty("DeveloperCorner", "border-bottom-color", "red");
                                document.getElementById("EchoOutputLink").click();
                            }
                        }, 500);
                        this.eventHandlers.onSuiteEnd(this.passingCount, this.failingCount);
                    };

                    handlePromiseCall(this.after(), afterHandler);
                });
            });

            this.eventHandlers.onSuiteStart();
            let beforeResult : string = <string>this.before();
            if (!ObjectValidator.IsSet(beforeResult)) {
                beforeResult = "";
            }
            if (!tests.IsEmpty()) {
                StaticPageContentManager.BodyAppend(progress.outerHTML);
            }
            return beforeResult;
        }

        protected before() : void | string {
            // override this method for ability to execute code BEFORE run of ALL tests in current ViewerTest
            return;
        }

        protected setUp() : void | IViewerTestPromise {
            // override this method for ability to execute code BEFORE EACH test in current ViewerTest
            return;
        }

        protected tearDown() : void | IViewerTestPromise {
            // override this method for ability to execute code AFTER EACH test in current ViewerTest
            return;
        }

        protected after() : void | IViewerTestPromise {
            // override this method for ability to execute code AFTER run of ALL tests in current ViewerTest
            return;
        }

        protected addButton($text : string, $onClick : () => void) : void {
            (<any>this.owner).addTestButton($text, $onClick);
        }

        protected emulateEvent($owner : GuiCommons, $eventType : EventType, $args? : Event) : void {
            this.getEventsManager().FireAsynchronousMethod(() : void => {
                const type : string = <string>$eventType;
                let eventsList : IArrayList<IArrayList<IEventsHandler>> = $owner.getEvents().getAll();
                if (!eventsList.KeyExists(type) && ObjectValidator.IsSet((<FormsObject>$owner).getSelectorEvents)) {
                    eventsList = (<FormsObject>$owner).getSelectorEvents().getAll();
                }
                if (!eventsList.KeyExists(type)) {
                    eventsList = EventsManager.getInstanceSingleton().getAll().getItem($owner.getEvents().getOwner());
                }
                if (!ObjectValidator.IsEmptyOrNull(eventsList) && eventsList.KeyExists(type)) {
                    const guiManager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                    const reflection : Reflection = Reflection.getInstance();
                    const handlerArgs : EventArgs = new EventArgs();
                    let owner : any = (<any>$owner.getEvents()).owner;
                    if (ObjectValidator.IsEmptyOrNull(owner)) {
                        owner = $owner;
                    }
                    handlerArgs.Owner(owner);
                    handlerArgs.NativeEventArgs(ObjectValidator.IsEmptyOrNull($args) ? event : $args);
                    handlerArgs.Type(type);
                    eventsList.getItem(type).foreach(($handler : IEventsHandler) : void => {
                        $handler(handlerArgs, guiManager, reflection);
                    });
                }
            });
        }

        protected assertEquals($actual : any, $expected : any, $message? : string) : void {
            const assertId : string = this.getClassNameWithoutNamespace() + "_Assert_";
            this.assertionsCount++;
            let output : string = "<h4 id=\"" + assertId + this.assertionsCount + "\">";
            if (ObjectValidator.IsSet($message)) {
                output += " - " + $message + ":" + StringUtils.NewLine();
            }

            let status : boolean = true;
            if ($actual !== $expected) {
                status = false;
                this.failingCount++;
                output += "<i id=\"" + assertId + "Fail_" + this.failingCount + "\" " +
                    "style=\"color: red;\">assert #" + this.assertionsCount + " has failed</i></h4>";
                Echo.Print(output);
                this.printFailure($actual, $expected);
            } else {
                this.passingCount++;
                output += "<i id=\"" + assertId + "Pass_" + this.passingCount + "\" " +
                    "style=\"color: green;\">assert #" + this.assertionsCount + " has passed</i></h4>";
                Echo.Print(output);
            }
            this.eventHandlers.onAssert(status);
        }

        protected assertDeepEqual($actual : any, $expected : any, $message? : string) : void {
            if (Reflection.getInstance().IsMemberOf($actual, ArrayList) && Reflection.getInstance().IsMemberOf($expected, ArrayList)) {
                if ((<ArrayList<any>>$actual).Equal($expected)) {
                    $actual = $expected;
                }
            }

            if (ObjectValidator.IsObject($actual) && ObjectValidator.IsObject($expected) ||
                ObjectValidator.IsNativeArray($actual) && ObjectValidator.IsNativeArray($expected)) {
                $actual = JSON.stringify($actual);
                $expected = JSON.stringify($expected);
            }

            this.assertEquals($actual, $expected, $message);
        }

        protected printFailure($actual : any, $expected : any) : void {
            $actual = Convert.ObjectToString($actual);
            if (StringUtils.Length($actual) > 1024) {
                $actual = StringUtils.Substring($actual, 0, 1024) + " ...";
            }
            $expected = Convert.ObjectToString($expected);
            if (StringUtils.Length($expected) > 1024) {
                $expected = StringUtils.Substring($expected, 0, 1024) + " ...";
            }

            Echo.Println("<u>actual:</u>" + StringUtils.NewLine() + $actual);
            Echo.Println("<u>expected:</u>" + StringUtils.NewLine() + $expected);
        }

        protected getRequest() : HttpRequestParser {
            return this.request;
        }

        protected getHttpManager() : HttpManager {
            return this.httpManager;
        }

        protected getEventsManager() : EventsManager {
            return this.events;
        }

        protected getEnvironmentArgs() : EnvironmentArgs {
            return this.environment;
        }
    }

    export type IViewerTestPromise = ($done : () => void) => void;

    export interface IViewerTestEvents {
        setOnSuiteStart($callback : () => void) : void;

        setOnSuiteEnd($callback : ($passing : number, $failing : number) => void) : void;

        setOnTestCaseStart($callback : ($testName? : string) => void) : void;

        setOnTestCaseEnd($callback : ($testName? : string) => void) : void;

        setOnAssert($callback : ($status? : boolean) => void) : void;
    }
}
