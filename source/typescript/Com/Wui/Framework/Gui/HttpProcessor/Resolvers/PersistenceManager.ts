/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.HttpProcessor.Resolvers {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import PersistenceFactory = Com.Wui.Framework.Commons.PersistenceApi.PersistenceFactory;
    import PersistenceType = Com.Wui.Framework.Gui.Enums.PersistenceType;
    import FormsObject = Com.Wui.Framework.Gui.Primitives.FormsObject;
    import FormValue = Com.Wui.Framework.Gui.Structures.FormValue;

    /**
     * PersistenceManager class provides automated store of UI state based on user interaction
     */
    export class PersistenceManager extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.AsyncHttpResolver {

        protected resolver() : void {
            const data : ArrayList<FormValue> = FormsObject.CollectValues();
            const addValues : ArrayList<string> = new ArrayList<string>();
            const removeValues : ArrayList<string> = new ArrayList<string>();
            const addErrors : ArrayList<boolean> = new ArrayList<boolean>();
            const removeErrors : ArrayList<string> = new ArrayList<string>();
            data.foreach(($formElement : FormValue) : void => {
                if (!ObjectValidator.IsEmptyOrNull($formElement.Value())) {
                    addValues.Add($formElement.Value(), $formElement.Id());
                } else {
                    removeValues.Add($formElement.Id());
                }
                if ($formElement.ErrorFlag()) {
                    addErrors.Add($formElement.ErrorFlag(), $formElement.Id());
                } else {
                    removeErrors.Add($formElement.Id());
                }
            });
            PersistenceFactory.getPersistence(PersistenceType.FORM_VALUES).Variable(addValues);
            PersistenceFactory.getPersistence(PersistenceType.FORM_VALUES).Destroy(removeValues);
            PersistenceFactory.getPersistence(PersistenceType.ERROR_FLAGS).Variable(addErrors);
            PersistenceFactory.getPersistence(PersistenceType.ERROR_FLAGS).Destroy(removeErrors);
            this.success();
        }
    }
}
