/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.HttpProcessor {
    "use strict";
    import EventsManager = Com.Wui.Framework.Gui.Events.EventsManager;
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;

    export class HttpResolver extends Com.Wui.Framework.Commons.HttpProcessor.HttpResolver {
        private readonly guiRegister : GuiObjectManager;

        constructor($baseUrl? : string) {
            super($baseUrl);

            const guiObjectManager : any = this.getGuiObjectManagerClass();
            this.guiRegister = new guiObjectManager();
        }

        public getManager() : HttpManager {
            return <HttpManager>super.getManager();
        }

        public getEvents() : EventsManager {
            return <EventsManager>super.getEvents();
        }

        /**
         * @return {GuiObjectManager} Returns GuiObjectManager instance used for management of GUI used in resolved requests.
         */
        public getGuiRegister() : GuiObjectManager {
            return this.guiRegister;
        }

        protected getStartupResolvers() : any {
            const resolvers : any = {
                "/"        : Com.Wui.Framework.Gui.Index,
                "/async/**/*" : Com.Wui.Framework.Gui.HttpProcessor.Resolvers.AsyncDrawGuiObject,
                "/design/**/*": Com.Wui.Framework.Gui.Designer.DesignerRunner,
                "/index"   : Com.Wui.Framework.Gui.Index,
                "/web/"    : Com.Wui.Framework.Gui.Index,
                "/web/**/*"   : Com.Wui.Framework.Gui.HttpProcessor.Resolvers.DrawGuiObject,
                "design/"  : Com.Wui.Framework.Gui.Designer.DesignerRunner
            };
            resolvers["/" + HttpRequestConstants.RUNTIME_TEST + "/**/*"] =
                Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner;
            /* dev:start */
            resolvers["/web/PersistenceManagerTest"] = Com.Wui.Framework.Gui.RuntimeTests.PersistenceManagerTest;
            /* dev:end */
            return resolvers;
        }

        protected getHttpManagerClass() : any {
            return HttpManager;
        }

        protected getEventsManagerClass() : any {
            return EventsManager;
        }

        protected getGuiObjectManagerClass() : any {
            return GuiObjectManager;
        }
    }
}
