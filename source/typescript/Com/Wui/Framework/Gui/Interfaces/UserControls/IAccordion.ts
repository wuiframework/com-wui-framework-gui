/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export interface IAccordion extends Com.Wui.Framework.Gui.Interfaces.Primitives.IBasePanel {
        GuiType($accordionType ? : any) : any;

        setPanelHoldersArgs($args : ArrayList<any>) : void;

        Clear() : void;

        Collapse($filter? : Array<number | ClassName>, $animate? : boolean) : void;

        Expand($filter? : Array<number | ClassName>, $animate? : boolean) : void;

        ExpandSingle($filter? : number | ClassName, $animate? : boolean) : void;

        getPanel($filter : number | ClassName) : void;
    }
}
