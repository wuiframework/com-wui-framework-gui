/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    import IButtonEvents = Com.Wui.Framework.Gui.Interfaces.Events.IButtonEvents;

    export interface IButton extends IImageButton {
        GuiType($buttonType? : any) : any;

        Text($value? : string) : string;

        Width($value? : number) : number;

        getEvents() : IButtonEvents;
    }
}
