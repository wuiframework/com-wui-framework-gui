/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import ITextAreaEvents = Com.Wui.Framework.Gui.Interfaces.Events.ITextAreaEvents;

    export interface ITextArea extends Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject {
        GuiType($textAreaType? : any) : any;

        Value($value? : string) : string;

        Width($value? : number) : number;

        MaxWidth($value? : number) : number;

        Height($value? : number) : number;

        MaxHeight($value? : number) : number;

        ResizeableType($type? : ResizeableType) : ResizeableType;

        ReadOnly($value? : boolean) : boolean;

        LengthLimit($value? : number) : number;

        Hint($value? : string) : string;

        getEvents() : ITextAreaEvents;
    }
}
