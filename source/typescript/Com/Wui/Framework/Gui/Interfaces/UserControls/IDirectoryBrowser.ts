/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    import IFileSystemItemProtocol = Com.Wui.Framework.Commons.Interfaces.IFileSystemItemProtocol;
    import FileSystemItemType = Com.Wui.Framework.Commons.Enums.FileSystemItemType;
    import IDirectoryBrowserEvents = Com.Wui.Framework.Gui.Interfaces.Events.IDirectoryBrowserEvents;
    import FileSystemFilter = Com.Wui.Framework.Commons.Utils.FileSystemFilter;

    export interface IDirectoryBrowser extends Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject {
        Path($value? : string) : string;

        Filter($value? : Array<string | FileSystemFilter | FileSystemItemType>) : string;

        setStructure($data : IFileSystemItemProtocol[], $path? : string) : void;

        Clear() : void;

        getEvents() : IDirectoryBrowserEvents;

        Value($value? : string) : string;
    }
}
