/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    import IDropDownListEvents = Com.Wui.Framework.Gui.Interfaces.Events.IDropDownListEvents;

    export interface IDropDownList extends Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject {
        GuiType($dropDownListType? : any) : any;

        Hint($value? : string) : string;

        Value($value? : string | number) : string;

        Width($value? : number) : number;

        Height($value? : number) : number;

        MaxVisibleItemsCount($value? : number) : number;

        Add($text : string, $value? : string | number, $styleName? : string) : void;

        Select($value : string | number) : void;

        Clear() : void;

        ShowField($value? : boolean) : boolean;

        ShowButton($value? : boolean) : boolean;

        getItemsCount() : number;

        getEvents() : IDropDownListEvents;
    }
}
