/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    import ImageOutputArgs = Com.Wui.Framework.Gui.ImageProcessor.ImageOutputArgs;

    export interface IImage extends Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseGuiObject {
        Source($value? : any) : any;

        GuiType($imageType? : any) : any;

        Link($value? : string, $openNewWindow? : boolean) : void;

        LoadingSpinnerEnabled($value? : boolean) : boolean;

        OpacityShowEnabled($value? : boolean) : boolean;

        setSize($width : number, $height : number) : void;

        getWidth() : number;

        getHeight() : number;

        IsSelected($value? : boolean) : boolean;

        TabIndex($value? : number) : number;

        getStream() : string;

        OutputArgs($value? : ImageOutputArgs) : ImageOutputArgs;

        Version($value? : number) : number;
    }
}
