/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    import IBasePanelViewer = Com.Wui.Framework.Gui.Interfaces.Primitives.IBasePanelViewer;
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import IDialogEvents = Com.Wui.Framework.Gui.Interfaces.Events.IDialogEvents;

    export interface IDialog extends Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject {
        GuiType($dialogType? : any) : any;

        TopOffset($value? : number) : number;

        MaxWidth($value? : number) : number;

        MaxHeight($value? : number) : number;

        AutoResize($value? : boolean) : boolean;

        AutoCenter($value? : boolean) : boolean;

        ResizeableType($type? : ResizeableType) : ResizeableType;

        Modal($value? : boolean) : boolean;

        Draggable($value? : boolean) : boolean;

        PanelViewer($instance? : IBasePanelViewer) : IBasePanelViewer;

        PanelViewerArgs($args? : BasePanelViewerArgs) : BasePanelViewerArgs;

        Width($value? : number) : number;

        Height($value? : number) : number;

        getEvents() : IDialogEvents;
    }
}
