/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export interface ITextField extends Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject {
        GuiType($textFieldType? : any) : any;

        Value($value? : string) : string;

        Width($value? : number) : number;

        setAutocompleteDisable() : void;

        ReadOnly($value? : boolean) : boolean;

        setPasswordEnabled() : void;

        setOnlyNumbersAllowed() : void;

        IconName($value? : any) : any;

        Hint($value? : string) : string;

        LengthLimit($value? : number) : number;

        setAutocompleteData($value : ArrayList<any>) : void;
    }
}
