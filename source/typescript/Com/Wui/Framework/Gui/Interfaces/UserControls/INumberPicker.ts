/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.UserControls {
    "use strict";

    export interface INumberPicker extends Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject {
        GuiType($numberPickerType? : any) : any;

        Value($value? : number) : number;

        ValueStep($value? : number) : number;

        RangeStart($value? : number) : number;

        RangeEnd($value? : number) : number;

        Width($value? : number) : number;

        Text($value? : string) : string;

        TabIndex($value? : number) : number;

        DecimalPlaces($value? : number) : number;
    }
}
