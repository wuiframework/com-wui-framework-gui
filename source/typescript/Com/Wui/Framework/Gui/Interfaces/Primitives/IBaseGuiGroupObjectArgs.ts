/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export interface IBaseGuiGroupObjectArgs extends Com.Wui.Framework.Commons.Interfaces.IBaseObject {
        Visible($value? : boolean) : boolean;

        ForceSetValue($value? : boolean) : boolean;

        Value($value? : string | number | boolean) : string | number | boolean;

        Enabled($value? : boolean) : boolean;

        Error($value? : boolean) : boolean;

        AddGuiOption($value : GuiOptionType) : void;

        getGuiOptionsList() : ArrayList<GuiOptionType>;

        TitleText($value? : string) : string;

        Width($value? : number) : number;

        Resize($value? : boolean) : boolean;
    }
}
