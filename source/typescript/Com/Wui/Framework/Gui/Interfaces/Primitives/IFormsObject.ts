/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    import INotification = Com.Wui.Framework.Gui.Interfaces.Components.INotification;
    import IFormsObjectEvents = Com.Wui.Framework.Gui.Interfaces.Events.IFormsObjectEvents;

    export interface IFormsObject extends IBaseGuiObject {
        Notification() : INotification;

        Error($value? : boolean) : boolean;

        TabIndex($value? : number) : number;

        getSelectorEvents() : IFormsObjectEvents;

        getEvents() : IFormsObjectEvents;

        IsPersistent($value? : boolean) : boolean;
    }
}
