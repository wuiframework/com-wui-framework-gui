/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    import BaseViewerLayerType = Com.Wui.Framework.Gui.Enums.BaseViewerLayerType;

    export interface IBaseViewer extends Com.Wui.Framework.Commons.Interfaces.IBaseObject {
        InstanceOwner($value? : IBaseViewer) : IBaseViewer;

        IsCached($value? : boolean) : boolean;

        IsPrinted() : boolean;

        TestModeEnabled($value? : boolean) : boolean;

        ViewerArgs($args? : IBaseViewerArgs) : IBaseViewerArgs;

        getInstance() : IBaseGuiObject;

        setLayer($type : BaseViewerLayerType) : void;

        getLayersType() : string;

        PrepareImplementation() : void;

        Show($EOL? : string) : string;
    }
}
