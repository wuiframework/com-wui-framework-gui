/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    import BaseEnum = Com.Wui.Framework.Commons.Primitives.BaseEnum;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IArrayList = Com.Wui.Framework.Commons.Interfaces.IArrayList;

    export interface IGuiElement extends Com.Wui.Framework.Commons.Interfaces.IBaseObject {
        Id($value : string) : IGuiElement;

        getId() : string;

        GuiTypeTag($value : string) : IGuiElement;

        StyleClassName($value : string | BaseEnum) : IGuiElement;

        Visible($value : boolean) : IGuiElement;

        getVisible() : boolean;

        Width($value : number) : IGuiElement;

        getWidth() : number;

        Height($value : number) : IGuiElement;

        getHeight() : number;

        setAttribute($key : string, $value : string) : IGuiElement;

        Add($value : string | IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer | HTMLElement) : IGuiElement;

        addChildElement($value : string | IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer | HTMLElement) : void;

        getChildElements() : IArrayList<string | IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer | HTMLElement>;

        getChildElement($index : number) : string | IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer | HTMLElement;

        getGuiChildElements() : IArrayList<IGuiCommons | IGuiElement | IResponsiveElement | IBaseViewer>;

        getGuiTypeTag() : string;

        getAttributes() : ArrayList<string>;

        Draw($EOL : string) : string;

        ToDOMElement($EOL : string) : HTMLElement;

        getWrappingElement() : IGuiElement;

        setWrappingElement($value : IGuiElement) : void;
    }
}
