/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import BaseEnum = Com.Wui.Framework.Commons.Primitives.BaseEnum;
    import VisibilityStrategy = Com.Wui.Framework.Gui.Enums.VisibilityStrategy;

    export interface IResponsiveElement extends Com.Wui.Framework.Commons.Interfaces.IBaseObject {

        StyleClassName($value : string | BaseEnum) : IGuiElement;

        WidthOfColumn($value : string, $isPropagated? : boolean) : IResponsiveElement;

        WidthOfColumn($value : () => PropagableNumber) : IResponsiveElement;

        WidthOfColumn($value : number, $unitType : UnitType, $isPropagated? : boolean) : IResponsiveElement;

        HeightOfRow($value : string, $isPropagated? : boolean) : IResponsiveElement;

        HeightOfRow($value : () => PropagableNumber) : IResponsiveElement;

        HeightOfRow($value : number, $unitType : UnitType, $isPropagated? : boolean) : IResponsiveElement;

        getWidthOfColumn() : PropagableNumber;

        getHeightOfRow() : PropagableNumber;

        Alignment($value : Alignment | (() => Alignment)) : IResponsiveElement;

        getAlignment() : Alignment;

        FitToParent($value : FitToParent | (() => FitToParent)) : IResponsiveElement;

        getFitToParent() : FitToParent;

        VisibilityStrategy($value : VisibilityStrategy | (() => VisibilityStrategy)) : IResponsiveElement;

        getVisibilityStrategy() : VisibilityStrategy;

        Add($value : string | IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer | HTMLElement) : IGuiElement;
    }
}
