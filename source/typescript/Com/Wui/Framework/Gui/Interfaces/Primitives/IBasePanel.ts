/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    import IIcon = Com.Wui.Framework.Gui.Interfaces.UserControls.IIcon;
    import ILabel = Com.Wui.Framework.Gui.Interfaces.UserControls.ILabel;
    import PanelContentType = Com.Wui.Framework.Gui.Enums.PanelContentType;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IBasePanelEvents = Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelEvents;
    import IArrayList = Com.Wui.Framework.Commons.Interfaces.IArrayList;

    export interface IBasePanel extends IFormsObject {
        loaderIcon : IIcon;
        loaderText : ILabel;

        AddChild($element : IGuiCommons, $variableName? : string) : void;

        getChildPanelList() : ArrayList<IBasePanelViewer>;

        setChildPanelArgs($instanceOrId : string | IBasePanel, $args : IBasePanelViewerArgs) : void;

        ContentType($type? : PanelContentType) : PanelContentType;

        Scrollable($value? : boolean) : boolean;

        Width($value? : number) : number;

        Height($value? : number) : number;

        getScrollTop() : number;

        getScrollLeft() : number;

        getEvents() : IBasePanelEvents;

        Value($value? : string | IBasePanelViewerArgs) : string | IBasePanelViewerArgs;
    }

    export class IContainerSizeInfo {
        public elementsWithoutWidth : IArrayList<IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer>;
        public elementsWithoutHeight : IArrayList<IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer>;
        public remainingWidth : number;
        public remainingHeight : number;
    }

    export class IContainerVisibilityInfo {
        public commonsNum : number;
        public visibleCommons : number;
    }
}
