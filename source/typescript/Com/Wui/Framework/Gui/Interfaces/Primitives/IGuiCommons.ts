/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.Primitives {
    "use strict";
    import IArrayList = Com.Wui.Framework.Commons.Interfaces.IArrayList;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import PositionType = Com.Wui.Framework.Gui.Enums.PositionType;
    import IGuiCommonsEvents = Com.Wui.Framework.Gui.Interfaces.Events.IGuiCommonsEvents;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;

    export interface IGuiCommons extends Com.Wui.Framework.Commons.Interfaces.IBaseObject {
        getGuiOptions() : GuiOptionsManager;

        getEvents() : IGuiCommonsEvents;

        getGuiElementClass() : any;

        Visible($value? : boolean) : boolean;

        Enabled($value? : boolean) : boolean;

        StyleClassName($value? : string) : string;

        Id() : string;

        Parent($value? : IGuiCommons) : IGuiCommons;

        InstanceOwner($value? : IBaseViewer) : IBaseViewer;

        InstancePath($value? : string) : string;

        getChildElements() : IArrayList<IGuiCommons>;

        getScreenPosition() : ElementOffset;

        setPosition($top : number, $left : number, $type? : PositionType) : void;

        getSize() : Size;

        Width($value? : number) : number;

        Height($value? : number) : number;

        FitToParent($value : FitToParent) : IGuiCommons;

        getFitToParent() : FitToParent;

        IsCached() : boolean;

        IsPrepared() : boolean;

        IsLoaded() : boolean;

        IsCompleted() : boolean;

        DisableAsynchronousDraw() : void;

        Draw($EOL? : string) : string;

        getArgs() : IGuiCommonsArg[];

        setArg($value : IGuiCommonsArg, $force? : boolean) : void;

        getInnerHtmlMap() : IGuiElement;

        getWrappingElement() : IGuiElement;

        setWrappingElement($value : IGuiElement) : void;

        IsPreventingScroll() : boolean;
    }
}
