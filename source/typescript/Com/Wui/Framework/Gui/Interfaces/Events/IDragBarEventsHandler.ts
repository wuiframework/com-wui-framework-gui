/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import MoveEventArgs = Com.Wui.Framework.Gui.Events.Args.MoveEventArgs;

    export interface IDragBarEventsHandler extends IEventsHandler {
        /* tslint:disable: no-duplicate-variable */
        ($eventArgs : MoveEventArgs, $guiObjectManager : GuiObjectManager, ...$args : any[]) : void;

        ($eventArgs : MoveEventArgs, $guiObjectManager : GuiObjectManager, $reflection : Reflection) : void;

        /* tslint:enable */
    }
}
