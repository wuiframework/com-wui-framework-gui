/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";
    import IArrayList = Com.Wui.Framework.Commons.Interfaces.IArrayList;

    export interface IGuiCommonsEvents extends Com.Wui.Framework.Commons.Interfaces.IBaseObject {
        getOwner() : string;

        Subscriber($value? : string) : string;

        Exists($type : string) : boolean;

        getAll() : IArrayList<IArrayList<IEventsHandler>>;

        setEvent($type : string, $handler : IEventsHandler) : void;

        /* tslint:disable: unified-signatures */
        FireAsynchronousMethod($handler : () => void, $clearBefore? : boolean) : number;

        FireAsynchronousMethod($handler : () => void, $waitForMilliseconds? : number) : number;

        FireAsynchronousMethod($handler : () => void, $clearBefore? : boolean, $waitForMilliseconds? : number) : number;

        /* tslint:enable */

        RemoveHandler($type : string, $handler : IEventsHandler) : void;

        Clear($type? : string) : void;

        /* tslint:disable: unified-signatures */
        Subscribe($force? : boolean) : void;

        Subscribe($targetId? : string, $force? : boolean) : void;

        /* tslint:enable */

        setOnStart($handler : IEventsHandler) : void;

        setBeforeLoad($handler : IEventsHandler) : void;

        setOnLoad($handler : IEventsHandler) : void;

        setOnComplete($handler : IEventsHandler) : void;

        setOnShow($handler : IEventsHandler) : void;

        setOnHide($handler : IEventsHandler) : void;

        setOnMouseOver($handler : IMouseEventsHandler) : void;

        setOnMouseOut($handler : IMouseEventsHandler) : void;

        setOnMouseMove($handler : IMouseEventsHandler) : void;

        setOnMouseDown($handler : IMouseEventsHandler) : void;

        setOnMouseUp($handler : IMouseEventsHandler) : void;

        setOnClick($handler : IMouseEventsHandler) : void;

        setOnDoubleClick($handler : IMouseEventsHandler) : void;
    }
}
