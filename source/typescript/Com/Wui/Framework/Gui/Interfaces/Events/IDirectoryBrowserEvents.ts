/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.Events {
    "use strict";

    export interface IDirectoryBrowserEvents extends IBasePanelEvents {
        setOnChange($handler : IDirectoryBrowserEventsHandler) : void;

        setOnPathRequest($handler : IDirectoryBrowserEventsHandler) : void;

        setOnDirectoryRequest($handler : IDirectoryBrowserEventsHandler) : void;

        setOnCreateDirectoryRequest($handler : IDirectoryBrowserEventsHandler) : void;

        setOnRenameRequest($handler : IDirectoryBrowserEventsHandler) : void;

        setOnDeleteRequest($handler : IDirectoryBrowserEventsHandler) : void;
    }
}
