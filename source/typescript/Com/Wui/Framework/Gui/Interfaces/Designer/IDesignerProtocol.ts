/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.Designer {
    "use strict";
    import DesignerTaskType = Com.Wui.Framework.Gui.Enums.DesignerTaskType;
    import GuiCommonsArgType = Com.Wui.Framework.Gui.Enums.GuiCommonsArgType;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;

    export abstract class IDesignerItemProtocol {
        public id : string;
        public top : number;
        public left : number;
        public width : number;
        public height : number;
        public zoom : number;
        public draggable : boolean;
        public resizeable : boolean;
    }

    export abstract class IDesignerItemArgProtocol {
        public id : string;
        public name : string;
        public type : GuiCommonsArgType;
        public value : string | number | boolean;
    }

    export abstract class IDesignerItemArgsProtocol {
        public id : string;
        public value : IGuiCommonsArg[];
    }

    export abstract class IDesignerEventProtocol {
        public type : string;
        public args : string;
    }

    export abstract class IDesignerProjectMapItems {
        public names : string[];
    }

    export abstract class IDesignerProjectMapViewers extends IDesignerProjectMapItems {
        public links : string[];
    }

    export abstract class IDesignerProjectMapGuiObjects {
        public components : IDesignerProjectMapItems;
        public userControls : IDesignerProjectMapItems;
        public panels : IDesignerProjectMapItems;
    }

    export abstract class IDesignerProjectMapProtocol {
        public host : string;
        public blankViewer : string;
        public viewers : IDesignerProjectMapViewers;
        public runtimeTests : IDesignerProjectMapViewers;
        public guiObjects : IDesignerProjectMapGuiObjects;
    }

    export abstract class IDesignerInstanceMapItem extends IDesignerElementMap {
        public top : number;
        public left : number;
        public width : number;
        public height : number;
    }

    export abstract class IDesignerProtocol {
        public task : DesignerTaskType;
        public data : any;
    }
}
