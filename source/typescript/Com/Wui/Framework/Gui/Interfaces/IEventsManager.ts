/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces {
    "use strict";
    import IArrayList = Com.Wui.Framework.Commons.Interfaces.IArrayList;
    import IEventArgs = Com.Wui.Framework.Commons.Interfaces.IEventArgs;

    export interface IEventsManager extends Com.Wui.Framework.Commons.Interfaces.IEventsManager {
        getAll() : IArrayList<IArrayList<IArrayList<IEventsHandler>>>;

        setEventArgs($owner : string | Primitives.IGuiCommons, $type : string, $args : IEventArgs) : void;

        setEvent($owner : string | Primitives.IGuiCommons, $type : string, $handler : IEventsHandler,
                 $args? : IEventArgs) : void;

        FireEvent($owner : string | Primitives.IGuiCommons, $type : string, $args? : any, $async? : boolean) : void;

        RemoveHandler($owner : string | Primitives.IGuiCommons, $type : string, $handler : IEventsHandler) : void;

        Exists($owner : string | Primitives.IGuiCommons, $type : string) : boolean;
    }
}
