/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.Components {
    "use strict";
    import IFileUploadEvents = Com.Wui.Framework.Gui.Interfaces.Events.IFileUploadEvents;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export interface IFileUpload extends Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons {
        getEvents() : IFileUploadEvents;

        setOpenElement($id : string | HTMLElement | IGuiCommons) : void;

        setDropZone($id : string | HTMLElement | IGuiCommons) : void;

        getStream($onSuccess : ($data : ArrayList<string>) => void) : void;

        Value() : string;

        Upload() : void;

        Aboard($fileHandle? : number) : void;

        MultipleSelectEnabled($value? : boolean) : boolean;

        Filter(...$value : string[]) : string;

        MaxFileSize($value? : number) : number;

        MaxChunkSize($value? : number) : number;
    }

    export class IFileUploadProtocol {
        public id : string;
        public index : number;
        public name : string;
        public type : string;
        public error : number;
        public start : number;
        public end : number;
        public size : number;
        public data? : string;
    }
}
