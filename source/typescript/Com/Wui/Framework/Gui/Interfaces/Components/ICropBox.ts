/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.Components {
    "use strict";
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import ICropBoxEvents = Com.Wui.Framework.Gui.Interfaces.Events.ICropBoxEvents;

    export interface ICropBox extends Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons {
        GuiType($cropBoxType? : any) : any;

        setDimensions($topX : number, $topY : number, $bottomX : number, $bottomY : number) : void;

        setSize($width : number, $height : number) : void;

        EnvelopOwner($owner? : string | IGuiCommons) : string;

        getEvents() : ICropBoxEvents;
    }
}
