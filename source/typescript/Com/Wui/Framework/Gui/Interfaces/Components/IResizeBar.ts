/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.Components {
    "use strict";
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import IResizeBarEvents = Com.Wui.Framework.Gui.Interfaces.Events.IResizeBarEvents;

    export interface IResizeBar extends Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons {
        ResizeableType($notificationType? : ResizeableType) : ResizeableType;

        getEvents() : IResizeBarEvents;
    }
}
