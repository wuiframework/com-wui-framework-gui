/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Interfaces.Components {
    "use strict";
    import OrientationType = Com.Wui.Framework.Gui.Enums.OrientationType;
    import IScrollBarEvents = Com.Wui.Framework.Gui.Interfaces.Events.IScrollBarEvents;

    export interface IScrollBar extends Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons {
        GuiType($orientationType? : OrientationType) : OrientationType;

        OrientationType($orientationType? : OrientationType) : OrientationType;

        Size($value? : number) : number;

        getEvents() : IScrollBarEvents;
    }
}
