/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Utils {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import BrowserType = Com.Wui.Framework.Commons.Enums.BrowserType;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import ValueProgressEventArgs = Com.Wui.Framework.Gui.Events.Args.ValueProgressEventArgs;
    import ProgressType = Com.Wui.Framework.Gui.Enums.ProgressType;
    import OpacityEventType = Com.Wui.Framework.Gui.Enums.Events.OpacityEventType;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import EventsManager = Com.Wui.Framework.Gui.Events.EventsManager;

    /**
     * ElementManager class provides static methods focused on handling of HTML elements.
     */
    export class ElementManager extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private static domCache : ArrayList<HTMLElement>;
        private static cssRules : CSSRuleList;

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element object or id, which should be looked up.
         * @param {boolean} [$force=false] Get fresh object instance for DOM.
         * @return {HTMLElement} Returns instance of DOM element, if element has been found.
         */
        public static getElement($id : string | HTMLElement | IGuiCommons, $force : boolean = false) : HTMLElement {
            if (ObjectValidator.IsEmptyOrNull($id)) {
                return null;
            }
            if (ObjectValidator.IsString($id)) {
                if ($force === true) {
                    this.CleanElementCache(<string>$id);
                }
                if (!ObjectValidator.IsSet(this.domCache)) {
                    this.domCache = new ArrayList<HTMLElement>();
                }
                if (!this.domCache.KeyExists(<string>$id)) {
                    const element : HTMLElement = document.getElementById(<string>$id);
                    if (!ObjectValidator.IsEmptyOrNull(element)) {
                        this.domCache.Add(element, <string>$id);
                        return element;
                    }
                    return null;
                }
                return this.domCache.getItem(<string>$id);
            } else if (ObjectValidator.IsFunction((<IGuiCommons>$id).Id)) {
                return this.getElement((<IGuiCommons>$id).Id());
            }
            return <HTMLElement>$id;
        }

        /**
         * @param {string} $idOrPattern Element id or pattern for element ids, which should be removed from DOM cache.
         * @return {void}
         */
        public static CleanElementCache($idOrPattern : string) : void {
            if (!ObjectValidator.IsEmptyOrNull(this.domCache)) {
                if (StringUtils.Contains($idOrPattern, "*")) {
                    let index : number;
                    let newIndex : number = 0;
                    const data : HTMLElement[] = [];
                    const keys : string[] = [];
                    for (index = 0; index < (<any>this.domCache).size; index++) {
                        if (!StringUtils.PatternMatched($idOrPattern, (<any>this.domCache).keys[index])) {
                            data[newIndex] = (<any>this.domCache).data[index];
                            keys[newIndex] = (<any>this.domCache).keys[index];
                            newIndex++;
                        }
                    }
                    (<any>this.domCache).data = data;
                    (<any>this.domCache).keys = keys;
                    (<any>this.domCache).size = newIndex;
                } else if (this.domCache.KeyExists($idOrPattern)) {
                    this.domCache.RemoveAt(this.domCache.IndexOf(this.domCache.getItem($idOrPattern)));
                }
            }
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element object or id, which should be validated.
         * @param {boolean} [$force=false] Get fresh object instance for DOM.
         * @return {boolean} Returns true, if element has been rendered, otherwise false.
         */
        public static Exists($id : string | HTMLElement | IGuiCommons, $force : boolean = false) : boolean {
            return !ObjectValidator.IsEmptyOrNull(this.getElement($id, $force));
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be processed.
         * @return {string} Returns element's class name,
         * if element has been rendered and class name has been registered, otherwise null.
         */
        public static getClassName($id : string | HTMLElement | IGuiCommons) : string {
            const element : HTMLElement = this.getElement($id);
            if (this.Exists(element)) {
                return element.className;
            }
            return null;
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @param {string} $value Class name, which should be set to the element.
         * @return {void}
         */
        public static setClassName($id : string | HTMLElement | IGuiCommons, $value : string) : void {
            const element : HTMLElement = this.getElement($id);
            if (this.Exists(element)) {
                element.className = $value;
            }
        }

        /**
         * @param {string|IGuiCommons} $id Element id or object, which should be processed.
         * @param {string} $property Property name, which should be found.
         * @param {boolean} [$force=false] Specify if returned value should be up-to-date (slower - needs layout recalculation).
         * @return {string} Returns value associated with the property name,
         * if element and property exists, otherwise null.
         */
        public static getCssValue($id : string | IGuiCommons, $property : string, $force : boolean = false) : string {
            let css : string;
            const element : any = this.getElement($id);
            const propertyName : string = StringUtils.ToCamelCase($property);
            if (this.Exists(element)) {
                if (ObjectValidator.IsEmptyOrNull(element.cachedStyle)) {
                    element.cachedStyle = {};
                }

                css = element.style[propertyName];
                if (!ObjectValidator.IsEmptyOrNull(css)) {
                    return css;
                }

                if (!$force) {
                    css = element.cachedStyle[propertyName];
                    if (!ObjectValidator.IsEmptyOrNull(css)) {
                        return css;
                    }
                }

                const cssProperties : CSSStyleDeclaration = this.getRuntimeCss(element);
                if (!ObjectValidator.IsEmptyOrNull(cssProperties)) {
                    if (ObjectValidator.IsSet(cssProperties.getPropertyValue)) {
                        css = cssProperties.getPropertyValue($property);
                    } else {
                        if ($property === "float") {
                            $property = "styleFloat";
                        }
                        css = cssProperties[$property];
                    }
                }
                if (!ObjectValidator.IsEmptyOrNull(css)) {
                    element.cachedStyle[propertyName] = css;
                    return css;
                }
                css = element.style.cssText;
            }
            if (ObjectValidator.IsEmptyOrNull(css) && ObjectValidator.IsString($id)) {
                let className : string = "";
                if (this.Exists(element)) {
                    className = "." + element.className;
                    if (className === ".") {
                        className = "#" + $id;
                    }
                } else {
                    className = "." + $id;
                }
                css = this.getCssClassText(className);
                if (ObjectValidator.IsEmptyOrNull(css)) {
                    className = "#" + $id;
                    css = this.getCssClassText(className);
                }

                if (ObjectValidator.IsEmptyOrNull(css)) {
                    css = this.getCssClassText(<string>$id);
                }
            }
            if (!ObjectValidator.IsEmptyOrNull(css)) {
                const split1 : string[] = StringUtils.Split(css, "; ");
                let index : number;
                const split1Length : number = split1.length;
                for (index = 0; index < split1Length; index++) {
                    const split2 : string[] = StringUtils.Split(split1[index], ": ");
                    if (<number>split2.length === 2) {
                        if (StringUtils.ToUpperCase(split2[0]) === StringUtils.ToUpperCase($property)) {
                            css = StringUtils.Remove(split2[1], ";");
                            element.cachedStyle[propertyName] = css;
                            return css;
                        }
                    }
                }
            }

            return null;
        }

        /**
         * @param {string|IGuiCommons} $id Element id or class name, which should be processed.
         * @param {string} $property Property name, which should be found.
         * @param {boolean} [$force=false] Specify if returned value should be up-to-date (slower - needs layout recalculation).
         * @return {number} Returns integer value associated with the property name,
         * if element and property exists, otherwise null.
         */
        public static getCssIntegerValue($id : string | IGuiCommons, $property : string, $force : boolean = false) : number {
            const output : number = Convert.StringToDouble(StringUtils.Remove(this.getCssValue($id, $property, $force), "px"));
            if (ObjectValidator.IsDigit(output)) {
                return Math.round(output);
            }
            return null;
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @param {string} $property Property name, which should be set.
         * @param {string|number} $value Value, which should be associated with the property name.
         * @return {void}
         */
        public static setCssProperty($id : string | HTMLElement | IGuiCommons, $property : string, $value : string | number) : void {
            const element : HTMLElement = this.getElement($id);
            if (this.Exists(element)) {
                const value : string = <string>(ObjectValidator.IsString($value) ? $value : $value + "px");
                if (Loader.getInstance().getHttpManager().getRequest().getBrowserType() === BrowserType.INTERNET_EXPLORER) {
                    let split1 : string[] = [];
                    let split2 : string[] = [];
                    let newCss : string = "";
                    let oldCss : string = element.style.cssText;
                    if (ObjectValidator.IsEmptyOrNull(oldCss)) {
                        let className : string = "." + element.className;
                        if (className === "." && ObjectValidator.IsString($id)) {
                            className = "#" + $id;
                        }
                        oldCss = this.getCssClassText(className);
                        if (oldCss === null) {
                            oldCss = "";
                        }
                    }

                    if (!ObjectValidator.IsEmptyOrNull(oldCss)) {
                        let propertyFound : boolean = false;
                        split1 = StringUtils.Split(oldCss, "; ");
                        let index : number;
                        const split1Length : number = split1.length;
                        for (index = 0; index < split1Length; index++) {
                            split2 = StringUtils.Split(split1[index], ": ");
                            if (<number>split2.length === 2) {
                                if (StringUtils.ToUpperCase(split2[0]) === StringUtils.ToUpperCase($property)) {
                                    split2[1] = value;
                                    propertyFound = true;
                                }
                                newCss += StringUtils.ToUpperCase(split2[0]) + ": " + split2[1] + "; ";
                            }
                        }
                        if (!propertyFound) {
                            newCss += StringUtils.ToUpperCase($property) + ": " + value + "; ";
                        }
                        element.style.cssText = newCss;
                    } else {
                        element.style.cssText = StringUtils.ToUpperCase($property) + ": " + value + "; ";
                    }
                } else {
                    element.style.setProperty($property, value, null);
                    ElementManager.setCssProperty =
                        ($id : string | HTMLElement | IGuiCommons, $property : string, $value : string | number) : void => {
                            const element : HTMLElement = ElementManager.getElement($id);
                            if (this.Exists(element)) {
                                const value : string = <string>(ObjectValidator.IsString($value) ? $value : $value + "px");
                                element.style.setProperty($property, value, null);
                            }
                        };
                }
            }
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @param {string} $property Property name, which should be cleaned up.
         * @return {void}
         */
        public static ClearCssProperty($id : string | HTMLElement | IGuiCommons, $property : string) : void {
            const element : any = this.getElement($id);
            if (this.Exists(element)) {
                const jsProperty : string = StringUtils.ToCamelCase($property);
                if (!ObjectValidator.IsEmptyOrNull(element.style[jsProperty])) {
                    element.style[jsProperty] = "";
                }
                if (!ObjectValidator.IsEmptyOrNull(element.cachedStyle)) {
                    element.cachedStyle[jsProperty] = "";
                }
                let split1 : string[] = [];
                let split2 : string[] = [];
                let newCss : string = "";
                let oldCss : string = element.style.cssText;
                if (ObjectValidator.IsEmptyOrNull(oldCss)) {
                    let className : string = "." + element.className;
                    if (className === "." && ObjectValidator.IsString($id)) {
                        className = "#" + $id;
                    }
                    oldCss = this.getCssClassText(className);
                    if (oldCss === null) {
                        oldCss = "";
                    }
                }

                if (!ObjectValidator.IsEmptyOrNull(oldCss)) {
                    split1 = StringUtils.Split(oldCss, "; ");
                    let index : number;
                    const split1Length : number = split1.length;
                    for (index = 0; index < split1Length; index++) {
                        split2 = StringUtils.Split(split1[index], ": ");
                        if (<number>split2.length === 2) {
                            if (StringUtils.ToUpperCase(split2[0]) !== StringUtils.ToUpperCase($property)) {
                                newCss += StringUtils.ToUpperCase(split2[0]) + ": " + split2[1] + "; ";
                            }
                        }
                    }
                    element.style.cssText = newCss;
                }
            }
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be processed.
         * @return {string} Returns content of the element, if element has been found, otherwise null.
         */
        public static getInnerHtml($id : string | HTMLElement | IGuiCommons) : string {
            const element : HTMLElement = this.getElement($id);
            if (this.Exists(element)) {
                return element.innerHTML;
            }
            return null;
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @param {string} $value Element's content, which should be set.
         * @return {void}
         */
        public static setInnerHtml($id : string | HTMLElement | IGuiCommons, $value : string) : void {
            const element : HTMLElement = this.getElement($id);
            if (this.Exists(element)) {
                element.innerHTML = $value;
            }
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @param {string} $value New content, which should be appended to current element's content.
         * @return {void}
         */
        public static AppendHtml($id : string | HTMLElement | IGuiCommons, $value : string) : void {
            const target : HTMLElement = this.getElement($id);
            if (this.Exists(target)) {
                const element : HTMLElement = document.createElement("span");
                if (StringUtils.StartsWith($value, " ")) {
                    $value = StringUtils.Substring($value, 1, StringUtils.Length($value));
                    $value = StringUtils.Space(1) + $value;
                }
                element.setAttribute("guiType", "HtmlAppender");
                element.innerHTML = $value;
                target.appendChild(element);
            }
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @param {string} $value New content, which should be prepended to current element's content.
         * @return {void}
         */
        public static PrependHtml($id : string | HTMLElement | IGuiCommons, $value : string) : void {
            const target : HTMLElement = this.getElement($id);
            if (this.Exists(target)) {
                const element : HTMLElement = document.createElement("span");
                if (StringUtils.StartsWith($value, " ")) {
                    $value = StringUtils.Substring($value, 1, StringUtils.Length($value));
                    $value = StringUtils.Space(1) + $value;
                }
                element.setAttribute("guiType", "HtmlAppender");
                element.innerHTML = $value;
                if (ObjectValidator.IsSet(target.firstChild)) {
                    target.insertBefore(element, target.firstChild);
                } else {
                    target.appendChild(element);
                }
            }
        }

        /**
         * @param {string|IGuiCommons} $id Element id or object, which should be processed.
         * @return {boolean} Returns true, if element exist and is visible on the screen.
         */
        public static IsVisible($id : string | IGuiCommons) : boolean {
            if (!this.Exists($id)) {
                return false;
            }
            const value : string = this.getCssValue($id, "display");
            return ObjectValidator.IsEmptyOrNull(value) || value === "block";
        }

        /**
         * Enable visibility of the element on the screen.
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @return {void}
         */
        public static Show($id : string | HTMLElement | IGuiCommons) : void {
            this.setCssProperty($id, "display", "block");
        }

        /**
         * Disable visibility of the element on the screen.
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @return {void}
         */
        public static Hide($id : string | HTMLElement | IGuiCommons) : void {
            this.setCssProperty($id, "display", "none");
        }

        /**
         * Enable or disable visibility of the element on the screen based on current element's visibility.
         * @param {string|IGuiCommons} $id Element id or object, which should be handled.
         * @return {void}
         */
        public static ToggleVisibility($id : string | IGuiCommons) : void {
            if (this.IsVisible($id)) {
                this.Hide($id);
            } else {
                this.Show($id);
            }
        }

        /**
         * Set general css class name "On" to the handled element.
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @return {void}
         */
        public static TurnOn($id : string | HTMLElement | IGuiCommons) : void {
            this.setClassName($id, GeneralCssNames.ON);
        }

        /**
         * Set general css class name "Off" to the handled element.
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @return {void}
         */
        public static TurnOff($id : string | HTMLElement | IGuiCommons) : void {
            this.setClassName($id, GeneralCssNames.OFF);
        }

        /**
         * Set general css class name "On" or "Off" based on current element's state.
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @return {void}
         */
        public static ToggleOnOff($id : string | HTMLElement | IGuiCommons) : void {
            const element : HTMLElement = this.getElement($id);
            if (this.Exists(element)) {
                if (element.className === GeneralCssNames.ON) {
                    element.className = GeneralCssNames.OFF;
                } else {
                    element.className = GeneralCssNames.ON;
                }
            }
        }

        /**
         * Set general css class name "Active" to the handled element.
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @return {void}
         */
        public static TurnActive($id : string | HTMLElement | IGuiCommons) : void {
            this.setClassName($id, GeneralCssNames.ACTIVE);
        }

        /**
         * Set z-index value to the handled element, so it will be at topmost position.
         * @param {string|IGuiCommons} $id Element id, which should be handled.
         * @return {void}
         */
        public static TopMost($id : string | IGuiCommons) : void {
            if (ObjectValidator.IsEmptyOrNull(this.getCssValue($id, "position"))) {
                this.setCssProperty($id, "position", "fixed");
            }
            this.setCssProperty($id, "z-index", "99999");
        }

        /**
         * Set z-index value to the handled element, so it will be at front position.
         * @param {string|IGuiCommons} $id Element id, which should be handled.
         * @return {void}
         */
        public static BringToFront($id : string | IGuiCommons) : void {
            if (ObjectValidator.IsEmptyOrNull(this.getCssValue($id, "position"))) {
                this.setCssProperty($id, "position", "relative");
            }
            this.setCssProperty($id, "z-index", "1000");
        }

        /**
         * Set z-index value to the handled element, so it will be at back position.
         * @param {string|IGuiCommons} $id Element id, which should be handled.
         * @return {void}
         */
        public static SendToBack($id : string | IGuiCommons) : void {
            if (ObjectValidator.IsEmptyOrNull(this.getCssValue($id, "position"))) {
                this.setCssProperty($id, "position", "relative");
            }
            this.setCssProperty($id, "z-index", "0");
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @param {number} $value Opacity value, which should be set to the handled element.
         * Value can be set in range <0;100> or <0.00;1.00>, otherwise it will be cut to closest limit.
         * @return {void}
         */
        public static setOpacity($id : string | HTMLElement | IGuiCommons, $value : number) : void {
            if (this.Exists($id)) {
                if ($value > 0 && $value < 1) {
                    $value = $value * 100;
                }
                if ($value < 0) {
                    $value = 0;
                }
                if ($value > 100) {
                    $value = 100;
                }
                const floatValue : string = ($value / 100).toFixed(2);
                this.setCssProperty($id, "-ms-filter", "progid:DXImageTransform.Microsoft.Alpha(Opacity=" + $value + ")");
                this.setCssProperty($id, "filter", "alpha(opacity=" + $value + ")");
                this.setCssProperty($id, "-moz-opacity", floatValue);
                this.setCssProperty($id, "-khtml-opacity", floatValue);
                this.setCssProperty($id, "opacity", floatValue);
            }
        }

        /**
         * Asynchronously change element opacity with defined change iterations count.
         * @param {IGuiCommons|string} $elementOrId GUI element or id of DOM element, which should be handled.
         * @param {DirectionType} [$directionType] Set direction in, which should be opacity changed.
         * @param {number} [$iterationCount] Set number or iterations, which should be done by value progress manager.
         * @param {number} [$callback] Set callback that should be invoked after opacity change is complete.
         * @return {void}
         */
        public static ChangeOpacity($elementOrId : IGuiCommons | string, $directionType? : DirectionType,
                                    $iterationCount? : number, $callback? : () => void) : void {
            let managerId : string = <string>$elementOrId;
            if (ObjectValidator.IsSet((<IGuiCommons>$elementOrId).Id)) {
                managerId = (<IGuiCommons>$elementOrId).Id();
            }
            managerId += "_Opacity";

            const manipulatorArgs : ValueProgressEventArgs = ValueProgressManager.get(managerId);
            manipulatorArgs.Owner(managerId);
            manipulatorArgs.DirectionType($directionType);
            if (manipulatorArgs.DirectionType() === DirectionType.UP) {
                manipulatorArgs.ProgressType(ProgressType.FAST_START);
            } else {
                manipulatorArgs.ProgressType(ProgressType.FAST_END);
            }
            manipulatorArgs.Step($iterationCount);
            manipulatorArgs.RangeStart(0);
            manipulatorArgs.RangeEnd(100);
            manipulatorArgs.CurrentValue(
                Math.ceil(StringUtils.ToDouble(this.getCssValue($elementOrId, "opacity", true)) * 100));
            manipulatorArgs.StartEventType(OpacityEventType.START);
            manipulatorArgs.ChangeEventType(OpacityEventType.CHANGE);
            manipulatorArgs.CompleteEventType(OpacityEventType.COMPLETE);

            EventsManager.getInstanceSingleton().setEvent(managerId, OpacityEventType.CHANGE,
                (eventArgs : ValueProgressEventArgs) : void => {
                    ElementManager.setOpacity($elementOrId, eventArgs.CurrentValue());
                });

            EventsManager.getInstanceSingleton().setEvent(managerId, OpacityEventType.COMPLETE, () : void => {
                if (ObjectValidator.IsFunction($callback)) {
                    $callback();
                }
            });
            ValueProgressManager.Execute(manipulatorArgs);
        }

        /**
         * Stop asynchronous change of element opacity, if element opacity is currently handled.
         * @param {IGuiCommons|string} $elementOrId GUI element or id of DOM element, which should be handled.
         * @return {void}
         */
        public static StopOpacityChange($elementOrId : IGuiCommons | string) : void {
            let managerId : string = <string>$elementOrId;
            if (ObjectValidator.IsSet((<IGuiCommons>$elementOrId).Id)) {
                managerId = (<IGuiCommons>$elementOrId).Id();
            }
            ValueProgressManager.Remove(managerId + "_Opacity");
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @param {boolean} $status Set element's visibility mode.
         * @return {void}
         */
        public static Enabled($id : string | HTMLElement | IGuiCommons, $status : boolean) : void {
            if (this.Exists($id)) {
                if ($status === true) {
                    this.Hide($id + "_Disabled");
                    this.Show($id + "_Enabled");
                } else {
                    this.Hide($id + "_Enabled");
                    this.Show($id + "_Disabled");
                }
            }
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be validated.
         * @return {void}
         */
        public static IsEnabled($id : string | HTMLElement | IGuiCommons) : boolean {
            return this.IsVisible($id + "_Enabled");
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @param {number} $value Set element's width value.
         * @return {void}
         */
        public static setWidth($id : string | HTMLElement | IGuiCommons, $value : number) : void {
            this.Hide($id);
            this.setCssProperty($id, "width", $value);
            this.Show($id);
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @param {number} $value Set element's height value.
         * @return {void}
         */
        public static setHeight($id : string | HTMLElement | IGuiCommons, $value : number) : void {
            this.Hide($id);
            this.setCssProperty($id, "height", $value);
            this.Show($id);
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @param {number} $width Set element's width value.
         * @param {number} $height Set element's height value.
         * @return {void}
         */
        public static setSize($id : string | HTMLElement | IGuiCommons, $width : number, $height : number) : void {
            this.Hide($id);
            this.setCssProperty($id, "width", $width);
            this.setCssProperty($id, "height", $height);
            this.Show($id);
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @param {ElementOffset} $position Specify required position.
         * @return {void}
         */
        public static setPosition($id : string | HTMLElement | IGuiCommons, $position : ElementOffset) : void {
            this.setCssProperty($id, "top", $position.Top());
            this.setCssProperty($id, "left", $position.Left());
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be processed.
         * @return {ElementOffset} Returns absolute top and left offset based on current element's position.
         */
        public static getAbsoluteOffset($id : string | HTMLElement | IGuiCommons) : ElementOffset {
            const offset : ElementOffset = new ElementOffset();
            let offsetLeft : number = 0;
            let offsetTop : number = 0;
            let element : HTMLElement = this.getElement($id);
            const getOffset : any = () : void => {
                offsetLeft = 0;
                offsetTop = 0;
                while (!ObjectValidator.IsEmptyOrNull(element)) {
                    if (element.offsetLeft > 0) {
                        offsetLeft += element.offsetLeft;
                    }
                    if (element.offsetTop > 0) {
                        offsetTop += element.offsetTop;
                    }
                    element = <HTMLElement>element.offsetParent;
                }
            };
            getOffset();
            if (Loader.getInstance().getHttpManager().getRequest().getBrowserType() === BrowserType.INTERNET_EXPLORER &&
                Loader.getInstance().getHttpManager().getRequest().getBrowserVersion() <= 7) {
                element = this.getElement($id);
                getOffset();
            }
            offset.Left(offsetLeft);
            offset.Top(offsetTop);

            return offset;
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be processed.
         * @return {ElementOffset} Returns element's position relative to screen top left corner.
         */
        public static getScreenPosition($id : string | HTMLElement | IGuiCommons) : ElementOffset {
            const offset : ElementOffset = new ElementOffset();
            let offsetLeft : number = 0;
            let offsetTop : number = 0;
            let element : HTMLElement = ElementManager.getElement($id);
            let offsetParent : HTMLElement = null;

            while (!ObjectValidator.IsEmptyOrNull(element)) {
                if (element === offsetParent || ObjectValidator.IsEmptyOrNull(offsetParent)) {
                    offsetLeft += element.offsetLeft;
                    offsetTop += element.offsetTop;
                    if (!ObjectValidator.IsEmptyOrNull(element.id)) {
                        offsetLeft += ElementManager.getCssIntegerValue(element.id, "border-left-width");
                        offsetTop += ElementManager.getCssIntegerValue(element.id, "border-top-width");
                    } else {
                        if (!ObjectValidator.IsEmptyOrNull(element.style.borderLeftWidth)) {
                            offsetLeft += StringUtils.ToInteger(StringUtils.Remove(element.style.borderLeftWidth, "px"));
                        }
                        if (!ObjectValidator.IsEmptyOrNull(element.style.borderTopWidth)) {
                            offsetTop += StringUtils.ToInteger(StringUtils.Remove(element.style.borderTopWidth, "px"));
                        }
                    }
                }
                offsetLeft -= element.scrollLeft;
                offsetTop -= element.scrollTop;
                offsetParent = <HTMLElement>element.offsetParent;
                element = <HTMLElement>element.parentElement;
            }
            const browser : BrowserType = Loader.getInstance().getHttpManager().getRequest().getBrowserType();
            if (browser === BrowserType.FIREFOX ||
                browser === BrowserType.INTERNET_EXPLORER && Loader.getInstance().getHttpManager().getRequest().getBrowserVersion() > 7) {
                offsetLeft -= (document.body.scrollLeft === 0 ? document.documentElement.scrollLeft : document.body.scrollLeft);
                offsetTop -= (document.body.scrollTop === 0 ? document.documentElement.scrollTop : document.body.scrollTop);
            }
            offset.Left(offsetLeft);
            offset.Top(offsetTop);
            return offset;
        }

        /**
         * @param {string|IGuiCommons} $id Element id or object, which should be processed.
         * @param {boolean} [$force=false] Specify if returned value should be up-to-date (slower - needs layout recalculation).
         * @param {boolean} [$ignoreHidden=true] Specify if returned value should be 0 if element is hidden.
         * @return {number} Returns total element width - eq. offsetWidth + margin-width.
         */
        public static getEnvelopWidth($id : string | IGuiCommons, $force : boolean = false, $ignoreHidden : boolean = true) : number {
            if (!ElementManager.IsVisible($id) && $ignoreHidden) {
                return 0;
            }
            return this.getCssIntegerValue($id, "width", $force)
                + this.getWidthOffset($id, $force);
        }

        /**
         * @param {string|IGuiCommons} $id Element id or object, which should be processed.
         * @param {boolean} [$force=false] Specify if returned value should be up-to-date (slower - needs layout recalculation).
         * @param {boolean} [$ignoreHidden=true] Specify if returned value should be 0 if element is hidden.
         * @return {number} Returns total element height - eq. offsetHeight + margin-height.
         */
        public static getEnvelopHeight($id : string | IGuiCommons, $force : boolean = false, $ignoreHidden : boolean = true) : number {
            if (!ElementManager.IsVisible($id) && $ignoreHidden) {
                return 0;
            }
            return this.getCssIntegerValue($id, "height", $force)
                + this.getHeightOffset($id, $force);
        }

        /**
         * @param {string|IGuiCommons} $id Element id or object, which should be processed.
         * @param {boolean} [$force=false] Specify if returned value should be up-to-date (slower - needs layout recalculation).
         * @param {boolean} [$ignoreHidden=true] Specify if returned value should be 0 if element is hidden.
         * @return {number} Returns offsetWidth property.
         */
        public static getOffsetWidth($id : string | IGuiCommons, $force : boolean = false, $ignoreHidden : boolean = true) : number {
            if ($force && $ignoreHidden) {
                const prevElement : any = this.getElement($id);
                const element : any = this.getElement($id, true);
                if (ObjectValidator.IsEmptyOrNull(element)) {
                    return 0;
                }
                if (!ObjectValidator.IsEmptyOrNull(prevElement)) {
                    element.cachedStyle = prevElement.cachedStyle;
                }
                return element.offsetWidth;
            } else {
                if (!ElementManager.IsVisible($id) && $ignoreHidden) {
                    return 0;
                }
                return this.getCssIntegerValue($id, "width", false)
                    + this.getWidthOffset($id, false, false);
            }
        }

        /**
         * @param {string|IGuiCommons} $id Element id or object, which should be processed.
         * @param {boolean} [$force=false] Specify if returned value should be up-to-date (slower - needs layout recalculation).
         * @param {boolean} [$ignoreHidden=true] Specify if returned value should be 0 if element is hidden.
         * @return {number} Returns offsetHeight property.
         */
        public static getOffsetHeight($id : string | IGuiCommons, $force : boolean = false, $ignoreHidden : boolean = true) : number {
            if ($force && $ignoreHidden) {
                const prevElement : any = this.getElement($id);
                const element : any = this.getElement($id, true);
                if (ObjectValidator.IsEmptyOrNull(element)) {
                    return 0;
                }
                if (!ObjectValidator.IsEmptyOrNull(prevElement)) {
                    element.cachedStyle = prevElement.cachedStyle;
                }
                return element.offsetHeight;
            } else {
                if (!ElementManager.IsVisible($id) && $ignoreHidden) {
                    return 0;
                }
                return this.getCssIntegerValue($id, "height", false)
                    + this.getHeightOffset($id, false, false);
            }
        }

        /**
         * @param {string|IGuiCommons} $id Element id or object, which should be processed.
         * @param {boolean} [$force=false] Specify if returned value should be up-to-date (slower - needs layout recalculation).
         * @param {boolean} [$includeMargin=true] Specify if margin should be included.
         * @param  {boolean} [$includePadding=true] Specify if padding and border should be included.
         * @return {number} Returns inner width of element's offset - eq. padding-width + border-width.
         */
        public static getWidthOffset($id : string | IGuiCommons, $force : boolean = false,
                                     $includeMargin : boolean = true, $includePadding : boolean = true) : number {
            return ($includePadding ? this.getCssIntegerValue($id, "padding-left", $force) +
                this.getCssIntegerValue($id, "padding-right", $force) +
                this.getCssIntegerValue($id, "border-left-width", $force) +
                this.getCssIntegerValue($id, "border-right-width", $force) : 0) +
                ($includeMargin ? this.getCssIntegerValue($id, "margin-left", $force) +
                    this.getCssIntegerValue($id, "margin-right", $force) : 0);
        }

        /**
         * @param {string|IGuiCommons} $id Element id or object, which should be processed.
         * @param {boolean} [$force=false] Specify if returned value should be up-to-date (slower - needs layout recalculation).
         * @param {boolean} [$includeMargin=true] Specify if margin should be included.
         * @param  {boolean} [$includePadding=true] Specify if padding and border should be included.
         * @return {number} Returns inner height of element's offset - eq. padding-height + border-height.
         */
        public static getHeightOffset($id : string | IGuiCommons, $force : boolean = false,
                                      $includeMargin : boolean = true, $includePadding : boolean = true) : number {
            return ($includePadding ? this.getCssIntegerValue($id, "padding-top", $force) +
                this.getCssIntegerValue($id, "padding-bottom", $force) +
                this.getCssIntegerValue($id, "border-top-width", $force) +
                this.getCssIntegerValue($id, "border-bottom-width", $force) : 0) +
                ($includeMargin ? this.getCssIntegerValue($id, "margin-top", $force) +
                    this.getCssIntegerValue($id, "margin-bottom", $force) : 0);
        }

        /**
         * @param {string|HTMLElement|IGuiCommons} $id Element id or object, which should be handled.
         * @param {number} $value Specify required zoom.
         * @return {void}
         */
        public static setZoom($id : string | HTMLElement | IGuiCommons, $value : number) : void {
            if ($value > 0 && $value < 1) {
                $value *= 100;
            }
            const percentage : number = Convert.ToFixed($value / 100, 2);
            ElementManager.setCssProperty($id, "-ms-zoom", percentage);
            ElementManager.setCssProperty($id, "-moz-transform", "scale(" + percentage + ")");
            ElementManager.setCssProperty($id, "-moz-transform-origin", "0 0");
            ElementManager.setCssProperty($id, "-o-transform", "scale(" + percentage + ")");
            ElementManager.setCssProperty($id, "-o-transform-origin", "0 0");
            ElementManager.setCssProperty($id, "-webkit-transform", "scale(" + percentage + ")");
            ElementManager.setCssProperty($id, "-webkit-transform-origin", "0 0");
        }

        private static getCssClassText($idOrClassName : string) : string {
            const styleSheets : StyleSheetList = document.styleSheets;
            if (!ObjectValidator.IsSet(styleSheets)) {
                return null;
            }

            if (!ObjectValidator.IsSet(ElementManager.cssRules)) {
                let styleIndex : number;
                const styleSheetsLength : number = styleSheets.length;
                for (styleIndex = 0; styleIndex < styleSheetsLength; styleIndex++) {
                    if (ObjectValidator.IsSet(styleSheets[styleIndex])) {
                        const styleSheet : CSSStyleSheet = <CSSStyleSheet>styleSheets[styleIndex];
                        try {
                            if (ObjectValidator.IsSet(styleSheet.cssRules)) {
                                ElementManager.cssRules = styleSheet.cssRules;
                            } else {
                                ElementManager.cssRules = styleSheet.rules;
                            }
                        } catch (ex) {
                            // unable to read rules
                        }
                    }
                }
            }

            if (!ObjectValidator.IsEmptyOrNull(ElementManager.cssRules)) {
                let ruleIndex : number;
                const cssRulesLength : number = ElementManager.cssRules.length;
                for (ruleIndex = 0; ruleIndex < cssRulesLength; ruleIndex++) {
                    const rule : CSSStyleRule = <CSSStyleRule>ElementManager.cssRules[ruleIndex];
                    if (rule.selectorText === $idOrClassName) {
                        if (ObjectValidator.IsSet(rule.style) && ObjectValidator.IsSet(rule.style.cssText)) {
                            return rule.style.cssText;
                        } else if (ObjectValidator.IsSet(rule.cssText)) {
                            return rule.cssText;
                        }
                    }
                }
            }

            return null;
        }

        private static getRuntimeCss($id : any) : CSSStyleDeclaration {
            let computedStyle : CSSStyleDeclaration = null;
            const element : HTMLElement = this.getElement($id);
            if (ObjectValidator.IsSet(window.getComputedStyle)) {
                computedStyle = window.getComputedStyle(element, null);
            } else {
                computedStyle = <CSSStyleDeclaration>(<any>element).currentStyle;
            }
            return computedStyle;
        }
    }
}
