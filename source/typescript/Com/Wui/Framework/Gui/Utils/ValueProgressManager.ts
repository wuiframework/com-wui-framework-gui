/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Utils {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ValueProgressEventArgs = Com.Wui.Framework.Gui.Events.Args.ValueProgressEventArgs;
    import ThreadPool = Com.Wui.Framework.Commons.Events.ThreadPool;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import ProgressType = Com.Wui.Framework.Gui.Enums.ProgressType;
    import EventsManager = Com.Wui.Framework.Gui.Events.EventsManager;

    /**
     * ValueProgressManager class provides asynchronous change of number value with warious progress types.
     */
    export class ValueProgressManager extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private static eventArgsList : ArrayList<ValueProgressEventArgs>;

        /**
         * @param {ValueProgressEventArgs} $args Set progress event args, which should be processed and
         * provided by ValueProgressManager events.
         * @return {void}
         */
        public static Execute($args : ValueProgressEventArgs) : void {
            if (!ValueProgressManager.Exists($args.OwnerId())) {
                ValueProgressManager.eventArgsList.Add($args, $args.OwnerId());
            }

            EventsManager.getInstanceSingleton().FireEvent($args.OwnerId(), $args.StartEventType(), $args);

            if ($args.DirectionType() === DirectionType.DOWN) {
                const endValue : number = $args.RangeEnd();
                $args.RangeEnd($args.RangeStart());
                $args.RangeStart(endValue);
            }

            ThreadPool.AddThread($args.OwnerId(), EventType.ON_CHANGE + "_Value", ValueProgressManager.handler, $args);

            if ($args.CurrentValue() !== $args.RangeEnd()) {
                ThreadPool.Execute();
            }
        }

        /**
         * @param {string} $id Set manager id, which should be removed from processing thread.
         * @return {void}
         */
        public static Remove($id : string) : void {
            ThreadPool.RemoveThread($id, EventType.ON_CHANGE + "_Value");
        }

        /**
         * @param {string} $id Manager id, which should be validated.
         * @return {boolean} Returns true, if ValueProgressManager has been registered, otherwise false.
         */
        public static Exists($id : string) : boolean {
            if (!ObjectValidator.IsSet(ValueProgressManager.eventArgsList)) {
                ValueProgressManager.eventArgsList = new ArrayList<ValueProgressEventArgs>();
            }
            return ValueProgressManager.eventArgsList.KeyExists($id);
        }

        /**
         * @param {string} $id Manager id, which should be get from value progress managers register.
         * @return {ValueProgressEventArgs} Returns managers args associated with manager id.
         * If managers has not been found, this method will automatically register new manager with desired id.
         */
        public static get($id : string) : ValueProgressEventArgs {
            if (!ValueProgressManager.Exists($id)) {
                ValueProgressManager.eventArgsList.Add(new ValueProgressEventArgs($id), $id);
            }
            return ValueProgressManager.eventArgsList.getItem($id);
        }

        private static handler(eventArgs : ValueProgressEventArgs) : void {
            let currentValue : number = eventArgs.CurrentValue();
            const endValue : number = eventArgs.RangeEnd();
            const stepValue : number = eventArgs.Step();

            if (eventArgs.DirectionType() === DirectionType.UP) {
                switch (eventArgs.ProgressType()) {
                case ProgressType.LINEAR:
                    currentValue += stepValue;
                    break;
                case ProgressType.SLOW_START:
                case ProgressType.FAST_END:
                    currentValue++;
                    currentValue += Math.ceil((endValue - currentValue) / stepValue);
                    break;
                case ProgressType.FAST_START:
                case ProgressType.SLOW_END:
                    currentValue++;
                    currentValue += Math.ceil(currentValue / stepValue);
                    break;
                default:
                    break;
                }

                if (currentValue > endValue) {
                    currentValue = endValue;
                }
            } else if (eventArgs.DirectionType() === DirectionType.DOWN) {
                switch (eventArgs.ProgressType()) {
                case ProgressType.LINEAR:
                    currentValue -= stepValue;
                    break;
                case ProgressType.SLOW_START:
                case ProgressType.FAST_END:
                    currentValue--;
                    currentValue -= Math.ceil(currentValue / stepValue);
                    break;
                case ProgressType.FAST_START:
                case ProgressType.SLOW_END:
                    currentValue--;
                    currentValue -= Math.ceil((eventArgs.RangeStart() - currentValue) / stepValue);
                    break;
                default:
                    break;
                }

                if (currentValue < endValue) {
                    currentValue = endValue;
                }
            }

            eventArgs.CurrentValue(currentValue);

            ThreadPool.setThreadArgs(eventArgs.OwnerId(), EventType.ON_CHANGE + "_Value", eventArgs);
            EventsManager.getInstanceSingleton().FireEvent(eventArgs.OwnerId(), eventArgs.ChangeEventType(), eventArgs);

            if (currentValue === endValue) {
                ThreadPool.RemoveThread(eventArgs.OwnerId(), EventType.ON_CHANGE + "_Value");
                EventsManager.getInstanceSingleton().FireEvent(eventArgs.OwnerId(), eventArgs.CompleteEventType(), eventArgs);
            }
        }
    }
}
