/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Utils {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;

    /**
     * KeyEventHandler class provides helper methods focused on validation of native keyboard events.
     */
    export class KeyEventHandler extends Com.Wui.Framework.Commons.Primitives.BaseObject {

        /**
         * @param {KeyboardEvent} $event Native keyboard event, which should be validated.
         * @return {boolean} Returns true, if pressed key is type of navigation key, otherwise false.
         */
        public static IsNavigate($event : KeyboardEvent) : boolean {
            return ($event.keyCode === KeyMap.F5 ||
                $event.keyCode === KeyMap.TAB ||
                $event.keyCode === KeyMap.BACKSPACE ||
                $event.keyCode === KeyMap.LEFT_ARROW ||
                $event.keyCode === KeyMap.RIGHT_ARROW ||
                $event.keyCode === KeyMap.DOWN_ARROW ||
                $event.keyCode === KeyMap.UP_ARROW ||
                ($event.altKey && $event.keyCode === KeyMap.F4) /* Alt+F4 */);
        }

        /**
         * @param {KeyboardEvent} $event Native keyboard event, which should be validated.
         * @return {boolean} Returns true, if pressed key is type of editor key, otherwise false.
         */
        public static IsEdit($event : KeyboardEvent) : boolean {
            return !($event.keyCode === KeyMap.F5 ||
                $event.keyCode === KeyMap.LEFT_ARROW ||
                $event.keyCode === KeyMap.RIGHT_ARROW ||
                $event.keyCode === KeyMap.DOWN_ARROW ||
                $event.keyCode === KeyMap.UP_ARROW ||
                ($event.altKey && $event.keyCode === KeyMap.F4) /* Alt+F4 */) &&
                ($event.keyCode === KeyMap.BACKSPACE ||
                    $event.keyCode === KeyMap.DELETE ||
                    ($event.ctrlKey && $event.charCode === KeyMap.A) /* Ctrl+A */ ||
                    ($event.ctrlKey && $event.charCode === KeyMap.C) /* Ctrl+C */ ||
                    ($event.ctrlKey && $event.charCode === KeyMap.V) /* Ctrl+V */ ||
                    ($event.ctrlKey && $event.charCode === KeyMap.X) /* Ctrl+X */);
        }

        /**
         * @param {KeyboardEvent} $event Native keyboard event, which should be validated.
         * @param {string} [$id] Element id, which should be handled by validation method.
         * @param {number} [$limit] Set max length limit for handled element.
         * @return {boolean} Returns true, if pressed key is type of integer, otherwise false.
         */
        public static IsInteger($event : KeyboardEvent, $id? : string, $limit? : number) : boolean {
            const code : number = this.getKeyCode($event);
            if (!this.IsNavigate($event)) {
                if (code !== KeyMap.MINUS && (code < KeyMap.ZERO || code > KeyMap.NINE)) {
                    return false;
                }

                if (ObjectValidator.IsSet($id)) {
                    const element : HTMLInputElement = <HTMLInputElement>ElementManager.getElement($id);
                    if (ObjectValidator.IsSet(element)) {
                        if (code === KeyMap.MINUS && !ObjectValidator.IsEmptyOrNull(element.value)
                            && TextSelectionManager.getSelectedText(element) !== element.value) {
                            return false;
                        }
                        if (!ObjectValidator.IsSet($limit)) {
                            $limit = 10;
                        }

                        if (StringUtils.Length(StringUtils.Remove(element.value, "-")) < $limit) {
                            return true;
                        } else {
                            return TextSelectionManager.Clear(element);
                        }
                    }
                }
            }

            return true;
        }

        /**
         * @param {KeyboardEvent} $event Native keyboard event, which should be validated.
         * @return {boolean} Returns true, if pressed key is type of double, otherwise false.
         */
        public static IsDouble($event : KeyboardEvent) : boolean {
            const code : number = this.getKeyCode($event);
            if (!this.IsNavigate($event) && code !== KeyMap.COMMA && code !== KeyMap.DOT) {
                return this.IsInteger($event);
            }

            return true;
        }

        /**
         * @param {KeyboardEvent} $event Native keyboard event, which should be validated.
         * @return {boolean} Returns true, if pressed key is type of positive integer or space, otherwise false.
         */
        public static IsPositiveIntegerOrSpace($event : KeyboardEvent) : boolean {
            const code : number = this.getKeyCode($event);
            if (!this.IsNavigate($event) && code !== KeyMap.PLUS && code !== KeyMap.SPACE) {
                return this.IsInteger($event);
            }

            return true;
        }

        private static getKeyCode($event : KeyboardEvent) : number {
            return ObjectValidator.IsSet($event.charCode) ? $event.charCode : $event.keyCode;
        }
    }
}
