/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Utils {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import WindowEventsManager = Com.Wui.Framework.Gui.Events.WindowEventsManager;

    /**
     * WindowManager class provides static methods for handling current window content.
     */
    export class WindowManager extends Com.Wui.Framework.Commons.Primitives.BaseObject {

        private static events : WindowEventsManager;

        /**
         * @return {string} Returns current window HTML code provided as printable and formatted string.
         */
        public static ViewHTMLCode() : string {
            return "<div class=\"ViewHtmlCode\"><pre>" + WindowManager.safeTagsReplace(document.documentElement.innerHTML) +
                "</pre></div>";
        }

        /**
         * @return {Size} Returns current window width and height.
         */
        public static getSize() : Size {
            const size : Size = new Size();
            size.Width(1024);
            size.Height(768);

            if (ObjectValidator.IsSet(document.body) && document.body.offsetWidth) {
                size.Width(document.body.offsetWidth);
                size.Height(document.body.offsetHeight);
            }

            if (document.compatMode === "CSS1Compat"
                && ObjectValidator.IsSet(document.documentElement)
                && document.documentElement.offsetWidth) {
                size.Width(document.documentElement.offsetWidth);
                size.Height(document.documentElement.offsetHeight);
            }

            if (ObjectValidator.IsSet(window.innerWidth) && ObjectValidator.IsSet(window.innerHeight)) {
                size.Width(window.innerWidth);
                size.Height(window.innerHeight);
            }

            return size;
        }

        /**
         * @param {MouseEvent} [$eventArgs] Specify event value instead global window.event
         * @param {boolean} [$ignoreScroll=false] Specify, if X position should be reflecting window scroll.
         * @return {number} Returns current mouse X position in the window.
         */
        public static getMouseX($eventArgs? : MouseEvent, $ignoreScroll : boolean = false) : number {
            if (ObjectValidator.IsSet($eventArgs)) {
                const mouseX : number = ObjectValidator.IsSet($eventArgs.pageX) ? $eventArgs.pageX : $eventArgs.clientX;
                return mouseX
                    - ($ignoreScroll ? (ObjectValidator.IsSet(document.documentElement.scrollLeft) ?
                        document.documentElement.scrollLeft : document.body.scrollLeft) : 0);
            }
            return Math.round(((<any>document).clientX - document.documentElement.clientLeft) / this.getZoomFactor())
                + ($ignoreScroll ? 0 : (ObjectValidator.IsSet(document.documentElement.scrollLeft) ?
                    document.documentElement.scrollLeft : document.body.scrollLeft));
        }

        /**
         * @param {MouseEvent} [$eventArgs] Specify event value instead global window.event
         * @param {boolean} [$ignoreScroll=false] Specify, if Y position should be reflecting window scroll.
         * @return {number} Returns current mouse Y position in the window.
         */
        public static getMouseY($eventArgs? : MouseEvent, $ignoreScroll : boolean = false) : number {
            if (ObjectValidator.IsSet($eventArgs)) {
                const mouseY : number = ObjectValidator.IsSet($eventArgs.pageY) ? $eventArgs.pageY : $eventArgs.clientY;
                return mouseY
                    - ($ignoreScroll ? (ObjectValidator.IsSet(document.documentElement.scrollTop) ?
                        document.documentElement.scrollTop : document.body.scrollTop) : 0);
            }
            return Math.round(((<any>document).clientY - document.documentElement.clientTop) / this.getZoomFactor())
                + ($ignoreScroll ? 0 : (ObjectValidator.IsSet(document.documentElement.scrollTop) ?
                    document.documentElement.scrollTop : document.body.scrollTop));
        }

        /**
         * @return {WindowEventsManager} Returns events manager connected with Window or Body
         */
        public static getEvents() : WindowEventsManager {
            if (ObjectValidator.IsEmptyOrNull(WindowManager.events)) {
                WindowManager.events = new WindowEventsManager();
                WindowManager.getEvents = () : WindowEventsManager => {
                    return WindowManager.events;
                };
            }
            return WindowManager.events;
        }

        /**
         * @return {boolean} Returns true, if window supports drop, otherwise false.
         */
        public static IsDropSupported() : boolean {
            const isSupported : boolean = "draggable" in document.createElement("span");
            WindowManager.IsDropSupported = () : boolean => {
                return isSupported;
            };
            return isSupported;
        }

        /**
         * Provided window scroll to bottom position
         * @return {void}
         */
        public static ScrollToBottom() : void {
            window.scrollTo(0, document.body.scrollHeight);
        }

        private static getZoomFactor() : number {
            let factor : number = 1;
            if (document.body.getBoundingClientRect) {
                const rect : ClientRect = document.body.getBoundingClientRect();
                factor = Math.round(((rect.right - rect.left) / document.body.offsetWidth) * 100) / 100;
            }
            return factor;
        }

        private static highlightHTML($input : string) : string {
            let index : number;
            let output : string = "";
            if (!ObjectValidator.IsEmptyOrNull($input)) {
                output = $input;
                const strings : string[] = (output + "").match(/[^"]+(?=.)/g);
                let tmp : string = output;
                const contents : string[] = (output + "").match(/([^>]*[<$])/g);

                if (!ObjectValidator.IsEmptyOrNull(strings)) {
                    output = StringUtils.Replace(output, "<", "#TS#&lt;");
                    output = StringUtils.Replace(output, ">", "&gt;#TE#");
                    for (index = 0; index < strings.length; index++) {
                        if (!StringUtils.EndsWith(strings[index], "=") && !StringUtils.Contains(strings[index], ">")) {
                            const search : string = "\"" + strings[index] + "\"";
                            output = StringUtils.Replace(output, search, "#SS#" + strings[index] + "#SE#");
                            tmp = StringUtils.Replace(tmp, search, "#ATR#");
                        }
                    }

                    const attributes : string[] = StringUtils.Split(tmp, " ");
                    for (index = 0; index < attributes.length; index++) {
                        let attribute : string = StringUtils.Split(attributes[index], "#ATR#")[0];
                        if (StringUtils.Contains(attribute, "=")) {
                            attribute = StringUtils.Remove(attribute, "<");
                            attribute = StringUtils.Remove(attribute, ">");
                            attribute = StringUtils.Remove(attribute, "=");
                            output = StringUtils.Replace(output, attribute + "=", "#AS#" + attribute + "#AE#");
                        }
                    }
                }

                if (!ObjectValidator.IsEmptyOrNull(contents)) {
                    for (index = 0; index < contents.length; index++) {
                        const content : string = StringUtils.Remove(contents[index], "<");
                        if (!ObjectValidator.IsEmptyOrNull(content) && content !== " " && !StringUtils.Contains(content, "#SCR")) {
                            output = StringUtils.Replace(output, "#TE#" + content + "#TS#", "#CS#" + content + "#CE#");
                        }
                    }
                }

                if (!ObjectValidator.IsEmptyOrNull(output)) {
                    output = StringUtils.Replace(output, "#SS#", "<span class=\"String\">\"");
                    output = StringUtils.Replace(output, "#SE#", "\"</span>");
                    output = StringUtils.Replace(output, "#AS#", "<span class=\"Attribute\">");
                    output = StringUtils.Replace(output, "#AE#", "=</span>");
                    output = StringUtils.Replace(output, "#CS#", "<span class=\"TagContent\">");
                    output = StringUtils.Replace(output, "#CE#", "</span>");
                    output = StringUtils.Replace(output, "#TS#", "<span class=\"Tag\">");
                    output = StringUtils.Replace(output, "#TE#", "</span>");
                    output = "<span class=\"Tag\">" + output + "</span>";
                }
            }

            return output;
        }

        private static safeTagsReplace($input : string) : string {
            let index : number;
            let lineNum : number;
            let content : string;
            const scripts : string[] = ($input + "").match(/<script type="text\/javascript">[^]*(?=<\/script)/gmi);
            if (!ObjectValidator.IsEmptyOrNull(scripts)) {
                for (index = 0; index < scripts.length; index++) {
                    content = StringUtils.Remove(scripts[index], "<script type=\"text/javascript\">");
                    if (!ObjectValidator.IsEmptyOrNull(content)) {
                        $input = StringUtils.Replace($input, content, "#SCR" + index + "#");
                    }
                }
            }

            $input = StringUtils.Replace($input, "<br>", "<br>#EOL#");
            $input = StringUtils.Replace($input, "<hr>", "<hr>#EOL#");
            $input = ($input + "").replace(/[\r\n]]/g, "#EOL#").replace(/[\r\n]/g, "#EOL#");
            let lines : string[] = StringUtils.Split($input, "#EOL#");
            let output : string = "";
            for (lineNum = 0; lineNum < lines.length; lineNum++) {
                const line : string = WindowManager.highlightHTML(lines[lineNum]);
                output += line + "#EOL#";
            }

            if (!ObjectValidator.IsEmptyOrNull(scripts)) {
                for (index = 0; index < scripts.length; index++) {
                    content = StringUtils.Remove(scripts[index], "<script type=\"text/javascript\">");
                    if (!ObjectValidator.IsEmptyOrNull(content)) {
                        content = StringUtils.Replace(content, "<", "&lt;");
                        content = StringUtils.Replace(content, ">", "&gt;");
                        content = (content + "").replace(/[\r\n]]/g, "#EOL#").replace(/[\r\n]/g, "#EOL#");
                        const scriptLines : string[] = StringUtils.Split(content, "#EOL#");
                        content = "";
                        let scriptIndex : number;
                        const scriptLinesLength : number = scriptLines.length;
                        for (scriptIndex = 0; scriptIndex < scriptLinesLength; scriptIndex++) {
                            content += "<span class=\"Script\">" + scriptLines[scriptIndex] + "</span>#EOL#";
                        }
                        output = StringUtils.Replace(output, "#SCR" + index + "#", content);
                    }
                }
            }

            lines = StringUtils.Split(output, "#EOL#");
            output = "";
            for (lineNum = 0; lineNum < lines.length; lineNum++) {
                output += "<div class=\"Line " + (lineNum % 2 === 0 ? " Odd" : " Even") + "\"><span class=\"Number\">" +
                    (lineNum + 1) + "</span>" + lines[lineNum] + "</div>";
            }

            return output;
        }
    }
}
