/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
declare class TextRange {
    public move($type : string, $position : number) : void;

    public moveToElementText($input : HTMLElement) : void;

    public select() : void;

    public getBookmark() : string;
}

declare class IE8HTMLInputElement extends HTMLInputElement {
    public createTextRange() : TextRange;
}

namespace Com.Wui.Framework.Gui.Utils {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * TextSelectionManager class provides static methods focused on handling of selected text.
     */
    export class TextSelectionManager extends Com.Wui.Framework.Commons.Primitives.BaseObject {

        /**
         * Select all text in desired HTML input element.
         * @param {string|HTMLElement|HTMLInputElement} $input HTML element, which should be handled.
         * @return {void}
         */
        public static SelectAll($input : string | HTMLElement | HTMLInputElement) : void {
            if (ObjectValidator.IsString($input)) {
                $input = ElementManager.getElement(<string>$input);
            }
            if (!ObjectValidator.IsEmptyOrNull($input)) {
                if (ObjectValidator.IsSet((<HTMLInputElement>$input).setSelectionRange)) {
                    (<HTMLInputElement>$input).setSelectionRange(0, StringUtils.Length((<HTMLInputElement>$input).value));
                } else if (ObjectValidator.IsSet((<any>document).selection)) {
                    const selectionRange : TextRange = (<any>document).body.createTextRange();
                    selectionRange.moveToElementText(<HTMLElement>$input);
                    selectionRange.select();
                } else if (ObjectValidator.IsSet(window.getSelection)) {
                    const windowRange : Range = document.createRange();
                    windowRange.selectNodeContents(<HTMLElement>$input);
                    window.getSelection().removeAllRanges();
                    window.getSelection().addRange(windowRange);
                }
            }
        }

        /**
         * @param {HTMLInputElement} [$input] HTML input element, which should be handled.
         * Element id should be provided, if browser does not supports document.selection.
         * @return {string} Returns currently selected string.
         */
        public static getSelectedText($input? : HTMLInputElement) : string {
            let output : string = "";
            if (!ObjectValidator.IsEmptyOrNull($input)) {
                if (ObjectValidator.IsSet($input.selectionStart) && $input.selectionStart !== $input.selectionEnd) {
                    output = ($input.value + "").substring($input.selectionStart, $input.selectionEnd);
                }
            } else if (ObjectValidator.IsSet(window.getSelection)) {
                output = window.getSelection().toString();
            } else if (ObjectValidator.IsSet((<any>document).selection) && (<any>document).selection.type !== "None") {
                output = (<any>document).selection.createRange().text;
            }
            return output;
        }

        /**
         * @param {HTMLInputElement} [$input] HTML input element, which should be handled.
         * Element id should be provided, if browser does not supports document.selection.
         * @return {number} Returns positive integer of current cursor position, if cursor has been initialized,
         * otherwise -1.
         */
        public static getCurrentPosition($input? : HTMLInputElement) : number {
            let position : number = -1;
            if (ObjectValidator.IsSet((<any>document).selection)) {
                const range : TextRange = (<any>document).selection.createRange();
                const bookmark : string = range.getBookmark();
                position = StringUtils.getCodeAt(bookmark, 2) - 2;
            } else if (ObjectValidator.IsSet($input.setSelectionRange)) {
                position = $input.selectionStart;
            }

            return position;
        }

        /**
         * @param {HTMLInputElement} $input HTML input element, which should be handled.
         * Element id should be provided, if browser does not supports document.selection.
         * @param {number} $position Specify new cursor position.
         * @return {void}
         */
        public static setCurrentPosition($input : HTMLInputElement, $position : number) : void {
            if (ObjectValidator.IsSet($input.setSelectionRange)) {
                $input.setSelectionRange($position, $position);
            } else if (ObjectValidator.IsSet((<IE8HTMLInputElement>$input).createTextRange)) {
                const range : TextRange = (<IE8HTMLInputElement>$input).createTextRange();
                range.move("character", $position);
                range.select();
            } else {
                $input.focus();
            }
        }

        /**
         * @param {string} $value Specify text value, which should be pasted after current position.
         * @return {void}
         */
        public static PasteTextAtCurrentPosition($value : string) : void {
            if (ObjectValidator.IsSet(window.getSelection)) {
                const sel : Selection = window.getSelection();
                if (sel.getRangeAt && sel.rangeCount) {
                    let range : Range = sel.getRangeAt(0);
                    range.deleteContents();

                    const textNode : Text = document.createTextNode($value);
                    range.insertNode(textNode);
                    range = range.cloneRange();
                    range.setStartAfter(textNode);
                    range.collapse(true);
                    sel.removeAllRanges();
                    sel.addRange(range);
                }
            } else if (ObjectValidator.IsSet((<any>document).selection) && (<any>document).selection.type !== "Control") {
                (<any>document).selection.createRange().text = $value;
            }
        }

        /**
         * @param {HTMLInputElement} [$input] HTML input element, which should be handled.
         * @return {boolean} Returns true, if selected text has been cleaned up, otherwise false.
         */
        public static Clear($input? : HTMLInputElement) : boolean {
            if (ObjectValidator.IsSet($input)) {
                const selected : string = this.getSelectedText($input);
                if (!ObjectValidator.IsEmptyOrNull(selected)) {
                    const position : number = this.getCurrentPosition($input);
                    $input.value = StringUtils.Remove($input.value, selected);

                    if (ObjectValidator.IsSet((<IE8HTMLInputElement>$input).createTextRange)) {
                        const range : TextRange = (<IE8HTMLInputElement>$input).createTextRange();
                        range.move("character", position);
                        range.select();
                    } else if (ObjectValidator.IsSet($input.selectionStart)) {
                        $input.focus();
                        $input.setSelectionRange(position, position);
                    } else {
                        $input.focus();
                    }

                    return true;
                }
            } else {
                if (ObjectValidator.IsSet(window.getSelection)) {
                    window.getSelection().removeAllRanges();
                    return true;
                } else if (ObjectValidator.IsSet((<any>document).selection)) {
                    (<any>document).selection.empty();
                    return true;
                }
            }

            return false;
        }

        /**
         * Handle user text selection on whole document or specific element
         * @param {HTMLElement} $value Specify if text selection should be disabled.
         * @param {HTMLElement} [$element] HTML element, which should be handled.
         * @return {void}
         */
        public static Disable($value : boolean, $element? : HTMLElement) : void {
            if (ObjectValidator.IsEmptyOrNull($element)) {
                $element = document.body;
            }
            [
                "user-select",
                "-o-user-select",
                "-moz-user-select",
                "-khtml-user-select",
                "-webkit-user-select",
                "-ms-user-select"
            ].forEach(($key : string) : void => {
                if ($value) {
                    ElementManager.setCssProperty($element, $key, "none");
                } else {
                    ElementManager.ClearCssProperty($element, $key);
                }
            });
            if ($value) {
                $element.setAttribute("unselectable", "on");
            } else {
                $element.setAttribute("unselectable", "off");
            }
        }
    }
}
