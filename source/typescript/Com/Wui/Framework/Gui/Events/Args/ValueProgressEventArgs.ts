/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import ProgressType = Com.Wui.Framework.Gui.Enums.ProgressType;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    /**
     * ValueProgressEventArgs class provides args connected with change of some value.
     */
    export class ValueProgressEventArgs extends Com.Wui.Framework.Commons.Events.Args.ProgressEventArgs {
        private ownerId : string;
        private directionType : DirectionType;
        private startEventType : string;
        private changeEventType : string;
        private completeEventType : string;
        private step : number;
        private progressType : ProgressType;

        /**
         * @param {string} $id Set value progress args owner id.
         */
        constructor($id : string) {
            super();
            this.OwnerId($id);
            this.directionType = DirectionType.UP;
            this.startEventType = $id + "_" + EventType.ON_START;
            this.changeEventType = $id + "_" + EventType.ON_CHANGE;
            this.completeEventType = $id + "_" + EventType.ON_COMPLETE;
            this.progressType = ProgressType.LINEAR;
            this.step = 6;
        }

        /**
         * @param {string} [$value] Set args owner id.
         * @return {string} Returns owner id of current args instance.
         */
        public OwnerId($value? : string) : string {
            return this.ownerId = Property.String(this.ownerId, $value);
        }

        /**
         * @param {DirectionType} [$value] Set type of direction in, which should be value changed.
         * Allowed is UP or DOWN direction.
         * @return {DirectionType} Returns direction type of value change.
         */
        public DirectionType($value? : DirectionType) : DirectionType {
            if (!ObjectValidator.IsEmptyOrNull($value) &&
                ($value === DirectionType.DOWN || $value === DirectionType.UP)) {
                this.directionType = $value;
            }
            return this.directionType;
        }

        /**
         * @param {string} [$value] Set type of event, which should be fired at start time of value change.
         * @return {string} Returns event type, which will be fired at start time of value change.
         */
        public StartEventType($value? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.startEventType = $value;
            }
            return this.startEventType;
        }

        /**
         * @param {string} [$value] Set type of event, which should be fired at time of value change.
         * @return {string} Returns event type, which will be fired at time of value change.
         */
        public ChangeEventType($value? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.changeEventType = $value;
            }
            return this.changeEventType;
        }

        /**
         * @param {string} [$value] Set type of event, which should be fired, when value change is finished.
         * @return {string} Returns event type, which will be fired, when value change is finished.
         */
        public CompleteEventType($value? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.completeEventType = $value;
            }
            return this.completeEventType;
        }

        /**
         * @param {number} [$value] Set value, which will be added to current value in case of linear progress,
         * otherwise set number of iterations.
         * @return {number} Returns value, which will be added to current value in case of linear progress,
         * otherwise returns number of iterations.
         */
        public Step($value? : number) : number {
            return this.step = Property.Integer(this.step, $value);
        }

        /**
         * @param {ProgressType} [$value] Set type of algorithm, which should be used for change of value.
         * @return {ProgressType} Returns type of algorithm, which is used for change of value.
         */
        public ProgressType($value? : ProgressType) : ProgressType {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.progressType = $value;
            }
            return this.progressType;
        }
    }
}
