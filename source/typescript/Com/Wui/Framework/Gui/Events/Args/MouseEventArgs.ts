/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";

    /**
     * MouseEventArgs class provides args connected with mouse events.
     */
    export class MouseEventArgs extends Com.Wui.Framework.Commons.Events.Args.EventArgs {
        /**
         * @param {MouseEvent} [$eventArgs] Specify native mouse event args provided by native mouse handler
         */
        constructor($eventArgs? : MouseEvent) {
            super();
            this.NativeEventArgs($eventArgs);
        }

        /**
         * @param {MouseEvent} [$value] If specified, set event args of native event.
         * @return {MouseEvent} Returns native event args provided by native event handler.
         */
        public NativeEventArgs($value? : MouseEvent) : MouseEvent {
            return <MouseEvent>super.NativeEventArgs($value);
        }
    }
}
