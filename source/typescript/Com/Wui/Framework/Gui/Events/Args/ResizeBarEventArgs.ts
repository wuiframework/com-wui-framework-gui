/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * ResizeBarEventArgs class provides args connected with resizing by ResizeBar type of element.
     */
    export class ResizeBarEventArgs extends MoveEventArgs {
        private resizeType : ResizeableType;
        private resizeDistanceX : number;
        private resizeDistanceY : number;

        constructor($eventArgs? : MouseEvent) {
            super($eventArgs);
            this.resizeDistanceX = super.getDistanceX();
            this.resizeDistanceY = super.getDistanceY();
        }

        public NativeEventArgs($value? : MouseEvent) : MouseEvent {
            const args : MouseEvent = <MouseEvent>super.NativeEventArgs($value);
            if (ObjectValidator.IsSet($value)) {
                this.resizeDistanceX = super.getDistanceX();
                this.resizeDistanceY = super.getDistanceY();
            }
            return args;
        }

        /**
         * @param {ResizeableType} [$type] Specify type of resize direction.
         * @return {number} Returns type of resize direction.
         */
        public ResizeableType($type? : ResizeableType) : ResizeableType {
            if (ObjectValidator.IsSet($type)) {
                this.resizeType = $type;
            }
            return this.resizeType;
        }

        /**
         * @return {number} Returns distance from last mouse horizontal position.
         */
        public getDistanceX() : number {
            return this.resizeDistanceX;
        }

        /**
         * @param {number} [$value] Specify value of the horizontal distance.
         * @return {number} Returns distance from last mouse horizontal position.
         */
        public DistanceX($value? : number) : number {
            this.resizeDistanceX = Property.Integer(this.resizeDistanceX, $value);
            return this.getDistanceX();
        }

        /**
         * @return {number} Returns distance from last mouse vertical position.
         */
        public getDistanceY() : number {
            return this.resizeDistanceY;
        }

        /**
         * @param {number} [$value] Specify value of the vertical distance.
         * @return {number} Returns distance from last mouse horizontal position.
         */
        public DistanceY($value? : number) : number {
            this.resizeDistanceY = Property.Integer(this.resizeDistanceY, $value);
            return this.getDistanceY();
        }
    }
}
