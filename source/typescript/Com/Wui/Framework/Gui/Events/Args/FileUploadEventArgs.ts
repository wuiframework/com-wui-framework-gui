/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import FileHandler = Com.Wui.Framework.Commons.IOApi.Handlers.FileHandler;

    /**
     * FileUploadEventArgs class provides args connected with file upload.
     */
    export class FileUploadEventArgs extends Com.Wui.Framework.Commons.Events.Args.ProgressEventArgs {
        private id : string;
        private index : number;
        private name : string;
        private handler : FileHandler;

        constructor() {
            super();
            this.id = "";
            this.index = -1;
            this.name = "";
            this.handler = null;
        }

        /**
         * @param {string} [$value] Set file id value.
         * @return {string} Returns file id value.
         */
        public Id($value? : string) : string {
            return this.id = Property.String(this.id, $value);
        }

        /**
         * @param {number} [$value] Set file chunk index value.
         * @return {number} Returns file chunk index value, if file chunk has been specified otherwise -1.
         */
        public Index($value? : number) : number {
            return this.index = Property.PositiveInteger(this.index, $value);
        }

        /**
         * @param {string} [$value] Set file name value.
         * @return {string} Returns file name value.
         */
        public Name($value? : string) : string {
            return this.name = Property.String(this.name, $value);
        }

        /**
         * @param {FileHandler} [$value] Set file handler instance.
         * @return {FileHandler} Returns file handler instance, if handler has been specified, otherwise null.
         */
        public File($value? : FileHandler) : FileHandler {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.handler = $value;
            }
            return this.handler;
        }
    }
}
