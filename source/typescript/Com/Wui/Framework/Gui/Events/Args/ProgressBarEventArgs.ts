/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;

    /**
     * ProgressBarEventArgs class provides args connected with progress bar processing.
     */
    export class ProgressBarEventArgs extends Com.Wui.Framework.Commons.Events.Args.ProgressEventArgs {
        private percentageValue : number;
        private directionType : DirectionType;

        constructor() {
            super();
            this.percentageValue = 0;
            this.directionType = DirectionType.UP;
        }

        /**
         * @param {DirectionType} [$type] Specify progress change direction.
         * @return {DirectionType} Returns type of progress change direction.
         */
        public DirectionType($type? : DirectionType) : DirectionType {
            if (ObjectValidator.IsSet($type)) {
                this.directionType = $type;
            }
            return this.directionType;
        }

        /**
         * @param {number} [$value] Set current value in percentage range <0;100>.
         * @return {number} Returns number value of the current value in percentage range <0;100>.
         */
        public Percentage($value? : number) : number {
            return this.percentageValue = Property.PositiveInteger(this.percentageValue, $value, 0, 100);
        }
    }
}
