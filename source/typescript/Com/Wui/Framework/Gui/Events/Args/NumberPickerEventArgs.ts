/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * ProgressBarEventArgs class provides args connected with selection of number from specified range.
     */
    export class NumberPickerEventArgs extends Com.Wui.Framework.Commons.Events.Args.EventArgs {
        private currentValue : number;
        private rangeStartValue : number;
        private rangeEndValue : number;
        private percentageValue : number;

        constructor() {
            super();
            this.currentValue = 0;
            this.rangeStartValue = 0;
            this.rangeEndValue = 100;
            this.percentageValue = 0;
        }

        /**
         * @param {number} [$value] Set start value of the progress.
         * @return {number} Returns start number value of the progress.
         */
        public RangeStart($value? : number) : number {
            if (ObjectValidator.IsDigit($value)) {
                this.rangeStartValue = $value;
            }
            return this.rangeStartValue;
        }

        /**
         * @param {number} [$value] Set end value of the progress.
         * @return {number} Returns end number value of the progress.
         */
        public RangeEnd($value? : number) : number {
            if (ObjectValidator.IsDigit($value)) {
                this.rangeEndValue = $value;
            }
            return this.rangeEndValue;
        }

        /**
         * @param {number} [$value] Set current value of the progress.
         * @return {number} Returns number value of the progress.
         */
        public CurrentValue($value? : number) : number {
            if (ObjectValidator.IsDigit($value)) {
                this.currentValue = $value;
            }
            return this.currentValue;
        }

        /**
         * @param {number} [$value] Set current value in percentage range <0;100>.
         * @return {number} Returns number value of the current value in percentage range <0;100>.
         */
        public Percentage($value? : number) : number {
            this.percentageValue = Property.PositiveInteger(this.percentageValue, $value, 0, 100);
            return this.percentageValue;
        }
    }
}
