/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import IDirectoryBrowser = Com.Wui.Framework.Gui.Interfaces.UserControls.IDirectoryBrowser;

    /**
     * DirectoryBrowserEventArgs class provides args connected with usage of DirectoryBrowser.
     */
    export class DirectoryBrowserEventArgs extends Com.Wui.Framework.Commons.Events.Args.EventArgs {
        private value : string;

        /**
         * @param {IDirectoryBrowser} [$value] Specified object, which owns current args
         * @return {IDirectoryBrowser} Returns event args owner object.
         */
        public Owner($value? : IDirectoryBrowser) : IDirectoryBrowser {
            return super.Owner($value);
        }

        /**
         * @param {string} [$value] Set current selected item value.
         * @return {string} Returns value of currently selected item.
         */
        public Value($value? : string) : string {
            return this.value = Property.String(this.value, $value);
        }
    }
}
