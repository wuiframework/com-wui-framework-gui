/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;

    /**
     * MouseEventArgs class provides args connected with move of element.
     */
    export class MoveEventArgs extends Com.Wui.Framework.Commons.Events.Args.EventArgs {
        private readonly startPositionX : number;
        private readonly startPositionY : number;
        private positionX : number;
        private positionY : number;
        private distanceX : number;
        private distanceY : number;

        /**
         * @param {MouseEvent} [$eventArgs] Specify native mouse event args provided by native mouse handler
         */
        constructor($eventArgs? : MouseEvent) {
            super();
            this.startPositionX = 0;
            this.startPositionY = 0;
            this.positionX = 0;
            this.positionY = 0;
            this.distanceX = 0;
            this.distanceY = 0;
            if (ObjectValidator.IsSet($eventArgs)) {
                this.startPositionX = $eventArgs.screenX;
                this.startPositionY = $eventArgs.screenY;
            }
            this.NativeEventArgs($eventArgs);
        }

        /**
         * @param {MouseEvent} [$value] If specified, set event args of native event.
         * @return {MouseEvent} Returns native event args provided by native event handler.
         */
        public NativeEventArgs($value? : MouseEvent) : MouseEvent {
            if (ObjectValidator.IsSet($value)) {
                this.distanceX = $value.screenX - this.startPositionX;
                this.distanceY = $value.screenY - this.startPositionY;
                this.positionX = WindowManager.getMouseX($value);
                this.positionY = WindowManager.getMouseY($value);
            }
            return <MouseEvent>super.NativeEventArgs($value);
        }

        /**
         * @return {number} Returns current mouse horizontal position.
         */
        public getPositionX() : number {
            return this.positionX;
        }

        /**
         * @return {number} Returns current mouse vertical position.
         */
        public getPositionY() : number {
            return this.positionY;
        }

        /**
         * @return {number} Returns distance from last mouse horizontal position.
         */
        public getDistanceX() : number {
            return this.distanceX;
        }

        /**
         * @return {number} Returns distance from last mouse vertical position.
         */
        public getDistanceY() : number {
            return this.distanceY;
        }
    }
}
