/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events {
    "use strict";
    import IEventsManager = Com.Wui.Framework.Gui.Interfaces.IEventsManager;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IEventArgs = Com.Wui.Framework.Commons.Interfaces.IEventArgs;
    import IEventsHandler = Com.Wui.Framework.Gui.Interfaces.IEventsHandler;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import GeneralEventOwner = Com.Wui.Framework.Gui.Enums.Events.GeneralEventOwner;
    import TextSelectionManager = Com.Wui.Framework.Gui.Utils.TextSelectionManager;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import OrientationType = Com.Wui.Framework.Gui.Enums.OrientationType;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import BrowserType = Com.Wui.Framework.Commons.Enums.BrowserType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    /**
     * EventsManager class provides handling of native events subscribed to HTML elements.
     */
    export class EventsManager extends Com.Wui.Framework.Commons.Events.EventsManager implements IEventsManager {

        public static getInstanceSingleton() : IEventsManager {
            return Loader.getInstance().getHttpResolver().getEvents();
        }

        private static bodyFocusEventHandler() : any {
            try {
                const eventArgs : EventArgs = new EventArgs();
                Loader.getInstance().getHttpResolver().getEvents().FireEvent(GeneralEventOwner.BODY, EventType.ON_FOCUS, eventArgs);
                Loader.getInstance().getHttpResolver().getEvents().FireEvent(GeneralEventOwner.WINDOW, EventType.ON_FOCUS, eventArgs);
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        }

        private static bodyBlurEventHandler() : any {
            try {
                const eventArgs : EventArgs = new EventArgs();
                Loader.getInstance().getHttpResolver().getEvents().FireEvent(GeneralEventOwner.BODY, EventType.ON_BLUR, eventArgs);
                Loader.getInstance().getHttpResolver().getEvents().FireEvent(GeneralEventOwner.WINDOW, EventType.ON_BLUR, eventArgs);
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        }

        /**
         * @param {string|IGuiCommons} $owner Event args owner name.
         * @param {string} $type Event args type of.
         * @param {IEventArgs} $args Set current event args.
         * @return {void}
         */
        public setEventArgs($owner : string | IGuiCommons, $type : string, $args : IEventArgs) : void {
            let eventOwner : string;
            if (ObjectValidator.IsSet((<IGuiCommons>$owner).Id)) {
                eventOwner = (<IGuiCommons>$owner).Id();
            } else {
                eventOwner = <string>$owner;
            }
            if (ObjectValidator.IsEmptyOrNull($args.Owner())) {
                $args.Owner($owner);
            }
            super.setEventArgs(eventOwner, $type, $args);
        }

        /**
         * @param {string|IGuiCommons} $owner Subscribed event to this owner value.
         * @param {string} $type Subscribe handler to this type of event.
         * @param {IEventsHandler} [$handler] Function suitable for handling of the event.
         * @param {IEventArgs} [$args] Set initial event args.
         * @return {void}
         */
        public setEvent($owner : string | IGuiCommons, $type : string, $handler? : IEventsHandler, $args? : IEventArgs) : void {
            let eventOwner : string;
            if (ObjectValidator.IsSet((<IGuiCommons>$owner).Id)) {
                eventOwner = (<IGuiCommons>$owner).Id();
            } else {
                eventOwner = <string>$owner;
            }
            super.setEvent(eventOwner, $type, $handler, $args);
            if (!ObjectValidator.IsSet($args) && ObjectValidator.IsSet((<IGuiCommons>$owner).Id)) {
                this.getArgsList().getItem(eventOwner).getItem($type).Owner($owner);
            }
        }

        /**
         * @param {string|IGuiCommons} $owner Specify fired event owner.
         * @param {string} $type Specify fired type of event.
         * @param {any} [$args] Specify event args for current event process.
         * @param {boolean} [$async] Specify, if event handlers can be execute asynchronously.
         * @return {void}
         */
        public FireEvent($owner : string | IGuiCommons, $type : string, $args? : any, $async? : boolean) : void {
            let eventOwner : string;
            if (ObjectValidator.IsSet((<IGuiCommons>$owner).Id)) {
                eventOwner = (<IGuiCommons>$owner).Id();
            } else {
                eventOwner = <string>$owner;
            }

            if (this.Exists(eventOwner, $type)) {
                const reflection : Reflection = Reflection.getInstance();
                if (!ObjectValidator.IsSet($args) ||
                    (ObjectValidator.IsBoolean($args) && !ObjectValidator.IsBoolean($async))) {
                    if (ObjectValidator.IsBoolean($args) && !ObjectValidator.IsBoolean($async)) {
                        $async = $args;
                    }
                    if (this.getArgsList().KeyExists(eventOwner)) {
                        const args : IEventArgs = this.getArgsList().getItem(eventOwner).getItem($type);
                        if (!ObjectValidator.IsEmptyOrNull(args)) {
                            $args = args;
                        }
                    }
                } else if (reflection.IsMemberOf($args, EventArgs)) {
                    this.setEventArgs($owner, $type, $args);
                }
                if (!ObjectValidator.IsBoolean($async)) {
                    $async = true;
                }

                const handlers : ArrayList<IEventsHandler> = this.getAll().getItem(eventOwner).getItem($type);
                try {
                    const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                    handlers.foreach(
                        ($handler : IEventsHandler) : void => {
                            if ($async) {
                                setTimeout(() : void => {
                                    try {
                                        $handler($args, manager, reflection);
                                    } catch (ex) {
                                        ExceptionsManager.HandleException(ex);
                                    }
                                });
                            } else {
                                $handler($args, manager, reflection);
                            }
                        });
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            }
        }

        /**
         * @param {string|IGuiCommons} $owner Event owner name of subscribed handler.
         * @param {string} $type Event type of subscribed handler.
         * @param {IEventsHandler} $handler Handler, which should be removed.
         * @return {void}
         */
        public RemoveHandler($owner : string | IGuiCommons, $type : string, $handler : IEventsHandler) : void {
            let eventOwner : string;
            if (ObjectValidator.IsSet((<IGuiCommons>$owner).Id)) {
                eventOwner = (<IGuiCommons>$owner).Id();
            } else {
                eventOwner = <string>$owner;
            }
            super.RemoveHandler(eventOwner, $type, $handler);
        }

        /**
         * @param {string|IGuiCommons} $owner Validate events subscribed to this owner value.
         * @param {string} $type Validate this type of subevents.
         * @return {boolean} Returns true, if $owner and $type has been registered, otherwise false.
         */
        public Exists($owner : string | IGuiCommons, $type : string) : boolean {
            let eventOwner : string;
            if (ObjectValidator.IsSet((<IGuiCommons>$owner).Id)) {
                eventOwner = (<IGuiCommons>$owner).Id();
            } else {
                eventOwner = <string>$owner;
            }
            return super.Exists(eventOwner, $type);
        }

        public Subscribe() : void {
            super.Subscribe();
            let onBeforeUnloadFired : boolean = false;

            this.setEvent(GeneralEventOwner.WINDOW, EventType.BEFORE_REFRESH, () : void => {
                if (!onBeforeUnloadFired) {
                    onBeforeUnloadFired = true;
                    Loader.getInstance().getHttpManager().ReloadTo("/PersistenceManager", null, true);
                }
            });

            this.setEvent(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST, () : void => {
                Loader.getInstance().getHttpManager().ReloadTo("/PersistenceManager", null, true);
            });

            const getEventArgs : ($value : Event) => Event = ($value : Event) : Event => {
                if (!ObjectValidator.IsSet($value)) {
                    return event;
                } else {
                    return $value;
                }
            };

            window.onresize = ($eventArgs : UIEvent) : any => {
                try {
                    const eventArgs : EventArgs = new EventArgs();
                    $eventArgs = <UIEvent>getEventArgs($eventArgs);
                    eventArgs.NativeEventArgs($eventArgs);
                    this.FireEvent(GeneralEventOwner.BODY, EventType.ON_RESIZE, eventArgs, false);
                    this.FireEvent(GeneralEventOwner.WINDOW, EventType.ON_RESIZE, eventArgs, false);
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };

            const isOnLoadKey : ($eventArgs : KeyboardEvent) => boolean = this.isOnLoadKey;
            window.document.onkeydown = ($eventArgs : KeyboardEvent) : any => {
                try {
                    $eventArgs = <KeyboardEvent>getEventArgs($eventArgs);
                    this.FireEvent(GeneralEventOwner.BODY, EventType.ON_KEY_DOWN, new Args.KeyEventArgs($eventArgs), false);
                    if (isOnLoadKey($eventArgs)) {
                        this.FireEvent(GeneralEventOwner.BODY, EventType.BEFORE_REFRESH);
                    }
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };
            window.document.onkeyup = ($eventArgs : KeyboardEvent) : any => {
                try {
                    $eventArgs = <KeyboardEvent>getEventArgs($eventArgs);
                    this.FireEvent(GeneralEventOwner.BODY, EventType.ON_KEY_UP, new Args.KeyEventArgs($eventArgs), false);
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };
            window.document.onkeypress = ($eventArgs : KeyboardEvent) : any => {
                try {
                    $eventArgs = <KeyboardEvent>getEventArgs($eventArgs);
                    this.FireEvent(GeneralEventOwner.BODY, EventType.ON_KEY_PRESS, new Args.KeyEventArgs($eventArgs), false);
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };

            window.document.onclick = ($eventArgs : MouseEvent) : any => {
                try {
                    $eventArgs = <MouseEvent>getEventArgs($eventArgs);
                    this.FireEvent(GeneralEventOwner.BODY, EventType.ON_CLICK, new Args.MouseEventArgs($eventArgs));
                    this.FireEvent(GeneralEventOwner.WINDOW, EventType.ON_CLICK, new Args.MouseEventArgs($eventArgs));
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };

            window.document.ondblclick = ($eventArgs : MouseEvent) : any => {
                try {
                    $eventArgs = <MouseEvent>getEventArgs($eventArgs);
                    this.FireEvent(GeneralEventOwner.BODY, EventType.ON_DOUBLE_CLICK, new Args.MouseEventArgs($eventArgs));
                    this.FireEvent(GeneralEventOwner.WINDOW, EventType.ON_DOUBLE_CLICK, new Args.MouseEventArgs($eventArgs));
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };

            window.document.oncontextmenu = ($eventArgs : MouseEvent) : any => {
                try {
                    $eventArgs = <MouseEvent>getEventArgs($eventArgs);
                    this.FireEvent(GeneralEventOwner.BODY, EventType.ON_RIGHT_CLICK, new Args.MouseEventArgs($eventArgs), false);
                    this.FireEvent(GeneralEventOwner.WINDOW, EventType.ON_RIGHT_CLICK, new Args.MouseEventArgs($eventArgs), false);
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };

            let moveArgs : Args.MoveEventArgs;
            let isMoveManagerActive : boolean = false;
            const onMouseDownHandler : any = ($eventArgs : MouseEvent) : any => {
                try {
                    $eventArgs = <MouseEvent>getEventArgs($eventArgs);
                    this.FireEvent(GeneralEventOwner.BODY, EventType.ON_MOUSE_DOWN, new Args.MouseEventArgs($eventArgs));
                    moveArgs = new Args.MoveEventArgs($eventArgs);
                    isMoveManagerActive = true;
                    this.FireEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_START, moveArgs);
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };

            let lastDeltaX : number = 0;
            let lastDeltaY : number = 0;
            window.document.onmousedown = onMouseDownHandler;
            window.document.ontouchstart = ($eventArgs : TouchEvent) : any => {
                const touches : TouchList = $eventArgs.changedTouches;
                let index : number;
                const touchesLength : number = touches.length;
                if (touchesLength === 1) {
                    lastDeltaX = 0;
                    lastDeltaY = 0;
                    onMouseDownHandler(<any>touches[0]);
                } else {
                    for (index = 0; index < touchesLength; index++) {
                        onMouseDownHandler(<any>touches[index]);
                    }
                }
            };

            const simulateScroll : any = ($eventArgs : MouseEvent, $delta : number, $orientation : OrientationType) : void => {
                const eventArgs : Args.ScrollEventArgs = new Args.ScrollEventArgs();
                eventArgs.NativeEventArgs($eventArgs);
                eventArgs.OrientationType($orientation);
                $delta = Math.abs(Math.ceil($delta));
                let position : number = 0;
                if ($orientation === OrientationType.HORIZONTAL) {
                    position = lastDeltaX - $delta;
                    lastDeltaX = $delta;
                } else {
                    position = lastDeltaY - $delta;
                    lastDeltaY = $delta;
                }
                eventArgs.DirectionType(position < 0 ? DirectionType.DOWN : DirectionType.UP);
                position = Math.abs(Math.ceil(position));
                if (position > 0) {
                    if (position > 100) {
                        position = 100;
                    }
                    eventArgs.Position(position);
                    this.FireEvent(GeneralEventOwner.BODY, EventType.ON_SCROLL, eventArgs, false);
                }
            };

            const onMouseMoveHandler : any = ($eventArgs : MouseEvent, $singleEvent : boolean) : any => {
                try {
                    $eventArgs = <MouseEvent>getEventArgs($eventArgs);
                    if (!ObjectValidator.IsSet(moveArgs)) {
                        moveArgs = new Args.MoveEventArgs($eventArgs);
                    }
                    moveArgs.NativeEventArgs($eventArgs);
                    this.FireEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE, moveArgs);
                    if (isMoveManagerActive) {
                        if (!this.isElementMoveAllowed()) {
                            moveArgs.PreventDefault();
                            TextSelectionManager.Clear();
                        } else if ($singleEvent) {
                            if (moveArgs.getDistanceY() !== 0) {
                                simulateScroll($eventArgs, moveArgs.getDistanceY(), OrientationType.VERTICAL);
                            }
                            if (moveArgs.getDistanceX() !== 0) {
                                simulateScroll($eventArgs, moveArgs.getDistanceX(), OrientationType.HORIZONTAL);
                            }
                        }
                    }
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };
            window.document.onmousemove = ($eventArgs : MouseEvent) : void => {
                onMouseMoveHandler($eventArgs, false);
            };
            window.document.ontouchmove = ($eventArgs : TouchEvent) : any => {
                const touches : TouchList = $eventArgs.changedTouches;
                let index : number;
                const touchesLength : number = touches.length;
                if (touchesLength === 1) {
                    onMouseMoveHandler(<any>touches[0], true);
                } else {
                    for (index = 0; index < touchesLength; index++) {
                        onMouseMoveHandler(<any>touches[index], false);
                    }
                }
            };

            const onMouseUpHandler : any = ($eventArgs : MouseEvent) : any => {
                try {
                    $eventArgs = <MouseEvent>getEventArgs($eventArgs);
                    this.FireEvent(GeneralEventOwner.BODY, EventType.ON_MOUSE_UP, new Args.MouseEventArgs($eventArgs));
                    moveArgs.NativeEventArgs($eventArgs);
                    document.body.style.cursor = "default";
                    isMoveManagerActive = false;
                    this.FireEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE, moveArgs);
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };
            window.document.onmouseup = onMouseUpHandler;
            window.document.ontouchend = ($eventArgs : TouchEvent) : any => {
                const touches : TouchList = $eventArgs.changedTouches;
                let index : number;
                const touchesLength : number = touches.length;
                for (index = 0; index < touchesLength; index++) {
                    onMouseUpHandler(<any>touches[index]);
                }
            };

            let wheelEventTypeFired : boolean = false;
            const scrollEventHandler : any = ($eventArgs : WheelEvent | MouseWheelEvent) : any => {
                try {
                    $eventArgs = <WheelEvent>getEventArgs($eventArgs);
                    if ($eventArgs.type && $eventArgs.type === "wheel") {
                        wheelEventTypeFired = true;
                    }
                    if (!$eventArgs.ctrlKey &&
                        (wheelEventTypeFired && $eventArgs.type === "wheel" || !wheelEventTypeFired)) {
                        let scrollDelta : number = 0;
                        let orientation : OrientationType = OrientationType.VERTICAL;
                        let direction : DirectionType = DirectionType.DOWN;
                        if (ObjectValidator.IsSet((<WheelEvent>$eventArgs).deltaY) ||
                            ObjectValidator.IsSet((<WheelEvent>$eventArgs).deltaX)) {
                            if ((<WheelEvent>$eventArgs).deltaX !== 0) {
                                orientation = OrientationType.HORIZONTAL;
                                scrollDelta = (<WheelEvent>$eventArgs).deltaX;
                            } else {
                                scrollDelta = (<WheelEvent>$eventArgs).deltaY;
                            }

                            if (Loader.getInstance().getHttpManager().getRequest().getBrowserType() === BrowserType.FIREFOX ||
                                Loader.getInstance().getHttpManager().getRequest().getBrowserType() === BrowserType.SAFARI) {
                                scrollDelta = scrollDelta * 100;
                            }
                            scrollDelta = (-1) * scrollDelta / 120;
                        } else if (ObjectValidator.IsSet((<any>$eventArgs).wheelDeltaY) ||
                            ObjectValidator.IsSet((<any>$eventArgs).wheelDeltaX)) {
                            if ((<any>$eventArgs).wheelDeltaX !== 0) {
                                orientation = OrientationType.HORIZONTAL;
                                scrollDelta = (<any>$eventArgs).wheelDeltaX;
                            } else {
                                scrollDelta = (<any>$eventArgs).wheelDeltaY;
                            }
                            scrollDelta = scrollDelta / 120;
                        } else if (ObjectValidator.IsSet($eventArgs.detail)) {
                            scrollDelta = (-1) * $eventArgs.detail / 3;
                        } else {
                            scrollDelta = (<any>$eventArgs).wheelDelta / 120;
                        }
                        if ((orientation === OrientationType.VERTICAL && scrollDelta > 0) ||
                            (orientation === OrientationType.HORIZONTAL && scrollDelta > 0)) {
                            direction = DirectionType.UP;
                        }
                        scrollDelta = Math.abs(Math.ceil(scrollDelta));
                        if (scrollDelta < 1) {
                            scrollDelta = 1;
                        }
                        if (scrollDelta > 100) {
                            scrollDelta = 100;
                        }

                        const eventArgs : Args.ScrollEventArgs = new Args.ScrollEventArgs();
                        eventArgs.NativeEventArgs($eventArgs);
                        eventArgs.Position(scrollDelta);
                        eventArgs.OrientationType(orientation);
                        eventArgs.DirectionType(direction);
                        this.FireEvent(GeneralEventOwner.BODY, EventType.ON_SCROLL, eventArgs, false);
                    }
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };
            if (ObjectValidator.IsSet(window.document.addEventListener)) {
                window.document.addEventListener("wheel", scrollEventHandler, false);
                window.document.addEventListener("DOMMouseScroll", scrollEventHandler, false);
            }
            if (ObjectValidator.IsSet((<any>window).document.onmousewheel)) {
                (<any>window).document.onmousewheel = scrollEventHandler;
            }
        }

        protected isOnLoadKey($eventArgs : KeyboardEvent) : boolean {
            return (!$eventArgs.ctrlKey && $eventArgs.keyCode === KeyMap.F5) ||
                ($eventArgs.altKey && $eventArgs.keyCode === KeyMap.F4) ||
                ($eventArgs.keyCode === KeyMap.BACKSPACE);
        }

        protected isElementMoveAllowed() : boolean {
            return true;
        }
    }
}
