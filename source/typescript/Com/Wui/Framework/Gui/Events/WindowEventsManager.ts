/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IEventsHandler = Com.Wui.Framework.Gui.Interfaces.IEventsHandler;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import GeneralEventOwner = Com.Wui.Framework.Gui.Enums.Events.GeneralEventOwner;
    import IErrorEventsHandler = Com.Wui.Framework.Gui.Interfaces.Events.IErrorEventsHandler;
    import IKeyEventsHandler = Com.Wui.Framework.Gui.Interfaces.Events.IKeyEventsHandler;
    import IMouseEventsHandler = Com.Wui.Framework.Gui.Interfaces.Events.IMouseEventsHandler;
    import IScrollEventsHandler = Com.Wui.Framework.Gui.Interfaces.Events.IScrollEventsHandler;
    import IResizeEventsHandler = Com.Wui.Framework.Gui.Interfaces.Events.IResizeEventsHandler;
    import IMoveEventsHandler = Com.Wui.Framework.Gui.Interfaces.Events.IMoveEventsHandler;
    import IHttpRequestEventsHandler = Com.Wui.Framework.Gui.Interfaces.Events.IHttpRequestEventsHandler;
    import IAsyncRequestEventsHandler = Com.Wui.Framework.Gui.Interfaces.Events.IAsyncRequestEventsHandler;
    import IMessageEventsHandler = Com.Wui.Framework.Gui.Interfaces.Events.IMessageEventsHandler;

    /**
     * WindowEventsManager class provides handling of native events subscribed to window or body elements.
     */
    export class WindowEventsManager extends BaseObject {
        private events : EventsManager;

        constructor() {
            super();

            this.events = <EventsManager>EventsManager.getInstanceSingleton();
        }

        /**
         * @param {string} $type Validate this type of event.
         * @return {boolean} Returns true, if event type has been registered, otherwise false.
         */
        public Exists($type : string) : boolean {
            return this.events.Exists(this.getOwner($type), $type);
        }

        /**
         * @return {ArrayList<ArrayList<IEventsHandler>>} Returns list of all registered events.
         */
        public getAll() : ArrayList<ArrayList<IEventsHandler>> {
            return this.events.getAll().getItem(GeneralEventOwner.BODY);
        }

        /**
         * @param {string} $type Subscribe handler to this type of event.
         * @param {IEventsHandler} $handler Function suitable for handling of the event.
         * @return {void}
         */
        public setEvent($type : string, $handler : IEventsHandler) : void {
            this.events.setEvent(this.getOwner($type), $type, $handler);
        }

        /* tslint:disable: unified-signatures */
        public FireAsynchronousMethod($handler : () => void, $clearBefore? : boolean) : number;

        public FireAsynchronousMethod($handler : () => void, $waitForMilliseconds? : number) : number;

        /**
         * @param {Function} $handler Function suitable for handling of the event.
         * @param {boolean} [$clearBefore=true] Clean up currently running thread with same handler.
         * @param {number} [$waitForMilliseconds] Specify wait time before execution of the handler in milliseconds.
         * @return {number} Returns thread number allocated for asynchronous execution.
         */
        public FireAsynchronousMethod($handler : () => void, $clearBefore? : any, $waitForMilliseconds? : number) : number {
            return this.events.FireAsynchronousMethod($handler, $clearBefore, $waitForMilliseconds);
        }

        /* tslint:enable */

        /**
         * @param {string} $type Event type of subscribed handler.
         * @param {IEventsHandler} $handler Handler, which should be removed.
         * @return {void}
         */
        public RemoveHandler($type : string, $handler : IEventsHandler) : void {
            this.events.RemoveHandler(this.getOwner($type), $type, $handler);
        }

        /**
         * Clear all events, if event type has not been specified.
         * Clear subset of events subscribed to event type, if type has been specified.
         * @param {string} [$type] Specify desired type of event.
         * @return {void}
         */
        public Clear($type? : string) : void {
            this.events.Clear(this.getOwner($type), $type);
        }

        public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
            let output : string = "Registered events list:";
            if ($htmlTag) {
                output = "<b>" + output + "</b>";
            }
            output = $prefix + output + StringUtils.NewLine($htmlTag);
            this.getAll().foreach(($value : ArrayList<any>, $type? : string) : void => {
                output += $prefix + StringUtils.Tab(1, $htmlTag) + "[\"" + $type + "\"]"
                    + " hooked handlers count: " + $value.Length() + StringUtils.NewLine($htmlTag);
            });
            return output;
        }

        public toString() : string {
            return this.ToString();
        }

        /**
         * @param {IErrorEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnError($handler : IErrorEventsHandler) : void {
            this.setEvent(EventType.ON_ERROR, $handler);
        }

        /**
         * @param {IEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setBeforeRefresh($handler : IEventsHandler) : void {
            this.setEvent(EventType.BEFORE_REFRESH, $handler);
        }

        /**
         * @param {IEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnLoad($handler : IEventsHandler) : void {
            this.setEvent(EventType.ON_LOAD, $handler);
        }

        /**
         * @param {IEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnBlur($handler : IEventsHandler) : void {
            this.setEvent(EventType.ON_BLUR, $handler);
        }

        /**
         * @param {IEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnFocus($handler : IEventsHandler) : void {
            this.setEvent(EventType.ON_FOCUS, $handler);
        }

        /**
         * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnClick($handler : IMouseEventsHandler) : void {
            this.setEvent(EventType.ON_CLICK, $handler);
        }

        /**
         * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnRightClick($handler : IMouseEventsHandler) : void {
            this.setEvent(EventType.ON_RIGHT_CLICK, $handler);
        }

        /**
         * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnDoubleClick($handler : IMouseEventsHandler) : void {
            this.setEvent(EventType.ON_DOUBLE_CLICK, $handler);
        }

        /**
         * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnMouseOver($handler : IMouseEventsHandler) : void {
            this.setEvent(EventType.ON_MOUSE_OVER, $handler);
        }

        /**
         * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnMouseOut($handler : IMouseEventsHandler) : void {
            this.setEvent(EventType.ON_MOUSE_OUT, $handler);
        }

        /**
         * @param {IMoveEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnMove($handler : IMoveEventsHandler) : void {
            this.events.setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE, $handler);
        }

        /**
         * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnMouseDown($handler : IMouseEventsHandler) : void {
            this.setEvent(EventType.ON_MOUSE_DOWN, $handler);
        }

        /**
         * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnMouseUp($handler : IMouseEventsHandler) : void {
            this.setEvent(EventType.ON_MOUSE_UP, $handler);
        }

        /**
         * @param {IKeyEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnKeyDown($handler : IKeyEventsHandler) : void {
            this.setEvent(EventType.ON_KEY_DOWN, $handler);
        }

        /**
         * @param {IKeyEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnKeyUp($handler : IKeyEventsHandler) : void {
            this.setEvent(EventType.ON_KEY_UP, $handler);
        }

        /**
         * @param {IKeyEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnKeyPress($handler : IKeyEventsHandler) : void {
            this.setEvent(EventType.ON_KEY_PRESS, $handler);
        }

        /**
         * @param {IResizeEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnResize($handler : IResizeEventsHandler) : void {
            this.setEvent(EventType.ON_RESIZE, $handler);
        }

        /**
         * @param {IScrollEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnScroll($handler : IScrollEventsHandler) : void {
            this.setEvent(EventType.ON_SCROLL, $handler);
        }

        /**
         * @param {IHttpRequestEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnRequest($handler : IHttpRequestEventsHandler) : void {
            this.setEvent(EventType.ON_HTTP_REQUEST, $handler);
        }

        /**
         * @param {IAsyncRequestEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnAsyncRequest($handler : IAsyncRequestEventsHandler) : void {
            this.setEvent(EventType.ON_ASYNC_REQUEST, $handler);
        }

        /**
         * @param {IMessageEventsHandler} $handler Specify event callback for current event type.
         * @return {void}
         */
        public setOnMessage($handler : IMessageEventsHandler) : void {
            this.setEvent(EventType.ON_MESSAGE, $handler);
        }

        private getOwner($type : string) : string {
            switch ($type) {
            case EventType.ON_MOUSE_MOVE:
                return GeneralEventOwner.MOUSE_MOVE;
            case EventType.ON_HTTP_REQUEST:
            case EventType.ON_ASYNC_REQUEST:
                return GeneralEventOwner.WINDOW;
            default:
                return GeneralEventOwner.BODY;
            }
        }
    }
}
