/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Primitives {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import BaseGuiGroupObjectArgs = Com.Wui.Framework.Gui.Structures.BaseGuiGroupObjectArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import IBaseGuiGroupObject = Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseGuiGroupObject;
    import EventsManager = Com.Wui.Framework.Gui.Events.EventsManager;

    /**
     * BaseGuiGroupObject class renders group of GUI elements based on passed arguments.
     */
    export abstract class BaseGuiGroupObject extends FormsObject implements IBaseGuiGroupObject {
        private args : BaseGuiGroupObjectArgs;
        private elements : ArrayList<GuiCommons>;
        private isHovered : boolean;

        /**
         * @param {BaseGuiGroupObjectArgs} $configuration Specify configuration arguments for the GUI element.
         * @param {string} [$id] Force set element id instead of generated one.
         */
        constructor($configuration : BaseGuiGroupObjectArgs, $id? : string) {
            super($id);

            this.elements = new ArrayList<GuiCommons>();
            if (!ObjectValidator.IsEmptyOrNull($configuration)) {
                this.args = $configuration;
                this.registerGroup(this.args);
            }
            this.Visible($configuration.Visible());
            this.isHovered = false;
        }

        /**
         * @param {BaseGuiGroupObjectArgs} [$value] Specify configuration arguments for the GUI element.
         * @return {BaseGuiGroupObjectArgs} Returns the element's configuration arguments.
         */
        public Configuration($value? : BaseGuiGroupObjectArgs) : BaseGuiGroupObjectArgs {
            if (ObjectValidator.IsSet($value)) {
                if (!this.IsCompleted()) {
                    if (!this.args.Visible() && $value.Visible()) {
                        this.args = $value;
                        this.applyArgs(this.args.ForceSetValue());
                    } else {
                        this.args = $value;
                    }
                } else {
                    if (!this.args.IsMemberOf($value.getClassName())) {
                        ExceptionsManager.Throw(this.getClassName(), "Configuration type can not be changed at run time.");
                    }
                    this.args = $value;
                    this.applyArgs(this.args.ForceSetValue());
                    this.args.ForceSetValue(false);
                }
            }
            return this.args;
        }

        /**
         * @return {number} Returns calculated width in case of, that element has been loaded otherwise, returns configured width.
         */
        public getWidth() : number {
            if (this.IsCompleted()) {
                let width : number = 0;
                const getBordersWidth : any = ($id : string) : number => {
                    return ElementManager.getCssIntegerValue($id, "margin-left") +
                        ElementManager.getCssIntegerValue($id, "margin-right") +
                        ElementManager.getCssIntegerValue($id, "padding-left") +
                        ElementManager.getCssIntegerValue($id, "padding-right");
                };
                this.elements.foreach(($element : GuiCommons) : void => {
                    width +=
                        $element.getSize().Width() +
                        getBordersWidth($element.Id());
                });
                return width;
            }
            return this.getSize().Width();
        }

        /**
         * @return {Size} Returns current element's width and height based on available information.
         */
        public getSize() : Size {
            const size : Size = super.getSize();
            size.Width(this.getWidth());
            if (this.IsLoaded()) {
                size.Height(ElementManager.getElement(this.guiContentId()).offsetHeight);
            }
            return size;
        }

        /**
         * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
         * @return {boolean} Returns true, if element is in enabled mode, otherwise false.
         */
        public Enabled($value? : boolean) : boolean {
            const enabled : boolean = super.Enabled($value);
            if (ObjectValidator.IsSet($value)) {
                this.elements.foreach(($element : GuiCommons) : void => {
                    $element.Enabled($value);
                });
            }
            return enabled;
        }

        /**
         * @param {boolean} [$value] Specify, if element is in error status or not.
         * @return {boolean} Returns true, if element is in error state, otherwise false.
         */
        public Error($value? : boolean) : boolean {
            const error : boolean = super.Error($value);
            if (ObjectValidator.IsSet($value)) {
                this.elements.foreach(($element : GuiCommons) : void => {
                    if ($element.IsMemberOf(FormsObject) || ObjectValidator.IsSet((<any>$element).Error)) {
                        (<FormsObject>$element).Error($value);
                    }
                });
            }
            return error;
        }

        /**
         * @param {string|number|boolean} [$value]  Value or arguments object, which should be set to the BaseGuiGroupObject.
         * @return {string|number|boolean} Returns Gui Group object value or arguments object, if value has been specified, otherwise null.
         */
        public Value($value? : string | number | boolean) : string | number | boolean {
            const element : FormsObject = this.getInputElement();
            if (!ObjectValidator.IsEmptyOrNull(element)) {
                if (!this.IsLoaded()) {
                    return this.Configuration().Value($value);
                } else {
                    return element.Value($value);
                }
            }
            return null;
        }

        protected registerGroup($args? : BaseGuiGroupObjectArgs) : void {
            // override this method for ability to create gui group and distribute initial args
        }

        protected registerElement($element : GuiCommons, $thisPropertyName? : string, $outputIndex? : number) : void {
            $outputIndex = !ObjectValidator.IsEmptyOrNull($outputIndex) ? $outputIndex : this.elements.Length();
            if (!this.elements.KeyExists($outputIndex)) {
                this.elements.Add($element, $outputIndex);
            } else {
                const elements : ArrayList<GuiCommons> = new ArrayList<GuiCommons>();
                this.elements.foreach(($item : GuiCommons, $index : number) : void => {
                    if ($index === $outputIndex) {
                        elements.Add($element);
                    }
                    elements.Add($item);
                });
                this.elements = elements;
            }
            if (!ObjectValidator.IsEmptyOrNull($thisPropertyName)) {
                this[$thisPropertyName] = $element;
            } else {
                this[$element.Id()] = $element;
            }
            if (this.IsLoaded()) {
                $element.Parent(this);
                $element.InstanceOwner(this.InstanceOwner());
                this.getChildElements().Add($element, $element.Id());
                this.getInnerHtmlMap().Add($element);
            }
        }

        protected getElement<T>($nameOrIndex : string | number) : T {
            if (!ObjectValidator.IsEmptyOrNull($nameOrIndex)) {
                if (ObjectValidator.IsString($nameOrIndex)) {
                    if (this.hasOwnProperty(<string>$nameOrIndex)) {
                        return this[<string>$nameOrIndex];
                    } else {
                        return null;
                    }
                } else {
                    return <any>this.elements.getItem($nameOrIndex);
                }
            }
            return null;
        }

        protected getInputElement() : FormsObject {
            return null;
        }

        protected innerCode() : IGuiElement {
            const zIndexActive : string = "1000";
            this.getEvents().setOnMouseOver(
                ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                    const element : BaseGuiGroupObject = <BaseGuiGroupObject>$eventArgs.Owner();
                    const elements : ArrayList<BaseGuiGroupObject> = <ArrayList<BaseGuiGroupObject>>$manager.getType(BaseGuiGroupObject);
                    elements.foreach(($element : BaseGuiGroupObject) : void => {
                        if ($element !== element && $element.IsCompleted() && $element.Visible() && !$manager.IsActive($element)) {
                            ElementManager.SendToBack($element.Id());
                        }
                    });
                    element.isHovered = true;
                    if (element.Enabled() && !$manager.IsActive(BaseGuiGroupObject) && !$manager.IsActive(element.getInputElement())) {
                        ElementManager.setCssProperty(element, "z-index", zIndexActive);
                    }
                });

            this.getEvents().setOnMouseOut(
                ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                    const element : BaseGuiGroupObject = <BaseGuiGroupObject>$eventArgs.Owner();
                    element.isHovered = false;
                    element.getEvents().FireAsynchronousMethod(() : void => {
                        if (!element.isHovered && !$manager.IsActive(element)) {
                            ElementManager.SendToBack(element.Id());
                        }
                    }, false, 30);
                });

            const input : FormsObject = this.getInputElement();
            if (!ObjectValidator.IsEmptyOrNull(input)) {
                input.StyleClassName("UserControlInput");

                input.getEvents().setOnMouseOver(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                    const parent : BaseGuiGroupObject = <BaseGuiGroupObject>(<FormsObject>$eventArgs.Owner()).Parent();
                    if (parent.Enabled() && !$manager.IsActive(BaseGuiGroupObject)) {
                        ElementManager.setCssProperty(parent, "z-index", zIndexActive);
                    }
                    parent.isHovered = true;
                });

                input.getEvents().setOnClick(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                    const element : BaseGuiGroupObject = <BaseGuiGroupObject>($eventArgs.Owner().Parent());
                    const elements : ArrayList<BaseGuiGroupObject> = <ArrayList<BaseGuiGroupObject>>$manager.getType(BaseGuiGroupObject);
                    elements.foreach(($element : BaseGuiGroupObject) : void => {
                        if ($element !== element && $element.IsCompleted() && $element.Visible()) {
                            ElementManager.SendToBack($element.Id());
                        }
                    });
                    element.isHovered = true;
                    ElementManager.setCssProperty(element, "z-index", zIndexActive);
                });

                this.getEvents().setOnComplete(() : void => {
                    input.getEvents().setOnFocus(
                        ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                            const parent : BaseGuiGroupObject = <BaseGuiGroupObject>(<FormsObject>$eventArgs.Owner()).Parent();
                            $manager.setActive(parent, true);
                            ElementManager.setCssProperty(parent, "z-index", zIndexActive);
                        });
                    input.getEvents().setOnBlur(
                        ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                            $manager.setActive(<BaseGuiGroupObject>(<FormsObject>$eventArgs.Owner()).Parent(), false);
                        });
                });

                input.getEvents().setOnChange(($eventArgs : KeyEventArgs) : void => {
                    const parent : BaseGuiGroupObject = <BaseGuiGroupObject>(<FormsObject>$eventArgs.Owner()).Parent();
                    if (!$eventArgs.IsTypeOf(KeyEventArgs) ||
                        $eventArgs.IsTypeOf(KeyEventArgs) && $eventArgs.getKeyCode() === KeyMap.ENTER) {
                        const eventArgs : KeyEventArgs = new KeyEventArgs($eventArgs.NativeEventArgs());
                        eventArgs.Owner(parent);
                        parent.setChanged();
                        EventsManager.getInstanceSingleton().FireEvent(parent, EventType.ON_CHANGE, eventArgs);
                    }
                });
            }

            this.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
                const element : BaseGuiGroupObject = <BaseGuiGroupObject>$eventArgs.Owner();
                if (element.Visible()) {
                    element.applyArgs();
                }
            });

            this.getEvents().setBeforeLoad(($eventArgs : EventArgs) : void => {
                const element : BaseGuiGroupObject = <BaseGuiGroupObject>$eventArgs.Owner();
                element.Visible(element.Configuration().Visible());
            });

            const innerCode : IGuiElement = super.innerCode();
            this.applyArgs();
            return innerCode;
        }

        protected groupInnerHtml() : IGuiElement {
            const output : IGuiElement = this.addElement();
            this.elements.SortByKeyUp();
            this.elements.foreach(($element : GuiCommons) : void => {
                output.Add($element);
            });
            return output;
        }

        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_Envelop")
                .StyleClassName(StringUtils.Remove(this.args.getClassNameWithoutNamespace(), "Args"))
                .Add(this.groupInnerHtml());
        }

        protected cssContainerName() : string {
            if (this.getClassNameWithoutNamespace() !== BaseGuiGroupObject.ClassNameWithoutNamespace()) {
                return BaseGuiGroupObject.ClassNameWithoutNamespace() + " " + this.getClassNameWithoutNamespace();
            }
            return super.cssContainerName();
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = super.excludeCacheData();
            exclude.push("isHovered");
            return exclude;
        }

        protected updateHandler($args? : BaseGuiGroupObjectArgs) : void {
            // override this method for ability to distribute gui group args
        }

        protected resizeHandler($args? : BaseGuiGroupObjectArgs) : void {
            const width : number = this.getWidth()
                - ElementManager.getCssIntegerValue(this.Id(), "margin-left")
                - ElementManager.getCssIntegerValue(this.Id(), "margin-right")
                - ElementManager.getCssIntegerValue(this.Id(), "padding-left")
                - ElementManager.getCssIntegerValue(this.Id(), "padding-right");
            ElementManager.setWidth(this.Id(), width);
            ElementManager.setWidth(this.Id() + "_Envelop", width);
        }

        private applyArgs($force : boolean = false) : void {
            if (!this.IsLoaded()) {
                this.args.getGuiOptionsList().foreach(($option : GuiOptionType) : void => {
                    if (ObjectValidator.IsSet($option)) {
                        this.getGuiOptions().Add($option);
                        this.elements.foreach(($element : GuiCommons) : void => {
                            $element.getGuiOptions().Add($option);
                        });
                    }
                });
            }

            const updateInput : any = () : void => {
                if (!this.IsCached() || this.IsCompleted()) {
                    this.Visible(this.args.Visible());
                    this.Enabled(this.args.Enabled());
                    this.Error(this.args.Error());
                }

                this.Title().Text(this.args.TitleText());
                if (!ObjectValidator.IsEmptyOrNull(this.getInputElement())) {
                    if (!this.getInputElement().IsLoaded() || $force) {
                        this.getInputElement().Value(this.args.Value());
                    }
                }
                this.updateHandler(this.args);
            };

            if (this.IsCompleted()) {
                this.getEvents().FireAsynchronousMethod(() : void => {
                    updateInput();
                }, false);

                if (this.args.Resize()) {
                    this.resizeHandler(this.args);
                }
                this.args.Resize(false);

                ElementManager.setCssProperty(this.Id(), "float", "left");
                ElementManager.setOpacity(this.Id(), 100);
            } else {
                updateInput();
            }
        }
    }
}
