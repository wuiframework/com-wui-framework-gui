/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Primitives {
    "use strict";
    import IFormsObject = Com.Wui.Framework.Gui.Interfaces.Primitives.IFormsObject;
    import IFormsObjectEvents = Com.Wui.Framework.Gui.Interfaces.Events.IFormsObjectEvents;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import IBaseGuiObject = Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseGuiObject;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import INotification = Com.Wui.Framework.Gui.Interfaces.Components.INotification;
    import FormValue = Com.Wui.Framework.Gui.Structures.FormValue;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import ElementEventsManager = Com.Wui.Framework.Gui.Events.ElementEventsManager;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import PersistenceFactory = Com.Wui.Framework.Commons.PersistenceApi.PersistenceFactory;
    import IPersistenceHandler = Com.Wui.Framework.Commons.Interfaces.IPersistenceHandler;
    import PersistenceType = Com.Wui.Framework.Gui.Enums.PersistenceType;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import GuiCommonsArgType = Com.Wui.Framework.Gui.Enums.GuiCommonsArgType;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import EventsManager = Com.Wui.Framework.Gui.Events.EventsManager;

    /**
     * FormsObject should be used as abstract class for extending to the GUI objects type of input
     * and it is providing base methods connected with form elements.
     */
    export abstract class FormsObject extends BaseGuiObject implements IFormsObject {

        protected valuesPersistence : IPersistenceHandler;
        protected errorFlags : IPersistenceHandler;
        private notification : INotification;
        private error : boolean;
        private tabIndex : number;
        private selectorEvents : ElementEventsManager;
        private isPersistent : boolean;

        /**
         * @param {FormsObject} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnOn($element : FormsObject, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
            if (!$manager.IsActive($element)) {
                ElementManager.TurnOn($element.Id() + "_Enabled");
            }
        }

        /**
         * @param {FormsObject} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnOff($element : FormsObject, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
            if (!$manager.IsActive($element)) {
                ElementManager.TurnOff($element.Id() + "_Enabled");
            }
        }

        /**
         * @param {FormsObject} $element Specify element, which should be handled.
         * @param {GuiObjectManager} [$manager] Specify instance of GuiObjectManager.
         * @param {Reflection} [$reflection] Specify instance of Reflection.
         * @return {void}
         */
        public static TurnActive($element : FormsObject, $manager? : GuiObjectManager, $reflection? : Reflection) : void {
            ElementManager.TurnActive($element.Id() + "_Enabled");
        }

        /**
         * @param {FormsObject} $element Specify element, which should be handled.
         * @return {void}
         */
        public static Focus($element : FormsObject) : void {
            const manager : GuiObjectManager = $element.getGuiManager();
            manager.setActive($element, true);
            const eventArgs : EventArgs = new EventArgs();
            eventArgs.Owner($element);
            EventsManager.getInstanceSingleton().FireEvent($element.getClassName(), EventType.ON_FOCUS, eventArgs);
            EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_FOCUS, eventArgs);
            const thisClass : any = Reflection.getInstance().getClass($element.getClassName());
            thisClass.TurnActive($element, manager, Reflection.getInstance());
        }

        /**
         * @return {void}
         */
        public static Blur() : void {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            const reflection : Reflection = Reflection.getInstance();
            const elements : ArrayList<IGuiCommons> = manager.getActive();
            elements.foreach(($element : IGuiCommons) : void => {
                (<FormsObject>$element).getGuiManager().setActive($element, false);
                const eventArgs : EventArgs = new EventArgs();
                eventArgs.Owner($element);
                EventsManager.getInstanceSingleton().FireEvent($element.getClassName(), EventType.ON_BLUR, eventArgs);
                EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_BLUR, eventArgs);
                const thisClass : any = reflection.getClass($element.getClassName());
                thisClass.TurnOff(<FormsObject>$element, manager, reflection);
            });
        }

        /**
         * @param {FormsObject|string} [$elementOrId] Specify GUI element or DOM element from, which should be collected values.
         * @return {ArrayList<FormValue>} Returns list of FormValue objects, which are representing all elements
         * interacted by the user. If element id has been specified, list of values is filtered by the id and returned
         * are only values of the element with desired id.
         * If element with desired id is type of panel, returned values will be focused on panel elements and it's
         * subpanels elements.
         */
        public static CollectValues($elementOrId? : FormsObject | string) : ArrayList<FormValue> {
            let output : ArrayList<FormValue> = new ArrayList<FormValue>();
            const register : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            if (ObjectValidator.IsSet($elementOrId)) {
                if (register.Exists(<FormsObject>$elementOrId)) {
                    output = FormsObject.valuesCollector(<FormsObject>$elementOrId, output);
                    (<FormsObject>$elementOrId).getChildElements().foreach(($child : IGuiCommons) : void => {
                        FormsObject.valuesCollector($child, output);
                    });
                } else if (ObjectValidator.IsSet(document.forms[<string>$elementOrId])) {
                    const inputs : HTMLCollectionOf<HTMLInputElement> =
                        document.forms[<string>$elementOrId].getElementsByTagName("input");
                    const allObjects : ArrayList<IGuiCommons> = register.getAll();
                    let index : number;
                    const inputsLength : number = inputs.length;
                    for (index = 0; index < inputsLength; index++) {
                        let id : string = inputs[index].getAttribute("id");
                        if (ObjectValidator.IsEmptyOrNull(id)) {
                            id = inputs[index].getAttribute("name");
                        }
                        if (!ObjectValidator.IsEmptyOrNull(id)) {
                            if (allObjects.KeyExists(id)) {
                                output = FormsObject.valuesCollector(allObjects.getItem(id), output);
                            } else {
                                output.Add(new FormValue(id, inputs[index].getAttribute("value")));
                            }
                        }
                    }
                }
            } else {
                register.getType(<ClassName>FormsObject).foreach(($element : IGuiCommons) : void => {
                    output = FormsObject.valuesCollector($element, output);
                });
            }
            return output;
        }

        /**
         * @param {string} $id Specify element id, which should be submitted.
         * @param {string} $controllerLink Specify controller link, which is suitable for processing of submitted values.
         * @return {void}
         */
        public static Submit($id : string, $controllerLink : string) : void {
            Loader.getInstance().getHttpManager().ReloadTo($controllerLink, this.CollectValues($id), true);
        }

        protected static onKeyEventHandler($eventArgs : KeyEventArgs, $manager : GuiObjectManager) : void {
            if ($eventArgs.getKeyCode() === KeyMap.SPACE || $eventArgs.getKeyCode() === KeyMap.ENTER) {
                EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                    const elements : ArrayList<IGuiCommons> = $manager.getActive();
                    elements.foreach(($element : IBaseGuiObject) : void => {
                        const element : HTMLElement = ElementManager.getElement($element.getEvents().Subscriber());
                        if (!ObjectValidator.IsEmptyOrNull(element)) {
                            element.click();
                        }
                    });
                });
            }
        }

        protected static setErrorStyle($element : FormsObject) : void {
            if (!ObjectValidator.IsEmptyOrNull($element)) {
                if ($element.Error()) {
                    ElementManager.setClassName($element.Id() + "_Status", GeneralCssNames.ERROR);
                } else {
                    ElementManager.setClassName($element.Id() + "_Status", "");
                }
            }
        }

        private static valuesCollector($object : IGuiCommons, $values : ArrayList<FormValue>) : ArrayList<FormValue> {
            if (Reflection.getInstance().Implements($object, IFormsObject)) {
                const object : FormsObject = <FormsObject>$object;
                if (object.Changed()) {
                    $values.Add(new FormValue(object.InstancePath(), object.Value(), object.Error()));
                }
            }
            return $values;
        }

        /**
         * @return {IFormsObjectEvents} Returns events connected with the element.
         */
        public getEvents() : IFormsObjectEvents {
            return <IFormsObjectEvents>super.getEvents();
        }

        /**
         * @return {INotification} Returns notification component associated with current GUI element.
         */
        public Notification() : INotification {
            if (ObjectValidator.IsEmptyOrNull(this.notification)) {
                const notificationClass : any = this.getNotificationClass();
                if (!ObjectValidator.IsEmptyOrNull(notificationClass)) {
                    this.notification = new notificationClass(null, null, this.Id() + "_Notification");
                }
            }
            return this.notification;
        }

        /**
         * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
         * @return {boolean} Returns true, if element is in enabled mode, otherwise false.
         */
        public Enabled($value? : boolean) : boolean {
            const enabled : boolean = super.Enabled($value);
            if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
                if (enabled || this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                    ElementManager.Enabled(this.Id(), enabled);
                }
                if (enabled) {
                    this.Error(!ObjectValidator.IsSet(this.error) ? false : this.error);
                }
            }

            return enabled;
        }

        /**
         * @param {boolean} [$value] Specify, if element is in error status or not.
         * @return {boolean} Returns true, if element is in error state, otherwise false.
         */
        public Error($value? : boolean) : boolean {
            if (!ObjectValidator.IsSet(this.error)) {
                this.error = false;
            }
            if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
                if (this.error !== $value) {
                    this.setChanged();
                    const eventArgs : EventArgs = new EventArgs();
                    eventArgs.Owner(this);
                    EventsManager.getInstanceSingleton().FireEvent(this, EventType.ON_CHANGE, eventArgs);
                    EventsManager.getInstanceSingleton().FireEvent(this.getClassName(), EventType.ON_CHANGE, eventArgs);
                }
            }

            if (!this.IsCompleted() && this.errorFlags.Exists(this.InstancePath())) {
                this.error = this.errorFlags.Variable(this.InstancePath());
            } else if (ObjectValidator.IsSet($value)) {
                this.error = Property.Boolean(this.error, $value);
            } else if (!ObjectValidator.IsSet(this.error)) {
                this.error = false;
            }

            if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id()) && !this.getGuiManager().IsActive(this)) {
                Reflection.getInstance().getClass(this.getClassName()).setErrorStyle(this);
            }

            return this.error;
        }

        /**
         * @param {number} [$value] Set index for TAB key navigation in the page.
         * @return {number} Returns index of element in the page based on TAB key elements register.
         */
        public TabIndex($value? : number) : number {
            this.tabIndex = Property.Integer(this.tabIndex, $value);
            return this.tabIndex;
        }

        /**
         * @return {IGuiCommonsArg[]} Returns array of element's attributes.
         */
        public getArgs() : IGuiCommonsArg[] {
            const args : IGuiCommonsArg[] = super.getArgs();
            args.push({
                name : "Error",
                type : GuiCommonsArgType.BOOLEAN,
                value: this.Error()
            });
            args.push({
                name : "TabIndex",
                type : GuiCommonsArgType.NUMBER,
                value: this.TabIndex()
            });
            return args;
        }

        /**
         * @param {IGuiCommonsArg} $value Specify argument value, which should be passed to element.
         * @param {boolean} [$force=false] Specify, if value should be set without fire of events connected with argument change.
         * @return {void}
         */
        public setArg($value : IGuiCommonsArg, $force : boolean = false) : void {
            switch ($value.name) {
            case "Error":
                this.Error(<boolean>$value.value);
                break;
            case "TabIndex":
                this.TabIndex(<number>$value.value);
                break;
            default:
                super.setArg($value, $force);
                break;
            }
        }

        /**
         * @return {IFormsObjectEvents} Returns events connected with selection of the element by TAB key.
         */
        public getSelectorEvents() : IFormsObjectEvents {
            if (ObjectValidator.IsEmptyOrNull(this.selectorEvents)) {
                this.selectorEvents = new ElementEventsManager(this, this.Id() + "_Input");
            }
            return <IFormsObjectEvents>this.selectorEvents;
        }

        /**
         * @return {boolean} Returns true, if element's value should be persistent, otherwise false.
         */
        public IsPersistent($value? : boolean) : boolean {
            if (!ObjectValidator.IsSet($value)) {
                const persistenceExists : boolean = this.valuesPersistence.Exists(this.InstancePath());
                if (!ObjectValidator.IsSet(this.isPersistent)) {
                    this.isPersistent = persistenceExists;
                }
                if (!this.isPersistent && persistenceExists) {
                    this.valuesPersistence.Destroy(this.InstancePath());
                }
            }
            return this.isPersistent = Property.Boolean(this.isPersistent, $value);
        }

        protected statusCss() : string {
            let envelopType : string;
            if (this.Enabled()) {
                this.Error() ? envelopType = GeneralCssNames.ERROR : envelopType = "";
            } else {
                envelopType = GeneralCssNames.DISABLE;
            }

            return envelopType;
        }

        protected errorCssName() : string {
            return this.Error() ? GeneralCssNames.ERROR : "";
        }

        /**
         * @return {INotification} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.INotification
         */
        protected getNotificationClass() : any {
            return null;
        }

        protected selectorElement() : IGuiElement {
            const input : HTMLButtonElement = document.createElement("button");
            input.id = this.Id() + "_Input";
            input.name = this.Id();
            input.style.opacity = "0";
            input.className = GeneralCssNames.GUI_SELECTOR;
            if (!ObjectValidator.IsEmptyOrNull(this.TabIndex())) {
                input.tabIndex = this.TabIndex();
            }
            return this.addElement().Add(input);
        }

        protected innerCode() : IGuiElement {
            this.getSelectorEvents().setOnFocus(
                ($eventArgs : MouseEventArgs, $manager? : GuiObjectManager, $reflection? : Reflection) : void => {
                    const element : FormsObject = <FormsObject>$eventArgs.Owner();
                    $reflection.getClass(element.getClassName()).Focus(element);
                });
            this.getSelectorEvents().setOnBlur(
                ($eventArgs : MouseEventArgs, $manager? : GuiObjectManager, $reflection? : Reflection) : void => {
                    $reflection.getClass($eventArgs.Owner().getClassName()).Blur();
                });

            const thisClass : any = Reflection.getInstance().getClass(this.getClassName());

            this.getEvents().setOnBlur(thisClass.onUnhoverEventHandler);

            if (!this.IsMemberOf(BasePanel)) {
                this.getEvents().setOnMouseOver(
                    ($eventArgs : MouseEventArgs, $manager? : GuiObjectManager, $reflection? : Reflection) : void => {
                        thisClass.TurnOn($eventArgs.Owner(), $manager, $reflection);
                    });
                this.getEvents().setOnMouseOut(
                    ($eventArgs : MouseEventArgs, $manager? : GuiObjectManager, $reflection? : Reflection) : void => {
                        thisClass.TurnOff($eventArgs.Owner(), $manager, $reflection);
                    });
            }
            this.getEvents().setOnMouseDown(
                ($eventArgs : MouseEventArgs, $manager? : GuiObjectManager, $reflection? : Reflection) : void => {
                    thisClass.TurnActive($eventArgs.Owner(), $manager, $reflection);
                });
            this.getEvents().setOnMouseUp(
                ($eventArgs : MouseEventArgs, $manager? : GuiObjectManager, $reflection? : Reflection) : void => {
                    thisClass.TurnOn($eventArgs.Owner(), $manager, $reflection);
                });

            WindowManager.getEvents().setOnKeyPress(thisClass.onKeyEventHandler);

            this.getEvents().setOnComplete(
                ($eventArgs : EventArgs) : void => {
                    const element : FormsObject = <FormsObject>$eventArgs.Owner();
                    element.getSelectorEvents().Subscribe();
                    if (element.IsCached()) {
                        if (element.errorFlags.Exists(element.InstancePath())) {
                            element.error = element.errorFlags.Variable(element.InstancePath());
                        } else if (!ObjectValidator.IsSet(element.error)) {
                            element.error = false;
                        }
                        thisClass.setErrorStyle(element);
                    }
                });

            return super.innerCode();
        }

        /**
         * Specify attributes of the instance after unserialization.
         */
        protected setInstanceAttributes() : void {
            super.setInstanceAttributes();

            this.valuesPersistence = PersistenceFactory.getPersistence(PersistenceType.FORM_VALUES);
            this.errorFlags = PersistenceFactory.getPersistence(PersistenceType.ERROR_FLAGS);
        }

        protected excludeSerializationData() : string[] {
            const exclude : string[] = super.excludeSerializationData();
            exclude.push("error", "tabIndex", "autofillPersistence", "valuesPersistence", "errorFlags", "isPersistent");
            return exclude;
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = super.excludeCacheData();
            exclude.push("notification", "selectorEvents");
            if (this.error === false) {
                exclude.push("error");
            }
            if (ObjectValidator.IsEmptyOrNull(this.TabIndex())) {
                exclude.push("tabIndex");
            }
            if (this.isPersistent === true) {
                exclude.push("isPersistent");
            }
            return exclude;
        }
    }
}
