/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Primitives {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IBasePanel = Com.Wui.Framework.Gui.Interfaces.Primitives.IBasePanel;
    import IBasePanelEvents = Com.Wui.Framework.Gui.Interfaces.Events.IBasePanelEvents;
    import IBasePanelViewer = Com.Wui.Framework.Gui.Interfaces.Primitives.IBasePanelViewer;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import IIcon = Com.Wui.Framework.Gui.Interfaces.UserControls.IIcon;
    import ILabel = Com.Wui.Framework.Gui.Interfaces.UserControls.ILabel;
    import IScrollBar = Com.Wui.Framework.Gui.Interfaces.Components.IScrollBar;
    import PanelContentType = Com.Wui.Framework.Gui.Enums.PanelContentType;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import HttpManager = Com.Wui.Framework.Commons.HttpProcessor.HttpManager;
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import PanelResizeEventArgs = Com.Wui.Framework.Gui.Events.Args.PanelResizeEventArgs;
    import ScrollEventArgs = Com.Wui.Framework.Gui.Events.Args.ScrollEventArgs;
    import OrientationType = Com.Wui.Framework.Gui.Enums.OrientationType;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import GeneralEventOwner = Com.Wui.Framework.Commons.Enums.Events.GeneralEventOwner;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IllegalArgumentException = Com.Wui.Framework.Commons.Exceptions.Type.IllegalArgumentException;
    import IEventsManager = Com.Wui.Framework.Gui.Interfaces.IEventsManager;
    import TimeoutManager = Com.Wui.Framework.Commons.Events.TimeoutManager;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import GuiCommonsArgType = Com.Wui.Framework.Gui.Enums.GuiCommonsArgType;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import IArrayList = Com.Wui.Framework.Commons.Interfaces.IArrayList;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import DisplayUnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import IResponsiveElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IResponsiveElement;
    import IBaseViewer = Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseViewer;
    import IBaseObject = Com.Wui.Framework.Commons.Interfaces.IBaseObject;
    import EventsManager = Com.Wui.Framework.Gui.Events.EventsManager;
    import VisibilityStrategy = Com.Wui.Framework.Gui.Enums.VisibilityStrategy;
    import IContainerSizeInfo = Com.Wui.Framework.Gui.Interfaces.Primitives.IContainerSizeInfo;
    import IContainerVisibilityInfo = Com.Wui.Framework.Gui.Interfaces.Primitives.IContainerVisibilityInfo;

    /**
     * BasePanel should be used as abstract class for extending to the GUI panels and
     * it is providing base methods connected with panel implementation and behaviour.
     * This class also provides static component methods determined to handle panel GUI objects.
     */
    export class BasePanel extends FormsObject implements IBasePanel {
        public loaderIcon : IIcon;
        public loaderText : ILabel;
        public verticalScrollBar : IScrollBar;
        public horizontalScrollBar : IScrollBar;
        private childPanelList : ArrayList<IBasePanelViewer>;
        private asyncChildLoaders : ArrayList<object>;
        private contentType : PanelContentType;
        private scrollable : boolean;
        private width : number;
        private height : number;
        private asyncInnerCode : IGuiElement;
        private asyncChildrenLoader : TimeoutManager;

        /**
         * @param {BasePanel} $element Specify element, which should be handled.
         * @return {void}
         */
        public static TurnOn($element : BasePanel) : void {
            // override this method for ability to handle element's ON state
        }

        /**
         * @param {BasePanel} $element Specify element, which should be handled.
         * @return {void}
         */
        public static TurnOff($element : BasePanel) : void {
            // override this method for ability to handle element's OFF state
        }

        /**
         * @param {BasePanel} $element Specify element, which should be handled.
         * @return {void}
         */
        public static TurnActive($element : BasePanel) : void {
            // override this method for ability to handle element's ACTIVE state
        }

        /**
         * @param {BasePanel} $element Specify element, which should be handled.
         * @return {void}
         */
        public static Show($element : BasePanel) : void {
            const id : string = $element.Id();
            if (ElementManager.Exists(id)) {
                if (Reflection.getInstance().IsMemberOf($element, BasePanel) && $element.Visible()) {
                    ElementManager.Show(id + "_PanelEnvelop");
                    ElementManager.Show(id);
                    ElementManager.Show(id + "_PanelContentEnvelop");
                    if (!$element.IsLoaded()) {
                        ElementManager.ClearCssProperty(id + "_PanelEnvelop", "float");
                    }
                    const panelSize : Size = new Size(id, true);
                    if ($element.Width() === -1) {
                        $element.Width(panelSize.Width());
                    }
                    if ($element.Height() === -1) {
                        $element.Height(panelSize.Height());
                        if ($element.Height() === 0) {
                            Echo.Println("Panel " + $element.Id() + " is missing height attribute. " +
                                "Did you forget to specify Height() or css height value?");
                            $element.Height(100);
                        }
                    }

                    if ($element.ContentType() === PanelContentType.ASYNC_LOADER && ElementManager.IsVisible(id + "_AsyncProgress")) {
                        const loaderSize : Size = new Size(id + "_AsyncProgress");
                        let loaderWidth : number = 0;
                        if (ObjectValidator.IsSet($element.loaderIcon)) {
                            loaderWidth += ElementManager.getElement($element.loaderIcon.Id()).offsetWidth;
                        }
                        if (ObjectValidator.IsSet($element.loaderText)) {
                            loaderWidth += ElementManager.getElement($element.loaderText.Id()).offsetWidth;
                        }
                        loaderSize.Width(loaderWidth);
                        if (loaderSize.Height() === 0) {
                            let loaderHeight : number = 0;
                            if (ObjectValidator.IsSet($element.loaderIcon) && $element.loaderIcon.Visible()) {
                                loaderHeight = ElementManager.getElement($element.loaderIcon.Id()).offsetHeight;
                            }
                            if (ObjectValidator.IsSet($element.loaderText) && $element.loaderText.Visible()) {
                                const loaderTextHeight : number = ElementManager.getElement($element.loaderText.Id()).offsetHeight;
                                if (loaderHeight < loaderTextHeight) {
                                    loaderHeight = loaderTextHeight;
                                }
                            }
                            loaderSize.Height(loaderHeight);
                        }

                        ElementManager.setSize(id, $element.Width() - ElementManager.getWidthOffset($element.Id()),
                            $element.Height() - ElementManager.getWidthOffset($element.Id()));
                        ElementManager.setCssProperty(id + "_PanelEnvelop", "float", "left");

                        // todo : change to absolute positioned automatically centered
                        ElementManager.setCssProperty(id + "_AsyncProgress", "top",
                            Math.ceil($element.Height() / 2 - loaderSize.Height() / 2));
                        ElementManager.setCssProperty(id + "_AsyncProgress", "left",
                            Math.ceil($element.Width() / 2 - loaderSize.Width() / 2));

                        const events : IEventsManager = EventsManager.getInstanceSingleton();
                        if (!$element.IsLoaded()) {
                            if (!$element.IsPrepared()) {
                                $element.asyncInnerCode = $element.innerCode();
                                if ($element.IsCached()) {
                                    $element.InstanceOwner().PrepareImplementation();
                                }
                            }
                            const loaderItems : ArrayList<() => void> = new ArrayList<() => void>();
                            if (!ObjectValidator.IsEmptyOrNull($element.Parent())) {
                                $element.getEvents().setOnLoad(
                                    ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                                        const element : any = $eventArgs.Owner();
                                        if ($reflection.IsMemberOf(element, BasePanel)) {
                                            loaderItems.Add(() : void => {
                                                if (element.Visible() && element.ContentType() === PanelContentType.ASYNC_LOADER) {
                                                    events.FireEvent(element, EventType.ON_ASYNC_REQUEST);
                                                }
                                            });
                                        } else if (ObjectValidator.IsFunction(element.PanelViewer)
                                            && !ObjectValidator.IsEmptyOrNull(element.PanelViewer())
                                            && !ObjectValidator.IsEmptyOrNull(element.PanelViewer().getInstance())) {
                                            loaderItems.Add(() : void => {
                                                if (element.Visible() &&
                                                    element.PanelViewer().getInstance().ContentType() === PanelContentType.ASYNC_LOADER) {
                                                    events.FireEvent(
                                                        element.PanelViewer().getInstance(), EventType.ON_ASYNC_REQUEST, false);
                                                }
                                            });
                                        }
                                        BasePanel.asyncLoadChildren(loaderItems);
                                    });
                            } else {
                                loaderItems.Add(() : void => {
                                    if ($element.Visible() && $element.ContentType() === PanelContentType.ASYNC_LOADER) {
                                        events.FireEvent($element, EventType.ON_ASYNC_REQUEST, false);
                                    }
                                });
                            }
                            BasePanel.asyncLoadChildren(loaderItems);
                        } else if (!$element.IsCompleted()) {
                            events.FireEvent($element, EventType.ON_LOAD);
                        }
                    } else {
                        if (!$element.IsCached()) {
                            ElementManager.ClearCssProperty(id, "overflow");
                            ElementManager.ClearCssProperty(id, "overflowX");
                            ElementManager.ClearCssProperty(id, "overflowY");
                            ElementManager.ClearCssProperty(id + "_PanelContentEnvelop", "float");
                            ElementManager.ClearCssProperty(id + "_PanelContentEnvelop", "overflow");
                            ElementManager.ClearCssProperty(id + "_PanelContentEnvelop", "overflowX");
                            ElementManager.ClearCssProperty(id + "_PanelContentEnvelop", "overflowY");
                            ElementManager.ClearCssProperty(id + "_PanelContentEnvelop", "width");
                            ElementManager.ClearCssProperty(id + "_PanelContentEnvelop", "height");

                            ElementManager.ClearCssProperty(id + "_PanelContent", "width");
                            ElementManager.ClearCssProperty(id + "_PanelContent", "height");

                            ElementManager.setSize(id, $element.Width() - ElementManager.getWidthOffset($element.Id()),
                                $element.Height() - ElementManager.getHeightOffset($element.Id()));

                            ElementManager.setCssProperty(id, "overflow-x", "hidden");
                            ElementManager.setCssProperty(id, "overflow-y", "hidden");
                            ElementManager.setCssProperty(id + "_PanelEnvelop", "float", "left");

                            ElementManager.setCssProperty(id + "_PanelContentEnvelop", "float", "left");
                            ElementManager.setCssProperty(id + "_PanelContentEnvelop", "overflow-x", "hidden");
                            ElementManager.setCssProperty(id + "_PanelContentEnvelop", "overflow-y", "hidden");

                            BasePanel.scrollBarVisibilityHandler($element);
                        }
                        if ($element.IsCompleted()) {
                            EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_SHOW);
                        }
                    }
                }
            }
        }

        /**
         * Provide panel's scroll bars update as reaction on panel content focus.
         * @param {EventArgs} $eventArgs Event args, which should be processed.
         * @param {GuiObjectManager} [$manager] Provide gui object manager instance.
         * @param {Reflection} [$reflection] Provide reflection instance.
         * @return {void}
         */
        public static ContentFocusHandler($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void {
            let element : BasePanel = <BasePanel>$eventArgs.Owner();
            if (!ObjectValidator.IsEmptyOrNull(element.Parent())) {
                element = <BasePanel>element.Parent();
                if ($reflection.IsMemberOf(element, BasePanel)) {
                    EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                        BasePanel.scrollBarVisibilityHandler(element);
                    }, 100);
                }
            }
        }

        protected static onHoverEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager, $reflection ? : Reflection) : void {
            $manager.setHovered($eventArgs.Owner(), true);
        }

        protected static getFirstScrollable($element : BasePanel, $manager : GuiObjectManager,
                                            $reflection? : Reflection, $orientationType? : OrientationType) : BasePanel {
            if (!ObjectValidator.IsEmptyOrNull($element) && !$element.IsPreventingScroll()) {
                if ($reflection.IsMemberOf($element, BasePanel) && $element.Scrollable() && ((
                    (ObjectValidator.IsEmptyOrNull($orientationType) || $orientationType === OrientationType.VERTICAL) &&
                    ObjectValidator.IsSet($element.verticalScrollBar) && $element.verticalScrollBar.Visible() &&
                    ElementManager.IsVisible($element.verticalScrollBar.Id())) ||
                    (ObjectValidator.IsEmptyOrNull($orientationType) || $orientationType === OrientationType.HORIZONTAL) &&
                    ObjectValidator.IsSet($element.horizontalScrollBar) && $element.horizontalScrollBar.Visible() &&
                    ElementManager.IsVisible($element.horizontalScrollBar.Id()))) {
                    return $element;
                } else {
                    return BasePanel.getFirstScrollable(<BasePanel>$element.Parent(), $manager, $reflection, $orientationType);
                }
            } else {
                return null;
            }
        }

        protected static onKeyEventHandler($eventArgs : KeyEventArgs, $manager : GuiObjectManager, $reflection? : Reflection) : void {
            const element : BasePanel = BasePanel.getFirstScrollable(<BasePanel>$manager.getHovered(), $manager, $reflection);
            if ($reflection.IsMemberOf(element, BasePanel) && element.Scrollable()) {
                const keyCode : number = $eventArgs.getKeyCode();
                if (keyCode === KeyMap.UP_ARROW ||
                    keyCode === KeyMap.DOWN_ARROW ||
                    keyCode === KeyMap.LEFT_ARROW ||
                    keyCode === KeyMap.RIGHT_ARROW ||
                    keyCode === KeyMap.SPACE ||
                    keyCode === KeyMap.END ||
                    keyCode === KeyMap.HOME) {
                    $eventArgs.PreventDefault();
                    $manager.setActive(element, true);
                    let position : number = -1;
                    const eventArgs : ScrollEventArgs = new ScrollEventArgs();
                    eventArgs.Owner(element);

                    if (keyCode === KeyMap.UP_ARROW ||
                        keyCode === KeyMap.DOWN_ARROW ||
                        keyCode === KeyMap.SPACE ||
                        keyCode === KeyMap.END ||
                        keyCode === KeyMap.HOME) {
                        if ((<BasePanel>element).verticalScrollBar.Visible()) {
                            position = BasePanel.scrollTop(element);
                            if (position >= 0) {
                                if (keyCode === KeyMap.DOWN_ARROW) {
                                    position += 10;
                                } else if (keyCode === KeyMap.UP_ARROW) {
                                    position -= 15;
                                } else if (keyCode === KeyMap.HOME) {
                                    position = 0;
                                } else if (keyCode === KeyMap.END) {
                                    position = 100;
                                } else {
                                    const content : HTMLElement = ElementManager.getElement(element.Id() + "_PanelContentEnvelop");
                                    if (ElementManager.Exists(content)) {
                                        position += Math.ceil(100 / content.clientHeight * content.scrollHeight);
                                    } else {
                                        position += 50;
                                    }
                                }
                                if (position < 0) {
                                    position = 0;
                                }
                                if (position > 100) {
                                    position = 100;
                                }

                                eventArgs.OrientationType(OrientationType.VERTICAL);
                                position = BasePanel.scrollTop(element, position);
                                eventArgs.Position(position);
                                EventsManager.getInstanceSingleton().FireEvent(element, EventType.ON_SCROLL, eventArgs);
                                EventsManager.getInstanceSingleton().FireEvent(BasePanel.ClassName(), EventType.ON_SCROLL, eventArgs);
                                EventsManager.getInstanceSingleton().FireEvent(element, EventType.ON_VERTICAL_SCROLL, eventArgs);
                                EventsManager.getInstanceSingleton()
                                    .FireEvent(BasePanel.ClassName(), EventType.ON_VERTICAL_SCROLL, eventArgs);
                            }
                        }
                    } else {
                        if (element.horizontalScrollBar.Visible()) {
                            position = BasePanel.scrollLeft(element);
                            if (position >= 0) {
                                if (keyCode === KeyMap.RIGHT_ARROW) {
                                    position += 10;
                                } else {
                                    position -= 15;
                                }
                                if (position < 0) {
                                    position = 0;
                                }
                                if (position > 100) {
                                    position = 100;
                                }

                                eventArgs.OrientationType(OrientationType.HORIZONTAL);
                                position = BasePanel.scrollLeft(element, position);
                                eventArgs.Position(position);
                                EventsManager.getInstanceSingleton()
                                    .FireEvent(element, EventType.ON_SCROLL, eventArgs);
                                EventsManager.getInstanceSingleton()
                                    .FireEvent(BasePanel.ClassName(), EventType.ON_SCROLL, eventArgs);
                                EventsManager.getInstanceSingleton()
                                    .FireEvent(element, EventType.ON_HORIZONTAL_SCROLL, eventArgs);
                                EventsManager.getInstanceSingleton()
                                    .FireEvent(BasePanel.ClassName(), EventType.ON_HORIZONTAL_SCROLL, eventArgs);
                            }
                        }
                    }
                }
            }
        }

        protected static resize($element : BasePanel, $width : number, $height : number) : void {
            if (Reflection.getInstance().IsMemberOf($element, BasePanel)) {
                const args : PanelResizeEventArgs = new PanelResizeEventArgs();
                args.Owner($element);
                args.Width($width);
                args.Height($height);
                args.Scrollable($element.Scrollable());

                EventsManager.getInstanceSingleton().FireEvent(BasePanel.ClassName(), EventType.BEFORE_RESIZE, args);
                EventsManager.getInstanceSingleton().FireEvent($element, EventType.BEFORE_RESIZE, args, false);
                BasePanel.scrollBarVisibilityHandler($element);
            }
        }

        protected static scrollToCurrentPosition($element : BasePanel) : void {
            let position : number = BasePanel.scrollTop($element);
            const eventArgs : ScrollEventArgs = new ScrollEventArgs();
            if (position >= 0) {
                eventArgs.Owner($element);
                eventArgs.OrientationType(OrientationType.VERTICAL);
                eventArgs.Position(position);
                EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_SCROLL, eventArgs, false);
                EventsManager.getInstanceSingleton().FireEvent(BasePanel.ClassName(), EventType.ON_SCROLL, eventArgs, false);
                EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_VERTICAL_SCROLL, eventArgs, false);
                EventsManager.getInstanceSingleton().FireEvent(BasePanel.ClassName(), EventType.ON_VERTICAL_SCROLL, eventArgs, false);
            }
            position = BasePanel.scrollLeft($element);
            if (position >= 0) {
                eventArgs.Owner($element);
                eventArgs.OrientationType(OrientationType.HORIZONTAL);
                eventArgs.Position(position);
                EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_SCROLL, eventArgs, false);
                EventsManager.getInstanceSingleton().FireEvent(BasePanel.ClassName(), EventType.ON_SCROLL, eventArgs, false);
                EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_HORIZONTAL_SCROLL, eventArgs, false);
                EventsManager.getInstanceSingleton().FireEvent(BasePanel.ClassName(), EventType.ON_HORIZONTAL_SCROLL, eventArgs, false);
            }
        }

        protected static scrollTop($element : BasePanel, $value? : number) : number {
            let value : number = -1;
            const content : HTMLElement = ElementManager.getElement($element.Id() + "_PanelContentEnvelop");
            if (ElementManager.Exists(content)) {
                if (ObjectValidator.IsSet($value)) {
                    value = Property.PositiveInteger(value, $value);
                    content.scrollTop = Math.ceil(
                        (content.scrollHeight - content.clientHeight) / 100 * value);
                } else {
                    value = Math.ceil(
                        100 / (content.scrollHeight - content.clientHeight) * content.scrollTop);
                    if (value > 100) {
                        value = 100;
                    } else if (value < 0 || !ObjectValidator.IsDigit(value)) {
                        value = 0;
                    }
                }
            }
            return value;
        }

        protected static scrollLeft($element : BasePanel, $value? : number) : number {
            let value : number = -1;
            const content : HTMLElement = ElementManager.getElement($element.Id() + "_PanelContentEnvelop");
            if (ElementManager.Exists(content)) {
                if (ObjectValidator.IsSet($value)) {
                    value = Property.PositiveInteger(value, $value);
                    content.scrollLeft = Math.ceil(
                        (content.scrollWidth - content.clientWidth) / 100 * value);
                } else {
                    value = Math.ceil(
                        100 / (content.scrollWidth - content.clientWidth) * content.scrollLeft);
                    if (value > 100) {
                        value = 100;
                    } else if (value < 0 || !ObjectValidator.IsDigit(value)) {
                        value = 0;
                    }
                }
            }
            return value;
        }

        protected static scrollBarVisibilityHandler($element : BasePanel) : void {
            let scrollBarWidth : number = 0;
            let scrollBarHeight : number = 0;
            let scrollBarWidthContentOffset : number = 0;
            let scrollBarHeightContentOffset : number = 0;
            let verticalBarChanged : boolean = false;
            let horizontalBarChanged : boolean = false;
            const elementSize : Size = new Size();

            elementSize.Width($element.Width() - ElementManager.getWidthOffset($element.Id()));
            elementSize.Height($element.Height() - ElementManager.getHeightOffset($element.Id()));
            ElementManager.setSize($element.Id(), elementSize.Width(), elementSize.Height());
            ElementManager.setSize($element.Id() + "_PanelContentEnvelop", elementSize.Width(), elementSize.Height());

            const args : PanelResizeEventArgs = new PanelResizeEventArgs();
            args.Owner($element);
            args.Width($element.Width());
            args.Height($element.Height());
            args.Scrollable($element.Scrollable());

            if ($element.Scrollable()) {
                BasePanel.responsiveHandler(args);
                const contentSize : Size = new Size($element.Id() + "_PanelContent", true);
                let verticalScrollBarId : string;
                let horizontalScrollBarId : string;
                if (ObjectValidator.IsSet($element.verticalScrollBar)) {
                    verticalScrollBarId = $element.verticalScrollBar.Id();
                }
                if (ObjectValidator.IsSet($element.horizontalScrollBar)) {
                    horizontalScrollBarId = $element.horizontalScrollBar.Id();
                }

                if (contentSize.Width() > elementSize.Width() || contentSize.Height() > elementSize.Height()) {
                    if (ObjectValidator.IsSet(verticalScrollBarId)) {
                        if (contentSize.Height() > elementSize.Height()) {
                            if (!ElementManager.IsVisible(verticalScrollBarId)) {
                                $element.verticalScrollBar.Visible(true);
                                ElementManager.setOpacity(verticalScrollBarId, 0);
                                EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                                    ElementManager.ChangeOpacity($element.verticalScrollBar, DirectionType.UP, 10);
                                }, false);
                                verticalBarChanged = true;
                            }
                            if (ElementManager.Exists(verticalScrollBarId)) {
                                scrollBarWidth = ElementManager.getElement(verticalScrollBarId).offsetWidth;
                                if (ElementManager.Exists(verticalScrollBarId + "_ContentOffset", true)) {
                                    scrollBarWidthContentOffset
                                        = ElementManager.getElement(verticalScrollBarId + "_ContentOffset").offsetWidth;
                                }
                            }
                        } else {
                            $element.verticalScrollBar.Visible(false);
                        }
                    }

                    if (ObjectValidator.IsSet(horizontalScrollBarId)) {
                        if (contentSize.Width() > elementSize.Width()) {
                            if (!ElementManager.IsVisible(horizontalScrollBarId)) {
                                $element.horizontalScrollBar.Visible(true);
                                ElementManager.setOpacity(horizontalScrollBarId, 0);
                                EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                                    ElementManager.ChangeOpacity($element.horizontalScrollBar, DirectionType.UP, 10);
                                }, false);
                                horizontalBarChanged = true;
                            }
                            if (ElementManager.Exists(horizontalScrollBarId)) {
                                scrollBarHeight = ElementManager.getElement(horizontalScrollBarId).offsetHeight;
                                if (ElementManager.Exists(horizontalScrollBarId + "_ContentOffset", true)) {
                                    scrollBarHeightContentOffset
                                        = ElementManager.getElement(horizontalScrollBarId + "_ContentOffset").offsetHeight;
                                }
                            }
                        } else {
                            $element.horizontalScrollBar.Visible(false);
                        }
                    }
                } else {
                    if (ElementManager.IsVisible(verticalScrollBarId)) {
                        ElementManager.Hide(verticalScrollBarId);
                        verticalBarChanged = true;
                    }
                    if (ElementManager.IsVisible(horizontalScrollBarId)) {
                        ElementManager.Hide(horizontalScrollBarId);
                        horizontalBarChanged = true;
                    }
                }

                BasePanel.onScrollBarVisibilityChange($element, verticalBarChanged, horizontalBarChanged);
                ElementManager.setSize($element.Id() + "_PanelContentEnvelop",
                    elementSize.Width() - scrollBarWidthContentOffset,
                    elementSize.Height() - scrollBarHeightContentOffset);
            } else {
                if (ObjectValidator.IsSet($element.verticalScrollBar)) {
                    $element.verticalScrollBar.Visible(false);
                }
                if (ObjectValidator.IsSet($element.horizontalScrollBar)) {
                    $element.horizontalScrollBar.Visible(false);
                }
            }

            args.AvailableWidth(elementSize.Width() - scrollBarWidthContentOffset);
            args.AvailableHeight(elementSize.Height() - scrollBarHeightContentOffset);
            args.ScrollBarWidth(elementSize.Width() - scrollBarWidth);
            args.ScrollBarHeight(elementSize.Height() - scrollBarHeight);
            args.ScrollBarChanged(verticalBarChanged || horizontalBarChanged);

            EventsManager.getInstanceSingleton().FireEvent(BasePanel.ClassName(), EventType.ON_RESIZE, args, false);
            EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_RESIZE, args, false);
            EventsManager.getInstanceSingleton().FireEvent(BasePanel.ClassName(), EventType.ON_RESIZE_COMPLETE, args);
            EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_RESIZE_COMPLETE, args);
            EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                BasePanel.scrollToCurrentPosition($element);
            });
        }

        private static onBodyScrollEventHandler($eventArgs : ScrollEventArgs, $manager? : GuiObjectManager,
                                                $reflection? : Reflection) : void {
            const element : BasePanel = BasePanel.getFirstScrollable(<BasePanel>$manager.getHovered(), $manager, $reflection,
                $eventArgs.OrientationType());
            if ($reflection.IsMemberOf(element, BasePanel) && element.Scrollable()) {
                if (($eventArgs.OrientationType() === OrientationType.VERTICAL &&
                    ObjectValidator.IsSet(element.verticalScrollBar)
                    && element.verticalScrollBar.Visible() && ElementManager.IsVisible(element.verticalScrollBar.Id())) ||
                    ($eventArgs.OrientationType() === OrientationType.HORIZONTAL &&
                        ObjectValidator.IsSet(element.horizontalScrollBar)
                        && element.horizontalScrollBar.Visible() && ElementManager.IsVisible(element.horizontalScrollBar.Id()))) {
                    $eventArgs.PreventDefault();
                    let position : number = -1;
                    if ($eventArgs.OrientationType() === OrientationType.VERTICAL) {
                        position = BasePanel.scrollTop(element);
                    } else if ($eventArgs.OrientationType() === OrientationType.HORIZONTAL) {
                        position = BasePanel.scrollLeft(element);
                    }
                    if (position >= 0) {
                        if ($eventArgs.DirectionType() === DirectionType.DOWN) {
                            position += $eventArgs.Position();
                        } else {
                            position -= $eventArgs.Position() * 3;
                        }
                        if (position < 0) {
                            position = 0;
                        }
                        if (position > 100) {
                            position = 100;
                        }

                        let eventType : string;
                        const eventArgs : ScrollEventArgs = new ScrollEventArgs();
                        eventArgs.Owner(element);
                        eventArgs.OrientationType($eventArgs.OrientationType());
                        if ($eventArgs.OrientationType() === OrientationType.VERTICAL) {
                            element.getEvents().FireAsynchronousMethod(() : void => {
                                position = BasePanel.scrollTop(element, position);
                            }, false);
                            eventType = EventType.ON_VERTICAL_SCROLL;
                        } else if ($eventArgs.OrientationType() === OrientationType.HORIZONTAL) {
                            element.getEvents().FireAsynchronousMethod(() : void => {
                                position = BasePanel.scrollLeft(element, position);
                            }, false);
                            eventType = EventType.ON_HORIZONTAL_SCROLL;
                        }
                        eventArgs.Position(position);
                        EventsManager.getInstanceSingleton().FireEvent(element, EventType.ON_SCROLL, eventArgs);
                        EventsManager.getInstanceSingleton().FireEvent(BasePanel.ClassName(), EventType.ON_SCROLL, eventArgs);
                        EventsManager.getInstanceSingleton().FireEvent(element, eventType, eventArgs);
                        EventsManager.getInstanceSingleton().FireEvent(BasePanel.ClassName(), eventType, eventArgs);
                    }
                }
            }
        }

        private static onScrollBarVisibilityChange($panel : BasePanel, $verticalBarChanged : boolean,
                                                   $horizontalBarChanged : boolean) {
            if ($verticalBarChanged || $horizontalBarChanged) {
                const element : IGuiElement = $panel.getInnerHtmlMap();
                if (ObjectValidator.IsSet(element)) {
                    const isRow : boolean = element.getGuiTypeTag() === GeneralCssNames.ROW && element.Implements(IResponsiveElement);
                    const isColumn : boolean = element.getGuiTypeTag() === GeneralCssNames.COLUMN && element.Implements(IResponsiveElement);

                    if (isRow || isColumn) {
                        let panelContentWidth : number;
                        let panelContentHeight : number;

                        if (ObjectValidator.IsEmptyOrNull(element.getWidth())) {
                            panelContentWidth = $panel.Width();
                        } else {
                            panelContentWidth = element.getWidth();
                        }

                        if (ObjectValidator.IsEmptyOrNull(element.getHeight())) {
                            panelContentHeight = $panel.Height();
                        } else {
                            panelContentHeight = element.getHeight();
                        }

                        let verticalScrollbarOffset : number;
                        let horizontalScrollbarOffset : number;
                        if ($panel.verticalScrollBar.IsLoaded() && $panel.verticalScrollBar.Visible() && $verticalBarChanged) {
                            verticalScrollbarOffset
                                = ElementManager.getElement($panel.verticalScrollBar.Id() + "_ContentOffset").offsetWidth;
                            ElementManager.setCssProperty(element.getId(), "width",
                                panelContentWidth - verticalScrollbarOffset + "px");
                        }
                        if ($panel.horizontalScrollBar.IsLoaded() && $panel.horizontalScrollBar.Visible() && $horizontalBarChanged) {
                            horizontalScrollbarOffset
                                = ElementManager.getElement($panel.horizontalScrollBar.Id() + "_ContentOffset").offsetHeight;
                            ElementManager.setCssProperty(element.getId(), "height",
                                panelContentHeight - horizontalScrollbarOffset + "px");
                        }
                    }
                }
            }
        }

        private static onCommonsShow($eventArgs : EventArgs, $manager? : GuiObjectManager,
                                     $reflection? : Reflection) : void {
            const commons : IGuiCommons = $eventArgs.Owner();
            const basePanel : BasePanel = <BasePanel>commons.Parent();
            if (!ObjectValidator.IsEmptyOrNull(basePanel) && basePanel.IsLoaded() && $reflection.Implements(basePanel, IBasePanel)) {
                const childElement : IGuiElement = basePanel.getInnerHtmlMap();
                if (childElement.Implements(IGuiElement) && childElement.Implements(IResponsiveElement)) {
                    if (BasePanel.isVisibilityToggleHandled(commons)) {
                        BasePanel.resize(basePanel, basePanel.Width(), basePanel.Height());
                    }
                }
            }
        }

        private static onCommonsHide($eventArgs : EventArgs, $manager? : GuiObjectManager,
                                     $reflection? : Reflection) : void {
            const commons : IGuiCommons = $eventArgs.Owner();
            const basePanel : BasePanel = <BasePanel>commons.Parent();
            if (!ObjectValidator.IsEmptyOrNull(basePanel) && basePanel.IsLoaded() && $reflection.Implements(basePanel, IBasePanel)) {
                const childElement : IGuiElement = basePanel.getInnerHtmlMap();
                if (childElement.Implements(IGuiElement) && childElement.Implements(IResponsiveElement)) {
                    if (BasePanel.isVisibilityToggleHandled(commons)) {
                        BasePanel.resize(basePanel, basePanel.Width(), basePanel.Height());
                    }
                }
            }
        }

        private static isVisibilityToggleHandled($target : IGuiCommons) : boolean {
            let wrapper : any = $target.getWrappingElement();
            while (!ObjectValidator.IsEmptyOrNull(wrapper)) {
                if (wrapper.Implements(IGuiElement) && wrapper.Implements(IResponsiveElement)) {
                    if (!ObjectValidator.IsEmptyOrNull(wrapper.getVisibilityStrategy())) {
                        return wrapper.getVisibilityStrategy() !== VisibilityStrategy.NONE;
                    }
                } else {
                    return false;
                }
                wrapper = wrapper.getWrappingElement();
            }
            return false;
        }

        private static responsiveHandler($eventArgs : EventArgs) : void {
            const basePanel : BasePanel = <BasePanel>$eventArgs.Owner();
            const element : IGuiElement = basePanel.getInnerHtmlMap();

            if (element.getGuiTypeTag() === GeneralCssNames.COLUMN ||
                element.getGuiTypeTag() === GeneralCssNames.ROW) {
                const envelopSize : Size = new Size(basePanel.Id() + "_PanelContentEnvelop");
                ElementManager.setCssProperty(element.getId(), "width", envelopSize.Width() + "px");
                ElementManager.setCssProperty(element.getId(), "height", envelopSize.Height() + "px");
                const baseObject = <IBaseObject>element;
                const responsiveElement : IResponsiveElement = <IResponsiveElement>baseObject;
                BasePanel.handleContainerVisibility(element, responsiveElement.getVisibilityStrategy());
                BasePanel.recomputeNestedGuiElementSizes(element, envelopSize, responsiveElement.getWidthOfColumn(),
                    responsiveElement.getHeightOfRow());
                BasePanel.alignNestedChildElements(element, responsiveElement.getAlignment());
                BasePanel.fitToParentNestedChildElements(element, responsiveElement.getFitToParent());
            }
        }

        private static handleContainerVisibility($element : IGuiElement,
                                                 $baseVisibilityStrategy : VisibilityStrategy) : IContainerVisibilityInfo {
            const visibility : IContainerVisibilityInfo = {commonsNum: 0, visibleCommons: 0};
            let index : number = 0;
            const childElements : IArrayList<IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer>
                = $element.getGuiChildElements();
            const size : number = childElements.Length();
            if (size > 0) {
                for (index; index < size; index++) {
                    const childElement : IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer = childElements.getItem(index);
                    if (childElement.Implements(IGuiElement) && childElement.Implements(IResponsiveElement)) {
                        const guiElement : IGuiElement = <IGuiElement>childElement;
                        if (guiElement.getGuiTypeTag() === GeneralCssNames.COLUMN ||
                            guiElement.getGuiTypeTag() === GeneralCssNames.ROW) {
                            const baseStrategy : VisibilityStrategy = VisibilityStrategy
                                .PriorityValue($baseVisibilityStrategy, (<IResponsiveElement>childElement).getVisibilityStrategy());
                            const childVisibility : IContainerVisibilityInfo
                                = BasePanel.handleContainerVisibility(guiElement, baseStrategy);
                            visibility.commonsNum += childVisibility.commonsNum;
                            visibility.visibleCommons += childVisibility.visibleCommons;
                        }
                    } else if (childElement.Implements(IGuiCommons)) {
                        const baseStrategy : VisibilityStrategy = VisibilityStrategy.PriorityValue($baseVisibilityStrategy);
                        const strategy : VisibilityStrategy = VisibilityStrategy.Normalize(baseStrategy);
                        const guiCommons : IGuiCommons = <IGuiCommons>childElement;
                        const columnId : string = guiCommons.Id() + "_" + GeneralCssNames.COLUMN;
                        visibility.commonsNum += 1;
                        if (guiCommons.Visible()) {
                            visibility.visibleCommons += 1;
                            ElementManager.setCssProperty(columnId, "display", "");
                        } else {
                            if (strategy === VisibilityStrategy.COLLAPSE_EMPTY) {
                                ElementManager.setCssProperty(columnId, "display", "none");
                            } else {
                                ElementManager.setCssProperty(columnId, "display", "");
                            }
                        }
                    }
                }
            }
            const strategy : VisibilityStrategy = VisibilityStrategy.Normalize($baseVisibilityStrategy);
            const isContainerVisible : boolean = $element.getVisible() !== false
                && (visibility.commonsNum === 0 || visibility.visibleCommons !== 0);
            if (strategy === VisibilityStrategy.COLLAPSE_EMPTY && !isContainerVisible) {
                ElementManager.setCssProperty($element.getId(), "display", "none");
            } else {
                ElementManager.setCssProperty($element.getId(), "display", "");
            }
            return visibility;
        }

        private static recomputeNestedGuiElementSizes($element : IGuiElement, $containerSize : Size, $widthOfColumn : PropagableNumber,
                                                      $heightOfRow : PropagableNumber) : void {
            const childElements : IArrayList<IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer> = $element.getGuiChildElements();
            const size : number = childElements.Length();
            const sizeInfo : IContainerSizeInfo = BasePanel.getContainerSizeInfo($element, $containerSize, $widthOfColumn, $heightOfRow);

            BasePanel.handleInnerPaddings($element, sizeInfo, $containerSize);

            let index : number = 0;
            for (index; index < size; index++) {
                const childElement : IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer = childElements.getItem(index);
                if (childElement.Implements(IGuiElement) && childElement.Implements(IResponsiveElement)) {
                    const element : IGuiElement = <IGuiElement>childElement;
                    if (ElementManager.getCssValue(element.getId(), "display") !== "none") {
                        const priorityWidth : PropagableNumber =
                            PropagableNumber.PriorityValue($widthOfColumn, (<IResponsiveElement>childElement).getWidthOfColumn());
                        const priorityHeight : PropagableNumber =
                            PropagableNumber.PriorityValue($heightOfRow, (<IResponsiveElement>childElement).getHeightOfRow());
                        const isWidthSet : boolean = !ObjectValidator.IsEmptyOrNull(priorityWidth) &&
                            priorityWidth.Normalize($containerSize.Width(), DisplayUnitType.PX) > 0;
                        const isHeightSet : boolean = !ObjectValidator.IsEmptyOrNull(priorityHeight) &&
                            priorityHeight.Normalize($containerSize.Height(), DisplayUnitType.PX) > 0;

                        const containerSize : Size = new Size();
                        if (element.getGuiTypeTag() === GeneralCssNames.COLUMN) {
                            containerSize.Height($containerSize.Height());
                            if (isWidthSet) {
                                containerSize.Width(priorityWidth.Normalize($containerSize.Width(), DisplayUnitType.PX));
                            } else if (!sizeInfo.elementsWithoutWidth.IsEmpty()) {
                                containerSize.Width(sizeInfo.remainingWidth > 0 ?
                                    Math.round(sizeInfo.remainingWidth / sizeInfo.elementsWithoutWidth.Length()) : 0);
                            }
                            ElementManager.setCssProperty(element.getId(), "width", containerSize.Width() + "px");
                            BasePanel.recomputeNestedGuiElementSizes(element, containerSize, priorityWidth, priorityHeight);
                        } else if (element.getGuiTypeTag() === GeneralCssNames.ROW) {
                            containerSize.Width($containerSize.Width());
                            if (isHeightSet) {
                                containerSize.Height(priorityHeight.Normalize($containerSize.Height(), DisplayUnitType.PX));
                            } else if (!sizeInfo.elementsWithoutHeight.IsEmpty()) {
                                containerSize.Height(sizeInfo.remainingHeight > 0 ?
                                    Math.round(sizeInfo.remainingHeight / sizeInfo.elementsWithoutHeight.Length()) : 0);
                            }
                            ElementManager.setCssProperty(element.getId(), "height", containerSize.Height() + "px");
                            BasePanel.recomputeNestedGuiElementSizes(element, containerSize, priorityWidth, priorityHeight);
                        }
                    }
                } else if (childElement.Implements(IGuiCommons)) {
                    const elementId : string = (<IGuiCommons>childElement).Id() + "_" + GeneralCssNames.COLUMN;
                    if (ElementManager.getCssValue(elementId, "display") !== "none") {
                        let width : number = null;
                        if (!ObjectValidator.IsEmptyOrNull($widthOfColumn)
                            && $widthOfColumn.Normalize($containerSize.Width(), DisplayUnitType.PX) > 0) {
                            width = $widthOfColumn.Normalize($containerSize.Width(), DisplayUnitType.PX);
                        } else if (!sizeInfo.elementsWithoutWidth.IsEmpty()) {
                            width = Math.round(sizeInfo.remainingWidth / sizeInfo.elementsWithoutWidth.Length());
                        }
                        ElementManager.setCssProperty(elementId, "height", $containerSize.Height() + "px");
                        if (width !== null) {
                            ElementManager.setCssProperty(elementId, "width", width + "px");
                        }
                    }
                }
            }

        }

        private static getContainerSizeInfo($element : IGuiElement, $containerSize : Size, $widthOfColumn : PropagableNumber,
                                            $heightOfRow : PropagableNumber) : IContainerSizeInfo {
            let index : number = 0;
            const childElements : IArrayList<IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer> = $element.getGuiChildElements();
            const size : number = childElements.Length();
            let remainingWidth : number = $containerSize.Width();
            let remainingHeight : number = $containerSize.Height();
            const elementsWithoutWidth : IArrayList<IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer> =
                new ArrayList<IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer>();
            const elementsWithoutHeight : IArrayList<IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer> =
                new ArrayList<IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer>();

            for (index; index < size; index++) {
                const childElement : IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer = childElements.getItem(index);
                let priorityWidth : PropagableNumber;
                let priorityHeight : PropagableNumber;
                let isVisible : boolean = false;
                if (childElement.Implements(IGuiElement) && childElement.Implements(IResponsiveElement)) {
                    const wrapper : any = childElement;
                    if (wrapper.getGuiTypeTag() === GeneralCssNames.COLUMN ||
                        wrapper.getGuiTypeTag() === GeneralCssNames.ROW) {
                        if (ElementManager.getCssValue(wrapper.getId(), "display") !== "none") {
                            priorityHeight = PropagableNumber.PriorityValue($heightOfRow, wrapper.getHeightOfRow());
                            priorityWidth = PropagableNumber.PriorityValue($widthOfColumn, wrapper.getWidthOfColumn());
                            isVisible = true;
                        }
                    }
                } else if (childElement.Implements(IGuiCommons)) {
                    const wrapper : IGuiCommons = <IGuiCommons>childElement;
                    if (ElementManager.getCssValue(wrapper.Id() + "_" + GeneralCssNames.COLUMN, "display") !== "none") {
                        priorityHeight = $heightOfRow;
                        priorityWidth = $widthOfColumn;
                        isVisible = true;
                    }
                }

                if (isVisible) {
                    if (!ObjectValidator.IsEmptyOrNull(priorityWidth) &&
                        priorityWidth.Normalize($containerSize.Width(), DisplayUnitType.PX) > 0) {
                        remainingWidth = remainingWidth - priorityWidth.Normalize($containerSize.Width(), DisplayUnitType.PX);
                    } else {
                        elementsWithoutWidth.Add(childElement);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(priorityHeight) &&
                        priorityHeight.Normalize($containerSize.Height(), DisplayUnitType.PX) > 0) {
                        remainingHeight = remainingHeight - priorityHeight.Normalize($containerSize.Height(), DisplayUnitType.PX);
                    } else {
                        elementsWithoutHeight.Add(childElement);
                    }
                }
            }
            return {elementsWithoutHeight, elementsWithoutWidth, remainingHeight, remainingWidth};
        }

        private static alignNestedChildElements($element : IGuiElement, $baseAlignment : Alignment) : void {
            let index : number = 0;
            const childElements : IArrayList<IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer> =
                $element.getGuiChildElements();
            const size : number = childElements.Length();

            BasePanel.recomputeInnerPaddings($element, Alignment.Normalize($baseAlignment));

            for (index; index < size; index++) {
                const childElement : IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer = childElements.getItem(index);
                if (childElement.Implements(IGuiElement) && childElement.Implements(IResponsiveElement)) {
                    const element : IGuiElement = <IGuiElement>childElement;
                    const responsiveElement : IResponsiveElement = <IResponsiveElement>childElement;
                    if (element.getGuiTypeTag() === "GuiWrapper") {
                        if (ObjectValidator.IsSet($baseAlignment)) {
                            ElementManager.setClassName($element.getId(),
                                Alignment.StyleClassName($baseAlignment, ElementManager.getClassName($element.getId())));
                        }
                    } else if (element.getGuiTypeTag() === GeneralCssNames.COLUMN ||
                        element.getGuiTypeTag() === GeneralCssNames.ROW) {
                        BasePanel.alignNestedChildElements(element,
                            Alignment.PriorityValue($baseAlignment, responsiveElement.getAlignment()));
                    }
                } else if (childElement.Implements(IGuiCommons)) {
                    const element : IGuiCommons = <IGuiCommons>childElement;
                    if (ObjectValidator.IsSet($baseAlignment)) {
                        const wrapperId : string = element.Id() + "_" + GeneralCssNames.COLUMN;
                        ElementManager.setClassName(wrapperId,
                            Alignment.StyleClassName($baseAlignment, ElementManager.getClassName(wrapperId)));
                    }
                }
            }
        }

        private static fitToParentNestedChildElements($element : IGuiElement, $baseFitToParent : FitToParent) : void {
            if (!ObjectValidator.IsEmptyOrNull($element.getId())) {
                let index : number = 0;
                const childElements : IArrayList<IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer>
                    = $element.getGuiChildElements();
                const size : number = childElements.Length();
                for (index; index < size; index++) {
                    const childElement : IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer = childElements.getItem(index);
                    let fitToParent : FitToParent;
                    if ((childElement.Implements(IGuiElement) && childElement.Implements(IResponsiveElement) ||
                        childElement.Implements(IGuiCommons))
                        && ObjectValidator.IsSet((<IGuiCommons | IResponsiveElement>childElement).getFitToParent())) {
                        fitToParent = (<IGuiCommons | IResponsiveElement>childElement).getFitToParent();
                    } else {
                        fitToParent = $baseFitToParent;
                    }
                    if (childElement.Implements(IGuiElement) && childElement.Implements(IResponsiveElement)) {
                        const element : IGuiElement = <IGuiElement>childElement;
                        if (element.getGuiTypeTag() === GeneralCssNames.COLUMN ||
                            element.getGuiTypeTag() === GeneralCssNames.ROW) {
                            BasePanel.fitToParentNestedChildElements(element, fitToParent);
                        }
                    } else if (childElement.Implements(IGuiCommons)) {
                        const element : IGuiCommons = <IGuiCommons>childElement;
                        if (element.Visible()) {
                            const columnId : string = element.Id() + "_" + GeneralCssNames.COLUMN;
                            if (ElementManager.Exists(columnId)) {
                                if (ObjectValidator.IsSet(fitToParent) && fitToParent !== FitToParent.NONE) {
                                    const width : number = ElementManager.getCssIntegerValue(columnId, "width");
                                    const height : number = ElementManager.getCssIntegerValue(columnId, "height");
                                    const isWidthSet : boolean = ObjectValidator.IsSet(width) && width > 0;
                                    const isHeightSet : boolean = ObjectValidator.IsSet(height) && height > 0;

                                    const wrapperId : string = element.Id() + "_GuiWrapper";
                                    ElementManager.setClassName(wrapperId,
                                        FitToParent.StyleClassName(fitToParent, ElementManager.getClassName(wrapperId)));

                                    switch (fitToParent) {
                                    case FitToParent.FULL:
                                        if (isHeightSet && element.Height() !== height) {
                                            element.Height(height);
                                        }
                                        if (isWidthSet && element.Width() !== width) {
                                            element.Width(width);
                                        }
                                        break;
                                    case FitToParent.HORIZONTAL:
                                        if (isWidthSet && element.Width() !== width) {
                                            element.Width(width);
                                        }
                                        break;
                                    case FitToParent.VERTICAL:
                                        if (isHeightSet && element.Height() !== height) {
                                            element.Height(height);
                                        }
                                        break;
                                    default:
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private static handleInnerPaddings($element : IGuiElement, $sizeInfo : IContainerSizeInfo, $containerSize : Size) : void {
            const isRow : boolean = $element.getGuiTypeTag() === GeneralCssNames.ROW;
            const isColumn : boolean = $element.getGuiTypeTag() === GeneralCssNames.COLUMN;
            const isCreate : boolean =
                isColumn && $sizeInfo.elementsWithoutHeight.IsEmpty() && $sizeInfo.remainingHeight !== $containerSize.Height()
                || isRow && $sizeInfo.elementsWithoutWidth.IsEmpty() && $sizeInfo.remainingWidth !== $containerSize.Width();

            let divBefore : HTMLElement;
            let divAfter : HTMLElement;
            if (ElementManager.Exists($element.getId() + "_" + GeneralCssNames.PADDING_BEFORE)
                && ElementManager.Exists($element.getId() + "_" + GeneralCssNames.PADDING_AFTER)) {
                divBefore = ElementManager.getElement($element.getId() + "_" + GeneralCssNames.PADDING_BEFORE);
                divAfter = ElementManager.getElement($element.getId() + "_" + GeneralCssNames.PADDING_AFTER);
            } else if (isCreate) {
                divBefore = document.createElement("div");
                divAfter = document.createElement("div");
                divBefore.id = $element.getId() + "_" + GeneralCssNames.PADDING_BEFORE;
                divAfter.id = $element.getId() + "_" + GeneralCssNames.PADDING_AFTER;

                let innerGuiType : string;
                if (isRow) {
                    innerGuiType = GeneralCssNames.COLUMN;
                } else {
                    innerGuiType = GeneralCssNames.ROW;
                }

                divBefore.setAttribute("guiType", innerGuiType);
                divAfter.setAttribute("guiType", innerGuiType);

                const parentHtmlElement : HTMLElement = ElementManager.getElement($element.getId());
                parentHtmlElement.insertBefore(divBefore, parentHtmlElement.firstChild);
                parentHtmlElement.appendChild(divAfter);
            }

            if (!ObjectValidator.IsEmptyOrNull(divBefore)) {
                if (isColumn) {
                    divBefore.style.height = "0";
                    divAfter.style.height = Math.max(0, isCreate ? $sizeInfo.remainingHeight : 0) + "px";
                } else if (isRow) {
                    divBefore.style.width = "0";
                    divAfter.style.width = Math.max(0, isCreate ? $sizeInfo.remainingWidth : 0) + "px";
                }
            }
        }

        private static recomputeInnerPaddings($element : IGuiElement, $alignment : Alignment) : void {
            if (ObjectValidator.IsSet($element.getId())
                && ElementManager.Exists($element.getId() + "_" + GeneralCssNames.PADDING_BEFORE)
                && ElementManager.Exists($element.getId() + "_" + GeneralCssNames.PADDING_AFTER)) {

                const beforeElement : HTMLElement = ElementManager.getElement($element.getId() + "_" + GeneralCssNames.PADDING_BEFORE);
                const afterElement : HTMLElement = ElementManager.getElement($element.getId() + "_" + GeneralCssNames.PADDING_AFTER);

                if ($element.getGuiTypeTag() === GeneralCssNames.COLUMN && ObjectValidator.IsSet(afterElement.style.height)) {
                    switch ($alignment) {
                    case Alignment.TOP:
                    case Alignment.TOP_LEFT:
                    case Alignment.TOP_RIGHT:
                        beforeElement.style.height = "0";
                        afterElement.style.height = "0";
                        break;
                    case Alignment.LEFT:
                    case Alignment.CENTER:
                    case Alignment.RIGHT:
                        const height : number = parseFloat(afterElement.style.height);
                        const beforeHeight : number = Math.round(height / 2);
                        const afterHeight : number = height - beforeHeight;
                        beforeElement.style.height = beforeHeight + "px";
                        afterElement.style.height = afterHeight + "px";
                        break;
                    case Alignment.BOTTOM_LEFT:
                    case Alignment.BOTTOM:
                    case Alignment.BOTTOM_RIGHT:
                        beforeElement.style.height = afterElement.style.height;
                        afterElement.style.height = "0";
                        break;
                    }
                } else if ($element.getGuiTypeTag() === GeneralCssNames.ROW && ObjectValidator.IsSet(afterElement.style.width)) {
                    switch ($alignment) {
                    case Alignment.TOP:
                    case Alignment.CENTER:
                    case Alignment.BOTTOM:
                        const width : number = parseFloat(afterElement.style.width);
                        const beforeWidth : number = Math.round(width / 2);
                        const afterWidth : number = width - beforeWidth;
                        beforeElement.style.width = beforeWidth + "px";
                        afterElement.style.width = afterWidth + "px";
                        break;
                    case Alignment.TOP_RIGHT:
                    case Alignment.RIGHT:
                    case Alignment.BOTTOM_RIGHT:
                        beforeElement.style.width = afterElement.style.width;
                        afterElement.style.width = "0";
                        break;
                    }
                }
            }
        }

        constructor($id? : string) {
            super($id);
            const loaderIconClass : any = this.getLoaderIconClass();
            if (!ObjectValidator.IsEmptyOrNull(loaderIconClass)) {
                this.loaderIcon = new loaderIconClass(null);
            }
            const loaderTextClass : any = this.getLoaderTextClass();
            if (!ObjectValidator.IsEmptyOrNull(loaderTextClass)) {
                this.loaderText = new loaderTextClass("Loading, please wait ...");
            }

            const scrollBarClass : any = this.getScrollBarClass();
            if (!ObjectValidator.IsEmptyOrNull(scrollBarClass)) {
                this.verticalScrollBar = new scrollBarClass(OrientationType.VERTICAL, this.Id() + "_Vertical_ScrollBar");
                this.verticalScrollBar.StyleClassName("PanelScrollBar");
                this.verticalScrollBar.Visible(false);
                this.verticalScrollBar.Size(50);

                this.horizontalScrollBar = new scrollBarClass(OrientationType.HORIZONTAL, this.Id() + "_Horizontal_ScrollBar");
                this.horizontalScrollBar.StyleClassName("PanelScrollBar");
                this.horizontalScrollBar.Visible(false);
                this.horizontalScrollBar.Size(50);
            }
        }

        /**
         * @return {IBasePanelEvents} Returns events connected with the element.
         */
        public getEvents() : IBasePanelEvents {
            return <IBasePanelEvents>super.getEvents();
        }

        /**
         * @param {PanelContentType} [$type] Set type of output content, which should be rendered.
         * @return {PanelContentType} Returns type of output content, which will be rendered.
         */
        public ContentType($type? : PanelContentType) : PanelContentType {
            if (ObjectValidator.IsSet($type)) {
                this.contentType = $type;
            } else if (!ObjectValidator.IsSet(this.contentType)) {
                this.contentType = PanelContentType.WITH_ELEMENT_WRAPPER;
            }
            return this.contentType;
        }

        /**
         * @param {IGuiCommons} $element Specify GUI element, which should be added to the panel's content.
         * @param {string} [$variableName] Specify variable name, which should be used as parent's public field.
         * @return {void}
         */
        public AddChild($element : IGuiCommons, $variableName? : string) : void {
            if (!ObjectValidator.IsEmptyOrNull($element)) {
                const loader : ($element : IGuiCommons) => void = ($element : IGuiCommons) : void => {
                    $element.Parent(this);
                    $element.InstanceOwner(this.InstanceOwner());
                    if (!ObjectValidator.IsEmptyOrNull($variableName)) {
                        this[$variableName] = $element;
                        $element.InstancePath(this.InstancePath() + "." + $variableName);
                    }
                    this.getChildElements().Add($element, $element.Id());

                    if (this.Scrollable()) {
                        $element.getEvents().setOnComplete(
                            ($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                                const classInstance : any = $reflection.getClass($eventArgs.Owner().Parent().getClassName());
                                classInstance.scrollBarVisibilityHandler($eventArgs.Owner().Parent());
                            });
                    }
                    if (this.IsCompleted()) {
                        const visible : boolean = $element.Visible();
                        if (visible) {
                            $element.Visible(false);
                        }
                        ElementManager.getElement(this.Id() + "_PanelContent")
                            .appendChild(new GuiElement().Add($element).ToDOMElement(this.outputEndOfLine));
                        if (visible) {
                            $element.Visible(true);
                        }
                    } else {
                        $element.Visible(true);
                    }
                };

                if (this.IsCompleted()) {
                    loader($element);
                } else {
                    this.getInnerHtmlMap().Add($element);
                    if (ObjectValidator.IsEmptyOrNull(this.asyncChildrenLoader)) {
                        this.asyncChildrenLoader = new TimeoutManager();
                        this.getEvents().setOnComplete(() : void => {
                            this.asyncChildrenLoader.Execute();
                        });
                    }
                    this.asyncChildrenLoader.Add(() : void => {
                        loader($element);
                    });
                }
            }
        }

        /**
         * @return {ArrayList<IBasePanelViewer>} Returns list of child panels, which has been subscribed to this panel.
         */
        public getChildPanelList() : ArrayList<IBasePanelViewer> {
            return this.childPanelList;
        }

        /**
         * @param {string|BasePanel} $instanceOrId Child panel instance or id.
         * @param {BasePanelViewerArgs} $args Arguments object, which should be set to the desired child panel.
         * @return {void}
         */
        public setChildPanelArgs($instanceOrId : string | BasePanel, $args : BasePanelViewerArgs) : void {
            if (!ObjectValidator.IsString(<BasePanel>$instanceOrId) && ObjectValidator.IsSet((<BasePanel>$instanceOrId).Id)) {
                $instanceOrId = (<BasePanel>$instanceOrId).Id();
            }
            if (!ObjectValidator.IsEmptyOrNull($args) && this.childPanelList.KeyExists(<string>$instanceOrId)) {
                this.childPanelList.getItem(<string>$instanceOrId).ViewerArgs($args);
            }
        }

        /**
         * @param {string|BasePanelViewerArgs} [$value] Panel value or arguments object, which should be set to the panel.
         * @return {string|BasePanelViewerArgs} Returns panel's value or arguments object, if value has been specified, otherwise null.
         */
        public Value($value? : string | BasePanelViewerArgs) : string | BasePanelViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull(this.Parent()) && this.Parent().IsMemberOf(BasePanel)) {
                if (ObjectValidator.IsSet($value)) {
                    (<BasePanel>this.Parent()).setChildPanelArgs(this.Id(), <BasePanelViewerArgs>$value);
                }
                if (this.childPanelList.KeyExists(<string>this.Id())) {
                    return <BasePanelViewerArgs>this.childPanelList.getItem(this.Id()).ViewerArgs();
                }
            }
            return super.Value($value);
        }

        /**
         * @param {boolean} [$value] Specify panel's scrolling ability.
         * @return {boolean} Returns true, if panel can be scrolled.
         */
        public Scrollable($value? : boolean) : boolean {
            if (!ObjectValidator.IsSet(this.scrollable)) {
                this.scrollable = false;
            }
            this.scrollable = Property.Boolean(this.scrollable, $value);
            if (this.scrollable) {
                this.getGuiOptions().Add(GuiOptionType.SCROLLABLE);
            }

            return this.scrollable;
        }

        /**
         * @param {number} [$value] Specify panel's width.
         * @return {number} Returns panel's width.
         */
        public Width($value? : number) : number {
            this.width = Property.PositiveInteger(this.width, $value);
            if (!ObjectValidator.IsEmptyOrNull($value) && this.IsLoaded() && this.Visible()) {
                Reflection.getInstance().getClass(this.getClassName()).resize(this, this.width, this.Height());
            }
            return this.width;
        }

        /**
         * @param {number} [$value] Specify panel's height.
         * @return {number} Returns panel's height.
         */
        public Height($value? : number) : number {
            this.height = Property.PositiveInteger(this.height, $value);
            if (!ObjectValidator.IsEmptyOrNull($value) && this.IsLoaded() && this.Visible()) {
                Reflection.getInstance().getClass(this.getClassName()).resize(this, this.Width(), this.height);
            }
            return this.height;
        }

        /**
         * @return {number} Returns current scroll top value in percentage.
         */
        public getScrollTop() : number {
            const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
            return thisClass.scrollTop(this);
        }

        /**
         * @return {number} Returns current scroll left value in percentage.
         */
        public getScrollLeft() : number {
            const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
            return thisClass.scrollLeft(this);
        }

        /**
         * @return {string} Returns panel's content suitable for renderings at the screen.
         */
        public Draw($EOL? : string) : string {
            if (ObjectValidator.IsEmptyOrNull($EOL)) {
                $EOL = StringUtils.NewLine(false);
            }

            if (this.ContentType() === PanelContentType.WITHOUT_ELEMENT_WRAPPER) {
                return this.guiContent().Draw($EOL + "            ") + $EOL + "        ";
            }

            return this.addElement(this.Id() + "_PanelEnvelop")
                .StyleClassName("Panel")
                .Visible(this.Visible())
                .Add(super.Draw($EOL + "    "))
                .Draw($EOL);
        }

        /**
         * @return {IGuiCommonsArg[]} Returns array of element's attributes.
         */
        public getArgs() : IGuiCommonsArg[] {
            const args : IGuiCommonsArg[] = super.getArgs();
            args.push({
                name : "Scrollable",
                type : GuiCommonsArgType.BOOLEAN,
                value: this.Scrollable()
            });
            return args;
        }

        /**
         * @param {IGuiCommonsArg} $value Specify argument value, which should be passed to element.
         * @param {boolean} [$force=false] Specify, if value should be set without fire of events connected with argument change.
         * @return {void}
         */
        public setArg($value : IGuiCommonsArg, $force : boolean = false) : void {
            switch ($value.name) {
            case "Width":
                this.Width(<number>$value.value);
                break;
            case "Height":
                this.Height(<number>$value.value);
                break;
            case "Scrollable":
                this.Scrollable(<boolean>$value.value);
                break;
            default:
                super.setArg($value, $force);
                break;
            }
        }

        protected asyncChildListLoad($handler : ($viewer : BasePanelViewer) => void,
                                     $asyncLoader : TimeoutManager) : void {
            if (!this.asyncChildLoaders.IsEmpty()) {
                const reflection : Reflection = Reflection.getInstance();
                this.asyncChildLoaders.foreach(($child : any, $key? : number) : void => {
                    $asyncLoader.Add(() : void => {
                        EventsManager.getInstanceSingleton().FireEvent(BaseViewer.ClassName(), EventType.ON_CHANGE, false);
                        const viewer : BasePanelViewer = new $child.viewer($child.args);
                        if (reflection.IsMemberOf(viewer, BasePanelViewer)) {
                            const instance : BasePanel = <BasePanel>viewer.getInstance();
                            if (!ObjectValidator.IsEmptyOrNull(instance)) {
                                instance.Parent(this);
                                this.childPanelList.Add(viewer, instance.Id());
                                instance.asyncChildListLoad(() : void => {
                                    $child.postprocessor(this, instance, $key);
                                    this.asyncChildLoaders.RemoveAt(this.asyncChildLoaders.getKeys().indexOf($key));
                                    if (this.asyncChildLoaders.IsEmpty()) {
                                        $handler(<BasePanelViewer>this.InstanceOwner());
                                    }
                                }, $asyncLoader);
                            } else {
                                ExceptionsManager.Throw(this.getClassName(),
                                    new IllegalArgumentException(
                                        "Instance of '" + viewer.getClassName() + "' can not be set as source " +
                                        "for child panel, because declared viewer class is missing instance of panel object. " +
                                        "Please, validate, if setInstance method has been used correctly."));
                            }
                        } else {
                            ExceptionsManager.Throw(this.getClassName(),
                                new IllegalArgumentException(
                                    "Instance of '" + viewer.getClassName() + "' can not be set as source " +
                                    "for child panel, because it is not member of '" +
                                    BasePanelViewer.ClassName() + "' class."));
                        }
                    });
                });
            } else {
                $handler(<BasePanelViewer>this.InstanceOwner());
            }
        }

        protected addChildPanel($panelViewerClass : any, $panelViewerArgs? : BasePanelViewerArgs,
                                $postprocessor? : ($parent : BasePanel, $child : BasePanel,
                                                   $childIndex? : number) => void) : BasePanel {
            const reflection : Reflection = Reflection.getInstance();
            if (ObjectValidator.IsString($panelViewerClass)) {
                $panelViewerClass = reflection.getClass($panelViewerClass);
            }
            if (ObjectValidator.IsFunction($postprocessor)) {
                this.asyncChildLoaders.Add({
                    args         : $panelViewerArgs,
                    postprocessor: $postprocessor,
                    viewer       : $panelViewerClass
                });
            } else {
                const viewer : BasePanelViewer = new $panelViewerClass($panelViewerArgs);
                if (reflection.IsMemberOf(viewer, BasePanelViewer)) {
                    const instance : BasePanel = <BasePanel>viewer.getInstance();
                    instance.Parent(this);
                    this.childPanelList.Add(viewer, instance.Id());
                    return instance;
                } else {
                    ExceptionsManager.Throw(this.getClassName(),
                        new IllegalArgumentException(
                            "Instance of '" + viewer.getClassName() + "' can not be set as source for child panel, " +
                            "because it is not member of '" + BasePanelViewer.ClassName() + "' class."));
                }
            }

            return null;
        }

        protected availableGuiOptions() : ArrayList<GuiOptionType> {
            const options : ArrayList<GuiOptionType> = super.availableGuiOptions();
            options.Add(GuiOptionType.SCROLLABLE);
            return options;
        }

        /**
         * @return {IIcon} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.IIcon
         */
        protected getLoaderIconClass() : any {
            return null;
        }

        /**
         * @return {ILabel} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.ILabel
         */
        protected getLoaderTextClass() : any {
            return null;
        }

        /**
         * @return {IScrollBar} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IScrollBar
         */
        protected getScrollBarClass() : any {
            return null;
        }

        protected innerCode() : IGuiElement {
            const events : IEventsManager = EventsManager.getInstanceSingleton();
            if (ObjectValidator.IsSet(this.verticalScrollBar)) {
                this.verticalScrollBar.getEvents().setOnStart(
                    ($args : EventArgs) : void => {
                        const element : IScrollBar = <IScrollBar>$args.Owner();
                        const parent : BasePanel = <BasePanel>element.Parent();
                        element.Size(parent.Height());
                    });

                this.verticalScrollBar.getEvents().setOnComplete(
                    ($args : EventArgs) : void => {
                        BasePanel.scrollBarVisibilityHandler($args.Owner().Parent());
                    });
            }

            if (ObjectValidator.IsSet(this.horizontalScrollBar)) {
                this.horizontalScrollBar.getEvents().setOnStart(
                    ($args : EventArgs) : void => {
                        const element : IScrollBar = <IScrollBar>$args.Owner();
                        const parent : BasePanel = <BasePanel>element.Parent();
                        element.Size(parent.Width());
                    });

                this.horizontalScrollBar.getEvents().setOnComplete(
                    ($args : EventArgs) : void => {
                        BasePanel.scrollBarVisibilityHandler($args.Owner().Parent());
                    });
            }

            const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
            WindowManager.getEvents().setOnScroll(BasePanel.onBodyScrollEventHandler);
            WindowManager.getEvents().setOnKeyDown(thisClass.onKeyEventHandler);

            EventsManager.getInstanceSingleton().setEvent(GuiCommons.ClassName(), EventType.ON_SHOW, BasePanel.onCommonsShow);
            EventsManager.getInstanceSingleton().setEvent(GuiCommons.ClassName(), EventType.ON_HIDE, BasePanel.onCommonsHide);

            this.getEvents().setOnMouseOver(
                ($args : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    $args.StopAllPropagation();
                    $reflection.getClass($args.Owner().getClassName()).onHoverEventHandler($args, $manager, $reflection);
                });
            this.getEvents().setOnClick(
                ($args : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    $reflection.getClass($args.Owner().getClassName()).onHoverEventHandler($args, $manager, $reflection);
                });

            this.getEvents().setOnStart(
                ($eventArgs : EventArgs) : void => {
                    const element : BasePanel = <BasePanel>$eventArgs.Owner();
                    if (ObjectValidator.IsSet(element.verticalScrollBar)) {
                        element.verticalScrollBar.Visible(false);
                    }
                    if (ObjectValidator.IsSet(element.horizontalScrollBar)) {
                        element.horizontalScrollBar.Visible(false);
                    }
                });

            this.getEvents().setOnResize(BasePanel.responsiveHandler);

            this.getEvents().setOnComplete(
                ($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                    const element : BasePanel = <BasePanel>$eventArgs.Owner();
                    if (!element.IsCached() || !element.IsCompleted()) {
                        thisClass.resize(element, element.Width(), element.Height());
                    } else {
                        BasePanel.scrollBarVisibilityHandler(element);
                    }
                    if (ObjectValidator.IsEmptyOrNull(element.Parent())) {
                        $manager.setHovered(element, true);
                    }
                });

            const owner : BasePanelViewer = <BasePanelViewer>this.InstanceOwner();
            if (!ObjectValidator.IsEmptyOrNull(owner)
                && !ObjectValidator.IsEmptyOrNull(owner.ViewerArgs()) && owner.ViewerArgs().AsyncEnabled()) {
                events.setEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_COMPLETE,
                    ($eventArgs : AsyncRequestEventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                        if ($eventArgs.POST().KeyExists(HttpRequestConstants.ELEMENT_INSTANCE)) {
                            const element : BasePanel = <BasePanel>$eventArgs.POST().getItem(HttpRequestConstants.ELEMENT_INSTANCE);
                            if (element.IsMemberOf(BasePanel) && element.Visible()) {
                                ElementManager.setInnerHtml(element.Id(), $eventArgs.Result());
                                events.FireEvent(element, EventType.ON_START, false);
                                const thisClass : any = $reflection.getClass(element.getClassName());
                                thisClass.Show(element);
                                events.FireEvent(element, EventType.BEFORE_LOAD);
                            }
                        }
                    });
            }
            return super.innerCode();
        }

        protected guiContent() : IGuiElement {
            if (this.ContentType() === PanelContentType.HIDDEN) {
                const hidden : IGuiElement = this.addElement().StyleClassName("Hidden").Add(this.Id());
                if (!ObjectValidator.IsEmptyOrNull(this.InstanceOwner())) {
                    const link : HTMLAnchorElement = document.createElement("a");
                    const manager : HttpManager = this.getHttpManager();
                    link.href = manager.CreateLink("/web/" + StringUtils.Replace(this.InstanceOwner().getClassName(), ".", "/"));
                    if (StringUtils.Contains(manager.CreateLink(""), "/" + HttpRequestConstants.TEST_MODE)) {
                        link.href = manager.CreateLink(link.href + "/" + HttpRequestConstants.TEST_MODE);
                    }
                    if (StringUtils.Contains(manager.CreateLink(""), "/" + HttpRequestConstants.HIDE_CHILDREN)) {
                        link.href = manager.CreateLink(link.href + "/" + HttpRequestConstants.HIDE_CHILDREN);
                    }
                    link.innerHTML = hidden.Draw(StringUtils.NewLine(false));
                    return this.addElement().Add(link);
                } else {
                    return hidden;
                }
            }

            if (this.ContentType() === PanelContentType.ASYNC_LOADER) {
                const loader : IGuiElement = this.addElement(this.Id() + "_AsyncProgress").StyleClassName("Async");
                if (ObjectValidator.IsSet(this.loaderIcon) || ObjectValidator.IsSet(this.loaderText)) {
                    if (ObjectValidator.IsSet(this.loaderIcon)) {
                        this.loaderIcon.DisableAsynchronousDraw();
                        loader.Add(this.loaderIcon);
                    }
                    if (ObjectValidator.IsSet(this.loaderText)) {
                        this.loaderText.DisableAsynchronousDraw();
                        loader.Add(this.loaderText);
                    }
                } else {
                    loader.Add(this.addElement().StyleClassName("Label").Add("Loading ..."));
                }
                if (this.width !== -1 || this.height !== -1) {
                    if (this.height !== -1) {
                        loader.setAttribute("top", Math.ceil(this.height / 2) + "px;");
                    }
                    if (this.width !== -1) {
                        loader.setAttribute("left", Math.ceil(this.width / 2) + "px;");
                    }
                }
                return loader;
            } else {
                if (ObjectValidator.IsSet(this.loaderIcon)) {
                    this.loaderIcon.Visible(false);
                }
                if (ObjectValidator.IsSet(this.loaderText)) {
                    this.loaderText.Visible(false);
                }
            }
            if (!ObjectValidator.IsSet(this.asyncInnerCode)) {
                this.asyncInnerCode = this.addElement();
            }

            return this.addElement()
                .GuiTypeTag("Panel")
                .Add(this.addElement(this.Id() + "_PanelContentEnvelop")
                    .StyleClassName("Envelop").Visible(false)
                    .Add(this.addElement(this.Id() + "_PanelContent").StyleClassName("Content")
                        .Add(this.asyncInnerCode)
                        .Add(super.guiContent())
                        .Add(this.addElement().setAttribute("clear", "both"))
                    )
                )
                .Add(this.getGuiOptions().Contains(GuiOptionType.SCROLLABLE) && ObjectValidator.IsSet(this.verticalScrollBar) ?
                    this.verticalScrollBar : "")
                .Add(this.getGuiOptions().Contains(GuiOptionType.SCROLLABLE) && ObjectValidator.IsSet(this.horizontalScrollBar) ?
                    this.horizontalScrollBar : "");
        }

        /**
         * Specify attributes of the instance after unserialization.
         */
        protected setInstanceAttributes() : void {
            super.setInstanceAttributes();
            if (ObjectValidator.IsEmptyOrNull(this.childPanelList)) {
                this.childPanelList = new ArrayList<IBasePanelViewer>();
            }
            this.asyncChildLoaders = new ArrayList<object>();
            this.asyncInnerCode = this.addElement();

            this.width = -1;
            this.height = -1;

            if (ObjectValidator.IsSet(this.verticalScrollBar)) {
                this.verticalScrollBar.StyleClassName("PanelScrollBar");
                this.verticalScrollBar.Visible(false);
                this.verticalScrollBar.Size(50);

            }
            if (ObjectValidator.IsSet(this.horizontalScrollBar)) {
                this.horizontalScrollBar.StyleClassName("PanelScrollBar");
                this.horizontalScrollBar.Visible(false);
                this.horizontalScrollBar.Size(50);
            }
        }

        protected excludeSerializationData() : string[] {
            const exclude : string[] = super.excludeSerializationData();
            exclude.push(
                "asyncChildLoaders",
                "asyncChildrenLoader",
                "contentType",
                "scrollable",
                "width", "height");
            return exclude;
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = super.excludeCacheData();
            exclude.push(
                "asyncChildLoaders", "asyncChildrenLoader", "contentType", "asyncInnerCodeOutput"
            );
            if (this.scrollable === false) {
                exclude.push("scrollable");
            }
            if (this.IsCompleted()) {
                if (ObjectValidator.IsSet(this.loaderIcon)) {
                    exclude.push("loaderIcon");
                }
                if (ObjectValidator.IsSet(this.loaderText)) {
                    exclude.push("loaderText");
                }
                let childPanelsLoaded : boolean = true;
                this.childPanelList.foreach(($child : IBasePanelViewer) : boolean => {
                    if (!ObjectValidator.IsEmptyOrNull($child.getInstance()) && !$child.getInstance().IsCompleted()) {
                        childPanelsLoaded = false;
                    }
                    return childPanelsLoaded;
                });
                if (childPanelsLoaded) {
                    exclude.push("outputEndOfLine");
                }
            }
            return exclude;
        }

        protected beforeCacheCreation($preparationResultHandler : ($id : string, $value : any) => void) : void {
            if (this.Scrollable()) {
                if (ObjectValidator.IsSet(this.verticalScrollBar) && this.verticalScrollBar.Visible()) {
                    this.verticalScrollBar.Visible(false);
                    $preparationResultHandler(this.verticalScrollBar.Id(), true);
                }
                if (ObjectValidator.IsSet(this.horizontalScrollBar) && this.horizontalScrollBar.Visible()) {
                    this.horizontalScrollBar.Visible(false);
                    $preparationResultHandler(this.horizontalScrollBar.Id(), true);
                }
            }
        }

        protected afterCacheCreation($id : string, $value : any) : void {
            if (this.Scrollable()) {
                if (ObjectValidator.IsSet(this.verticalScrollBar) && this.verticalScrollBar.Id() === $id) {
                    this.verticalScrollBar.Visible($value);
                }
                if (ObjectValidator.IsSet(this.horizontalScrollBar) && this.horizontalScrollBar.Id() === $id) {
                    this.horizontalScrollBar.Visible($value);
                }
            }
        }
    }
}
