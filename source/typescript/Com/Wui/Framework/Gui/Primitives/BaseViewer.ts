/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Primitives {
    "use strict";
    import IBaseViewer = Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseViewer;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import BaseViewerLayerType = Com.Wui.Framework.Gui.Enums.BaseViewerLayerType;
    import HttpManager = Com.Wui.Framework.Gui.HttpProcessor.HttpManager;
    import EventsManager = Com.Wui.Framework.Gui.Events.EventsManager;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;

    /**
     * BaseViewer should be used as abstract class for extending to the GUI viewers and
     * it is providing base methods connected with preparation of GUI object.
     */
    export class BaseViewer extends Com.Wui.Framework.Commons.Primitives.BaseObject implements IBaseViewer {

        private args : BaseViewerArgs;
        private argsHash : number;
        private instance : BaseGuiObject;
        private owner : BaseViewer;
        private testAppend : string;
        private testModeEnabled : boolean;
        private isCached : boolean;
        private isPrinted : boolean;
        private testButtons : ArrayList<AbstractGuiObject>;
        private layer : BaseViewerLayerType;
        private testSubscriber : any;

        /**
         * @param {boolean} [$testMode] Set, if created link should be in test mode.
         * @return {string} Returns link, which can be used for resolver registration or invoke by link element.
         */
        public static CallbackLink($testMode : boolean = false) : string {
            const prefix : string = Loader.getInstance().getHttpManager()
                .getRequest().getUrlArgs().KeyExists(HttpRequestConstants.WUI_DESIGNER) ? "design" : "web";
            let link : string = Loader.getInstance().getHttpManager()
                .CreateLink("/" + prefix + "/" + StringUtils.Replace(this.ClassName(), ".", "/"));
            link = StringUtils.Replace(link, "/#/", "#/");
            if (!StringUtils.StartsWith(link, "#")) {
                link = "#" + link;
            }
            if ($testMode) {
                link += "/" + HttpRequestConstants.TEST_MODE;
            }
            return link;
        }

        protected static getTestViewerArgs() : BaseViewerArgs {
            return null;
        }

        /**
         * @param {BaseViewerArgs} [$args] Set initialization arguments object.
         */
        constructor($args? : BaseViewerArgs) {
            super();
            this.instance = null;
            this.testAppend = "";
            this.isCached = false;
            this.isPrinted = false;
            this.owner = null;

            this.argsHash = null;
            this.ViewerArgs($args);
            this.TestModeEnabled(false);
        }

        /**
         * @param {IBaseViewer} [$value] Set owner of the viewer instance.
         * @return {IBaseViewer} Returns owner instance of the current instance, if the instance has owner, otherwise null.
         */
        public InstanceOwner($value? : IBaseViewer) : IBaseViewer {
            if (ObjectValidator.IsSet($value)) {
                this.owner = <BaseViewer>$value;
            }
            return this.owner;
        }

        /**
         * @param {boolean} [$value] Set viewer cache status.
         * @return {boolean} Returns true, if viewer has been loaded from cache, otherwise false.
         */
        public IsCached($value? : boolean) : boolean {
            return this.isCached = Property.Boolean(this.isCached, $value);
        }

        /**
         * @return {boolean} Returns true, if viewer's instance HTML output has been printed, otherwise false.
         */
        public IsPrinted() : boolean {
            if (!ObjectValidator.IsSet(this.isPrinted)) {
                this.isPrinted = false;
            }
            return this.isPrinted;
        }

        /**
         * @param {boolean} [$value] Specify, if viewer's object instance should be processed in test mode.
         * @return {boolean} Returns true, if viewer's object instance will be processed in test mode, otherwise false.
         */
        public TestModeEnabled($value? : boolean) : boolean {
            return this.testModeEnabled = Property.Boolean(this.testModeEnabled, $value);
        }

        /**
         * @param {BaseViewerArgs} [$args] Set viewer arguments.
         * @return {BaseViewerArgs} Returns viewer arguments.
         */
        public ViewerArgs($args? : BaseViewerArgs) : BaseViewerArgs {
            if (!ObjectValidator.IsEmptyOrNull($args) && !ObjectValidator.IsString($args)) {
                this.args = $args;
                if (!ObjectValidator.IsEmptyOrNull(this.instance) &&
                    (this.instance.IsLoaded() || this.IsCached() || !this.instance.IsLoaded() && !this.instance.Visible())) {
                    const newHash : number = $args.getHash();
                    if (this.argsHash !== newHash) {
                        this.argsHash = newHash;
                        this.PrepareImplementation();
                    }
                }
            }
            return this.args;
        }

        /**
         * @return {BaseGuiObject} Returns GUI object instance held by this viewer.
         */
        public getInstance() : BaseGuiObject {
            return this.instance;
        }

        /**
         * @return {string} Returns class name with namespace for structure of supported layers.
         */
        public getLayersType() : string {
            return BaseViewerLayerType.ClassName();
        }

        /**
         * @param {BaseViewerLayerType} $type Specify type of layer, which should be aplied on current view.
         * @return {void}
         */
        public setLayer($type : BaseViewerLayerType) : void {
            if (!ObjectValidator.IsEmptyOrNull(this.instance) && !ObjectValidator.IsEmptyOrNull($type) && this.layer !== $type) {
                this.layerHandler($type, this.instance);
                this.layer = $type;
            }
        }

        /**
         * Provides preparation of GUI object instance, which is held by this viewer.
         * @return {void}
         */
        public PrepareImplementation() : void {
            if (!ObjectValidator.IsEmptyOrNull(this.instance)) {
                this.instance.InstanceOwner(this);
                const instanceWithViewer : any = this.instance;
                if (ObjectValidator.IsFunction(instanceWithViewer.PanelViewer)
                    && !ObjectValidator.IsEmptyOrNull(instanceWithViewer.PanelViewer())) {
                    instanceWithViewer.PanelViewer().InstanceOwner(this);
                }

                if (!ObjectValidator.IsEmptyOrNull(this.args)) {
                    if (!this.args.Visible()) {
                        this.instance.Visible(false);
                    }
                    this.argsHandler(this.instance, this.args);
                }

                if (!this.instance.IsCompleted()) {
                    this.setLayer(BaseViewerLayerType.DEFAULT);
                    this.beforeLoad(this.instance, this.args);
                    this.instance.getEvents().setOnComplete(() : void => {
                        this.afterLoad(this.instance, this.args);
                    });
                }
            }

            if (this.testModeEnabled) {
                this.testButtons = new ArrayList<AbstractGuiObject>();
                if (!ObjectValidator.IsEmptyOrNull(this.testSubscriber)) {
                    const testProcessor : any = new this.testSubscriber();
                    testProcessor.setOwner(this);
                    this.testAppend = testProcessor.Process();
                    if (!ObjectValidator.IsSet(this.testAppend)) {
                        this.testAppend = "";
                    }
                }
                let testImplementationAppend : string = <string>this.testImplementation(this.instance, this.args);
                if (!ObjectValidator.IsSet(testImplementationAppend)) {
                    testImplementationAppend = "";
                }
                this.testAppend += testImplementationAppend;
                this.testButtons.foreach(($button : AbstractGuiObject) : void => {
                    this.testAppend += $button.Draw();
                });
                if (!ObjectValidator.IsEmptyOrNull(this.testAppend)) {
                    this.testAppend = "<div style=\"clear: both; height: 50px;\"></div>" + this.testAppend;
                }
                this.testAppend += "<style>body{overflow: auto; margin: inherit;}</style>";
            } else {
                this.testAppend = "";
                this.normalImplementation(this.instance, this.args);
            }

            if (!this.testModeEnabled && ObjectValidator.IsEmptyOrNull(this.instance) ||
                this.testModeEnabled &&
                ObjectValidator.IsEmptyOrNull(this.instance) && ObjectValidator.IsEmptyOrNull(this.testAppend)) {
                ExceptionsManager.Throw(this.getClassName(),
                    "Class '" + this.getClassName() + "' does not contains valid implementation for current view mode.");
            }
        }

        /**
         * @param {string} [$EOL] Specify element, which should be handled.
         * @return {string} Returns content of the viewer in string format, suitable for further rendering to the screen.
         */
        public Show($EOL? : string) : string {
            if (ObjectValidator.IsEmptyOrNull($EOL)) {
                $EOL = StringUtils.NewLine(false);
            }
            let output : string = "";

            if (!ObjectValidator.IsEmptyOrNull(this.instance)) {
                output += this.instance.Draw($EOL);
            }

            if (!ObjectValidator.IsEmptyOrNull(this.testAppend)) {
                output += this.testAppend;
            }
            this.isPrinted = true;

            return output;
        }

        /**
         * @return {object} Returns data suitable for object serialization.
         */
        public SerializationData() : object {
            return {isCached: this.isCached, instance: this.instance};
        }

        public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
            let id : string = "instance NOT DEFINED";
            if (!ObjectValidator.IsEmptyOrNull(this.getInstance())) {
                id = this.getInstance().Id();
            }
            return this.getClassName() + " (" + id + ")";
        }

        public toString() : string {
            return this.ToString();
        }

        protected setInstance($value : BaseGuiObject) : void {
            this.instance = $value;
            if (!ObjectValidator.IsEmptyOrNull(this.instance)) {
                this.instance.InstanceOwner(this);
            }
        }

        protected asyncLoad($handler : ($viewer : BaseViewer) => void) : void {
            $handler(this);
        }

        protected beforeLoad($instance? : BaseGuiObject, $args? : BaseViewerArgs) : void {
            // override this method for ability to prepare instance before it is loaded
        }

        protected afterLoad($instance? : BaseGuiObject, $args? : BaseViewerArgs) : void {
            // override this method for ability to setup instance after it is loaded
        }

        protected layerHandler($layer : BaseViewerLayerType, $instance? : BaseGuiObject) : void {
            // override this method for ability to setup instance according to selected layer
        }

        protected argsHandler($instance? : BaseGuiObject, $args? : BaseViewerArgs) : void {
            // override this method for ability distribute viewer args into the instance
        }

        protected normalImplementation($instance? : BaseGuiObject, $args? : BaseViewerArgs) : void {
            // override this method for ability to setup default instance behaviour
        }

        protected testImplementation($instance? : BaseGuiObject, $args? : BaseViewerArgs) : string | void {
            this.normalImplementation($instance, $args);
            return "";
        }

        protected addTestButton($text : string, $onClick : () => void, $buttonClassName? : any) : void {
            if (ObjectValidator.IsEmptyOrNull($buttonClassName) || !ObjectValidator.IsSet($buttonClassName.ClassName)) {
                $buttonClassName = AbstractGuiObject;
            }
            let button : AbstractGuiObject;
            try {
                button = new $buttonClassName($text);
            } catch (ex) {
                button = new AbstractGuiObject($text);
            }
            button.getEvents().setOnClick($onClick);
            this.testButtons.Add(button);
        }

        protected setTestSubscriber($className : ClassName | string) : void {
            this.testSubscriber = $className;
            if (ObjectValidator.IsString($className)) {
                this.testSubscriber = Reflection.getInstance().getClass(<string>$className);
            }
        }

        protected getHttpManager() : HttpManager {
            return Loader.getInstance().getHttpResolver().getManager();
        }

        protected getEventsManager() : EventsManager {
            return Loader.getInstance().getHttpResolver().getEvents();
        }

        protected getGuiManager() : GuiObjectManager {
            return GuiObjectManager.getInstanceSingleton();
        }

        protected excludeCacheData() : string[] {
            return ["testAppend", "testButtons", "isCached", "isPrinted", "args", "argsHash", "layer"];
        }
    }
}
