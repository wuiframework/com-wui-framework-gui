/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Primitives {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import IBasePanelViewerArgs = Com.Wui.Framework.Gui.Interfaces.Primitives.IBasePanelViewerArgs;

    /**
     * BasePanelViewerArgs is abstract structure handling data connected with BaseViewer.
     */
    export class BasePanelViewerArgs extends BaseViewerArgs implements IBasePanelViewerArgs {

        private asyncEnabled : boolean = false;

        /**
         * @param {boolean} [$value] Specify, if panel content should be loaded asynchronously.
         * @return {boolean} Returns true, if panel content can be loaded asynchronously, otherwise false.
         */
        public AsyncEnabled($value? : boolean) : boolean {
            this.asyncEnabled = Property.Boolean(this.asyncEnabled, $value);
            return this.asyncEnabled;
        }
    }
}
