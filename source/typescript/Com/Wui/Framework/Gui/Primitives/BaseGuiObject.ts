/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Primitives {
    "use strict";
    import IBaseGuiObject = Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseGuiObject;
    import IToolTip = Com.Wui.Framework.Gui.Interfaces.Components.IToolTip;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import GuiCommonsArgType = Com.Wui.Framework.Gui.Enums.GuiCommonsArgType;

    /**
     * BaseGuiObject should be used as abstract class for extending to the GUI objects
     * and it is providing base GUI object methods.
     */
    export abstract class BaseGuiObject extends GuiCommons implements IBaseGuiObject {

        private title : IToolTip;
        private changed : boolean;

        protected static onHoverEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager,
                                             $reflection? : Reflection) : void {
            if (!ObjectValidator.IsEmptyOrNull($eventArgs.Owner().Parent())) {
                $manager.setHovered($eventArgs.Owner().Parent(), true);
            } else {
                $manager.setHovered($eventArgs.Owner(), true);
            }
        }

        protected static onUnhoverEventHandler($eventArgs : MouseEventArgs, $manager : GuiObjectManager,
                                               $reflection? : Reflection) : void {
            $manager.setHovered($eventArgs.Owner(), false);
            if (!ObjectValidator.IsEmptyOrNull($eventArgs.Owner().Parent())) {
                $manager.setHovered($eventArgs.Owner().Parent(), true);
            }
        }

        /**
         * @return {IToolTip} Returns tooltip component associated with current GUI element.
         */
        public Title() : IToolTip {
            if (ObjectValidator.IsEmptyOrNull(this.title)) {
                const toolTipClass : any = this.getTitleClass();
                if (!ObjectValidator.IsEmptyOrNull(toolTipClass)) {
                    this.title = new toolTipClass(null, null, this.Id() + "_ToolTip");
                    this.title.Parent(this);
                    this.getGuiOptions().Add(GuiOptionType.TOOL_TIP);
                }
            }
            return this.title;
        }

        /**
         * @return {boolean} Returns true, if user has been interacting with the element, otherwise false.
         */
        public Changed() : boolean {
            if (!ObjectValidator.IsSet(this.changed)) {
                this.changed = false;
            }
            return this.changed;
        }

        /**
         * @param {any} [$value] Set element's value, which can be used by controllers.
         * @return {any} Returns value connected with the element, if element is type of input or value has been set,
         * otherwise null.
         */
        public Value($value? : any) : any {
            return null;
        }

        /**
         * @return {IGuiCommonsArg[]} Returns array of element's attributes.
         */
        public getArgs() : IGuiCommonsArg[] {
            const args : IGuiCommonsArg[] = super.getArgs();
            args.push({
                name : "Title",
                type : GuiCommonsArgType.TEXT,
                value: this.Title().Text()
            });
            args.push({
                name : "Value",
                type : GuiCommonsArgType.TEXT,
                value: this.Value()
            });
            return args;
        }

        /**
         * @param {IGuiCommonsArg} $value Specify argument value, which should be passed to element.
         * @param {boolean} [$force=false] Specify, if value should be set without fire of events connected with argument change.
         * @return {void}
         */
        public setArg($value : IGuiCommonsArg, $force : boolean = false) : void {
            switch ($value.name) {
            case "Title":
                this.Title().Text(<string>$value.value);
                break;
            case "Value":
                this.Value($value.value);
                break;
            default:
                super.setArg($value, $force);
                break;
            }
        }

        protected availableGuiOptions() : ArrayList<GuiOptionType> {
            const options : ArrayList<GuiOptionType> = super.availableGuiOptions();
            options.Add(GuiOptionType.TOOL_TIP);
            return options;
        }

        /**
         * @return {IToolTip} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IToolTip
         */
        protected getTitleClass() : any {
            return null;
        }

        protected setChanged() : void {
            this.changed = true;
        }

        protected innerCode() : IGuiElement {
            let titleContent : string | IGuiCommons = "";

            if (ObjectValidator.IsSet(this.title) || this.getGuiOptions().Contains(GuiOptionType.TOOL_TIP)) {
                titleContent = this.Title();
                this.getEvents().setOnMouseOver(($args : MouseEventArgs) : void => {
                    const element : BaseGuiObject = <BaseGuiObject>$args.Owner();
                    element.getTitleClass().Show(element.Title(), $args);
                });
                this.getEvents().setOnMouseOut(($args : MouseEventArgs) : void => {
                    const element : BaseGuiObject = <BaseGuiObject>$args.Owner();
                    element.getTitleClass().Hide(element.Title());
                });
                this.getEvents().setOnMouseMove(($args : MouseEventArgs) : void => {
                    const element : BaseGuiObject = <BaseGuiObject>$args.Owner();
                    element.getTitleClass().Move(element.Title(), $args);
                });
            }

            this.getEvents().setOnClick(
                ($args : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    $reflection.getClass($args.Owner().getClassName()).onHoverEventHandler($args, $manager, $reflection);
                });
            this.getEvents().setOnMouseOut(
                ($args : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    $reflection.getClass($args.Owner().getClassName()).onUnhoverEventHandler($args, $manager, $reflection);
                });

            return super.innerCode().Add(titleContent);
        }

        protected excludeSerializationData() : string[] {
            const exclude : string[] = super.excludeSerializationData();
            exclude.push("changed");
            return exclude;
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = super.excludeCacheData();
            exclude.push("title", "changed");
            return exclude;
        }
    }
}
