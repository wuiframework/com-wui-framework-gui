/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Primitives {
    "use strict";
    import IBaseViewerArgs = Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseViewerArgs;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    /**
     * BaseViewerArgs is abstract structure handling data connected with BaseViewer.
     */
    export class BaseViewerArgs extends Com.Wui.Framework.Commons.Primitives.BaseArgs implements IBaseViewerArgs {

        private visible : boolean = true;

        /**
         * @param {boolean} [$value] Specify, if viewer's content should be visible at the screen.
         * @return {boolean} Returns true, if viewer's content will be visible at the screen, otherwise false.
         */
        public Visible($value? : boolean) : boolean {
            this.visible = Property.Boolean(this.visible, $value);
            return this.visible;
        }
    }
}
