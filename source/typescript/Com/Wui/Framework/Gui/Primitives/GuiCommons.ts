/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Primitives {
    "use strict";
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import GuiOptionsManager = Com.Wui.Framework.Gui.GuiOptionsManager;
    import Constants = Com.Wui.Framework.Commons.Enums.SyntaxConstants;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import IEventsManager = Com.Wui.Framework.Gui.Interfaces.IEventsManager;
    import ElementEventsManager = Com.Wui.Framework.Gui.Events.ElementEventsManager;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import IBaseViewer = Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseViewer;
    import TimeoutManager = Com.Wui.Framework.Commons.Events.TimeoutManager;
    import IGuiCommonsEvents = Com.Wui.Framework.Gui.Interfaces.Events.IGuiCommonsEvents;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import Size = Com.Wui.Framework.Gui.Structures.Size;
    import IEventsHandler = Com.Wui.Framework.Gui.Interfaces.IEventsHandler;
    import PositionType = Com.Wui.Framework.Gui.Enums.PositionType;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import GuiCommonsArgType = Com.Wui.Framework.Gui.Enums.GuiCommonsArgType;
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import PropagableNumber = Com.Wui.Framework.Gui.Structures.PropagableNumber;
    import IArrayList = Com.Wui.Framework.Commons.Interfaces.IArrayList;
    import IResponsiveElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IResponsiveElement;
    import EventsManager = Com.Wui.Framework.Gui.Events.EventsManager;
    import HttpManager = Com.Wui.Framework.Gui.HttpProcessor.HttpManager;

    /**
     * GuiCommons should be used as abstract class for extending to any of GUI object
     * and it is providing base methods connected with GUI elements.
     */
    export abstract class GuiCommons extends Com.Wui.Framework.Commons.Primitives.BaseObject implements IGuiCommons {
        private static maxAsyncConcurrentCount : number = 50;
        private static guiElementClass : any = GuiElement;

        protected outputEndOfLine : string;
        private options : GuiOptionsManager;
        private availableOptionsList : ArrayList<GuiOptionType>;
        private events : ElementEventsManager;
        private readonly guiId : string;
        private guiPath : string;
        private parent : GuiCommons;
        private owner : BaseViewer;
        private visible : boolean;
        private enabled : boolean;
        private interfaceClassName : string;
        private styleClassName : string;
        private containerClassName : string;
        private loaded : boolean;
        private asyncDrawEnabled : boolean;
        private contentLoaded : boolean;
        private childElements : ArrayList<IGuiCommons>;
        private waitFor : ArrayList<IGuiCommons>;
        private cached : boolean;
        private prepared : boolean;
        private completed : boolean;
        private innerHtmlMap : IGuiElement;
        private alignment : Alignment;
        private fitToParent : FitToParent;
        private widthOfColumn : PropagableNumber;
        private wrappingElement : IGuiElement;

        /**
         * @param {any} [$dataObject] Data object suitable for data reconstruction.
         * @return {any} Returns class instance in case of, that data object is suitable for object reconstruction,
         * otherwise null. This method is mainly focused to unserialization purposes, but it can also returns class singleton.
         */
        public static getInstance($dataObject? : any) : any {
            const output : any = Reflection.getInstanceOf(this.ClassName(), $dataObject);
            if (!ObjectValidator.IsEmptyOrNull(output)) {
                output.setInstanceAttributes();
            }
            return output;
        }

        /**
         * @param {IGuiElement} $value Specify GuiElement class, which should be used for element rendering.
         * @return {IGuiElement} Returns GuiElement class, which is used for element rendering.
         */
        public static GuiElementClass($value? : any) : any {
            if (ObjectValidator.IsSet($value) && Reflection.getInstance().ClassHasInterface($value, IGuiElement)) {
                GuiCommons.guiElementClass = $value;
            }
            return GuiCommons.guiElementClass;
        }

        /**
         * @param {GuiCommons} $element Specify element, which should be handled.
         * @return {void}
         */
        public static Show($element : GuiCommons) : void {
            ElementManager.Show($element.Id());
            const eventArgs : EventArgs = new EventArgs();
            eventArgs.Owner($element);
            if ($element.IsLoaded()) {
                EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_SHOW, eventArgs, false);
                EventsManager.getInstanceSingleton().FireEvent(GuiCommons.ClassName(), EventType.ON_SHOW, eventArgs, false);
            } else {
                const parent : IGuiCommons = $element.Parent();
                if (!ObjectValidator.IsEmptyOrNull(parent) && parent.IsCompleted()) {
                    EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_SHOW, eventArgs, false);
                    EventsManager.getInstanceSingleton().FireEvent(GuiCommons.ClassName(), EventType.ON_SHOW, eventArgs, false);
                }
            }
        }

        /**
         * @param {GuiCommons} $element Specify element, which should be handled.
         * @return {void}
         */
        public static Hide($element : GuiCommons) : void {
            ElementManager.Hide($element.Id());
            const eventArgs : EventArgs = new EventArgs();
            eventArgs.Owner($element);
            if ($element.IsLoaded()) {
                EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_HIDE, eventArgs);
                EventsManager.getInstanceSingleton().FireEvent(GuiCommons.ClassName(), EventType.ON_HIDE, eventArgs);
            } else {
                const parent : IGuiCommons = $element.Parent();
                if (!ObjectValidator.IsEmptyOrNull(parent) && parent.IsCompleted()) {
                    EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_HIDE, eventArgs);
                    EventsManager.getInstanceSingleton().FireEvent(GuiCommons.ClassName(), EventType.ON_HIDE, eventArgs);
                }
            }
        }

        protected static asyncLoadChildren($loaderItems : ArrayList<() => void>) : void {
            if (GuiCommons.maxAsyncConcurrentCount > 1) {
                const loaderItemsLength : number = $loaderItems.Length();
                const loadManager : TimeoutManager = new TimeoutManager();
                let loaderIndex : number;
                for (loaderIndex = 0;
                     loaderIndex < Math.ceil(loaderItemsLength / GuiCommons.maxAsyncConcurrentCount);
                     loaderIndex++) {
                    loadManager.Add(($loaderIndex? : number) : void => {
                        let index : number;
                        for (index = 0; index < GuiCommons.maxAsyncConcurrentCount; index++) {
                            const itemIndex : number = $loaderIndex * GuiCommons.maxAsyncConcurrentCount + index;
                            if (itemIndex < loaderItemsLength) {
                                $loaderItems.getItem(itemIndex)();
                            } else {
                                break;
                            }
                        }
                    });
                }
                loadManager.Execute();
            } else {
                EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                    $loaderItems.foreach(($item : () => void) : void => {
                        $item();
                    });
                });
            }
        }

        /**
         * @param {string} [$id] Force to set element's id, instead of generated one.
         */
        constructor($id? : string) {
            super();
            if (ObjectValidator.IsEmptyOrNull($id)) {
                const manager : GuiObjectManager = this.getGuiManager();
                do {
                    this.guiId =
                        this.getClassNameWithoutNamespace()
                        + (new Date().getTime()).toString()
                        + Math.floor(Math.random() * 1000).toString();
                } while (manager.Exists(this));
            } else {
                this.guiId = $id;
            }
            this.setInstanceAttributes();
        }

        /**
         * @return {IGuiCommonsEvents} Returns events connected with the element.
         */
        public getEvents() : IGuiCommonsEvents {
            let initialized : boolean = false;
            if (ObjectValidator.IsEmptyOrNull(this.events)) {
                let eventsManagerClass : any = this.getEventsManagerClass();
                if (ObjectValidator.IsEmptyOrNull(eventsManagerClass)) {
                    eventsManagerClass = ElementEventsManager;
                }
                this.events = new eventsManagerClass(this);
                initialized = true;
            } else if (ObjectValidator.IsEmptyOrNull(this.events.getOwner())) {
                (<any>this.events).owner = this;
                initialized = true;
            }
            if (initialized) {
                this.getEvents = () : IGuiCommonsEvents => {
                    return <IGuiCommonsEvents>this.events;
                };
            }
            return <IGuiCommonsEvents>this.events;
        }

        /**
         * @return {GuiOptionsManager} Returns manager of element's output and behaviour options.
         */
        public getGuiOptions() : GuiOptionsManager {
            if (ObjectValidator.IsEmptyOrNull(this.options)) {
                this.options = new GuiOptionsManager(this.availableGuiOptions());
            }
            return this.options;
        }

        /**
         * @return {IGuiElement} Returns class with IGuiElement interface, which is used for element rendering.
         */
        public getGuiElementClass() : any {
            return GuiCommons.guiElementClass;
        }

        /**
         * @param {boolean} [$value] Set visibility of element content at the screen.
         * @return {boolean} Returns true, if content will be visible, otherwise false.
         */
        public Visible($value? : boolean) : boolean {
            if (ObjectValidator.IsSet($value)) {
                this.visible = Property.Boolean(this.visible, $value);
                if (!ObjectValidator.IsEmptyOrNull(this.InstanceOwner())) {
                    if (ElementManager.Exists(this.Id())) {
                        const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
                        if (this.visible) {
                            const events : IEventsManager = EventsManager.getInstanceSingleton();
                            if (!this.isContentLoaded() || !this.IsLoaded()) {
                                events.FireAsynchronousMethod(() : void => {
                                    if (this.visible) {
                                        if (!this.isContentLoaded()) {
                                            if (ObjectValidator.IsEmptyOrNull(this.outputEndOfLine)) {
                                                this.outputEndOfLine = StringUtils.NewLine(false);
                                            }
                                            const EOL : string = this.outputEndOfLine;
                                            ElementManager.setInnerHtml(this.Id(), this.guiContent().Draw(EOL + "        ") + EOL + "   ");
                                            events.FireEvent(this, EventType.ON_START);
                                        }
                                        thisClass.Show(this);
                                        if (!this.IsLoaded() && ElementManager.IsVisible(this.Id())) {
                                            if (this.IsCached() && !this.IsPrepared()) {
                                                this.innerCode();
                                            }
                                            events.FireEvent(this, EventType.ON_LOAD);
                                        }
                                    }
                                }, false);
                            } else {
                                thisClass.Show(this);
                                if (this.IsCached() && (this.IsPrepared() || !ObjectValidator.IsEmptyOrNull(this.Parent()))) {
                                    if (!this.IsPrepared()) {
                                        this.innerCode();
                                    }
                                    if (!this.IsCompleted()) {
                                        let fireCompleted : boolean = true;
                                        const validateVisibility : any = ($parent : GuiCommons) : void => {
                                            if (!ObjectValidator.IsEmptyOrNull($parent)) {
                                                if (!$parent.Visible() || !ElementManager.IsVisible($parent.Id())) {
                                                    fireCompleted = false;
                                                } else {
                                                    validateVisibility($parent.Parent());
                                                }
                                            }
                                        };
                                        validateVisibility(this.Parent());
                                        if (fireCompleted) {
                                            this.completed = true;
                                            events.FireEvent(this, EventType.ON_COMPLETE);
                                        }
                                    }
                                }
                            }
                        } else {
                            thisClass.Hide(this);
                        }
                    }
                }
            }
            if (!ObjectValidator.IsSet(this.visible)) {
                this.visible = true;
            }
            return this.visible;
        }

        /**
         * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
         * @return {boolean} Returns true, if element is in enabled mode, otherwise false.
         */
        public Enabled($value? : boolean) : boolean {
            if (!ObjectValidator.IsSet(this.enabled)) {
                this.enabled = true;
            }
            this.enabled = Property.Boolean(this.enabled, $value);
            if (!this.enabled) {
                if (!this.IsLoaded()) {
                    this.getGuiOptions().Add(GuiOptionType.DISABLE);
                } else if (!this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                    Echo.Printf("Disabled HTML mod can be missing at run time for element " + this.Id() + ". " +
                        "Did you add GuiOptionType?");
                }
            }
            if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
                if (!this.enabled && this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                    ElementManager.setClassName(this.Id() + "_Status", GeneralCssNames.DISABLE);
                } else {
                    ElementManager.setClassName(this.Id() + "_Status", "");
                }
            }

            return this.enabled;
        }

        /**
         * @param {string} [$value] Set type of css class name connected with the object instance.
         * @return {string} Returns css class name connected with the object instance.
         */
        public StyleClassName($value? : string) : string {
            if (ObjectValidator.IsSet($value)) {
                if (this.styleClassNameSetterValidator($value)) {
                    this.styleClassName = StringUtils.Remove($value, ".");
                    if (this.IsLoaded()) {
                        ElementManager.setClassName(this.Id() + "_GuiWrapper", this.styleClassName);
                    }
                }
            } else if (!ObjectValidator.IsSet(this.styleClassName)) {
                this.styleClassName = "";
            }

            return this.styleClassName;
        }

        /**
         * @return {string} Returns element id suitable to correct element identification in page context.
         */
        public Id() : string {
            return this.guiId;
        }

        /**
         * @param {GuiCommons} [$value] Set instance of the element owner.
         * @return {GuiCommons} Returns instance of the element owner, if the element has owner, otherwise null.
         */
        public Parent($value? : GuiCommons) : GuiCommons {
            if (ObjectValidator.IsSet($value)) {
                this.parent = $value;
                if (!ObjectValidator.IsSet(this.owner)) {
                    this.owner = <BaseViewer>this.parent.InstanceOwner();
                }
            }
            return this.parent;
        }

        /**
         * @param {IBaseViewer} [$value] Set viewer instance of the GUI object instance owner.
         * @return {IBaseViewer} Returns viewer instance of the GUI object instance owner, if the instance has owner, otherwise null.
         */
        public InstanceOwner($value? : IBaseViewer) : IBaseViewer {
            if (ObjectValidator.IsSet($value)) {
                this.owner = <BaseViewer>$value;
            }
            if (ObjectValidator.IsEmptyOrNull(this.owner)) {
                if (!ObjectValidator.IsEmptyOrNull(this.Parent())) {
                    this.owner = <BaseViewer>this.Parent().InstanceOwner();
                } else {
                    this.owner = null;
                }
            }
            return this.owner;
        }

        /**
         * @param {string} [$value] Set path to instance of the GUI object, which can be used for its usage identification.
         * @return {string} Returns path to instance of the GUI object in current usage.
         */
        public InstancePath($value? : string) : string {
            if (ObjectValidator.IsEmptyOrNull(this.guiPath)) {
                this.guiPath = this.getClassName();
                if (!ObjectValidator.IsEmptyOrNull(this.InstanceOwner())) {
                    let property : any;
                    for (property in this.owner) {
                        if (this.owner[property] === this) {
                            this.guiPath += "." + property;
                            break;
                        }
                    }
                }
            }
            return this.guiPath = Property.String(this.guiPath, $value);
        }

        /**
         * @return {IArrayList<IGuiCommons>} Returns list of child elements belonging to this instance.
         */
        public getChildElements() : IArrayList<IGuiCommons> {
            if (!ObjectValidator.IsSet(this.childElements)) {
                this.childElements = new ArrayList<IGuiCommons>();
            }
            return this.childElements;
        }

        /**
         * @return {ElementOffset} Returns element's position relative to screen top left corner.
         */
        public getScreenPosition() : ElementOffset {
            return ElementManager.getScreenPosition(this.guiContentId());
        }

        /**
         * Specify rendering position of the element or override position defined by CSS
         * @param {number} $top Specify element's top position
         * @param {number} $left Specify element's left position
         * @param {PositionType} [$type] Specify element's position type
         * @return {void}
         */
        public setPosition($top : number, $left : number, $type? : PositionType) : void {
            const id : string = this.Id() + "_GuiWrapper";
            const setPosition : IEventsHandler = () : void => {
                if (PositionType.Contains($type)) {
                    ElementManager.setCssProperty(id, "position", $type.toString());
                } else {
                    const positionType : string = ElementManager.getCssValue(id, "position");
                    if (ObjectValidator.IsEmptyOrNull(positionType) || positionType === "static") {
                        ElementManager.setCssProperty(id, "position", "relative");
                    }
                }
                ElementManager.setCssProperty(id, "top", $top);
                ElementManager.setCssProperty(id, "left", $left);
            };
            if (this.IsLoaded()) {
                setPosition();
            } else {
                this.getEvents().setOnComplete(setPosition);
            }
        }

        /**
         * @return {Size} Returns current element's width and height based on available information.
         */
        public getSize() : Size {
            if (this.IsLoaded()) {
                return new Size(this.guiContentId());
            }
            return new Size();
        }

        /**
         * Can be overriden in extending classes for FitToParent capability.
         * @param {number} [$value] Specify element's width value.
         * @return {number} Returns element's width value if reachable otherwise undefined.
         */
        public Width($value? : number) : number {
            return undefined;
        }

        /**
         * Can be overridden in extending classes for FitToParent capability.
         * @param {number} [$value] Specify element's height value.
         * @return {number} Returns element's height value if reachable otherwise undefined.
         */
        public Height($value? : number) : number {
            return undefined;
        }

        /**
         * @param {FitToParent} [$value] Specify how should this element be stretched inside its parent.
         * @return {IGuiCommons} Returns reference to the current GuiCommons instance.
         */
        public FitToParent($value : FitToParent) : IGuiCommons {
            this.fitToParent = $value;
            return this;
        }

        /**
         * @return {FitToParent} Returns set FitToParent.
         */
        public getFitToParent() : FitToParent {
            return this.fitToParent;
        }

        /**
         * @return {boolean} Returns true, if element has been loaded from cache, otherwise false.
         */
        public IsCached() : boolean {
            if (!ObjectValidator.IsSet(this.cached)) {
                this.cached = !ObjectValidator.IsEmptyOrNull(this.InstanceOwner()) && this.InstanceOwner().IsCached() && this.IsLoaded();
            }
            return this.cached;
        }

        /**
         * @return {boolean} Returns true, if element has been prepared by it's owner, otherwise false.
         */
        public IsPrepared() : boolean {
            if (!ObjectValidator.IsSet(this.prepared)) {
                this.prepared = false;
            }
            return this.prepared;
        }

        /**
         * @return {boolean} Returns true, if element has been loaded, otherwise false.
         */
        public IsLoaded() : boolean {
            if (!ObjectValidator.IsSet(this.loaded)) {
                this.loaded = false;
            }
            return this.loaded;
        }

        /**
         * @return {boolean} Returns true, if element's ON_COMPLETE event has been fired, otherwise false.
         */
        public IsCompleted() : boolean {
            if (!ObjectValidator.IsSet(this.completed)) {
                this.completed = false;
            }
            return this.completed;
        }

        /**
         * Disable asynchronous rendering of the element to the screen.
         * @return {void}
         */
        public DisableAsynchronousDraw() : void {
            this.asyncDrawEnabled = false;
        }

        /**
         * @param {string} [$EOL] Specify end of line format for generated html output.
         * @return {string} Returns element's content suitable for renderings at the screen.
         */
        public Draw($EOL? : string) : string {
            if (ObjectValidator.IsEmptyOrNull($EOL)) {
                $EOL = StringUtils.NewLine(false);
            }
            this.outputEndOfLine = $EOL;

            const id : string = this.Id();
            let parentId : string = "not registered";
            if (!ObjectValidator.IsEmptyOrNull(this.Parent())) {
                parentId = this.Parent().Id();
            }
            let content : string | IGuiElement = "";
            if (!this.hasAsynchronousDraw()) {
                content = this.guiContent();
            }

            if (ObjectValidator.IsEmptyOrNull(this.InstanceOwner()) ||
                !ObjectValidator.IsEmptyOrNull(this.InstanceOwner()) &&
                ObjectValidator.IsSet(this.InstanceOwner().TestModeEnabled) && this.InstanceOwner().TestModeEnabled()) {
                this.getEvents().FireAsynchronousMethod(() : void => {
                    LogIt.Debug("Draw of GUI object placeholder " + id + " with parent " + parentId);
                }, false);
            }

            const mainWrapper : IGuiElement = this.addElement().StyleClassName(this.getCssInterfaceName());
            if (ObjectValidator.IsSet(this.getWrappingElement())) {
                if (this.getWrappingElement().getGuiTypeTag() === GeneralCssNames.ROW) {
                    mainWrapper.Id(this.guiId + "_GuiCol").GuiTypeTag("GuiCol");
                }
            }
            return mainWrapper
                .Add(this.addElement(this.guiId + "_GuiWrapper")
                    .GuiTypeTag("GuiWrapper")
                    .StyleClassName(this.StyleClassName())
                    .Add(this.addElement(this.guiId)
                        .StyleClassName(this.getCssContainerName())
                        .Visible(!ObjectValidator.IsEmptyOrNull(content) && this.Visible())
                        .Add(content)
                    )
                )
                .Draw($EOL);
        }

        /**
         * @return {IGuiCommonsArg[]} Returns array of element's attributes.
         */
        public getArgs() : IGuiCommonsArg[] {
            const size : Size = this.getSize();
            const position : ElementOffset = this.getScreenPosition();

            return [
                {
                    name : "Id",
                    type : GuiCommonsArgType.TEXT,
                    value: this.Id()
                },
                {
                    name : "StyleClassName",
                    type : GuiCommonsArgType.TEXT,
                    value: this.StyleClassName()
                },
                {
                    name : "Enabled",
                    type : GuiCommonsArgType.BOOLEAN,
                    value: this.Enabled()
                },
                {
                    name : "Visible",
                    type : GuiCommonsArgType.BOOLEAN,
                    value: this.Visible()
                },
                {
                    name : "Width",
                    type : GuiCommonsArgType.NUMBER,
                    value: size.Width()
                },
                {
                    name : "Height",
                    type : GuiCommonsArgType.NUMBER,
                    value: size.Height()
                },
                {
                    name : "Top",
                    type : GuiCommonsArgType.NUMBER,
                    value: position.Top()
                },
                {
                    name : "Left",
                    type : GuiCommonsArgType.NUMBER,
                    value: position.Left()
                }
            ];
        }

        /**
         * @param {IGuiCommonsArg} $value Specify argument value, which should be passed to element.
         * @param {boolean} [$force=false] Specify, if value should be set without fire of events connected with argument change.
         * @return {void}
         */
        public setArg($value : IGuiCommonsArg, $force : boolean = false) : void {
            switch ($value.name) {
            case "StyleClassName":
                this.StyleClassName(<string>$value.value);
                break;
            case "Visible":
                this.Visible(<boolean>$value.value);
                break;
            case "Enabled":
                this.Enabled(<boolean>$value.value);
                break;
            case "Width":
                ElementManager.setCssProperty(this.guiContentId(), "width", <number>$value.value);
                break;
            case "Height":
                ElementManager.setCssProperty(this.guiContentId(), "height", <number>$value.value);
                break;
            case "Top":
                ElementManager.setCssProperty(this.guiContentId(), "top", <number>$value.value);
                break;
            case "Left":
                ElementManager.setCssProperty(this.guiContentId(), "left", <number>$value.value);
                break;
            default:
                break;
            }
        }

        /**
         * @return {IGuiElement} Returns reference to element's inner HTML structure object.
         */
        public getInnerHtmlMap() : IGuiElement {
            if (ObjectValidator.IsEmptyOrNull(this.innerHtmlMap)) {
                this.innerHtmlMap = this.innerHtml();
            }
            return this.innerHtmlMap;
        }

        /**
         * @return {IGuiElement} Returns reference to element wrapping this instance.
         */
        public getWrappingElement() : IGuiElement {
            return this.wrappingElement;
        }

        /**
         * @param {IGuiCommonsArg} $value Specify wrapping element.
         */
        public setWrappingElement($value : IGuiElement) : void {
            this.wrappingElement = $value;
        }

        /**
         * @return {boolean} Returns true if element should prevent scroll otherwise false.
         */
        public IsPreventingScroll() : boolean {
            return false;
        }

        public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
            return this.getClassName() + " (" + this.Id() + ")";
        }

        public toString() : string {
            return this.ToString();
        }

        /**
         * @return {ElementEventsManager} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Events.IGuiCommonsEvents
         */
        protected getEventsManagerClass() : any {
            return ElementEventsManager;
        }

        protected addElement($id? : string) : IGuiElement {
            return new GuiCommons.guiElementClass().Id($id);
        }

        protected addRow($id? : string) : IResponsiveElement {
            if (ObjectValidator.IsEmptyOrNull($id)) {
                $id = this.getRandomGuiId() + "_" + GeneralCssNames.ROW;
            }
            return new GuiCommons.guiElementClass().Id($id).GuiTypeTag(GeneralCssNames.ROW);
        }

        protected addColumn($id? : string) : IResponsiveElement {
            if (ObjectValidator.IsEmptyOrNull($id)) {
                $id = this.getRandomGuiId() + "_" + GeneralCssNames.COLUMN;
            }
            return new GuiCommons.guiElementClass().Id($id).GuiTypeTag(GeneralCssNames.COLUMN);
        }

        /**
         * Specify attributes of the instance after unserialization.
         */
        protected setInstanceAttributes() : void {
            this.parent = null;
            this.owner = null;

            this.prepared = false;
            this.completed = false;
            this.loaded = false;
            this.innerHtmlMap = null;
            this.getGuiManager().Add(this);
        }

        protected beforeCacheCreation($preparationResultHandler : ($id : string, $value : any) => void) : void {
            // override this method for ability to prepare gui element for cache creation
        }

        protected afterCacheCreation($id : string, $value : any) : void {
            // override this method for ability to restore gui element after cache creation
        }

        protected availableGuiOptions() : ArrayList<GuiOptionType> {
            if (!ObjectValidator.IsSet(this.availableOptionsList)) {
                this.availableOptionsList = new ArrayList<GuiOptionType>();
            }
            this.availableOptionsList.Add(GuiOptionType.DISABLE);
            return this.availableOptionsList;
        }

        protected styleClassNameSetterValidator($value : string) : boolean {
            return true;
        }

        protected cssInterfaceName() : string {
            return this.getNamespaceName();
        }

        protected cssContainerName() : string {
            return this.getClassNameWithoutNamespace();
        }

        protected getGuiTypeTag() : string {
            return this.getCssContainerName();
        }

        /**
         * @return {IGuiElement} String Used for definition of Code, which should be included as part of GUI object
         */
        protected innerCode() : IGuiElement {
            if (!ObjectValidator.IsSet(this.waitFor)) {
                this.waitFor = new ArrayList<IGuiCommons>();
            }

            const events : IEventsManager = EventsManager.getInstanceSingleton();

            if (this.getChildElements().IsEmpty()) {
                let parameter : any;
                for (parameter in this) {
                    if (parameter !== "parent" && parameter !== "owner" &&
                        typeof this[parameter] !== Constants.FUNCTION) {
                        const propertyInstance : GuiCommons = this[parameter];
                        if (!ObjectValidator.IsEmptyOrNull(propertyInstance) &&
                            ObjectValidator.IsSet(propertyInstance.Parent)) {
                            propertyInstance.Parent(this);
                            propertyInstance.InstancePath(this.InstancePath() + "." + parameter);
                            this.getChildElements().Add(propertyInstance, propertyInstance.Id());
                        }
                    }
                }
            }

            if (!this.IsLoaded()) {
                this.cached = false;

                this.getEvents().setOnStart(
                    ($eventArgs : EventArgs) : void => {
                        const element : GuiCommons = <GuiCommons>$eventArgs.Owner();
                        if (ObjectValidator.IsEmptyOrNull(element.InstanceOwner()) ||
                            !ObjectValidator.IsEmptyOrNull(element.InstanceOwner()) &&
                            ObjectValidator.IsSet(element.InstanceOwner().TestModeEnabled) && element.InstanceOwner().TestModeEnabled()) {
                            LogIt.Debug("Start " + element.Id());
                        }
                        element.getChildElements().foreach(($child : IGuiCommons) : void => {
                            if (!ObjectValidator.IsEmptyOrNull($child)) {
                                if ($child.Visible() && !$child.IsLoaded() && !element.waitFor.Contains($child)) {
                                    element.waitFor.Add($child);
                                }
                                $child.getEvents().setOnComplete(
                                    ($eventArgs : EventArgs) : void => {
                                        const child : GuiCommons = <GuiCommons>$eventArgs.Owner();
                                        const parent : GuiCommons = child.Parent();
                                        if (!ObjectValidator.IsEmptyOrNull(parent)) {
                                            if (!parent.waitFor.IsEmpty()) {
                                                parent.waitFor.RemoveAt(parent.waitFor.IndexOf(child));
                                            }
                                            if (parent.waitFor.IsEmpty() && !parent.IsCompleted()) {
                                                parent.completed = true;
                                                events.FireEvent(parent, EventType.ON_COMPLETE);
                                            }
                                        }
                                    });
                            }
                        });
                    });

                this.getEvents().setOnLoad(
                    ($eventArgs : EventArgs) : void => {
                        const element : GuiCommons = <GuiCommons>$eventArgs.Owner();
                        if (ObjectValidator.IsEmptyOrNull(element.InstanceOwner()) ||
                            !ObjectValidator.IsEmptyOrNull(element.InstanceOwner()) &&
                            ObjectValidator.IsSet(element.InstanceOwner().TestModeEnabled) && element.InstanceOwner().TestModeEnabled()) {
                            LogIt.Debug("Loaded " + element.Id());
                        }
                        element.cached = false;
                        element.loaded = true;
                        if (element.Visible()) {
                            if (ObjectValidator.IsSet(element.waitFor) && !element.waitFor.IsEmpty()) {
                                const loaderItems : ArrayList<() => void> = new ArrayList<() => void>();
                                element.waitFor.foreach(($child : IGuiCommons) : void => {
                                    loaderItems.Add(() : void => {
                                        if (!$child.IsLoaded() && $child.Visible()) {
                                            $child.Visible(true);
                                        }
                                    });
                                });

                                GuiCommons.asyncLoadChildren(loaderItems);
                            } else if (!element.IsCompleted()) {
                                element.completed = true;
                                events.FireEvent(element, EventType.ON_COMPLETE);
                            }
                        }
                    });

                this.getEvents().setBeforeLoad(
                    ($eventArgs : EventArgs) : void => {
                        const element : GuiCommons = <GuiCommons>$eventArgs.Owner();
                        if (ObjectValidator.IsEmptyOrNull(element.InstanceOwner()) ||
                            !ObjectValidator.IsEmptyOrNull(element.InstanceOwner()) &&
                            ObjectValidator.IsSet(element.InstanceOwner().TestModeEnabled) && element.InstanceOwner().TestModeEnabled()) {
                            LogIt.Debug("Prepare for load " + $eventArgs.Owner().Id());
                        }
                        events.FireEvent(element, EventType.ON_LOAD);
                    });
            } else {
                this.cached = true;
            }

            this.getEvents().setOnComplete(
                ($eventArgs : EventArgs) : void => {
                    const element : GuiCommons = <GuiCommons>$eventArgs.Owner();
                    if (element.Visible()) {
                        if (ObjectValidator.IsEmptyOrNull(element.InstanceOwner()) ||
                            !ObjectValidator.IsEmptyOrNull(element.InstanceOwner()) &&
                            ObjectValidator.IsSet(element.InstanceOwner().TestModeEnabled) && element.InstanceOwner().TestModeEnabled()) {
                            LogIt.Debug("Completed " + element.Id());
                        }
                        const parentIsCached : boolean = element.IsCached();
                        element.getChildElements().foreach(($child : GuiCommons) : void => {
                            if (parentIsCached && $child.IsLoaded()) {
                                $child.cached = true;
                            }
                            if (!$child.IsCompleted()) {
                                let fireCompleted : boolean = false;
                                if ($child.Visible() && ElementManager.IsVisible($child.Id())) {
                                    fireCompleted = true;
                                }
                                if (fireCompleted) {
                                    const validateVisibility : any = ($parent : GuiCommons) : void => {
                                        if (!ObjectValidator.IsEmptyOrNull($parent)) {
                                            if (!$parent.Visible() || !ElementManager.IsVisible($parent.Id())) {
                                                fireCompleted = false;
                                            } else {
                                                validateVisibility($parent.Parent());
                                            }
                                        }
                                    };
                                    validateVisibility($child.Parent());
                                    if (fireCompleted) {
                                        if (!$child.IsPrepared()) {
                                            $child.innerCode();
                                        }
                                        $child.completed = true;
                                        events.FireEvent($child, EventType.ON_COMPLETE);
                                    }
                                }
                            }
                        });

                        element.completed = true;
                        element.getEvents().Subscribe();
                    }
                });

            this.prepared = true;

            return this.addElement();
        }

        /**
         * @return {IGuiElement} String Used for definition of HTML with final settings
         */
        protected innerHtml() : IGuiElement {
            return this.addElement();
        }

        protected getHttpManager() : HttpManager {
            return Loader.getInstance().getHttpResolver().getManager();
        }

        protected getEventsManager() : EventsManager {
            return Loader.getInstance().getHttpResolver().getEvents();
        }

        protected getGuiManager() : GuiObjectManager {
            return GuiObjectManager.getInstanceSingleton();
        }

        protected guiContent() : IGuiElement {
            this.contentLoaded = true;
            let innerCode : string | IGuiElement = "";
            if (!this.IsPrepared()) {
                innerCode = this.innerCode();
            }
            this.innerHtmlMap = this.innerHtml();
            return this.addElement().Add(innerCode).Add(this.innerHtmlMap);
        }

        protected guiContentId() : string {
            return this.Id();
        }

        protected unhide($process : ($id : string, $element : HTMLElement) => boolean) : void {
            const clone : HTMLElement = <HTMLElement>ElementManager.getElement(this.Id())
                .parentElement.parentElement.cloneNode(true);
            ElementManager.setOpacity(clone, 0);
            document.body.appendChild(clone);
            const elements : any = clone.getElementsByTagName("div");
            let index : number;
            for (index = 0; index < elements.length; index++) {
                if (!$process((<HTMLElement>elements[index]).id, <HTMLElement>elements[index])) {
                    break;
                }
            }
            document.body.removeChild(clone);
        }

        protected excludeSerializationData() : string[] {
            const exclude : string[] = super.excludeSerializationData();
            exclude.push(
                "options", "availableOptionsList",
                "parent", "owner", "guiPath",
                "visible", "enabled", "prepared", "completed",
                "interfaceClassName", "styleClassName", "containerClassName",
                "loaded", "asyncDrawEnabled", "contentLoaded",
                "waitFor",
                "outputEndOfLine",
                "innerHtmlMap"
            );
            if (this.getEvents().getAll().IsEmpty()) {
                exclude.push("events");
            }
            return exclude;
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = [
                "options", "availableOptionsList",
                "events",
                "childElements", "waitFor",
                "cached", "prepared", "completed",
                "parent", "owner", "guiPath",
                "interfaceClassName", "styleClassName", "containerClassName",
                "innerHtmlMap"
            ];
            if (this.visible === true) {
                exclude.push("visible");
            }
            if (this.enabled === true) {
                exclude.push("enabled");
            }
            if (this.asyncDrawEnabled === true) {
                exclude.push("asyncDrawEnabled");
            }
            if (this.contentLoaded === false) {
                exclude.push("contentLoaded");
            } else if (this.contentLoaded === true && !this.IsMemberOf(BasePanel)) {
                exclude.push("outputEndOfLine");
            }
            if (this.loaded === false) {
                exclude.push("loaded");
            }

            return exclude;
        }

        private isContentLoaded() : boolean {
            if (!ObjectValidator.IsSet(this.contentLoaded)) {
                this.contentLoaded = false;
            }
            return this.contentLoaded;
        }

        private hasAsynchronousDraw() : boolean {
            if (!ObjectValidator.IsSet(this.asyncDrawEnabled)) {
                this.asyncDrawEnabled = true;
            }
            return this.asyncDrawEnabled;
        }

        private getCssInterfaceName() : string {
            if (!ObjectValidator.IsSet(this.interfaceClassName)) {
                this.interfaceClassName = StringUtils.Remove(this.cssInterfaceName(), ".");
            }
            return this.interfaceClassName;
        }

        private getCssContainerName() : string {
            if (!ObjectValidator.IsSet(this.containerClassName)) {
                this.containerClassName = StringUtils.Remove(this.cssContainerName(), ".");
            }
            return this.containerClassName;
        }

        private getRandomGuiId() : string {
            return (new Date().getTime()).toString()
                + Math.floor(Math.random() * 100000).toString();
        }
    }
}
