/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Primitives {
    "use strict";
    import IBasePanelViewer = Com.Wui.Framework.Gui.Interfaces.Primitives.IBasePanelViewer;
    import IBaseViewer = Com.Wui.Framework.Gui.Interfaces.Primitives.IBaseViewer;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import PanelContentType = Com.Wui.Framework.Gui.Enums.PanelContentType;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import GeneralEventOwner = Com.Wui.Framework.Commons.Enums.Events.GeneralEventOwner;
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import TimeoutManager = Com.Wui.Framework.Commons.Events.TimeoutManager;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;
    import IEventsManager = Com.Wui.Framework.Gui.Interfaces.IEventsManager;
    import IBasePanel = Com.Wui.Framework.Gui.Interfaces.Primitives.IBasePanel;
    import EventsManager = Com.Wui.Framework.Gui.Events.EventsManager;

    /**
     * BasePanelViewer should be used as abstract class for extending to the panel viewers and
     * it is providing base methods connected with preparation of panel content.
     */

    export class BasePanelViewer extends BaseViewer implements IBasePanelViewer {
        private static asyncRequestsList : ArrayList<string>;

        /**
         * @param {BasePanelViewerArgs} [$args] Set initialization arguments object.
         */
        constructor($args? : BasePanelViewerArgs) {
            super($args);
        }

        /**
         * @return {BasePanel} Returns Panel instance held by this viewer.
         */
        public getInstance() : BasePanel {
            return <BasePanel>super.getInstance();
        }

        /**
         * @param {BasePanelViewerArgs} [$args] Set panel viewer arguments.
         * @return {BasePanelViewerArgs} Returns panel viewer arguments.
         */
        public ViewerArgs($args? : BasePanelViewerArgs) : BasePanelViewerArgs {
            return <BasePanelViewerArgs>super.ViewerArgs(<BaseViewerArgs>$args);
        }

        /**
         * Provides preparation of panel's instance, which is held by this viewer.
         * @return {void}
         */
        public PrepareImplementation() : void {
            super.PrepareImplementation();
            const instance : BasePanel = <BasePanel>this.getInstance();

            if (!ObjectValidator.IsEmptyOrNull(instance) &&
                Reflection.getInstance().Implements(instance, IBasePanel)) {

                const events : IEventsManager = EventsManager.getInstanceSingleton();
                instance.getChildPanelList().foreach(($childViewer : BasePanelViewer) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($childViewer)) {
                        $childViewer.InstanceOwner(<IBaseViewer>this);
                    }
                });

                if (!ObjectValidator.IsEmptyOrNull(this.ViewerArgs()) && this.ViewerArgs().AsyncEnabled()) {
                    if (instance.ContentType() === PanelContentType.WITH_ELEMENT_WRAPPER ||
                        instance.ContentType() === PanelContentType.ASYNC_LOADER && this.IsCached()) {
                        instance.ContentType(PanelContentType.ASYNC_LOADER);

                        let viewerName : string = this.getClassName();
                        let parentViewer : BaseViewer = <BaseViewer>this.InstanceOwner();
                        while (!ObjectValidator.IsEmptyOrNull(parentViewer)) {
                            viewerName = parentViewer.getClassName();
                            parentViewer = <BaseViewer>parentViewer.InstanceOwner();
                        }

                        viewerName = StringUtils.Replace(viewerName, ".", "/");
                        if (!ObjectValidator.IsSet(BasePanelViewer.asyncRequestsList)) {
                            BasePanelViewer.asyncRequestsList = new ArrayList<string>();
                        }

                        events.setEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_COMPLETE,
                            ($eventArgs : AsyncRequestEventArgs) : void => {
                                if ($eventArgs.POST().KeyExists(HttpRequestConstants.ELEMENT_INSTANCE)) {
                                    BasePanelViewer.asyncRequestsList.RemoveAt(
                                        BasePanelViewer.asyncRequestsList.IndexOf(
                                            $eventArgs.POST().getItem(HttpRequestConstants.ELEMENT_INSTANCE)));
                                    if (!BasePanelViewer.asyncRequestsList.IsEmpty()) {
                                        const eventArgs : EventArgs = new EventArgs();
                                        eventArgs.Owner(BasePanelViewer.asyncRequestsList.getLast());
                                        events.FireEvent(BasePanelViewer.ClassName(), EventType.ON_ASYNC_REQUEST, eventArgs);
                                    }
                                }
                            });

                        events.setEvent(BasePanelViewer.ClassName(), EventType.ON_ASYNC_REQUEST, ($eventArgs : EventArgs) : void => {
                            const element : BasePanel = <BasePanel>$eventArgs.Owner();
                            const postData : ArrayList<any> = new ArrayList<any>();
                            postData.Add(element, HttpRequestConstants.ELEMENT_INSTANCE);
                            postData.Add(
                                StringUtils.Contains(this.getHttpManager().getRequest().getScriptPath(),
                                    HttpRequestConstants.HIDE_CHILDREN), HttpRequestConstants.HIDE_CHILDREN);
                            this.getHttpManager().ReloadTo("/async/" + viewerName, postData, true);
                        });

                        events.setEvent(instance, EventType.ON_ASYNC_REQUEST, ($eventArgs : EventArgs) : void => {
                            BasePanelViewer.asyncRequestsList.Add($eventArgs.Owner());
                            if (BasePanelViewer.asyncRequestsList.Length() === 1) {
                                events.FireEvent(BasePanelViewer.ClassName(), EventType.ON_ASYNC_REQUEST, $eventArgs);
                            }
                        });
                    }
                }
            }
        }

        /**
         * @param {string} [$EOL] Specify element, which should be handled.
         * @return {string}
         */
        public Show($EOL? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull(this.getInstance()) &&
                this.getInstance().ContentType() === PanelContentType.WITHOUT_ELEMENT_WRAPPER) {
                return this.getInstance().Draw($EOL);
            }
            return super.Show($EOL);
        }

        protected asyncLoad($handler : ($viewer : BasePanelViewer) => void) : void {
            if (!ObjectValidator.IsEmptyOrNull(this.getInstance())) {
                const asyncLoader : TimeoutManager = new TimeoutManager();
                (<any>this.getInstance()).asyncChildListLoad($handler, asyncLoader);
                asyncLoader.Execute();
            } else {
                $handler(this);
            }
        }
    }
}
