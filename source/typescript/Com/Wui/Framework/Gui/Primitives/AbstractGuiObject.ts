/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Primitives {
    "use strict";
    import IAbstractGuiObject = Com.Wui.Framework.Gui.Interfaces.Primitives.IAbstractGuiObject;
    import INotification = Com.Wui.Framework.Gui.Interfaces.Components.INotification;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;

    /**
     * AbstractGuiObject class provides GUI object, which can be used as temporary element needed
     * for development purposes.
     */
    export class AbstractGuiObject extends BaseGuiObject implements IAbstractGuiObject {

        private notification : INotification;
        private width : number;
        private height : number;
        private attributes : string;

        /**
         * @param {string} [$id] Force to set element's id, instead of generated one.
         */
        constructor($id? : string) {
            super($id);
        }

        /**
         * @return {INotification} Returns notification component associated with current GUI element.
         */
        public Notification() : INotification {
            if (ObjectValidator.IsEmptyOrNull(this.notification)) {
                const notificationClass : any = this.getNotificationClass();
                if (!ObjectValidator.IsEmptyOrNull(notificationClass)) {
                    this.notification = new notificationClass(null, null, this.Id() + "_Notification");
                }
            }
            return this.notification;
        }

        /**
         * @param {number} $width Set element's width.
         * @param {number} $height Set element's height.
         * @return {void}
         */
        public setSize($width : number, $height : number) : void {
            this.width = Property.PositiveInteger(this.width, $width);
            this.height = Property.PositiveInteger(this.height, $height);
        }

        /**
         * @param {string} [$value] Specify css style attributes in string format.
         * @return {string} Returns css style attributes subscribed to the element.
         */
        public StyleAttributes($value? : string) : string {
            return this.attributes = Property.String(this.attributes, $value);
        }

        /**
         * @param {string} $value Specify css style attributes in string format, which should be appended to current ones.
         * @return {void}
         */
        public AppendStyleAttributes($value : string) : void {
            let newAttribute : string = "";
            newAttribute = Property.String(newAttribute, $value);
            if (!StringUtils.Contains(newAttribute, ";")) {
                newAttribute += ";";
            }
            if (!ObjectValidator.IsSet(this.attributes)) {
                this.attributes = "";
            }
            if (!ObjectValidator.IsEmptyOrNull(this.attributes)) {
                this.attributes += " ";
            }
            this.attributes += newAttribute;
        }

        /**
         * @return {INotification} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.INotification
         */
        protected getNotificationClass() : any {
            return null;
        }

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Width(this.getWidth())
                .Height(this.getHeight())
                .setAttribute("border", "1px solid red")
                .setAttribute("color", "red")
                .setAttribute("overflow-x", "hidden")
                .setAttribute("overflow-y", "hidden")
                .setAttribute("", this.attributes)
                .Add(this.Id());
        }

        protected excludeSerializationData() : string[] {
            const exclude : string[] = super.excludeSerializationData();
            exclude.push("width", "height", "attributes");
            return exclude;
        }

        protected excludeCacheData() : string[] {
            const exclude : string[] = super.excludeCacheData();
            exclude.push("notification");

            if (this.width === 100) {
                exclude.push("width");
            }
            if (this.height === 20) {
                exclude.push("height");
            }
            if (ObjectValidator.IsEmptyOrNull(this.attributes)) {
                exclude.push("attributes");
            }
            return exclude;
        }

        private getWidth() : number {
            if (!ObjectValidator.IsSet(this.width)) {
                this.width = 100;
            }
            return this.width;
        }

        private getHeight() : number {
            if (!ObjectValidator.IsSet(this.height)) {
                this.height = 20;
            }
            return this.height;
        }
    }
}
