/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import GuiElement = Com.Wui.Framework.Gui.Primitives.GuiElement;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;

    class MockGuiCommons extends GuiCommons {
    }

    export class GuiObjectManagerTest extends UnitTestRunner {
        public testgetInstanceSingleton() : void {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            assert.equal(ObjectValidator.IsEmptyOrNull(manager), false);
            assert.equal(manager.IsTypeOf(GuiObjectManager), true);
        }

        public testExists() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            let gui : GuiCommons; // tslint:disable-line
            assert.equal(manager.Exists(gui), false);
            const basegui : GuiElement = new GuiElement();
            assert.equal(manager.Exists(<any>GuiElement), false);
            assert.equal(manager.Exists(<any>basegui), false);
            assert.equal(manager.Exists(<any>GuiCommons), false);
        }

        public testClear() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            const gui : GuiCommons = new MockGuiCommons();
            const gui2 : GuiCommons = new MockGuiCommons();
            const gui4 : GuiCommons = new MockGuiCommons();
            gui.Parent(gui4);
            manager.Add(gui);
            manager.Add(gui2);
            manager.Clear(gui2);
            assert.equal(manager.Exists(gui2), false);
            manager.Clear(gui4);
            let gui3 : GuiCommons; // tslint:disable-line
            assert.equal(manager.Exists(gui3), false);
            manager.Clear(gui3);
        }

        public testClearSecond() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            const parent : GuiCommons = new MockGuiCommons("id2");
            manager.Add(parent);
            const gol : GuiCommons = new MockGuiCommons("id25");
            gol.Parent(parent);
            const gol2 : GuiCommons = new MockGuiCommons("id2222222");
            const gol3 : GuiCommons = new MockGuiCommons("id3333333");
            const gol4 : GuiCommons = new MockGuiCommons("id4444444");
            gol.getChildElements().Add(gol2);
            gol.getChildElements().Add(gol3);
            gol.getChildElements().Add(gol4);
            manager.Add(gol);
            assert.deepEqual(manager.Exists(gol.getChildElements().getLast()), false);
            manager.Clear(gol4);
            assert.deepEqual(manager.Exists(gol4), false);
            manager.Clear(gol);
        }

        public testsetActive() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            const gui : GuiCommons = new MockGuiCommons();
            manager.setActive(gui, true);
            assert.equal(manager.IsActive(gui), true);
            const manager2 : GuiObjectManager = new GuiObjectManager();
            let gui2 : GuiCommons; // tslint:disable-line
            manager2.setActive(gui2, true);
            assert.equal(manager2.IsActive(gui2), false);
            const manager3 : GuiObjectManager = new GuiObjectManager();
            let gui3 : GuiCommons; // tslint:disable-line
            manager3.setActive(gui3, null);
            assert.equal(manager3.IsActive(gui3), false);
            const manager4 : GuiObjectManager = new GuiObjectManager();
            const gui4 : GuiCommons = new MockGuiCommons();
            manager4.setActive(gui4, false);
            assert.equal(manager4.IsActive(gui4), false);
        }

        public testIsHovered() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            const gui : GuiCommons = new MockGuiCommons();
            gui.setArg(<IGuiCommonsArg>{
                name : "",
                type : "",
                value: ""
            }, true);
            manager.setHovered(gui, true);
            assert.equal(manager.IsHovered(gui), false);
            manager.Add(gui);
            manager.setHovered(gui, true);
            assert.equal(manager.IsHovered(gui), true);

            const guinext : GuiCommons = new MockGuiCommons();
            manager.setHovered(guinext, false);
            assert.equal(manager.IsHovered(guinext), false);
            const manager2 : GuiObjectManager = new GuiObjectManager();
            const guielement : GuiElement = new GuiElement();
            manager2.setHovered(<any>guielement, true);
            assert.equal(manager2.IsHovered(guielement), false);
            const manager3 : GuiObjectManager = new GuiObjectManager();
            let element : IGuiCommons; // tslint:disable-line
            assert.equal(manager3.IsHovered(element), false);
        }

        public testToString() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            const gui : GuiCommons = new MockGuiCommons("id15");
            gui.setArg(<IGuiCommonsArg>{
                name : "",
                type : "",
                value: ""
            }, true);
            assert.equal(manager.ToString(""),
                "<b>All elements:</b>" +
                "<br/>none of GUI element has been registered yet<br/>" +
                "<b>Active elements:</b>" +
                "<br/>no active elements has been found<br/>");

            const manager2 : GuiObjectManager = new GuiObjectManager();
            const gui2 : GuiCommons = new MockGuiCommons("id16");
            manager2.IsActive(gui2);
            assert.equal(manager2.ToString("", false), "All elements:\r\n" +
                "none of GUI element has been registered yet\r\n" +
                "Active elements:\r\n" +
                "no active elements has been found\r\n");
            manager.setActive(gui, true);
            manager2.setActive(gui2, true);
            assert.equal(manager.ToString("", false), "All elements:\r\n" +
                "none of GUI element has been registered yet\r\nActive elements:\r\n" +
                "id: id15, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.GuiCommons, childrenList: EMPTY\r\n" +
                "Com.Wui.Framework.Gui.Primitives.GuiCommons\r\n    " +
                "id: id15, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.GuiCommons, childrenList: EMPTY\r\n");
        }

        public testgetActive() : void {
            const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
            const gui : GuiCommons = new MockGuiCommons("id10");
            const gui2 : GuiCommons = new MockGuiCommons("id11");
            manager.Add(gui);
            manager.Add(gui2);
            manager.setActive(gui, true);
            manager.setActive(gui2, false);
            assert.deepEqual(manager.IsActive(<any>gui), true);
            assert.deepEqual(manager.IsActive(MockGuiCommons), true);
            assert.equal(manager.getActive(gui).ToString("", false),
                "Com.Wui.Framework.Commons.Primitives.ArrayList object\r\nData object EMPTY");
            this.resetCounters();
            assert.equal(manager.getActive().ToString("", true),
                "<i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "[ \"id10\" ]&nbsp;&nbsp;&nbsp;&nbsp;Com.Wui.Framework.Gui.Primitives.GuiCommons (id10)<br/></span>");
        }

        public testgetType() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            const element : Primitives.AbstractGuiObject = new Primitives.AbstractGuiObject("id5");
            manager.Add(element);
            assert.equal(manager.getType(Primitives.AbstractGuiObject).IsEmpty(), false,
                "validate that element can be get by type");
            let TestScrollBar; // tslint:disable-line
            assert.equal(manager.getType(TestScrollBar).IsEmpty(), true,
                "validate that not registered element type can not be get by type");
            assert.equal(manager.ToString(), "<b>All elements:</b><br/>" +
                "id: id5, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.AbstractGuiObject, childrenList: EMPTY<br/><b>" +
                "Com.Wui.Framework.Gui.Primitives.GuiCommons</b><br/>&nbsp;&nbsp;&nbsp;" +
                "id: id5, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.AbstractGuiObject, childrenList: EMPTY<br/><b>" +
                "Com.Wui.Framework.Gui.Primitives.BaseGuiObject</b><br/>&nbsp;&nbsp;&nbsp;" +
                "id: id5, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.AbstractGuiObject, childrenList: EMPTY<br/><b>" +
                "Com.Wui.Framework.Gui.Primitives.AbstractGuiObject</b><br/>&nbsp;&nbsp;&nbsp;" +
                "id: id5, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.AbstractGuiObject, childrenList: EMPTY<br/><b>" +
                "Active elements:</b><br/>no active elements has been found<br/>");
        }

        public testgetAll() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            const gui : GuiCommons = new MockGuiCommons("id4");
            const gui2 : GuiCommons = new MockGuiCommons("id5");
            const gui3 : GuiCommons = new MockGuiCommons("id6");
            manager.Add(gui);
            manager.Add(gui2);
            manager.Add(gui3);
            this.resetCounters();
            assert.patternEqual(manager.getAll().toString(), "<i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> " +
                "<span onclick=\"" +
                "document.getElementById('ContentBlock_0').style.display=" +
                "document.getElementById('ContentBlock_0').style.display==='block'?'none':'block';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "[ \"id4\" ]&nbsp;&nbsp;&nbsp;&nbsp;Com.Wui.Framework.Gui.Primitives.GuiCommons (id4)<br/>" +
                "[ \"id5\" ]&nbsp;&nbsp;&nbsp;&nbsp;Com.Wui.Framework.Gui.Primitives.GuiCommons (id5)<br/>" +
                "[ \"id6\" ]&nbsp;&nbsp;&nbsp;&nbsp;Com.Wui.Framework.Gui.Primitives.GuiCommons (id6)<br/></span>");
        }

        public testIsHoveredSecond() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            const element : Primitives.AbstractGuiObject = new Primitives.AbstractGuiObject();
            const gui : GuiCommons = new MockGuiCommons("id3");
            manager.Add(gui);
            manager.setHovered(element, true);
            assert.equal(manager.IsHovered(<any>Primitives.AbstractGuiObject), false);
            manager.setHovered(gui, true);
            assert.equal(manager.IsHovered(<any>GuiCommons), true);
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
