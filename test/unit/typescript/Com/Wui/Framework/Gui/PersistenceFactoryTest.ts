/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui {
    "use strict";
    import PersistenceType = Com.Wui.Framework.Gui.Enums.PersistenceType;

    export class PersistenceFactoryTest extends UnitTestRunner {

        public testsessionIdGenerator() : void {
            PersistenceFactory.getPersistence(PersistenceType.AUTOFILL, "config");
            assert.equal(PersistenceFactory.sessionIdGenerator(PersistenceType.AUTOFILL, "config"),
                "dc20884a3836c5a3880a68af360c8e175974b65e");

            PersistenceFactory.getPersistence(PersistenceType.FORM_VALUES, "user");
            assert.equal(PersistenceFactory.sessionIdGenerator(PersistenceType.FORM_VALUES, "user"),
                "b8501ac6ac763230452bc4036e165ca3e2fb9e91");

            PersistenceFactory.getPersistence(PersistenceType.ERROR_FLAGS, "user2");
            assert.equal(PersistenceFactory.sessionIdGenerator(PersistenceType.ERROR_FLAGS, "user2"),
                "08c885cdfc30613b8d0e13ea2f4ffec3f45b308d");

            PersistenceFactory.getPersistence("browser", "user3");
            assert.ok(PersistenceFactory.sessionIdGenerator("browser", "user3"),
                "fa3645548ac2004aac5a25727d4a8ee7e33d802a");
        }

        public testsessionIdGeneratorSecond() : void {
            (<any>PersistenceFactory).globalOwner = "body";
            PersistenceFactory.getPersistence(PersistenceType.FORM_VALUES);
            assert.ok(PersistenceFactory.sessionIdGenerator(PersistenceType.FORM_VALUES, PersistenceType.FORM_VALUES),
                "a60c16a5e073c18739b4ef3f5ac4d52bbd69641d");
            (<any>PersistenceFactory).globalOwner = null;
        }

        public testGlobalOwner() : void {
            assert.equal(PersistenceFactory.GlobalOwner(), undefined);
            assert.equal(PersistenceFactory.GlobalOwner("user"), "user");
        }
    }
}
