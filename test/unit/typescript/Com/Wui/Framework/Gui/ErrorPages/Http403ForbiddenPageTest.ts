/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.ErrorPages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class Http403ForbiddenPageTest extends UnitTestRunner {
        public testgetMessageBody() : void {
            assert.resolveEqual(Http403ForbiddenPage, "" +
                "<head>\n<title>WUI - HTTP 403</title>\n\n" +
                "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
                "<link href=\"resource/graphics/Com/Wui/Framework/Gui/ForbiddenIcon.ico\" rel=\"shortcut icon\" type=\"text/css\">\n" +
                "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
                "</head>\n\n" +
                "<body onfocus=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
                "onblur=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyBlurEventHandler();\">\n" +
                "<div id=\"Browser\" class=\"FIREFOX\">\n" +
                "<div id=\"Language\" class=\"En\">\n" +
                "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n" +
                "<div class=\"Exception\">\n   " +
                "<h1>HTTP status 403</h1>\n" +
                "   <div class=\"Message\">\n" +
                "Access denied.\n" +
                "   </div>\n" +
                "</div>\n" +
                "<div class=\"Logo\">\n" +
                "   <div class=\"WUI\"></div>\n" +
                "</div>\n\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</body>");
        }
    }
}
