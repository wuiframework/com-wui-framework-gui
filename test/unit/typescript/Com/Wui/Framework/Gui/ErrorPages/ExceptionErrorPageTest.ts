/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.ErrorPages {
    "use strict";
    import Exception = Com.Wui.Framework.Commons.Exceptions.Type.Exception;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ExceptionCode = Com.Wui.Framework.Commons.Enums.ExceptionCode;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;

    class MockExceptionErrorPage extends ExceptionErrorPage {
        public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
            throw new Error("Com.Wui.Framework.Gui.ErrorPages.ExceptionErrorPage self error.");
        }

        public testPageTitle() : string {
            return this.getPageTitle();
        }

        public testMessageHeader() : string {
            return this.getMessageHeader();
        }
    }

    export class ExceptionErrorPageTest extends UnitTestRunner {

        public testgetExceptionsList() : void {
            const exception : Exception = new Exception();
            const exception1 : Exception = new Exception();
            const exceptionList : ArrayList<Exception> = new ArrayList<Exception>();
            exceptionList.Add(exception, 0);
            exceptionList.Add(exception1, 1);
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add(exceptionList, HttpRequestConstants.EXCEPTIONS_LIST);
            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            assert.equal(args.POST().getItem(HttpRequestConstants.EXCEPTIONS_LIST), exceptionList);
            assert.equal(args.POST().KeyExists(HttpRequestConstants.EXCEPTIONS_LIST), true);
            console.log = ($message : string) : void => { // tslint:disable-line
                console.info($message);  // tslint:disable-line
            };
            assert.resolveEqual(ExceptionErrorPage, "" +
                "<head>\n" +
                "<title>WUI - Exception</title>\n\n" +
                "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
                "<link href=\"resource/graphics/Com/Wui/Framework/Gui/ErrorIcon.ico\" rel=\"shortcut icon\" type=\"text/css\">\n" +
                "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
                "</head>\n\n" +
                "<body onfocus=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
                "onblur=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyBlurEventHandler();\">\n" +
                "<div id=\"Browser\" class=\"FIREFOX\">\n" +
                "<div id=\"Language\" class=\"En\">\n" +
                "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n" +
                "<div class=\"Exception\">\n" +
                "   <h1>Oops, something went wrong...</h1>\n" +
                "   <div class=\"Message\">\nthrown by: <b></b>: <br><br><br>thrown by: <b></b>: <br><br><br>\n" +
                "       <div class=\"Echo\">\n" +
                "           <span onclick=\"" +
                "document.getElementById(\'exceptionEcho\').style.display=" +
                "document.getElementById(\'exceptionEcho\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">" +
                "Echo output before exception           </span>\n" +
                "           <div id=\"exceptionEcho\" class=\"Text\" style=\"border: 0 solid black; display: none;\">\n" +
                "Nothing has been printed by Echo yet.\n           </div><br>" +
                "           <a style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif; " +
                "text-decoration: none;\" href=\"#/com-wui-framework-builder/about/Cache\">Cache info</a>\n" +
                "       </div>\n" +
                "   </div>\n" +
                "</div>\n" +
                "<div class=\"Logo\">\n" +
                "   <div class=\"WUI\"></div>\n" +
                "</div>\n\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</body>", args);
            this.initSendBox();
        }

        public testisFatalError() : void {
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add(ExceptionCode.FATAL_ERROR, HttpRequestConstants.EXCEPTION_TYPE);

            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            assert.equal(args.GET(data).getItem(HttpRequestConstants.EXCEPTION_TYPE), ExceptionCode.FATAL_ERROR);

            assert.resolveEqual(ExceptionErrorPage, "" +
                "<head>\n" +
                "<title>WUI - Fatal Error</title>\n\n" +
                "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
                "<link href=\"resource/graphics/Com/Wui/Framework/Gui/ErrorIcon.ico\" rel=\"shortcut icon\" type=\"text/css\">\n" +
                "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
                "</head>\n\n" +
                "<body onfocus=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
                "onblur=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyBlurEventHandler();\">\n" +
                "<div id=\"Browser\" class=\"FIREFOX\">\n<div id=\"Language\" class=\"En\">\n" +
                "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n" +
                "<div class=\"Exception\">\n" +
                "   <h1>FATAL Error!</h1>\n" +
                "   <div class=\"Message\">\n\n" +
                "       <div class=\"Echo\">\n" +
                "           <span onclick=\"" +
                "document.getElementById('exceptionEcho').style.display=" +
                "document.getElementById('exceptionEcho').style.display==='block'?'none':'block';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">" +
                "Echo output before exception           </span>\n" +
                "           <div id=\"exceptionEcho\" class=\"Text\" style=\"border: 0 solid black; display: none;\">\n" +
                "Nothing has been printed by Echo yet.\n" +
                "           </div><br>" +
                "           <a style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif; " +
                "text-decoration: none;\" href=\"#/com-wui-framework-builder/about/Cache\">Cache info</a>\n" +
                "       </div>\n" +
                "   </div>\n" +
                "</div>\n" +
                "<div class=\"Logo\">\n" +
                "   <div class=\"WUI\"></div>\n" +
                "</div>\n\n" +
                "</div>\n" +
                "</div>\n</div>\n</body>", args);
        }

        public testgetPageTitle() : void {
            const data : ArrayList<any> = new ArrayList<any>();
            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            console.log = ($message : string) : void => { // tslint:disable-line
                console.info($message);  // tslint:disable-line
            };
            assert.resolveEqual(MockExceptionErrorPage, "<head>\n<title>WUI - Exception</title>*", args);
        }

        public testgetPageTitleSecond() : void {
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add(ExceptionCode.FATAL_ERROR, HttpRequestConstants.EXCEPTION_TYPE);

            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            assert.equal(args.GET(data).getItem(HttpRequestConstants.EXCEPTION_TYPE), ExceptionCode.FATAL_ERROR);
            assert.resolveEqual(MockExceptionErrorPage, "*WUI - Fatal Error*", args);
        }

        public testgetMessageHeader() : void {
            const data : ArrayList<any> = new ArrayList<any>();
            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            console.log = ($message : string) : void => { // tslint:disable-line
                console.info($message);  // tslint:disable-line
            };
            assert.resolveEqual(MockExceptionErrorPage, "*Oops, something went wrong...*", args);
            this.initSendBox();
        }

        public testgetMessageHeaderSecond() : void {
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add(ExceptionCode.FATAL_ERROR, HttpRequestConstants.EXCEPTION_TYPE);

            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            assert.equal(args.GET(data).getItem(HttpRequestConstants.EXCEPTION_TYPE), ExceptionCode.FATAL_ERROR);
            assert.resolveEqual(MockExceptionErrorPage, "*<h1>FATAL Error!</h1>*", args);
            this.initSendBox();
        }

        public testExceptionErrorPage() : void {
            assert.resolveEqual(ExceptionErrorPage, "" +
                "<head>\n" +
                "<title>WUI - Exception</title>\n\n" +
                "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
                "<link href=\"resource/graphics/Com/Wui/Framework/Gui/ErrorIcon.ico\" rel=\"shortcut icon\" type=\"text/css\">\n" +
                "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
                "</head>\n\n" +
                "<body onfocus=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
                "onblur=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyBlurEventHandler();\">\n" +
                "<div id=\"Browser\" class=\"FIREFOX\">\n" +
                "<div id=\"Language\" class=\"En\">\n" +
                "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n" +
                "<div class=\"Exception\">\n" +
                "   <h1>Oops, something went wrong...</h1>\n" +
                "   <div class=\"Message\">\n\n" +
                "       <div class=\"Echo\">\n" +
                "           <span onclick=\"" +
                "document.getElementById(\'exceptionEcho\').style.display=" +
                "document.getElementById(\'exceptionEcho\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">" +
                "Echo output before exception" +
                "           </span>\n" +
                "           <div id=\"exceptionEcho\" class=\"Text\" style=\"border: 0 solid black; display: none;\">\n" +
                "Nothing has been printed by Echo yet.\n" +
                "           </div>" +
                "<br>" +
                "           <a style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif; " +
                "text-decoration: none;\" href=\"#/com-wui-framework-builder/about/Cache\">Cache info</a>\n" +
                "       </div>\n" +
                "   </div>\n" +
                "</div>\n" +
                "<div class=\"Logo\">\n" +
                "   <div class=\"WUI\"></div>\n" +
                "</div>\n\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</body>");
            this.initSendBox();
        }

        public testMockException() : void {
            assert.resolveEqual(MockExceptionErrorPage, "<head>\n<title>WUI - Exception</title>\n\n" +
                "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
                "<link href=\"resource/graphics/Com/Wui/Framework/Gui/ErrorIcon.ico\" rel=\"shortcut icon\" type=\"text/css\">" +
                "\n" +
                "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css\" rel=\"stylesheet\" type=\"text/css\">\n" +
                "\n" +
                "</head>\n" +
                "\n" +
                "<body onfocus=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyFocusEventHandler();" +
                "\" onblur=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyBlurEventHandler();\">" +
                "\n" +
                "<div id=\"Browser\" class=\"FIREFOX\">\n" +
                "<div id=\"Language\" class=\"En\">\n" +
                "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n" +
                "<div class=\"Exception\">\n   " +
                "<h1>Oops, something went wrong...</h1>\n   " +
                "<div class=\"Message\">\n\n       " +
                "<div class=\"Echo\">\n           " +
                "<span onclick=\"" +
                "document.getElementById(\'exceptionEcho\').style.display=" +
                "document.getElementById(\'exceptionEcho\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">" +
                "Echo output before exception           </span>\n           " +
                "<div id=\"exceptionEcho\" class=\"Text\" style=\"border: 0 solid black; display: none;\">\n" +
                "Nothing has been printed by Echo yet.\n           </div>" +
                "<br>           " +
                "<a style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif; text-decoration: none;" +
                "\" href=\"#/com-wui-framework-builder/about/Cache\">Cache info</a>\n       " +
                "</div>\n   " +
                "</div>\n</div>\n" +
                "<div class=\"Logo\">\n   " +
                "<div class=\"WUI\">" +
                "</div>\n" +
                "</div>\n\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</body>");
            this.initSendBox();
        }
    }
}
