/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.ErrorPages {
    "use strict";

    export class BrowserErrorPageTest extends UnitTestRunner {
        public testgetPageBody() : void {
            assert.resolveEqual(BrowserErrorPage, "<head>\n<title>WUI - Browser Error</title>\n\n" +
                "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
                "<link href=\"resource/graphics/Com/Wui/Framework/Gui/ErrorIcon.ico\" rel=\"shortcut icon\" type=\"text/css\">\n" +
                "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css\" rel=\"stylesheet\" type=\"text/css\">\n" +
                "\n" +
                "</head>\n\n<body onfocus=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
                "onblur=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyBlurEventHandler();\">\n<div id=\"Browser\" class=\"FIREFOX\">\n" +
                "<div id=\"Language\" class=\"En\">\n<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n" +
                "<div class=\"Exception\">\n   <h1>Unsupported browser</h1>\n   <div class=\"Message\">\n" +
                "You are using unsupported type or version of the browser. " +
                "Please, choose one of the supported browser from the list below:<br>" +
                "<a href=\"http://windows.microsoft.com/en-us/internet-explorer/download-ie\" target=\"_blank\">Internet Explorer 5+</a>" +
                "<br><a href=\"https://www.mozilla.org/en-US/firefox/new/\" target=\"_blank\">" +
                "Firefox</a><br><a href=\"http://www.google.com/chrome/\" target=\"_blank\">" +
                "Google Chrome</a><br><a href=\"http://www.opera.com/\" target=\"_blank\">" +
                "Opera</a><br><a href=\"https://www.apple.com/safari/\" target=\"_blank\">" +
                "Safari</a>\n   </div>\n</div>\n<div class=\"Logo\">\n   " +
                "<div class=\"WUI\"></div>\n</div>\n\n</div>\n</div>\n</div>\n</body>");
        }
    }
}
