/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.ErrorPages {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;

    export class Http404NotFoundPageTest extends UnitTestRunner {
        public testgetMessageBody() : void {
            assert.resolveEqual(Http404NotFoundPage, "" +
                "<head>\n" +
                "<title>WUI - HTTP 404</title>\n\n" +
                "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
                "<link href=\"resource/graphics/Com/Wui/Framework/Gui/ErrorIcon.ico\" rel=\"shortcut icon\" type=\"text/css\">\n" +
                "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
                "</head>\n\n" +
                "<body onfocus=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
                "onblur=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyBlurEventHandler();\">\n" +
                "<div id=\"Browser\" class=\"FIREFOX\">\n" +
                "<div id=\"Language\" class=\"En\">\n" +
                "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n<div class=\"Exception\">\n" +
                "   <h1>HTTP status 404</h1>\n" +
                "   <div class=\"Message\">\n" +
                "File has not been found.\n" +
                "   </div>\n" +
                "</div>\n" +
                "<div class=\"Logo\">\n" +
                "   <div class=\"WUI\"></div>\n" +
                "</div>\n\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</body>");
        }

        public testgetMessageBody2() : void {
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add("Com/Wui/Framework/GuiErrorPages/Http404NotFoundPage.ts/", HttpRequestConstants.HTTP404_FILE_PATH);

            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            assert.resolveEqual(Http404NotFoundPage, "" +
                "<head>\n" +
                "<title>WUI - HTTP 404</title>\n\n" +
                "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
                "<link href=\"resource/graphics/Com/Wui/Framework/Gui/ErrorIcon.ico\" rel=\"shortcut icon\" type=\"text/css\">\n" +
                "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
                "</head>\n\n" +
                "<body onfocus=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
                "onblur=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyBlurEventHandler();\">\n" +
                "<div id=\"Browser\" class=\"FIREFOX\">\n<div id=\"Language\" class=\"En\">\n" +
                "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n" +
                "<div class=\"Exception\">\n" +
                "   <h1>HTTP status 404</h1>\n" +
                "   <div class=\"Message\">\n" +
                "File has not been found. Required file path is:<br>" +
                "<a href=\"Com/Wui/Framework/GuiErrorPages/Http404NotFoundPage.ts/\">" +
                "Com/Wui/Framework/GuiErrorPages/Http404NotFoundPage.ts/</a>\n" +
                "   </div>\n" +
                "</div>\n" +
                "<div class=\"Logo\">\n" +
                "   <div class=\"WUI\"></div>\n" +
                "</div>\n\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</body>", args);
            assert.equal(args.POST().getItem(HttpRequestConstants.HTTP404_FILE_PATH),
                "Com/Wui/Framework/GuiErrorPages/Http404NotFoundPage.ts/");
        }
    }
}
