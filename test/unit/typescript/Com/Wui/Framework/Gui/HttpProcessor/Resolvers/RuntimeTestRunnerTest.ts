/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.HttpProcessor.Resolvers {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;
    import FormsObject = Com.Wui.Framework.Gui.Primitives.FormsObject;
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import GeneralEventOwner = Com.Wui.Framework.Gui.Enums.Events.GeneralEventOwner;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import StaticPageContentManager = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;
    import EventsManager = Com.Wui.Framework.Gui.Events.EventsManager;

    class MockFormsObject extends FormsObject {
    }

    class MockGuiCommons extends GuiCommons {
    }

    class MockRuntimeTestRunner extends RuntimeTestRunner {
    }

    class MockRuntimeTestRunnerResolve extends RuntimeTestRunner {

        public testData() : void {
            this.assertEquals("hello", "hello");
        }

        protected setUp() : void {  // tslint: disable-line
        }

        protected tearDown() : void {   // tslint: disable-line
        }
    }

    class MockRuntimeTestRunnerEquals extends RuntimeTestRunner {
        public testCreateLinkRun() : void {
            assert.equal(RuntimeTestRunner.CreateLink(RuntimeTests.ImageProcessor.ImageTransformTest),
                "#/com-wui-framework-builder/RuntimeTest/Com/Wui/Framework/Gui/RuntimeTests/ImageProcessor/ImageTransformTest");
        }
    }

    class MockRequest extends RuntimeTestRunner {
        public testincludeChildren() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const form : FormsObject = new MockFormsObject();
                const gui : GuiCommons = new MockGuiCommons();
                form.Parent(gui);

                const data : ArrayList<any> = new ArrayList<any>();
                data.Add("value2", "key2");
                data.Add("value3", "key3");
                data.Add(form, HttpRequestConstants.HIDE_CHILDREN);

                const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
                assert.equal(args.POST().getItem(HttpRequestConstants.HIDE_CHILDREN), form);
                assert.equal(args.POST().getItem(HttpRequestConstants.HIDE_CHILDREN), form);
                assert.equal(args.POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN), true);
                assert.equal(args.POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN), true);

                const guiobjectdraw : AsyncDrawGuiObject = new AsyncDrawGuiObject();
                guiobjectdraw.RequestArgs(args);
                assert.equal(guiobjectdraw.ObjectClassName("Com.Wui.Framework.Gui.Primitives.GuiElement"),
                    "Com.Wui.Framework.Gui.Primitives.GuiElement");
                guiobjectdraw.Process();
                $done();
            };
        }

        public testCreateLinkThird() : void {
            this.assertEquals(this.createLink("/" + HttpRequestConstants.RUNTIME_TEST + "/testName"),
                "/com-wui-framework-commons/testName",
                "absolute link '/testName'");
        }
    }

    export class RuntimeTestRunnerTest extends UnitTestRunner {
        public testCreateLink() : void {
            assert.equal(RuntimeTestRunner.CreateLink(RuntimeTests.ImageProcessor.ImageTransformTest),
                "#/com-wui-framework-builder/RuntimeTest/Com/Wui/Framework/Gui/RuntimeTests/ImageProcessor/ImageTransformTest");
        }

        public testCallbackLink() : void {
            assert.equal(RuntimeTestRunner.CallbackLink(),
                "#/com-wui-framework-builder/RuntimeTest/Com/Wui/Framework/Gui/HttpProcessor/Resolvers/RuntimeTestRunner");
        }

        public testresolve() : void {
            assert.resolveEqual(MockRuntimeTestRunnerResolve, "" +
                "<head>\n" +
                "<title>WUI - Unit Test</title>\n\n" +
                "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
                "<link href=\"resource/graphics/icon.ico\" rel=\"shortcut icon\" type=\"text/css\">\n" +
                "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
                "</head>\n\n" +
                "<body onfocus=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
                "onblur=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyBlurEventHandler();\" class=\"RuntimeTest\">\n" +
                "<div id=\"Browser\" class=\"FIREFOX\">\n" +
                "<div id=\"Language\" class=\"En\">\n" +
                "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n\n" +
                "<span guitype=\"HtmlAppender\"><h2>Runtime UNIT Test</h2><h2></h2></span><span guitype=\"HtmlAppender\">" +
                "<h3>Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner - Data</h3></span>" +
                "<span guitype=\"HtmlAppender\">" +
                "<h4 id=\"RuntimeTestRunner_Assert_1\">" +
                "<i id=\"RuntimeTestRunner_Assert_Pass_1\" style=\"color: green;\">assert #1 has passed</i></h4>" +
                "</span><span guitype=\"HtmlAppender\"><hr></span><span guitype=\"HtmlAppender\"><br>" +
                "<span class=\"Result\">SUCCESS<br></span>Tests: 1, Assertions: 1, Failures: 0.</span>" +
                "<span guitype=\"HtmlAppender\"><br><br>Page was generated in * seconds.</span>" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</body>");
        }

        public testresolve2() : void {
            assert.resolveEqual(MockRuntimeTestRunnerEquals, "" +
                "<head>\n" +
                "<title>WUI - Unit Test</title>\n\n" +
                "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
                "<link href=\"resource/graphics/icon.ico\" rel=\"shortcut icon\" type=\"text/css\">\n" +
                "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
                "</head>\n\n" +
                "<body onfocus=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
                "onblur=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyBlurEventHandler();\" class=\"RuntimeTest\">\n" +
                "<div id=\"Browser\" class=\"FIREFOX\">\n" +
                "<div id=\"Language\" class=\"En\">\n" +
                "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n\n" +
                "<span guitype=\"HtmlAppender\">" +
                "<h2>Runtime UNIT Test</h2>" +
                "<h2></h2></span>" +
                "<span guitype=\"HtmlAppender\">" +
                "<h3>Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner - CreateLinkRun</h3></span>" +
                "<span guitype=\"HtmlAppender\"><hr></span>" +
                "<span guitype=\"HtmlAppender\"><br>" +
                "<span class=\"Result\">SUCCESS<br></span>" +
                "Tests: 1, Assertions: 0, Failures: 0.</span>" +
                "<span guitype=\"HtmlAppender\"><br><br>Page was generated in * seconds.</span></div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</body>");
        }

        public __Ignoretestresolve3() : IUnitTestRunnerPromise {
            assert.resolveEqual(MockRequest, "" +
                "<head>\n" +
                "<title>WUI - Unit Test</title>\n\n" +
                "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
                "<link href=\"resource/graphics/icon.ico\" rel=\"shortcut icon\" type=\"text/css\">\n" +
                "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
                "</head>\n\n" +
                "<body " +
                "onfocus=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
                "onblur=\"Com.Wui.Framework.Gui.Events.EventsManager.bodyBlurEventHandler();\" class=\"RuntimeTest\">\n" +
                "<div id=\"Browser\" class=\"FIREFOX\">\n" +
                "<div id=\"Language\" class=\"En\">\n" +
                "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n\n" +
                "<span guitype=\"HtmlAppender\"><h2>Runtime UNIT Test</h2><h2></h2></span>" +
                "<span guitype=\"HtmlAppender\">" +
                "<h3>Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner - includeChildren</h3></span>" +
                "<span guitype=\"HtmlAppender\"><hr></span><span guitype=\"HtmlAppender\">" +
                "<h3>Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner - CreateLinkThird</h3></span>" +
                "<span guitype=\"HtmlAppender\"><h4 id=\"RuntimeTestRunner_Assert_1\"> - absolute link '/testName':<br>" +
                "<i id=\"RuntimeTestRunner_Assert_Fail_1\" style=\"color: red;\">assert #1 has failed</i></h4></span>" +
                "<span guitype=\"HtmlAppender\"><br>" +
                "<u>actual:</u><br>/com-wui-framework-builder/RuntimeTest/testName</span><span guitype=\"HtmlAppender\"><br>" +
                "<u>expected:</u><br>/com-wui-framework-commons/testName</span><span guitype=\"HtmlAppender\"><hr>" +
                "</span><span guitype=\"HtmlAppender\"><br>" +
                "<span class=\"Result\">FAILURES!<br></span>Tests: 2, Assertions: 1, Failures: 1.</span>" +
                "<span guitype=\"HtmlAppender\"><br><br>" +
                "Page was generated in * seconds.</span>" +
                "<span guitype=\"HtmlAppender\">" +
                "<div id=\"RuntimeTestRunner_GoToNextFailing\" class=\"GoToNextFailing\">Go to next failing assert</div></span></div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</body>"
            );
            this.registerElement("RuntimeTestRunner_GoToNextFailing");
            return ($done : any) : void => {
                setTimeout(() : void => {
                    this.initSendBox();
                    $done();
                }, 200);
            };
        }

        public __IgnoretestincludeChildren() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const form : FormsObject = new MockFormsObject();
                const gui : GuiCommons = new MockGuiCommons();
                form.Parent(gui);

                const data : ArrayList<any> = new ArrayList<any>();
                data.Add("value2", "key2");
                data.Add("value3", "key3");
                data.Add(form, HttpRequestConstants.HIDE_CHILDREN);
                data.Add("#/#/RuntimeTest/Com/Wui/Framework/Gui/RuntimeTests/ImageProcessor/ImageTransformTest",
                    HttpRequestConstants.RUNTIME_TEST);
                const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
                assert.equal(args.POST().getItem(HttpRequestConstants.HIDE_CHILDREN), form);
                assert.equal(args.POST().getItem(HttpRequestConstants.HIDE_CHILDREN), form);
                assert.equal(args.POST().KeyExists(HttpRequestConstants.RUNTIME_TEST), true);
                assert.equal(args.POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN), true);
                assert.equal(args.POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN), true);

                const runner : RuntimeTestRunner = new MockRuntimeTestRunner();
                runner.Process();
                EventsManager.getInstanceSingleton().FireEvent(GeneralEventOwner.BODY, EventType.ON_START, false);
                $done();
            };
        }

        protected before() : void {
            StaticPageContentManager.Clear(true);
        }

        protected tearDown() : void {
            this.initSendBox();
            document.documentElement.innerHTML = "";
            this.registerElement("Content");
        }
    }
}
