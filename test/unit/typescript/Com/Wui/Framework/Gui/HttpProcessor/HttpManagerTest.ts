/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.HttpProcessor {
    "use strict";
    import HttpRequestParser = Com.Wui.Framework.Commons.HttpProcessor.HttpRequestParser;

    export class HttpManagerTest extends UnitTestRunner {
        public testConstructor() : void {
            const parser : HttpRequestParser = new HttpRequestParser("example.com");
            const manager : HttpManager = new HttpManager(parser);
            assert.equal(manager.HttpPatternExists("/PersistenceManager"), true);
            assert.equal(manager.HttpPatternExists("/test/not/exists/Pattern"), false);
        }
    }
}
