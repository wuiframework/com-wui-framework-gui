/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.HttpProcessor.Resolvers {
    "use strict";
    import FormsObject = Com.Wui.Framework.Gui.Primitives.FormsObject;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import PersistenceType = Com.Wui.Framework.Gui.Enums.PersistenceType;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;

    class MockFormsObject extends FormsObject {
        private value : any;

        public Value($value? : any) : any {
            if (ObjectValidator.IsSet($value)) {
                this.value = $value;
                this.setChanged();
            }
            return this.value;
        }
    }

    export class PersistenceManagerTest extends UnitTestRunner {

        public testConstructor() : void {
            const persistencemanager : PersistenceManager = new PersistenceManager();
        }

        public testPersistence() : void {
            const persistencemanager : PersistenceManager = new PersistenceManager();
            const form : FormsObject = new MockFormsObject("id6");
            form.Value("Any Value");
            form.Error(true);
            persistencemanager.RequestArgs(new AsyncRequestEventArgs(this.getRequest().getUrl()));
            persistencemanager.Process();
            this.resetCounters();
            assert.equal(FormsObject.CollectValues().getItem(0).toString(), "" +
                "[Id] Com.Wui.Framework.Gui.Primitives.FormsObject<br/>" +
                "[Value] Any Value<br/>" +
                "[ErrorFlag] <i>Object type:</i> boolean. <i>Return value:</i> true<br/>" +
                "[getClassName] Com.Wui.Framework.Gui.Structures.FormValue<br/>" +
                "[getNamespaceName] Com.Wui.Framework.Gui.Structures<br/>" +
                "[getClassNameWithoutNamespace] FormValue<br/>" +
                "[IsMemberOf] <i>Object type:</i> boolean. <i>Return value:</i> false<br/>" +
                "[Implements] <i>Object type:</i> boolean. <i>Return value:</i> false<br/>" +
                "[getHash] <i>Object type:</i> number. <i>Return value:</i> -640289823<br/>" +
                "[excludeSerializationData] <i>Object type:</i> array. <i>Return values:</i><br/><i>Array object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;objectNamespace<br/>" +
                "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;objectClassName<br/></span><br/>" +
                "[excludeIdentityHashData] <i>Object type:</i> array. <i>Return values:</i><br/>" +
                "<i>Array object</i> <span onclick=\"" +
                "document.getElementById(\'ContentBlock_1\').style.display=" +
                "document.getElementById(\'ContentBlock_1\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span><br/>");
            localStorage.clear();
        }

        public testPersistenceSecond() : void {
            const persistencemanager : PersistenceManager = new PersistenceManager();
            const form : FormsObject = new MockFormsObject("id8");
            form.Value(null);
            const form1 : FormsObject = new MockFormsObject("id9");
            form.Value("Test Id");
            persistencemanager.RequestArgs(new AsyncRequestEventArgs(this.getRequest().getUrl()));
            persistencemanager.Process();
            assert.equal(FormsObject.CollectValues().getAll().length, 1);
            assert.equal(FormsObject.CollectValues().getAll().length, 1);
        }

        public testPersistenceError() : void {
            const persistencemanager : PersistenceManager = new PersistenceManager();
            const form : FormsObject = new MockFormsObject("id8");
            const form1 : FormsObject = new MockFormsObject("id9");
            const form2 : FormsObject = new MockFormsObject("id10");
            form2.Error(true);
            const form3 : FormsObject = new MockFormsObject("id111");
            const form4 : FormsObject = new MockFormsObject("id112");
            form4.Error(true);
            persistencemanager.RequestArgs(new AsyncRequestEventArgs(this.getRequest().getUrl()));
            persistencemanager.Process();
            assert.ok(form.Error(false), "true");
            assert.equal(PersistenceFactory.getPersistence(PersistenceType.FORM_VALUES).Exists("id8"), false);
        }

        public testPersistenceThird() : void {
            const persistencemanager : PersistenceManager = new PersistenceManager();
            persistencemanager.RequestArgs(new AsyncRequestEventArgs(this.getRequest().getUrl()));
            persistencemanager.Process();
            assert.equal(FormsObject.CollectValues().getAll().length, 0);
            assert.equal(FormsObject.CollectValues().getAll().length, 0);
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
