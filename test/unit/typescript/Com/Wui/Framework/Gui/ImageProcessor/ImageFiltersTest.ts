/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.ImageProcessor {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GrayscaleType = Com.Wui.Framework.Gui.Enums.GrayscaleType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class ImageFiltersTest extends UnitTestRunner {
        private imageIndex : number = 0;

        public __IgnoretestGrayscale() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                    const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                    let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                    pixels = ImageFilters.Grayscale(pixels);

                    sourceContext.clearRect(0, 0, $input.width, $input.height);
                    sourceContext.putImageData(pixels, 0, 0);
                    return $input;
                }, "ImageFiltersTestGrayscale.jsonp");

                this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                    const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                    let pixels2 : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                    pixels2 = ImageFilters.Grayscale(pixels2, GrayscaleType.AVERAGE);

                    sourceContext.clearRect(0, 0, $input.width, $input.height);
                    sourceContext.putImageData(pixels2, 0, 0);
                    return $input;
                }, "ImageFiltersTestGrayscaleSecond.jsonp");
                this.initSendBox();

                this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                    const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                    let pixels3 : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                    pixels3 = ImageFilters.Grayscale(pixels3, GrayscaleType.LUMINANCE);

                    sourceContext.clearRect(0, 0, $input.width, $input.height);
                    sourceContext.putImageData(pixels3, 0, 0);
                    return $input;
                }, "ImageFiltersTestLuminance.jsonp", $done);
                this.initSendBox();
            };
        }

        public __IgnoretestThreshold() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                pixels = ImageFilters.Threshold(pixels, 100);

                sourceContext.clearRect(0, 0, $input.width, $input.height);
                sourceContext.putImageData(pixels, 0, 0);
                return $input;
            }, "ImageFiltersTestThreshold.jsonp");

            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                pixels = ImageFilters.Threshold(pixels, 110);

                sourceContext.clearRect(0, 0, $input.width, $input.height);
                sourceContext.putImageData(pixels, 0, 0);
                return $input;
            }, "ImageFiltersTestThresholdSecond.jsonp");

            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                pixels = ImageFilters.Threshold(pixels, 200);

                sourceContext.clearRect(0, 0, $input.width, $input.height);
                sourceContext.putImageData(pixels, 0, 0);
                return $input;
            }, "ImageFiltersTestThresholdThird.jsonp");

            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                pixels = ImageFilters.Threshold(pixels, 200, 100);

                sourceContext.clearRect(0, 0, $input.width, $input.height);
                sourceContext.putImageData(pixels, 0, 0);
                return $input;
            }, "ImageFiltersTestThresholdFourth.jsonp");
            this.initSendBox();
        }

        public __IgnoretestInvert() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                pixels = ImageFilters.Invert(pixels);

                sourceContext.clearRect(0, 0, $input.width, $input.height);
                sourceContext.putImageData(pixels, 0, 0);
                return $input;
            }, "ImageFiltersTestInvert.jsonp");
            this.initSendBox();
        }

        public __IgnoretestBrightness() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                pixels = ImageFilters.Brightness(pixels, -200);

                sourceContext.clearRect(0, 0, $input.width, $input.height);
                sourceContext.putImageData(pixels, 0, 0);
                return $input;
            }, "ImageFiltersTestBrightness.jsonp");

            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                pixels = ImageFilters.Brightness(pixels, 100);

                sourceContext.clearRect(0, 0, $input.width, $input.height);
                sourceContext.putImageData(pixels, 0, 0);
                return $input;
            }, "ImageFiltersTestBrightnessSeconds.jsonp");
            this.initSendBox();
        }

        public __IgnoretestContrast() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                pixels = ImageFilters.Contrast(pixels, 50);

                sourceContext.clearRect(0, 0, $input.width, $input.height);
                sourceContext.putImageData(pixels, 0, 0);
                return $input;
            }, "ImageFiltersTestContrast.jsonp");

            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                pixels = ImageFilters.Contrast(pixels, -50);

                sourceContext.clearRect(0, 0, $input.width, $input.height);
                sourceContext.putImageData(pixels, 0, 0);
                return $input;
            }, "ImageFiltersTestContrastSecond.jsonp");
            this.initSendBox();
        }

        public __IgnoretestHorizontalFlip() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                pixels = ImageFilters.HorizontalFlip(pixels);

                sourceContext.clearRect(0, 0, $input.width, $input.height);
                sourceContext.putImageData(pixels, 0, 0);
                return $input;
            }, "ImageFiltersTestHorizontalFlip.jsonp");
            this.initSendBox();
        }

        public __IgnoretestVerticalFlip() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                pixels = ImageFilters.VerticalFlip(pixels);

                sourceContext.clearRect(0, 0, $input.width, $input.height);
                sourceContext.putImageData(pixels, 0, 0);
                return $input;
            }, "ImageFiltersTestVerticalFlip.jsonp");
            this.initSendBox();
        }

        public __IgnoretestConvolve() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                pixels = ImageFilters.Convolve(pixels, [200]);

                sourceContext.clearRect(0, 0, $input.width, $input.height);
                sourceContext.putImageData(pixels, 0, 0);
                return $input;
            }, "ImageFiltersTestConvolve.jsonp");
            this.initSendBox();
        }

        public __IgnoretestGaussianBlur() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                pixels = ImageFilters.GaussianBlur(pixels, 30);

                sourceContext.clearRect(0, 0, $input.width, $input.height);
                sourceContext.putImageData(pixels, 0, 0);
                return $input;
            }, "ImageFiltersTestGaussian.jsonp");
            this.initSendBox();
        }

        public __IgnoretestLaplace() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                pixels = ImageFilters.Laplace(pixels);

                sourceContext.clearRect(0, 0, $input.width, $input.height);
                sourceContext.putImageData(pixels, 0, 0);
                return $input;
            }, "ImageFiltersTestLaplace.jsonp");
            this.initSendBox();
        }

        public __IgnoretestSobel() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                pixels = ImageFilters.Sobel(pixels);

                sourceContext.clearRect(0, 0, $input.width, $input.height);
                sourceContext.putImageData(pixels, 0, 0);
                return $input;
            }, "ImageFiltersTestSobel.jsonp");
            this.initSendBox();
        }

        public __IgnoretestSharp() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
                let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

                pixels = ImageFilters.Sharp(pixels);

                sourceContext.clearRect(0, 0, $input.width, $input.height);
                sourceContext.putImageData(pixels, 0, 0);
                return $input;
            }, "ImageFiltersTestSharp.jsonp");
            this.initSendBox();
        }

        private assertImage($actual : ($input : HTMLCanvasElement) => HTMLCanvasElement, $expected : string, $done? : () => void) : void {
            try {
                const input : HTMLImageElement = document.createElement("img");
                const output : HTMLCanvasElement = document.createElement("canvas");

                input.onload = () : void => {
                    let data : HTMLCanvasElement = ImageTransform.ToCanvas(input);
                    data = $actual(data);
                    output.width = data.width;
                    output.height = data.height;
                    output.getContext("2d").drawImage(data, 0, 0);

                    assert.equal(ImageTransform.getStream(output, true),
                        this.getRegressionData("test/resource/data/Com/Wui/Framework/Gui/RuntimeTests/ImageProcessor/" + $expected));
                    if (ObjectValidator.IsSet($done)) {
                        $done();
                    }
                };

                input.src = "test/resource/graphics/Com/Wui/Framework/Gui/RuntimeTests/ImageProcessor/ChessBoard.png";

                window.document.body.appendChild(input);
                this.imageIndex++;
            } catch (ex) {
                LogIt.Error(this.getClassName(), ex);
            }
        }

        private getRegressionData($path : string) : any {
            let data : string = builder.getFileSystemHandler().Read($path).toString();
            data = data.substring(data.indexOf("({") + 1, data.lastIndexOf("});") + 1);

            let dataObj : any;
            try {
                /* tslint:disable: no-eval */
                dataObj = eval("(" + data + ")");
                /* tslint:enable */
            } catch (ex) {
                LogIt.Warning(ex.stack);
                dataObj = null;
            }
            return data;
        }
    }
}
