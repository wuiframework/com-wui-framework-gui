/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.ImageProcessor {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ImageCropDimensions = Com.Wui.Framework.Gui.Structures.ImageCropDimensions;
    import ImageCorners = Com.Wui.Framework.Gui.Structures.ImageCorners;
    import GuiElement = Com.Wui.Framework.Gui.Primitives.GuiElement;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import JsonpFileReader = Com.Wui.Framework.Commons.IOApi.Handlers.JsonpFileReader;

    export class ImageTransformTest extends UnitTestRunner {
        private imageIndex : number = 0;

        public __IgnoretestResize() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                return ImageTransform.Resize($input, 300, 300, false);
            }, "ImageTestResize.jsonp");

            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                return ImageTransform.Resize($input, 70, 70, false);
            }, "ImageTestResizeSecond.jsonp");

            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                return ImageTransform.Resize($input, 300, 150);
            }, "ImageTestResizeThird.jsonp");
            this.initSendBox();
        }

        public __IgnoretestQuality() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                return ImageTransform.Quality($input, 50);
            }, "ImageTestQuality.jsonp");

            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                return ImageTransform.Quality($input, 10);
            }, "ImageTestQualitySecond.jsonp");
            this.initSendBox();
        }

        public __IgnoretestZoom() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                return ImageTransform.Zoom($input, 10);
            }, "ImageTestZoom.jsonp");
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                return ImageTransform.Zoom($input, 100);
            }, "ImageTestZoom.jsonp");
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                return ImageTransform.Zoom($input, 250);
            }, "ImageTestZoomThird.jsonp");
            this.initSendBox();
        }

        public __IgnoretestRotate() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                return ImageTransform.Rotate($input, 180);
            }, "ImageTestRotate.jsonp");

            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                return ImageTransform.Rotate($input, 90);
            }, "ImageTestRotateSecond.jsonp");

            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                return ImageTransform.Rotate($input, 270);
            }, "ImageTestRotateThird.jsonp");
            this.initSendBox();
        }

        public __IgnoretestCrop() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const imagedimension : ImageCropDimensions = new ImageCropDimensions(0, 50, 30, 0);
                return ImageTransform.Crop($input, imagedimension);
            }, "ImageTestCrop.jsonp");

            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const imagedimension2 : ImageCropDimensions = new ImageCropDimensions(100, 0, 100, 0);
                return ImageTransform.Crop($input, imagedimension2);
            }, "ImageTestCropSecond.jsonp");
            this.initSendBox();
        }

        public __IgnoretestModifyCorners() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const corner : ImageCorners = new ImageCorners(20, 20, 20, 20);
                return ImageTransform.ModifyCorners($input, corner, 2);
            }, "ImageTestModifyCorners.jsonp");

            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const corner2 : ImageCorners = new ImageCorners(0, 50, 50, 0);
                return ImageTransform.ModifyCorners($input, corner2, 4);
            }, "ImageTestModifyCornersSecond.jsonp");

            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                const corner2 : ImageCorners = new ImageCorners(110, 0, 0, 110);
                return ImageTransform.ModifyCorners($input, corner2, 14);
            }, "ImageTestModifyCornersThird.jsonp");
            this.initSendBox();
        }

        public __IgnoretestAddWatermark() : void {
            this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
                return ImageTransform.AddWatermark($input, $input, true, 90);
            }, "ImageTestWatermark.jsonp");
            this.initSendBox();
        }

        private assertImage($actual : ($input : HTMLCanvasElement) => HTMLCanvasElement, $expected : string) : void {
            const input : HTMLImageElement = document.createElement("img");
            const output : HTMLCanvasElement = document.createElement("canvas");
            const imageId : string = "testImage_" + this.imageIndex;

            input.onload = () : void => {
                let data : HTMLCanvasElement = ImageTransform.ToCanvas(input);
                data = $actual(data);
                output.width = data.width;
                output.height = data.height;
                output.getContext("2d").drawImage(data, 0, 0);
                ElementManager.getElement(imageId).parentNode.appendChild(output);

                JsonpFileReader.Load("test/resource/data/Com/Wui/Framework/Gui/RuntimeTests/ImageProcessor/" + $expected,
                    ($data : string) : void => {
                        assert.equal(ImageTransform.getStream(output, true), $data);
                    });
            };

            input.src = "test/resource/graphics/Com/Wui/Framework/Gui/RuntimeTests/ImageProcessor/ChessBoard.png";

            const data : GuiElement = new GuiElement();
            data.Id(imageId);
            data.Add(<HTMLElement>input);
            Echo.Println(data.Draw(""));
            this.imageIndex++;
        }
    }
}
