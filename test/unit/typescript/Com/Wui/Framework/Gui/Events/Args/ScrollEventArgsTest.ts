/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import OrientationType = Com.Wui.Framework.Gui.Enums.OrientationType;

    export class ScrollEventArgsTest extends UnitTestRunner {
        public testPosition() : void {
            const scrollevent : ScrollEventArgs = new ScrollEventArgs();
            assert.equal(scrollevent.Position(101), 100);
            const scrollevent2 : ScrollEventArgs = new ScrollEventArgs();
            assert.equal(scrollevent2.Position(-3), 0);
            const scrollevent3 : ScrollEventArgs = new ScrollEventArgs();
            assert.equal(scrollevent3.Position(25), 25);
            const scrollevent4 : ScrollEventArgs = new ScrollEventArgs();
            assert.equal(scrollevent4.Position(0.5), 50);
            const scrollevent5 : ScrollEventArgs = new ScrollEventArgs();
            assert.equal(scrollevent5.Position(), null);
        }

        public testDirectionType() : void {
            const scrollevent : ScrollEventArgs = new ScrollEventArgs();
            assert.equal(scrollevent.DirectionType(DirectionType.DOWN), DirectionType.DOWN);
            const scrollevent2 : ScrollEventArgs = new ScrollEventArgs();
            assert.equal(scrollevent2.DirectionType(DirectionType.RIGHT), DirectionType.DOWN);
            const scrollevent3 : ScrollEventArgs = new ScrollEventArgs();
            assert.equal(scrollevent3.DirectionType(DirectionType.LEFT), DirectionType.UP);
        }

        public testOrientationType() : void {
            const scrollevent : ScrollEventArgs = new ScrollEventArgs();
            assert.equal(scrollevent.OrientationType(OrientationType.HORIZONTAL), OrientationType.HORIZONTAL);
            const scrollevent2 : ScrollEventArgs = new ScrollEventArgs();
            assert.equal(scrollevent2.OrientationType(OrientationType.VERTICAL), OrientationType.VERTICAL);
        }
    }
}
