/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";
    import BaseViewerArgs = Com.Wui.Framework.Gui.Primitives.BaseViewerArgs;
    import BaseViewer = Com.Wui.Framework.Gui.Primitives.BaseViewer;
    import BasePanel = Com.Wui.Framework.Gui.Primitives.BasePanel;

    export class ViewerManagerEventArgsTest extends UnitTestRunner {
        public testViewerClass() : void {
            const viewermanager : ViewerManagerEventArgs = new ViewerManagerEventArgs();
            assert.equal(viewermanager.ViewerClass(), null);
            const object : BaseViewer = new BaseViewer();
            assert.equal(viewermanager.ViewerClass(object), object);
        }

        public testViewerArgs() : void {
            const viewermanager : ViewerManagerEventArgs = new ViewerManagerEventArgs();
            const args : BaseViewerArgs = new BaseViewerArgs();
            assert.equal(viewermanager.ViewerArgs(args), args);
            const viewermanager2 : ViewerManagerEventArgs = new ViewerManagerEventArgs();
            assert.equal(viewermanager2.ViewerArgs(), null);
        }

        public testChildElement() : void {
            const viewermanager : ViewerManagerEventArgs = new ViewerManagerEventArgs();
            const gui : BasePanel = new BasePanel();
            assert.equal(viewermanager.ChildElement(gui), gui);
            const viewermanager2 : ViewerManagerEventArgs = new ViewerManagerEventArgs();
            assert.equal(viewermanager2.ChildElement(), null);
            this.initSendBox();
        }

        public testReloadCache() : void {
            const viewermanager : ViewerManagerEventArgs = new ViewerManagerEventArgs();
            assert.equal(viewermanager.ReloadCache(true), true);
        }

        public testTestMode() : void {
            const viewermanager : ViewerManagerEventArgs = new ViewerManagerEventArgs();
            assert.equal(viewermanager.TestMode(true), true);
        }

        public testIncludeChildren() : void {
            const viewermanager : ViewerManagerEventArgs = new ViewerManagerEventArgs();
            assert.equal(viewermanager.IncludeChildren(true), true);
        }

        public testAsyncCallback() : void {
            const viewermanager : ViewerManagerEventArgs = new ViewerManagerEventArgs();
            assert.equal(viewermanager.AsyncCallback(false), false);
        }

        public testResult() : void {
            const viewermanager : ViewerManagerEventArgs = new ViewerManagerEventArgs();
            const baseviewer : BaseViewer = new BaseViewer();
            assert.equal(viewermanager.Result(baseviewer), baseviewer);
            const viewermanager2 : ViewerManagerEventArgs = new ViewerManagerEventArgs();
            assert.equal(viewermanager2.Result(), null);
        }
    }
}
