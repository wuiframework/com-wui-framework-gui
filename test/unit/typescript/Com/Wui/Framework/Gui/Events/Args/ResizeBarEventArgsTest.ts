/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;

    export class ResizeBarEventArgsTest extends UnitTestRunner {
        public testConstructor() : void {
            const resize : ResizeBarEventArgs = new ResizeBarEventArgs();
            assert.equal(resize.DistanceX(25), 25);
            assert.equal(resize.DistanceY(65), 65);
        }

        public testNativeEventArgs() : void {
            const mouseEventInit : any = {screenX: 20, screenY: 40, clientX: 50, clientY: 60, button: 2, buttons: 4};
            const mouseEvent : any = {typeArgs: "onclick", eventInitDict: mouseEventInit};
            const resize : ResizeBarEventArgs = new ResizeBarEventArgs();
            assert.equal(resize.NativeEventArgs(mouseEvent), mouseEvent);
        }

        public testResizeableType() : void {
            const resize : ResizeBarEventArgs = new ResizeBarEventArgs();
            assert.equal(resize.ResizeableType("XY"), ResizeableType.HORIZONTAL_AND_VERTICAL);
            const resize2 : ResizeBarEventArgs = new ResizeBarEventArgs();
            assert.equal(resize2.ResizeableType(), undefined);
        }
    }
}
