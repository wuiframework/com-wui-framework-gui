/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events {
    "use strict";
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IEventsHandler = Com.Wui.Framework.Gui.Interfaces.IEventsHandler;
    import IMouseEventsHandler = Com.Wui.Framework.Gui.Interfaces.Events.IMouseEventsHandler;
    import DialogEventType = Com.Wui.Framework.Gui.Enums.Events.DialogEventType;
    import BasePanelHolderEventType = Com.Wui.Framework.Gui.Enums.Events.BasePanelHolderEventType;
    import DirectoryBrowserEventType = Com.Wui.Framework.Gui.Enums.Events.DirectoryBrowserEventType;
    import ScrollBarEventType = Com.Wui.Framework.Gui.Enums.Events.ScrollBarEventType;
    import BasePanel = Com.Wui.Framework.Gui.Primitives.BasePanel;

    export class ElementEventsManagerTest extends UnitTestRunner {

        public testConstructor() : void {
            const manager : ElementEventsManager = new ElementEventsManager();
            const manager2 : ElementEventsManager = new ElementEventsManager("Id");
        }

        public testgetOwner() : void {
            const manager : ElementEventsManager = new ElementEventsManager("owner", "subscriber");
            assert.equal(manager.getOwner(), "owner");
        }

        public testsetEvent() : void {
            const manager : ElementEventsManager = new ElementEventsManager();
            manager.setEvent(EventType.ON_CLICK, () : void => {
                LogIt.Debug("this is event test");
            });
            const manager2 : ElementEventsManager = new ElementEventsManager();
            const handler : IEventsHandler = () : void => {
                LogIt.Debug("this is event test");
            };
            assert.throws(() : void => {
                manager2.setEvent(null, handler);
            }, /Event type can not be null./);
        }

        public testExists() : void {
            const manager : ElementEventsManager = new ElementEventsManager();
            manager.setEvent(EventType.ON_CLICK, () : void => {
                LogIt.Debug("this is event test");
            });
            assert.equal(manager.Exists(EventType.ON_CLICK), true);
            assert.equal(manager.Exists(EventType.ON_BLUR), false);
        }

        public testgetAll() : void {
            const handler : IEventsHandler = () : void => {
                LogIt.Debug("this is event test");
            };
            const manager : ElementEventsManager = new ElementEventsManager();
            manager.setEvent(EventType.ON_CLICK, handler);
            manager.setEvent(EventType.ON_CLICK, handler);
            manager.setEvent(EventType.ON_CLICK, () : void => {
                LogIt.Debug("this is event test2");
            });
            manager.setEvent(EventType.ON_BLUR, () : void => {
                LogIt.Debug("this is event test");
            });
            assert.equal(manager.getAll().Length(), 2);
            assert.equal(manager.getAll().getItem(EventType.ON_CLICK).Length(), 2);
            assert.equal(manager.getAll().getItem(EventType.ON_BLUR).Length(), 1);
            assert.equal(manager.getAll().KeyExists(EventType.ON_FOCUS), false);
        }

        public testRemoveHandler() : void {
            const handler : IEventsHandler = () : void => {
                LogIt.Debug("this is event test");
            };
            const manager : ElementEventsManager = new ElementEventsManager();
            manager.setEvent(EventType.ON_CLICK, handler);
            manager.setEvent(EventType.ON_CLICK, () : void => {
                LogIt.Debug("this is event test2");
            });
            manager.setEvent(EventType.ON_BLUR, () : void => {
                LogIt.Debug("this is event test for blur");
            });
            assert.equal(manager.getAll().Length(), 2);
            assert.equal(manager.getAll().getItem(EventType.ON_CLICK).Length(), 2);
            manager.RemoveHandler(EventType.ON_CLICK, handler);
            assert.equal(manager.getAll().getItem(EventType.ON_CLICK).Length(), 1);
            manager.RemoveHandler(EventType.ON_BLUR, handler);
            assert.equal(manager.getAll().getItem(EventType.ON_BLUR).Length(), 1);
            manager.RemoveHandler(EventType.ON_FOCUS, handler);
        }

        public testClear() : void {
            const manager : ElementEventsManager = new ElementEventsManager();
            manager.setEvent(EventType.ON_CLICK, () : void => {
                LogIt.Debug("this is event test");
            });
            manager.setEvent(EventType.ON_CLICK, () : void => {
                LogIt.Debug("this is event test2");
            });
            manager.setEvent(EventType.ON_BLUR, () : void => {
                LogIt.Debug("this is event test");
            });
            assert.equal(manager.getAll().Length(), 2);
            assert.equal(manager.getAll().getItem(EventType.ON_CLICK).Length(), 2);
            manager.Clear(EventType.ON_CLICK);
            assert.equal(manager.getAll().KeyExists(EventType.ON_CLICK), true);
            assert.equal(manager.getAll().getItem(EventType.ON_CLICK).IsEmpty(), true);
            manager.Clear();
            assert.equal(manager.getAll().getItem(EventType.ON_CLICK).IsEmpty(), true);
            assert.equal(manager.getAll().getItem(EventType.ON_BLUR).IsEmpty(), true);

            const manager2 : ElementEventsManager = new ElementEventsManager("id456");
            manager2.setEvent(EventType.ON_BLUR, () : void => {
                LogIt.Debug("this is event test");
            });
            manager2.Subscribe("id456");
            manager2.setEvent(EventType.ON_CLICK, () : void => {
                LogIt.Debug("this is event test");
            });
            manager2.Subscribe("id456");
            manager2.Clear(EventType.ON_BLUR);
            assert.equal(manager.getAll().getItem(EventType.ON_BLUR).IsEmpty(), true);
            manager2.Clear();
        }

        public testSubscribe() : void {
            const manager : ElementEventsManager = new ElementEventsManager();
            manager.setEvent(EventType.ON_CLICK, () : void => {
                LogIt.Debug("this is event test");
            });
            manager.Subscribe();
            assert.equal(manager.Exists(EventType.ON_CLICK), true);

            const manager2 : ElementEventsManager = new ElementEventsManager("id456", "subscriber");
            manager2.setEvent(EventType.ON_BLUR, () : void => {
                LogIt.Debug("this is event test");
            });
            manager2.Subscribe("id456", true);
            assert.equal(manager.Exists(EventType.ON_BLUR), false);

            const manager3 : ElementEventsManager = new ElementEventsManager("id456");
            manager3.setEvent(EventType.ON_BLUR, () : void => {
                LogIt.Debug("this is event test");
            });
            manager3.Subscribe("id456");

            const manager4 : ElementEventsManager = new ElementEventsManager("subscriber");
            manager4.setEvent(EventType.ON_BLUR, () : void => {
                LogIt.Debug("this is event test");
            });
            manager4.Subscribe(true);
        }

        public testToString() : void {
            const handler : IEventsHandler = () : void => {
                LogIt.Debug("this is event test");
            };
            const manager : ElementEventsManager = new ElementEventsManager();
            manager.setEvent(EventType.ON_CLICK, handler);
            manager.setEvent(EventType.ON_CLICK, handler);
            manager.setEvent(EventType.ON_CLICK, () : void => {
                LogIt.Debug("this is event test2");
            });
            manager.setEvent(EventType.ON_BLUR, handler);

            LogIt.setLevel(Com.Wui.Framework.Commons.Enums.LogLevel.ALL);
            LogIt.Debug(manager);
            assert.equal(manager.ToString(""), "<b>Registered events list:</b><br/>&nbsp;&nbsp;&nbsp;" +
                "[\"onclick\"] hooked handlers count: 2<br/>&nbsp;&nbsp;&nbsp;" +
                "[\"onblur\"] hooked handlers count: 1<br/>");
        }

        public testtoString() : void {
            const manager : ElementEventsManager = new ElementEventsManager();
            assert.equal(manager.toString(), "<b>Registered events list:</b><br/>");
        }

        public testsetOnStart() : void {
            const manager2 : ElementEventsManager = new ElementEventsManager("id616", "subscriber");
            manager2.setOnStart(() : void => {
                LogIt.Debug("this is event test");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id616", EventType.ON_START), true);
            EventsManager.getInstanceSingleton().Clear("id616", EventType.ON_START);
        }

        public testsetBeforeLoad() : void {
            const manager2 : ElementEventsManager = new ElementEventsManager("id616", "subscriber");
            manager2.setBeforeLoad(() : void => {
                LogIt.Debug("this is event test ");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id616", EventType.BEFORE_LOAD), true);
            EventsManager.getInstanceSingleton().Clear("id616", EventType.BEFORE_LOAD);
        }

        public testsetOnLoad() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id717", "subscriber");
            manager.setOnLoad(() : void => {
                LogIt.Debug("this is event test onload");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id717", EventType.ON_LOAD), true);
            EventsManager.getInstanceSingleton().Clear("id717", EventType.ON_LOAD);
        }

        public testsetOnComplete() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnComplete(() : void => {
                LogIt.Debug("this is event test oncomplete");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", EventType.ON_COMPLETE), true);
            EventsManager.getInstanceSingleton().Clear("id919", EventType.ON_COMPLETE);
        }

        public testsetOnShow() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnShow(() : void => {
                LogIt.Debug("this is event test onshow");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", EventType.ON_SHOW), true);
            EventsManager.getInstanceSingleton().Clear("id919", EventType.ON_SHOW);
        }

        public testsetOnHide() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnHide(() : void => {
                LogIt.Debug("this is event test onhide");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", EventType.ON_HIDE), true);
            EventsManager.getInstanceSingleton().Clear("id919", EventType.ON_HIDE);
        }

        public testsetOnFocus() : void {
            const gui : BasePanel = new BasePanel("12345");
            const manager2 : ElementEventsManager = new ElementEventsManager(gui, "subscriber");
            manager2.setOnFocus(() : void => {
                LogIt.Debug("this is event test onfocus");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists(gui, EventType.ON_FOCUS), false);
            const manager : ElementEventsManager = new ElementEventsManager("id333");
            const handler : IEventsHandler = () : void => {
                LogIt.Debug("this is event test onfocus");
            };
            manager.setOnFocus(handler);
            assert.equal(manager.Exists(EventType.ON_FOCUS), true);
            EventsManager.getInstanceSingleton().Clear("id333", EventType.ON_FOCUS);
            this.initSendBox();
        }

        public testsetOnBlur() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id515", "subscriber");
            manager.setOnBlur(() : void => {
                LogIt.Debug("this is event test onblur");
            });
            assert.equal(manager.Exists(EventType.ON_BLUR), true);
        }

        public testsetOnMouseOver() : void {
            const handler : IMouseEventsHandler = () : void => {
                LogIt.Debug("this is event test onmouseover");
            };
            const manager : ElementEventsManager = new ElementEventsManager("id515", "subscriber");
            manager.setOnMouseOver(handler);
            assert.equal(manager.Exists(EventType.ON_MOUSE_OVER), true);
        }

        public testsetOnMouseOut() : void {
            const handler : IMouseEventsHandler = () : void => {
                LogIt.Debug("this is event test onmouseout");
            };
            const manager : ElementEventsManager = new ElementEventsManager("id515", "subscriber");
            manager.setOnMouseOut(handler);
            assert.equal(manager.Exists(EventType.ON_MOUSE_OUT), true);
        }

        public testsetOnMouseMove() : void {
            const handler : IMouseEventsHandler = () : void => {
                LogIt.Debug("this is event test onmousemove");
            };
            const manager : ElementEventsManager = new ElementEventsManager("id515", "subscriber");
            manager.setOnMouseMove(handler);
            assert.equal(manager.Exists(EventType.ON_MOUSE_MOVE), true);
        }

        public testsetOnMouseDown() : void {
            const handler : IMouseEventsHandler = () : void => {
                LogIt.Debug("this is event test onmousedown");
            };
            const manager : ElementEventsManager = new ElementEventsManager("id515", "subscriber");
            manager.setOnMouseDown(handler);
            assert.equal(manager.Exists(EventType.ON_MOUSE_DOWN), true);
        }

        public testsetOnMouseUp() : void {
            const handler : IMouseEventsHandler = () : void => {
                LogIt.Debug("this is event test onmouseup");
            };
            const manager : ElementEventsManager = new ElementEventsManager("id515", "subscriber");
            manager.setOnMouseUp(handler);
            assert.equal(manager.Exists(EventType.ON_MOUSE_UP), true);
        }

        public testsetOnClick() : void {
            const handler : IMouseEventsHandler = () : void => {
                LogIt.Debug("this is event test onclick");
            };
            const manager : ElementEventsManager = new ElementEventsManager("id515", "subscriber");
            manager.setOnClick(handler);
            assert.equal(manager.Exists(EventType.ON_CLICK), true);
        }

        public testsetOnDoubleClick() : void {
            const handler : IMouseEventsHandler = () : void => {
                LogIt.Debug("this is event test ondoubleclick");
            };
            const manager : ElementEventsManager = new ElementEventsManager("id515", "subscriber");
            manager.setOnDoubleClick(handler);
            assert.equal(manager.Exists(EventType.ON_DOUBLE_CLICK), true);
        }

        public testsetOnChange() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnChange(() : void => {
                LogIt.Debug("this is event test onchange");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", EventType.ON_CHANGE), true);
            EventsManager.getInstanceSingleton().Clear("id919", EventType.ON_CHANGE);
        }

        public testsetOnBeforeResize() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setBeforeResize(() : void => {
                LogIt.Debug("this is event test onbeforeresize");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", EventType.BEFORE_RESIZE), true);
            EventsManager.getInstanceSingleton().Clear("id919", EventType.BEFORE_RESIZE);
        }

        public testsetOnResize() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnResize(() : void => {
                LogIt.Debug("this is event test onresize");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", EventType.ON_RESIZE), true);
            EventsManager.getInstanceSingleton().Clear("id919", EventType.ON_RESIZE);
        }

        public testsetOnResizeStart() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnResizeStart(() : void => {
                LogIt.Debug("this is event test onresizestart");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", EventType.ON_RESIZE_START), true);
            EventsManager.getInstanceSingleton().Clear("id919", EventType.ON_RESIZE_START);
        }

        public testsetOnResizeChange() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnResizeChange(() : void => {
                LogIt.Debug("this is event test onresizechange");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", EventType.ON_RESIZE_CHANGE), true);
            EventsManager.getInstanceSingleton().Clear("id919", EventType.ON_RESIZE_CHANGE);
        }

        public testsetOnResizeComplete() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnResizeComplete(() : void => {
                LogIt.Debug("this is event test onresizecomplete");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", EventType.ON_RESIZE_COMPLETE), true);
            EventsManager.getInstanceSingleton().Clear("id919", EventType.ON_RESIZE_COMPLETE);
        }

        public testsetOnDragStart() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnDragStart(() : void => {
                LogIt.Debug("this is event test ondragstart");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", EventType.ON_DRAG_START), true);
            EventsManager.getInstanceSingleton().Clear("id919", EventType.ON_DRAG_START);
        }

        public testsetOnDrag() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnDrag(() : void => {
                LogIt.Debug("this is event test ondrag");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", EventType.ON_DRAG), true);
            EventsManager.getInstanceSingleton().Clear("id919", EventType.ON_DRAG);
        }

        public testsetOnDragChange() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnDragChange(() : void => {
                LogIt.Debug("this is event test ondragchange");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", EventType.ON_DRAG_CHANGE), true);
            EventsManager.getInstanceSingleton().Clear("id919", EventType.ON_DRAG_CHANGE);
        }

        public testsetOnDragComplete() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnDragComplete(() : void => {
                LogIt.Debug("this is event test ondragcomplete");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", EventType.ON_DRAG_COMPLETE), true);
            EventsManager.getInstanceSingleton().Clear("id919", EventType.ON_DRAG_COMPLETE);
        }

        public testsetOnScroll() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnScroll(() : void => {
                LogIt.Debug("this is event test onscroll");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", EventType.ON_SCROLL), true);
            EventsManager.getInstanceSingleton().Clear("id919", EventType.ON_SCROLL);
        }

        public testsetOnOpen() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnOpen(() : void => {
                LogIt.Debug("this is event test onopen");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", DialogEventType.ON_OPEN), true);
            EventsManager.getInstanceSingleton().Clear("id919", DialogEventType.ON_OPEN);
        }

        public testsetOnClose() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnClose(() : void => {
                LogIt.Debug("this is event test onclose");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", DialogEventType.ON_CLOSE), true);
            EventsManager.getInstanceSingleton().Clear("id919", DialogEventType.ON_CLOSE);
        }

        public testsetBeforeOpen() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setBeforeOpen(() : void => {
                LogIt.Debug("this is event test beforeopen");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", BasePanelHolderEventType.BEFORE_OPEN), true);
            EventsManager.getInstanceSingleton().Clear("id919", BasePanelHolderEventType.BEFORE_OPEN);
        }

        public testsetBeforeClose() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setBeforeClose(() : void => {
                LogIt.Debug("this is event test beforeclose");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", BasePanelHolderEventType.BEFORE_CLOSE), true);
            EventsManager.getInstanceSingleton().Clear("id919", BasePanelHolderEventType.BEFORE_CLOSE);
        }

        public testsetOnPathRequest() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnPathRequest(() : void => {
                LogIt.Debug("this is event test onpathrequest");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", DirectoryBrowserEventType.ON_PATH_REQUEST), true);
            EventsManager.getInstanceSingleton().Clear("id919", DirectoryBrowserEventType.ON_PATH_REQUEST);
        }

        public testsetOnDirectoryRequest() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnDirectoryRequest(() : void => {
                LogIt.Debug("this is event test ondirectoryrequest");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", DirectoryBrowserEventType.ON_DIRECTORY_REQUEST), true);
            EventsManager.getInstanceSingleton().Clear("id919", DirectoryBrowserEventType.ON_DIRECTORY_REQUEST);
        }

        public testsetOnCreateDirectoryRequest() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnCreateDirectoryRequest(() : void => {
                LogIt.Debug("this is event test oncreatedirectoryrequest");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", DirectoryBrowserEventType.ON_CREATE_DIRECTORY_REQUEST), true);
            EventsManager.getInstanceSingleton().Clear("id919", DirectoryBrowserEventType.ON_CREATE_DIRECTORY_REQUEST);
        }

        public testsetOnRenameRequest() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnRenameRequest(() : void => {
                LogIt.Debug("this is event test onrenamerequest");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", DirectoryBrowserEventType.ON_RENAME_REQUEST), true);
            EventsManager.getInstanceSingleton().Clear("id919", DirectoryBrowserEventType.ON_RENAME_REQUEST);
        }

        public testsetOnDeleteRequest() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnDeleteRequest(() : void => {
                LogIt.Debug("this is event test ondeleterequest");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", DirectoryBrowserEventType.ON_DELETE_REQUEST), true);
            EventsManager.getInstanceSingleton().Clear("id919", DirectoryBrowserEventType.ON_DELETE_REQUEST);
        }

        public testsetOnArrow() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnArrow(() : void => {
                LogIt.Debug("this is event test onarrow");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", ScrollBarEventType.ON_ARROW), true);
            EventsManager.getInstanceSingleton().Clear("id919", ScrollBarEventType.ON_ARROW);
        }

        public testsetOnButton() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnButton(() : void => {
                LogIt.Debug("this is event test onbutton");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", ScrollBarEventType.ON_BUTTON), true);
            EventsManager.getInstanceSingleton().Clear("id919", ScrollBarEventType.ON_BUTTON);
        }

        public testsetOnTracker() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id919", "subscriber");
            manager.setOnTracker(() : void => {
                LogIt.Debug("this is event test ontracker");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id919", ScrollBarEventType.ON_TRACKER), true);
            EventsManager.getInstanceSingleton().Clear("id919", ScrollBarEventType.ON_TRACKER);
        }

        public testsetOnSelect() : void {
            const manager : ElementEventsManager = new ElementEventsManager("id200", "subscriber");
            manager.setOnSelect(() : void => {
                LogIt.Debug("this is event test onselect");
            });
            assert.equal(EventsManager.getInstanceSingleton().Exists("id200", BasePanelHolderEventType.BEFORE_OPEN), false);
            EventsManager.getInstanceSingleton().Clear("id200", BasePanelHolderEventType.ON_SELECT);
        }
    }
}
