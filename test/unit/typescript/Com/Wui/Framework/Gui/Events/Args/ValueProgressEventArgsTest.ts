/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import ProgressType = Com.Wui.Framework.Gui.Enums.ProgressType;

    export class ValueProgressEventArgsTest extends UnitTestRunner {
        public testConstructor() : void {
            const progressevent : ValueProgressEventArgs = new ValueProgressEventArgs("id203");
            assert.equal(progressevent.OwnerId(), "id203");
        }

        public testDirectionType() : void {
            const progressevent : ValueProgressEventArgs = new ValueProgressEventArgs("id506");
            assert.equal(progressevent.DirectionType(DirectionType.DOWN), DirectionType.DOWN);
            const progressevent2 : ValueProgressEventArgs = new ValueProgressEventArgs("id506");
            assert.equal(progressevent2.DirectionType(DirectionType.UP), DirectionType.UP);
            const progressevent3 : ValueProgressEventArgs = new ValueProgressEventArgs("id506");
            assert.equal(progressevent3.DirectionType(), "Up");
        }

        public testStartEventType() : void {
            const progressevent : ValueProgressEventArgs = new ValueProgressEventArgs("id506");
            assert.equal(progressevent.StartEventType("modelInstance"), "modelInstance");
            const progressevent2 : ValueProgressEventArgs = new ValueProgressEventArgs("id506");
            assert.equal(progressevent2.StartEventType(null), "id506_onstart");
        }

        public testChangeEventType() : void {
            const progressevent : ValueProgressEventArgs = new ValueProgressEventArgs("id506");
            assert.equal(progressevent.ChangeEventType("changeEvent"), "changeEvent");
            const progressevent2 : ValueProgressEventArgs = new ValueProgressEventArgs("id506");
            assert.equal(progressevent2.ChangeEventType(null), "id506_onchange");
        }

        public testCompleteEventType() : void {
            const progressevent : ValueProgressEventArgs = new ValueProgressEventArgs("id506");
            assert.equal(progressevent.CompleteEventType("completeEvent"), "completeEvent");
            const progressevent2 : ValueProgressEventArgs = new ValueProgressEventArgs("id506");
            assert.equal(progressevent2.CompleteEventType(null), "id506_oncomplete");
        }

        public testStep() : void {
            const progressevent : ValueProgressEventArgs = new ValueProgressEventArgs("id506");
            assert.equal(progressevent.Step(7), 7);
            const progressevent2 : ValueProgressEventArgs = new ValueProgressEventArgs("id506");
            assert.equal(progressevent2.Step(0), 0);
        }

        public testProgressType() : void {
            const progressevent : ValueProgressEventArgs = new ValueProgressEventArgs("id506");
            assert.equal(progressevent.ProgressType(ProgressType.FAST_END), "valueProgressFastEnd");
            const progressevent2 : ValueProgressEventArgs = new ValueProgressEventArgs("id506");
            assert.equal(progressevent2.ProgressType(null), "valueProgressLinear");
        }
    }
}
