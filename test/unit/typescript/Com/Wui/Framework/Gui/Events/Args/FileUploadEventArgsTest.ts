/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";

    export class FileUploadEventArgsTest extends UnitTestRunner {
        public testId() : void {
            const fileupload : FileUploadEventArgs = new FileUploadEventArgs();
            assert.equal(fileupload.Id("id001"), "id001");
        }

        public testIndex() : void {
            const fileupload : FileUploadEventArgs = new FileUploadEventArgs();
            assert.equal(fileupload.Index(0), 0);
            assert.equal(fileupload.Index(5), 5);
        }

        public testFileName() : void {
            const fileupload : FileUploadEventArgs = new FileUploadEventArgs();
            assert.equal(fileupload.Name("file.txt"), "file.txt");
            const fileupload2 : FileUploadEventArgs = new FileUploadEventArgs();
            assert.equal(fileupload2.Name(""), "");
            const fileupload3 : FileUploadEventArgs = new FileUploadEventArgs();
            assert.equal(fileupload3.Name(null), "");
        }

        public testFile() : void {
            const file : any = {lastModifiedDate: "07052016", name: "File.txt"};
            const domError : any = {name: "testname", toString: "testname"};
            const fileReader : any = {error: domError};
            const fileHandler : any = {source: file, reader: fileReader, data: "testdata"};
            const fileupload : FileUploadEventArgs = new FileUploadEventArgs();
            assert.equal(fileupload.File(fileHandler), fileHandler);
            const fileupload2 : FileUploadEventArgs = new FileUploadEventArgs();
            assert.equal(fileupload2.File(), null);
        }
    }
}
