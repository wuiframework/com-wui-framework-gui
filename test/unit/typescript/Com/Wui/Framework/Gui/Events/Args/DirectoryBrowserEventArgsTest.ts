/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";
    import BasePanel = Com.Wui.Framework.Gui.Primitives.BasePanel;

    export class DirectoryBrowserEventArgsTest extends UnitTestRunner {
        public testOwner() : void {
            const directory : DirectoryBrowserEventArgs = new DirectoryBrowserEventArgs();
            const owner : any = new BasePanel();
            directory.Owner(owner);
            assert.equal(directory.Owner(), owner);
            this.initSendBox();
        }

        public testValue() : void {
            const directory : DirectoryBrowserEventArgs = new DirectoryBrowserEventArgs();
            assert.equal(directory.Value("string"), "string");
        }
    }
}
