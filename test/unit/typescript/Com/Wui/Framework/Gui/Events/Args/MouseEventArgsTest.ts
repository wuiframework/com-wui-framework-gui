/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";

    export class MouseEventArgsTest extends UnitTestRunner {
        public testConstructor() : void {
            const mouse : MouseEventArgs = new MouseEventArgs();
            assert.equal(mouse.getClassName(), "Com.Wui.Framework.Gui.Events.Args.MouseEventArgs");
        }
    }
}
