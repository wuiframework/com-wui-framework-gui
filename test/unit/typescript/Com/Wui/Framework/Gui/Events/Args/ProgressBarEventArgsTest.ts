/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;

    export class ProgressBarEventArgsTest extends UnitTestRunner {
        public testDirectionType() : void {
            const barevent : ProgressBarEventArgs = new ProgressBarEventArgs();
            assert.equal(barevent.DirectionType(DirectionType.LEFT), "Left");
            const barevent2 : ProgressBarEventArgs = new ProgressBarEventArgs();
            assert.equal(barevent2.DirectionType(), "Up");
        }

        public testPercentage() : void {
            const barevent : ProgressBarEventArgs = new ProgressBarEventArgs();
            assert.equal(barevent.Percentage(65), 65);
        }
    }
}
