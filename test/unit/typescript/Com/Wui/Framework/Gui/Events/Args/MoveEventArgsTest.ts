/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";

    export class MoveEventArgsTest extends UnitTestRunner {
        public testConstructor() : void {
            const mouseEventInit : any = {screenX: 20, screenY: 40, clientX: 50, clientY: 60, button: 2, buttons: 4};
            const mouseEvent : any = {typeArgs: "onclick", eventInitDict: mouseEventInit};
            const moveevent : MoveEventArgs = new MoveEventArgs(mouseEvent);
            assert.equal(moveevent.NativeEventArgs(mouseEvent), mouseEvent);
        }

        public testgetPositionX() : void {
            const moveevent : MoveEventArgs = new MoveEventArgs();
            assert.equal(moveevent.getPositionX(), 0);
        }

        public testgetPositionY() : void {
            const moveevent : MoveEventArgs = new MoveEventArgs();
            assert.equal(moveevent.getPositionY(), 0);
        }

        public testgetDistanceY() : void {
            const moveevent : MoveEventArgs = new MoveEventArgs();
            assert.equal(moveevent.getDistanceY(), 0);
        }

        public testgetDistanceX() : void {
            const moveevent : MoveEventArgs = new MoveEventArgs();
            assert.equal(moveevent.getDistanceX(), 0);
        }
    }
}
