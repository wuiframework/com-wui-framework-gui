/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";

    export class ResizeEventArgsTest extends UnitTestRunner {
        public testWidth() : void {
            const resizeevent : ResizeEventArgs = new ResizeEventArgs();
            assert.equal(resizeevent.Width(50), 50);
            const resizeevent2 : ResizeEventArgs = new ResizeEventArgs();
            assert.equal(resizeevent2.Width(), null);
        }

        public testHeight() : void {
            const resizeevent : ResizeEventArgs = new ResizeEventArgs();
            assert.equal(resizeevent.Height(100), 100);
            const resizeevent2 : ResizeEventArgs = new ResizeEventArgs();
            assert.equal(resizeevent2.Height(), null);
        }

        public testAvailableWidth() : void {
            const resizeevent : ResizeEventArgs = new ResizeEventArgs();
            assert.equal(resizeevent.AvailableWidth(120), 120);
            const resizeevent2 : ResizeEventArgs = new ResizeEventArgs();
            assert.equal(resizeevent2.AvailableWidth(), null);
        }

        public testAvailableHeight() : void {
            const resizeevent : ResizeEventArgs = new ResizeEventArgs();
            assert.equal(resizeevent.AvailableHeight(120), 120);
            const resizeevent2 : ResizeEventArgs = new ResizeEventArgs();
            assert.equal(resizeevent2.AvailableHeight(), null);
        }

        public testScrollBarWidth() : void {
            const resizeevent : ResizeEventArgs = new ResizeEventArgs();
            assert.equal(resizeevent.ScrollBarWidth(120), 120);
            const resizeevent2 : ResizeEventArgs = new ResizeEventArgs();
            assert.equal(resizeevent2.ScrollBarWidth(), null);
        }

        public testScrollBarHeight() : void {
            const resizeevent : ResizeEventArgs = new ResizeEventArgs();
            assert.equal(resizeevent.ScrollBarHeight(120), 120);
            const resizeevent2 : ResizeEventArgs = new ResizeEventArgs();
            assert.equal(resizeevent2.ScrollBarHeight(), null);
        }
    }
}
