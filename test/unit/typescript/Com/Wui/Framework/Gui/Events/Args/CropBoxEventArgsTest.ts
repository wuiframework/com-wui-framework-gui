/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";

    export class CropBoxEventArgsTest extends UnitTestRunner {
        public testOffsetLeft() : void {
            const cropbox : CropBoxEventArgs = new CropBoxEventArgs();
            assert.equal(cropbox.OffsetLeft(), null);
            assert.equal(cropbox.OffsetLeft(100), 100);
        }

        public testOffsetTop() : void {
            const cropbox : CropBoxEventArgs = new CropBoxEventArgs();
            assert.equal(cropbox.OffsetTop(), null);
            assert.equal(cropbox.OffsetTop(100), 100);
        }
    }
}
