/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events.Args {
    "use strict";

    export class PanelResizeEventArgsTest extends UnitTestRunner {
        public testScrollable() : void {
            const panelresize : PanelResizeEventArgs = new PanelResizeEventArgs();
            assert.equal(panelresize.Scrollable(true), true);
        }
    }
}
