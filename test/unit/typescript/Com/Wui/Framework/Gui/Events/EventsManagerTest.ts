/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Events {
    "use strict";
    import BaseGuiObject = Com.Wui.Framework.Gui.Primitives.BaseGuiObject;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import IEventsHandler = Com.Wui.Framework.Gui.Interfaces.IEventsHandler;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import GuiCommonsArgType = Com.Wui.Framework.Gui.Enums.GuiCommonsArgType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import GeneralEventOwner = Com.Wui.Framework.Gui.Enums.Events.GeneralEventOwner;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import MouseEventArgs = Com.Wui.Framework.Gui.Events.Args.MouseEventArgs;
    import TextSelectionManager = Com.Wui.Framework.Gui.Utils.TextSelectionManager;
    import MoveEventArgs = Com.Wui.Framework.Gui.Events.Args.MoveEventArgs;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import IEventsManager = Com.Wui.Framework.Gui.Interfaces.IEventsManager;

    class MockBaseGuiObject extends BaseGuiObject {
    }

    export class EventsManagerTest extends UnitTestRunner {

        public testsetEventArgs() : void {
            const manager : EventsManager = new EventsManager();
            const baseobject : BaseGuiObject = new MockBaseGuiObject("id7083");
            const baseobject2 : BaseGuiObject = new MockBaseGuiObject("id9053");

            baseobject2.setArg(<IGuiCommonsArg>{
                name : GuiCommonsArgType.TEXT,
                type : "testItem",
                value: "testValue"
            }, true);
            const args2 : EventArgs = new EventArgs();
            const args : EventArgs = new EventArgs();
            args2.Owner(null);
            manager.setEventArgs(baseobject, "onclick", args);
            manager.setEventArgs("id9053", "string", args2);
            manager.setEventArgs(baseobject2, "typeX", args);

            const manager2 : EventsManager = new EventsManager();
            const args3 : EventArgs = new EventArgs();
            args3.Owner(null);
            manager2.setEventArgs("owner", "onclick", args3);
            manager2.FireEvent("owner", "onclick", args3);
        }

        public testsetEvent() : void {
            const manager : EventsManager = new EventsManager();
            const baseobject : BaseGuiObject = new MockBaseGuiObject("id888");
            const baseobject2 : BaseGuiObject = new MockBaseGuiObject("id454");
            const args : EventArgs = new EventArgs();
            const handler : IEventsHandler = () : void => {
                LogIt.Debug("this is event test");
            };
            manager.setEvent(baseobject, "onclick");
            manager.setEvent(baseobject2, "onblur", handler, args);
            assert.equal(manager.Exists(baseobject2, "onblur"), true);
            assert.equal(manager.Exists(baseobject.Id(), "onclick"), true);
        }

        public testFireEvent() : void {
            const manager : EventsManager = new EventsManager();
            const args : EventArgs = new EventArgs();
            const basegui : BaseGuiObject = new MockBaseGuiObject("id454");
            manager.FireEvent("gui", "onmouse", args, true);
            manager.FireEvent("gui", "onclick");
            manager.FireEvent(basegui, "onclick");
            manager.FireEvent("guiwui", "onclick", args);
            manager.FireEvent(basegui, "onmouse", args, false);
            manager.FireEvent(basegui, "onclick", false);
            manager.FireEvent("testArgsOwner", "testType", false);
            manager.FireEvent("testArgsOwner", "testType", true);
            manager.FireEvent("testArgsOwner", "testType", args, false);
            manager.FireEvent("testArgsOwner", "testType", false, <any>args);
            manager.FireEvent("testArgsOwner", "testType", true, <any>args);
            Reflection.getInstance();
            assert.equal(manager.Exists(basegui.Id(), "onclick"), false);
            assert.equal(manager.Exists("guiwui", "onclick"), false);
        }

        public testRemoveHandler() : void {
            const manager : EventsManager = new EventsManager();
            const baseobject2 : BaseGuiObject = new MockBaseGuiObject("id454");
            const handler : IEventsHandler = () : void => {
                LogIt.Debug("this is event test");
            };
            const args : EventArgs = new EventArgs();
            manager.setEvent(baseobject2, "onblur", handler, args);
            manager.RemoveHandler(baseobject2, "onblur", handler);
            assert.equal(manager.Exists(baseobject2.Id(), "onblur"), true);
            manager.setEvent("basegui", "onblur", handler);
            manager.RemoveHandler("basegui", "onblur", handler);
            assert.equal(manager.Exists("basegui", "onblur"), true);
        }

        public testgetInstanceSingleton() : void {
            assert.ok(EventsManager.getInstanceSingleton().IsMemberOf(EventsManager));
            assert.ok(EventsManager.getInstanceSingleton().Implements(IEventsManager));
        }

        public testBindOnresize() : void {
            const initUIEvent : any = {bubbles: true, cancelable: true, view: window.onresize, detail: 5};
            const uiEvent : any = {type: "onresize", InitUIEvent: initUIEvent};
            const handler : IEventsHandler = () : void => {
                LogIt.Debug("this is event test resize");
            };
            EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.WINDOW, EventType.ON_RESIZE, handler);
            window.onresize(uiEvent);

            const initUIEvent2 : any = {bubbles: true, cancelable: true, view: window.onresize, detail: 5};
            const uiEvent2 : any = {type: "onresize", InitUIEvent: initUIEvent2};
            EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.WINDOW, EventType.ON_RESIZE, handler);
            window.onresize(uiEvent2);
            EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.WINDOW, EventType.ON_RESIZE);
            this.initSendBox();
        }

        public testOnresizeException() : void {
            let uiEvent : any; // tslint:disable-line
            const handler : IEventsHandler = () : void => {
                // do nothing function
            };
            EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_RESIZE, handler);
            window.onresize(uiEvent);
            EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.WINDOW, EventType.ON_RESIZE);
            this.initSendBox();
            ExceptionsManager.Clear();
        }

        public testBindOnKeydown() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const event : any = {altKey: true, char: "T", charCode: 5, ctrlKey: true, keyCode: 2};
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_KEY_DOWN,
                    ($eventArgs : KeyEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, true);
                        assert.equal($eventArgs.NativeEventArgs().char, "T");
                        EventsManager.getInstanceSingleton().Clear("body", "onkeydown");
                        $done();
                    });
                window.document.onkeydown(event);
                EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.BODY, EventType.ON_KEY_DOWN);
            };
        }

        public testBindOnKeyUp() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const event : any = {altKey: true, char: "T", keyCode: 115, ctrlKey: true};
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_KEY_UP,
                    ($eventArgs : KeyEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, true);
                        assert.equal($eventArgs.NativeEventArgs().char, "T");
                        EventsManager.getInstanceSingleton().Clear("body", "onkeyup");
                        $done();
                    });
                window.document.onkeyup(event);
            };
        }

        public testOnclick() : IUnitTestRunnerPromise {
            LogIt.Debug(EventsManager.getInstanceSingleton().toString());
            return ($done : () => void) : void => {
                const event : any = {altKey: true, button: 2};
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_CLICK,
                    ($eventArgs : MouseEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, true);
                        assert.equal($eventArgs.NativeEventArgs().button, 2);
                        EventsManager.getInstanceSingleton().Clear("body", EventType.ON_CLICK);
                        LogIt.Debug(EventsManager.getInstanceSingleton().toString());
                        this.initSendBox();
                        $done();
                    });
                window.document.onclick(event);
            };
        }

        public __IgnoretestOnclickException() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                let event : any; // tslint:disable-line
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_CLICK,
                    ($eventArgs : MouseEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, true);
                        assert.equal($eventArgs.NativeEventArgs().button, 2);
                        ExceptionsManager.Clear();
                        this.initSendBox();
                        $done();
                    });
                window.document.onclick(event);
            };
        }

        public testOndblclick() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const event : any = {altKey: true, button: 2};
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_DOUBLE_CLICK,
                    ($eventArgs : MouseEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, true);
                        assert.equal($eventArgs.NativeEventArgs().button, 2);
                        ExceptionsManager.Clear();
                        this.initSendBox();
                        $done();
                    });
                window.document.ondblclick(event);
            };
        }

        public __IgnoretestOndblclickException() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                let event : any; // tslint:disable-line
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_DOUBLE_CLICK,
                    ($eventArgs : MouseEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, true);
                        assert.equal($eventArgs.NativeEventArgs().button, 2);
                        ExceptionsManager.Clear();
                        this.initSendBox();
                        $done();
                    });
                window.document.ondblclick(event);
            };
        }

        public __IgnoretestOnContextmenu() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const event : any = {altKey: true, button: 2};
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_RIGHT_CLICK,
                    ($eventArgs : MouseEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, true);
                        assert.equal($eventArgs.NativeEventArgs().button, 2);
                        this.initSendBox();
                        $done();
                    });
                window.document.oncontextmenu(event);
            };
        }

        public __IgnoretestOnContextmenuException() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                let event : any; // tslint:disable-line
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_RIGHT_CLICK,
                    ($eventArgs : MouseEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, true);
                        assert.equal($eventArgs.NativeEventArgs().button, 2);
                        ExceptionsManager.Clear();
                        this.initSendBox();
                        $done();
                    });
                window.document.oncontextmenu(event);
            };
        }

        public __IgnoretestOnMousedown() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const event : any = {altKey: true, button: 2};
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_MOUSE_DOWN,
                    ($eventArgs : MouseEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, true);
                        assert.equal($eventArgs.NativeEventArgs().button, 2);
                        EventsManager.getInstanceSingleton().Clear("body", "onmousedown");
                        $done();
                    });
                window.document.onmousedown(event);
            };
        }

        public __IgnoretestOnMousedownException() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                let event : any; // tslint:disable-line
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_MOUSE_DOWN,
                    ($eventArgs : MouseEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, true);
                        assert.equal($eventArgs.NativeEventArgs().button, 2);
                        this.initSendBox();
                        $done();
                    });
                window.document.onmousedown(event);
            };
        }

        public __IgnoretestOnMouseup() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const event : any = {altKey: true, button: 2};
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_MOUSE_UP,
                    ($eventArgs : MouseEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, true);
                        assert.equal($eventArgs.NativeEventArgs().button, 2);
                        this.initSendBox();
                        $done();
                    });
                window.document.onmouseup(event);
                EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.BODY, EventType.ON_MOUSE_UP);
            };
        }

        public __IgnoretestOnKeydown() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const event : any = {altKey: true, char: "T", charCode: 5, ctrlKey: true, keyCode: 2};
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_KEY_DOWN,
                    ($eventArgs : KeyEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, true);
                        assert.equal($eventArgs.NativeEventArgs().char, "T");
                        this.initSendBox();
                        $done();
                    });
                window.document.onkeydown(event);
            };
        }

        public __IgnoretestOnTouchstart() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const touchlist : any = [
                    {altKey: false, button: 1},
                    {altKey: false, button: 2},
                    {altKey: false, button: 3}
                ];
                const event : any = {altKey: true, changedTouches: touchlist, ctrlKey: true};
                EventsManager.getInstanceSingleton().setEvent("mousemove", EventType.ON_START,
                    ($eventArgs : MoveEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, false);
                        ExceptionsManager.Clear();
                        this.initSendBox();
                        $done();
                    });
                window.document.ontouchstart(event);
                EventsManager.getInstanceSingleton().Clear("mousemove", EventType.ON_START);
            };
        }

        public __IgnoretestOnKeyup() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const event : any = {altKey: true, char: "T", charCode: 5, ctrlKey: true};
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_KEY_UP,
                    ($eventArgs : KeyEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, true);
                        assert.equal($eventArgs.NativeEventArgs().char, "T");
                        this.initSendBox();
                        ExceptionsManager.Clear();
                        $done();
                    });
                window.document.onkeyup(event);
            };
        }

        public __IgnoretestOnKeypress() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const event : any = {altKey: true, char: "T", charCode: 5, ctrlKey: true, keyCode: 10};
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_KEY_PRESS,
                    ($eventArgs : KeyEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, true);
                        assert.equal($eventArgs.NativeEventArgs().char, "T");
                        this.initSendBox();
                        ExceptionsManager.Clear();
                        $done();
                    });
                window.document.onkeypress(event);
                EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.BODY, EventType.ON_KEY_PRESS);
            };
        }

        public __IgnoretestOnKeypressException() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                let event : any; // tslint:disable-line
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_KEY_PRESS,
                    ($eventArgs : KeyEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, true);
                        assert.equal($eventArgs.NativeEventArgs().char, "T");
                        $done();
                    });
                window.document.onkeypress(event);
                EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.BODY, EventType.ON_KEY_PRESS);
            };
        }

        public __IgnoretestOnLoadkeyException() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.BEFORE_REFRESH,
                    ($eventArgs : KeyEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, true);
                        $done();
                    });
                window.document.onkeydown(null);
            };
        }

        public __IgnoretestOnMousemove() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                EventsManager.getInstanceSingleton();
                const event : any = {altKey: false, button: 3};
                EventsManager.getInstanceSingleton().setEvent("mousemove", EventType.ON_CHANGE,
                    ($eventArgs : MouseEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, false);
                        assert.equal($eventArgs.NativeEventArgs().button, 3);
                        this.initSendBox();
                        $done();
                    });
                window.document.onmousemove(event);
                EventsManager.getInstanceSingleton().Clear("mousemove", EventType.ON_CHANGE);
            };
        }

        public __IgnoretestOnTouchedSecond() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const event : any = {altKey: false, button: 3};
                EventsManager.getInstanceSingleton().setEvent("mousemove", EventType.ON_CHANGE,
                    ($eventArgs : MouseEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, false);
                        assert.equal($eventArgs.NativeEventArgs().button, 3);
                        this.initSendBox();
                        $done();
                    });
                window.document.onmousemove(event);
                EventsManager.getInstanceSingleton().Clear("mousemove", EventType.ON_CHANGE);
            };
        }

        public __IgnoretestOnTouchendThird() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const touchlist : any = [
                    {altKey: false, button: 1},
                    {altKey: false, button: 2},
                    {altKey: false, button: 3}
                ];
                const event : any = {altKey: true, changedTouches: touchlist, ctrlKey: true};

                EventsManager.getInstanceSingleton().setEvent("mousemove", EventType.ON_COMPLETE,
                    ($eventArgs : MouseEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs(), touchlist);
                        this.initSendBox();
                        ExceptionsManager.Clear();
                        $done();
                    });

                window.document.ontouchend(event);
                EventsManager.getInstanceSingleton().Clear("mousemove", EventType.ON_COMPLETE);
            };
        }

        public __IgnoretestOnTouchmove() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const touchlist : any = [
                    {altKey: false, button: 1},
                    {altKey: false, button: 2},
                    {altKey: false, button: 3}
                ];
                const event : any = {altKey: true, changedTouches: touchlist, ctrlKey: true};
                EventsManager.getInstanceSingleton().setEvent("mousemove", EventType.ON_CHANGE,
                    ($eventArgs : MoveEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, false);
                        this.initSendBox();
                        ExceptionsManager.Clear();
                        $done();
                    });
                TextSelectionManager.Clear();

                window.document.ontouchmove(event);
            };
        }

        public __IgnoretestOnmousewheel() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const whellEvent : any = {deltaMode: 10, deltaX: 20, deltaY: 30, deltaZ: 40};
                EventsManager.getInstanceSingleton().setEvent("mousemove", EventType.ON_COMPLETE,
                    ($eventArgs : MouseEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs().altKey, false);
                        assert.equal($eventArgs.NativeEventArgs().button, 3);
                        this.initSendBox();
                        ExceptionsManager.Clear();
                        $done();
                    });
                (<any>window.document).onmousewheel(whellEvent);
            };
        }

        public __IgnoretestaddEventListener() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const whellEvent : any = {deltaMode: 10, deltaX: 20, deltaY: 30, deltaZ: 40};
                ExceptionsManager.Clear();
                this.initSendBox();
                const mouseWheelEvent : any = {wheelDelta: 10, wheelDeltaX: 20, wheelDeltaY: 30};
                this.initSendBox();
                ExceptionsManager.Clear();
                $done();
            };
        }

        public __IgnoretestbodyFocusEventHandler() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const handler : IEventsHandler = () : void => {
                    LogIt.Debug("this is event test onfocus");
                    EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.BODY, EventType.ON_FOCUS);
                    ExceptionsManager.Clear();
                    $done();
                };
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_FOCUS, handler);
            };
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
