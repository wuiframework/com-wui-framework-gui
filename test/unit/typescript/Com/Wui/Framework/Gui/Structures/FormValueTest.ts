/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Structures {
    "use strict";

    export class FormValueTest extends UnitTestRunner {

        public testConstructor() : void {
            const form : FormValue = new FormValue("value", 100, true);
            assert.equal(form.Id(), "value");
            assert.equal(form.Value(), 100);

            const form2 : FormValue = new FormValue("value", 100);
            assert.equal(form2.ErrorFlag(), false);
        }

        public testId() : void {
            const form : FormValue = new FormValue("value", 100, true);
            assert.equal(form.Id(), "value");
        }

        public testValue() : void {
            const form : FormValue = new FormValue("value", 100, true);
            assert.equal(form.Value(), 100);
        }

        public testErrorFlag() : void {
            const form : FormValue = new FormValue("value", 100, true);
            assert.equal(form.ErrorFlag(), true);
        }
    }
}
