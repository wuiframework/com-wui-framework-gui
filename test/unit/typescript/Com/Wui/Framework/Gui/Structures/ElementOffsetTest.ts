/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Structures {
    "use strict";

    export class ElementOffsetTest extends UnitTestRunner {
        public testConstructor() : void {
            const offset : ElementOffset = new ElementOffset(10, 3);
            assert.equal(offset.Top(), 10);
        }

        public testTop() : void {
            const offset : ElementOffset = new ElementOffset(10, 3);
            assert.equal(offset.Top(15), 15);
        }

        public testLeft() : void {
            const offset : ElementOffset = new ElementOffset();
            assert.equal(offset.Left(0), 0);
            assert.equal(offset.Left(3), 3);
            assert.equal(offset.Left(8), 8);
        }
    }
}
