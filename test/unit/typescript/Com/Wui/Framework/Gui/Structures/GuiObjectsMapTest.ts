/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Structures {
    "use strict";
    import BasePanel = Com.Wui.Framework.Gui.Primitives.BasePanel;
    import BaseGuiObject = Com.Wui.Framework.Gui.Primitives.BaseGuiObject;
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import GuiElement = Com.Wui.Framework.Gui.Primitives.GuiElement;

    class MockBaseGuiObject extends BaseGuiObject {
    }

    class MockGuiCommons extends GuiCommons {
    }

    export class GuiObjectsMapTest extends UnitTestRunner {

        public testConstructor() : void {
            const guiObject : GuiObjectsMap = new GuiObjectsMap();
            assert.equal(guiObject.IsEmpty(), true);
        }

        public testLength() : void {
            const guiObject : GuiObjectsMap = new GuiObjectsMap();
            assert.equal(guiObject.Length(), 0);
        }

        public testIsEmpty() : void {
            const guiObject : GuiObjectsMap = new GuiObjectsMap();
            assert.equal(guiObject.IsEmpty(), true);
        }

        public testExists() : void {
            const guiObject : GuiObjectsMap = new GuiObjectsMap();
            const baseGuiObject : BaseGuiObject = new MockBaseGuiObject();
            assert.equal(guiObject.Exists(baseGuiObject), false);
        }

        public testgetAll() : void {
            const guiObject : GuiObjectsMap = new GuiObjectsMap();
            const guicommons : GuiCommons = new MockGuiCommons("888");
            const guicommons1 : GuiCommons = new MockGuiCommons("999");

            guiObject.Add(guicommons);
            guiObject.Add(guicommons1);
            assert.deepEqual(guiObject.getAll().getAll(), [guicommons, guicommons1]);
        }

        public testgetType() : void {
            const guiObject : GuiObjectsMap = new GuiObjectsMap();
            const guicommons : GuiCommons = new MockGuiCommons("888");
            const guicommons1 : GuiCommons = new MockGuiCommons("999");

            guiObject.Add(guicommons);
            guiObject.Add(guicommons1);
            assert.deepEqual(guiObject.getType(guicommons).getItem(1), null);

            const guiObject2 : GuiObjectsMap = new GuiObjectsMap();
            const guicommons2 : GuiCommons = new MockGuiCommons("888");
            const guicommons3 : GuiCommons = new MockGuiCommons("999");

            guiObject.Add(guicommons2);
            guiObject.Add(guicommons3);
            assert.deepEqual(guiObject2.getType(GuiCommons).getItem(1), null);
            assert.deepEqual(guiObject2.getType(GuiElement).getItem(2), null);
            guiObject.Clear();
            assert.equal(guiObject.Exists(guicommons2), false);
        }

        public testClear() : void {
            const guiObject : GuiObjectsMap = new GuiObjectsMap();
            const guicommons : GuiCommons = new MockGuiCommons("222");
            const guicommons1 : GuiCommons = new MockGuiCommons("333");

            guiObject.Add(guicommons);
            guiObject.Add(guicommons1);
            assert.equal(guiObject.getAll().getFirst(), guicommons);
            guiObject.Clear(guicommons);
            assert.equal(guiObject.getAll().getFirst(), guicommons1);
            guiObject.Clear();
            guiObject.Clear();
        }

        public testClear2() : void {
            const guiObject : GuiObjectsMap = new GuiObjectsMap();
            const gui : GuiCommons = new MockGuiCommons("101");
            const gui2 : GuiCommons = new MockGuiCommons("202");
            const gui3 : GuiCommons = new MockGuiCommons("303");
            const gui4 : GuiCommons = new MockGuiCommons("404");
            guiObject.Add(gui);
            guiObject.Add(gui2);
            guiObject.Add(gui3);
            guiObject.Add(gui4);
            guiObject.Clear(gui4);
            assert.equal(guiObject.Exists(gui4), false);

            guiObject.getType(GuiCommons);
            guiObject.Clear(gui3);
            assert.equal(guiObject.Exists(gui3), false);

            const guiObject2 : GuiObjectsMap = new GuiObjectsMap();
            let gui5 : GuiCommons; // tslint:disable-line
            guiObject2.Clear(gui5);
        }

        public testToString() : void {
            const guiObject : GuiObjectsMap = new GuiObjectsMap();

            const baseGuiObject : BaseGuiObject = new MockBaseGuiObject();
            const basePanel : BasePanel = new BasePanel();

            guiObject.Add(baseGuiObject);
            guiObject.Add(basePanel);

            assert.ok(guiObject.ToString(""), "id:*, parentId: not registered," +
                " type: Com.Wui.Framework.Gui.Primitives.BaseGuiObject, childrenList: EMPTY<br/>" +
                "id:*, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: EMPTY<br/><b>" +
                "Com.Wui.Framework.Gui.Primitives.GuiCommons</b><br/>&nbsp;&nbsp;&nbsp;" +
                "id:*, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BaseGuiObject," +
                " childrenList: EMPTY<br/>&nbsp;&nbsp;&nbsp;" +
                "id:*, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: EMPTY<br/><b>" +
                "Com.Wui.Framework.Gui.Primitives.BaseGuiObject</b><br/>&nbsp;&nbsp;&nbsp;" +
                "id:*, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BaseGuiObject," +
                " childrenList: EMPTY<br/>&nbsp;&nbsp;&nbsp;" +
                "id:*, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: EMPTY<br/><b>" +
                "Com.Wui.Framework.Gui.Primitives.FormsObject</b><br/>&nbsp;&nbsp;&nbsp;" +
                "id:*, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: EMPTY<br/><b>" +
                "Com.Wui.Framework.Gui.Primitives.BasePanel</b><br/>&nbsp;&nbsp;&nbsp;" +
                "id:*, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: EMPTY<br/>");
        }

        public testToString20() : void {
            const guiObject : GuiObjectsMap = new GuiObjectsMap();
            const baseGuiObject : BaseGuiObject = new MockBaseGuiObject("555");
            const basePanel : BasePanel = new BasePanel("444");

            guiObject.Add(baseGuiObject);
            guiObject.Add(basePanel);
            (<any>GuiObjectsMap).refresh = false;

            assert.equal(guiObject.ToString("", false),
                "id: 555, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BaseGuiObject," +
                " childrenList: EMPTY\r\nid: 444, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: EMPTY\r\nCom.Wui.Framework.Gui.Primitives.GuiCommons\r\n    id: 555, parentId: not registered," +
                " type: Com.Wui.Framework.Gui.Primitives.BaseGuiObject, childrenList: EMPTY\r\n    id: 444, parentId: not registered," +
                " type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: EMPTY\r\nCom.Wui.Framework.Gui.Primitives.BaseGuiObject\r\n    id: 555, parentId: not registered," +
                " type: Com.Wui.Framework.Gui.Primitives.BaseGuiObject, childrenList: EMPTY\r\n    id: 444, parentId: not registered," +
                " type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: EMPTY\r\nCom.Wui.Framework.Gui.Primitives.FormsObject\r\n    id: 444, parentId: not registered," +
                " type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: EMPTY\r\nCom.Wui.Framework.Gui.Primitives.BasePanel\r\n    id: 444, parentId: not registered," +
                " type: Com.Wui.Framework.Gui.Primitives.BasePanel, childrenList: EMPTY\r\n");
        }

        public testToString30() : void {
            const guiObject : GuiObjectsMap = new GuiObjectsMap();
            assert.equal(guiObject.ToString("", false), "");
        }

        public testToString40() : void {
            const guiObject : GuiObjectsMap = new GuiObjectsMap();
            const baseGuiObject : BaseGuiObject = new MockBaseGuiObject("BaseObject1");
            const basePanel : BasePanel = new BasePanel("BasePanel1");
            basePanel.getChildElements().Add(new MockGuiCommons("GuiCommons1"));
            basePanel.getChildElements().Add(new MockGuiCommons("GuiCommons2"));

            guiObject.Add(baseGuiObject);
            guiObject.Add(basePanel);
            assert.equal(guiObject.IsEmpty().toString(), "false");
            assert.equal(guiObject.ToString("", false),
                "id: BaseObject1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BaseGuiObject, " +
                "childrenList: EMPTY\r\n" +
                "id: BasePanel1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel, " +
                "childrenList: [GuiCommons1,GuiCommons2]\r\n" +
                "Com.Wui.Framework.Gui.Primitives.GuiCommons\r\n" +
                "    id: BaseObject1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BaseGuiObject, " +
                "childrenList: EMPTY\r\n" +
                "    id: BasePanel1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel, " +
                "childrenList: [GuiCommons1,GuiCommons2]\r\n" +
                "Com.Wui.Framework.Gui.Primitives.BaseGuiObject\r\n" +
                "    id: BaseObject1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BaseGuiObject, " +
                "childrenList: EMPTY\r\n" +
                "    id: BasePanel1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel, " +
                "childrenList: [GuiCommons1,GuiCommons2]\r\n" +
                "Com.Wui.Framework.Gui.Primitives.FormsObject\r\n" +
                "    id: BasePanel1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel, " +
                "childrenList: [GuiCommons1,GuiCommons2]\r\n" +
                "Com.Wui.Framework.Gui.Primitives.BasePanel\r\n" +
                "    id: BasePanel1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel, " +
                "childrenList: [GuiCommons1,GuiCommons2]\r\n");

            assert.equal(guiObject.ToString(""),
                "id: BaseObject1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BaseGuiObject," +
                " childrenList: EMPTY<br/>" +
                "id: BasePanel1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: [GuiCommons1,GuiCommons2]<br/><b>Com.Wui.Framework.Gui.Primitives.GuiCommons</b><br/>" +
                "&nbsp;&nbsp;&nbsp;id: BaseObject1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BaseGuiObject," +
                " childrenList: EMPTY<br/>" +
                "&nbsp;&nbsp;&nbsp;id: BasePanel1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: [GuiCommons1,GuiCommons2]<br/><b>Com.Wui.Framework.Gui.Primitives.BaseGuiObject</b><br/>" +
                "&nbsp;&nbsp;&nbsp;id: BaseObject1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BaseGuiObject," +
                " childrenList: EMPTY<br/>" +
                "&nbsp;&nbsp;&nbsp;id: BasePanel1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: [GuiCommons1,GuiCommons2]<br/><b>Com.Wui.Framework.Gui.Primitives.FormsObject</b><br/>" +
                "&nbsp;&nbsp;&nbsp;id: BasePanel1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: [GuiCommons1,GuiCommons2]<br/><b>Com.Wui.Framework.Gui.Primitives.BasePanel</b><br/>" +
                "&nbsp;&nbsp;&nbsp;id: BasePanel1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: [GuiCommons1,GuiCommons2]<br/>");

            guiObject.Add(baseGuiObject);
            assert.equal(guiObject.ToString(),
                "id: BaseObject1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BaseGuiObject," +
                " childrenList: EMPTY<br/>id: BasePanel1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: [GuiCommons1,GuiCommons2]<br/>" +
                "<b>Com.Wui.Framework.Gui.Primitives.GuiCommons</b><br/>" +
                "&nbsp;&nbsp;&nbsp;id: BaseObject1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BaseGuiObject," +
                " childrenList: EMPTY<br/>" +
                "&nbsp;&nbsp;&nbsp;id: BasePanel1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: [GuiCommons1,GuiCommons2]<br/><b>Com.Wui.Framework.Gui.Primitives.BaseGuiObject</b><br/>" +
                "&nbsp;&nbsp;&nbsp;id: BaseObject1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BaseGuiObject," +
                " childrenList: EMPTY<br/>" +
                "&nbsp;&nbsp;&nbsp;id: BasePanel1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: [GuiCommons1,GuiCommons2]<br/><b>Com.Wui.Framework.Gui.Primitives.FormsObject</b><br/>" +
                "&nbsp;&nbsp;&nbsp;id: BasePanel1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: [GuiCommons1,GuiCommons2]<br/><b>Com.Wui.Framework.Gui.Primitives.BasePanel</b><br/>" +
                "&nbsp;&nbsp;&nbsp;id: BasePanel1, parentId: not registered, type: Com.Wui.Framework.Gui.Primitives.BasePanel," +
                " childrenList: [GuiCommons1,GuiCommons2]<br/>");
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
