/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Structures {
    "use strict";
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;

    class MockBaseGuiGroupObjectArgs extends BaseGuiGroupObjectArgs {
    }

    export class BaseGuiGroupObjectArgsTest extends UnitTestRunner {

        public testConstructor() : void {
            const baseguiObject : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            assert.equal(baseguiObject.Visible(true), true);
        }

        public testVisible() : void {
            const baseguiObject : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            assert.equal(baseguiObject.Visible(true), true);
        }

        public testForceSetValue() : void {
            const baseguiObject : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            assert.equal(baseguiObject.ForceSetValue(true), true);
        }

        public testValue() : void {
            const baseguiObject : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            assert.equal(baseguiObject.Value("pageSize"), "pageSize");
        }

        public testValue20() : void {
            const baseguiObject : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            assert.equal(baseguiObject.Value(), null);
        }

        public testEnabled() : void {
            const baseguiObject : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            assert.equal(baseguiObject.Enabled(false), false);
        }

        public testError() : void {
            const baseguiObject : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            assert.equal(baseguiObject.Error(true), true);
        }

        public testAddGuiOption() : void {
            const baseguiObject : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            baseguiObject.AddGuiOption(GuiOptionType.ALL);
            assert.equal(baseguiObject.getGuiOptionsList().getLast(), GuiOptionType.ALL);
        }

        public getGuiOptionsList() : void {
            const baseguiObject : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            assert.equal(baseguiObject.getGuiOptionsList(), (<any>BaseGuiGroupObjectArgs).guiOptions);
        }

        public TitleText() : void {
            const baseguiObject : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            assert.equal(baseguiObject.TitleText("testWuiGui"), "testWuiGui");

        }

        public TitleText20() : void {
            const baseguiObject : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            assert.equal(baseguiObject.TitleText(), "");
        }

        public testWidth() : void {
            const baseguiObject : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            assert.equal(baseguiObject.Width(30), -1);
            assert.equal(baseguiObject.Width(60), 60);
            assert.equal(baseguiObject.Width(50), 50);
            assert.equal(baseguiObject.Width(null), 50);
        }

        public testResize() : void {
            const baseguiObject : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            assert.equal(baseguiObject.Resize(true), true);
        }
    }
}
