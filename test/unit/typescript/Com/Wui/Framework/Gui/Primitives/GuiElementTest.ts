/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Primitives {
    "use strict";
    import Alignment = Com.Wui.Framework.Gui.Enums.Alignment;
    import FitToParent = Com.Wui.Framework.Gui.Enums.FitToParent;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import UnitType = Com.Wui.Framework.Gui.Enums.UnitType;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;

    class MockGuiCommons extends GuiCommons {
    }

    export class GuiElementTest extends UnitTestRunner {

        public testConstructor() : void {
            const guiElement : GuiElement = new GuiElement();
            assert.equal(guiElement.Add("GuiElement"), "GuiElement");
        }

        public testId() : void {
            const guiElement : GuiElement = new GuiElement();
            assert.equal(guiElement.Id("test"), guiElement);
        }

        public testgetId() : void {
            const guiElement : GuiElement = new GuiElement();
            guiElement.Id("555");
            assert.equal(guiElement.getId(), "555");
        }

        public testGuiTypeTag() : void {
            const guiElement : GuiElement = new GuiElement();
            assert.equal(guiElement.GuiTypeTag("typeTag"), guiElement);
        }

        public testStyleClassName() : void {
            const guiElement : GuiElement = new GuiElement();
            assert.equal(guiElement.StyleClassName("base"), guiElement);
        }

        public testVisible() : void {
            const guiElement : GuiElement = new GuiElement();
            assert.equal(guiElement.Visible(true), guiElement);
        }

        public testWidth() : void {
            const guiElement : GuiElement = new GuiElement();
            assert.equal(guiElement.Width(20), guiElement);
            assert.equal(guiElement.getWidth(), 20);
        }

        public testHeight() : void {
            const guiElement : GuiElement = new GuiElement();
            assert.equal(guiElement.Height(40), guiElement);
            assert.equal(guiElement.getHeight(), 40);
        }

        public testAlignment() : void {
            const guiElement : GuiElement = new GuiElement();
            const alignment : Alignment = new Alignment();
            guiElement.Alignment(alignment);
            assert.equal(guiElement.getAlignment(), alignment);
        }

        public testFitToParent() : void {
            const guiElement : GuiElement = new GuiElement();
            const parent : FitToParent = new FitToParent();
            guiElement.FitToParent(parent);
            assert.equal(guiElement.getFitToParent(), parent);
        }

        public testsetAttribute() : void {
            const guiElement : GuiElement = new GuiElement();
            assert.equal(guiElement.setAttribute("key", "value"), guiElement);
            assert.equal(guiElement.setAttribute("key", null), guiElement);
            assert.equal(guiElement.setAttribute(null, "value"), guiElement);
        }

        public testAdd() : void {
            const guiElement : GuiElement = new GuiElement();
            assert.equal(guiElement.Add("test"), guiElement);
            const element : GuiElement = new GuiElement();
            const elementgui : GuiCommons = new MockGuiCommons();
            const elementviewer : BasePanelViewer = new BasePanelViewer();
            element.Add(elementgui);
            element.Add(elementviewer);
            assert.deepEqual(element.getChildElement(0), elementgui, "");
        }

        public testAddSecond() : void {
            const guiElement : GuiElement = new GuiElement();

            const gui : GuiCommons = new MockGuiCommons("wars");
            const elementviewer : BasePanelViewer = new BasePanelViewer();
            gui.setWrappingElement(guiElement);
            gui.Enabled(true);
            gui.Visible(true);
            const gui2 : GuiCommons = new MockGuiCommons("star");
            gui2.setWrappingElement(guiElement);
            gui2.Enabled(true);
            gui2.Visible(true);
            const gui3 : GuiCommons = new MockGuiCommons("test");
            gui3.setWrappingElement(guiElement);
            guiElement.Add(gui);
            guiElement.Add(gui2);
            guiElement.Add(gui3);
            guiElement.Add(elementviewer);
            // guiElement.GuiTypeTag(GeneralCssNames.ROW);
            guiElement.GuiTypeTag(GeneralCssNames.COLUMN);
            guiElement.setAttribute("guiType", GeneralCssNames.COLUMN);
            assert.equal(guiElement.getGuiTypeTag(), GeneralCssNames.COLUMN);
        }

        public testAddThird() : void {
            const guiElement : GuiElement = new GuiElement();

            const gui : GuiCommons = new MockGuiCommons("wars");
            const elementviewer : BasePanelViewer = new BasePanelViewer();
            gui.setWrappingElement(guiElement);
            gui.Enabled(true);
            gui.Visible(true);
            const gui2 : GuiCommons = new MockGuiCommons("star");
            gui2.setWrappingElement(guiElement);
            gui2.Enabled(true);
            gui2.Visible(true);
            const gui3 : GuiCommons = new MockGuiCommons("test");
            gui3.setWrappingElement(guiElement);
            guiElement.Add(gui);
            guiElement.Add(gui2);
            guiElement.Add(gui3);
            guiElement.Add(elementviewer);
            // guiElement.GuiTypeTag(GeneralCssNames.ROW);
            guiElement.GuiTypeTag(GeneralCssNames.ROW);
            guiElement.setAttribute("guiType", GeneralCssNames.ROW);
            assert.equal(guiElement.getGuiTypeTag(), GeneralCssNames.ROW);
        }

        public testgetChildElements() : void {
            const guiElement : GuiElement = new GuiElement();
            const elementgui : GuiCommons = new MockGuiCommons();
            const elementviewer : BasePanelViewer = new BasePanelViewer();
            guiElement.Add("test");
            guiElement.Add(elementgui);
            guiElement.Add(elementviewer);
            assert.deepEqual(guiElement.getChildElements().getAll(), ["test", elementgui, elementviewer]);
            assert.deepEqual(guiElement.getChildElements().getKeys(), [0, 1, 2]);
            assert.patternEqual(guiElement.Draw(""),
                "test\r\n<div class=\"ComWuiFrameworkGuiPrimitives\">\r\n" +
                "   <div id=\"GuiCommons*_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
                "      <div id=\"GuiCommons*\" class=\"GuiCommons\" style=\"display: none;\"></div>\r\n" +
                "   </div>\r\n</div>");
        }

        public testgetChildElement() : void {
            const guiElement : GuiElement = new GuiElement();
            const guiElemChild1 : GuiElement = new GuiElement();
            const guiElemChild2 : GuiElement = new GuiElement();
            const guiElemChild3 : GuiElement = new GuiElement();
            guiElement.Add(guiElemChild1);
            guiElement.Add(guiElemChild2);
            guiElement.Add(guiElemChild3);
            assert.equal(guiElement.getChildElement(2), guiElemChild3);
            assert.equal(guiElement.getChildElement(666), null);
            assert.equal(guiElement.getChildElement(null), null);
        }

        public testgetGuiChildElements() : void {
            const guiElement : GuiElement = new GuiElement();
            const elementgui : GuiCommons = new MockGuiCommons();
            const elementviewer : BasePanelViewer = new BasePanelViewer();
            guiElement.Add("test");
            guiElement.Add(elementgui);
            guiElement.Add(elementviewer);
            assert.patternEqual(guiElement.getGuiChildElements().ToString("", false),
                "Com.Wui.Framework.Commons.Primitives.ArrayList object\r\n" +
                "[ 0 ]    Com.Wui.Framework.Gui.Primitives.GuiCommons (GuiCommons*)\r\n");
        }

        public testgetWrappingElement() : void {
            const guiElement : GuiElement = new GuiElement();
            const elementgui : GuiCommons = new MockGuiCommons();
            guiElement.Add(elementgui);
            guiElement.setWrappingElement(guiElement);
            assert.equal(guiElement.getWrappingElement(), guiElement);
        }

        public testgetAttributes() : void {
            const guiElement : GuiElement = new GuiElement();
            guiElement.setAttribute("src", "w3schools.jpg");
            guiElement.setAttribute("href", "I'm a tooltip");
            guiElement.setAttribute("title", "http://www.w3schools.com");
            assert.deepEqual(guiElement.getAttributes().getAll(), ["w3schools.jpg", "I'm a tooltip", "http://www.w3schools.com"]);
            assert.deepEqual(guiElement.getAttributes().getKeys(), ["src", "href", "title"]);

            const guiElement1 : GuiElement = new GuiElement();
            assert.deepEqual(guiElement1.getAttributes().getAll(), []);
            assert.deepEqual(guiElement1.getAttributes().getKeys(), []);
        }

        public testDraw() : void {
            const guiElement : GuiElement = new GuiElement();
            guiElement.setAttribute("src", "w3schools.jpg");
            guiElement.setAttribute("href", "I'm a tooltip");
            guiElement.setAttribute("title", "http://www.w3schools.com");
            guiElement.setAttribute("width", "200px");
            guiElement.getAttributes().Add("opacity: 50");
            guiElement.getAttributes().Add("color: black;");
            guiElement.StyleClassName("TestClassName");
            guiElement.Width(100);
            guiElement.Visible(true);
            guiElement.Height(200);
            guiElement.Id("enterprice");
            guiElement.GuiTypeTag("wraper");
            Echo.PrintCode(guiElement.Draw(""));
            Echo.Println("<i>After set element properties</i>");
            Echo.Println("<div style=\"width: 200px; height: 200px; position: relative;\">" + guiElement.Draw("") + "</div>");
            assert.patternEqual(guiElement.Draw(""), "<div id=\"enterprice\" guiType=\"wraper\" class=\"TestClassName\" " +
                "style=\"opacity: 50; color: black; display: block; height: 200px;" +
                " href: I'm a tooltip; src: w3schools.jpg; title: http://www.w3schools.com; width: 100px;\"></div>");
        }

        public testDrawSecond() : void {
            const guiElement : GuiElement = new GuiElement();
            guiElement.setAttribute("src", "w3schools.jpg");
            guiElement.setAttribute("href", "I'm a tooltip");
            guiElement.setAttribute("title", "http://www.w3schools.com");
            guiElement.StyleClassName("TestClassName");
            guiElement.Width(100);
            guiElement.Height(200);
            guiElement.Visible(false);
            guiElement.Id("starwars");
            guiElement.GuiTypeTag("wraper");
            Echo.PrintCode(guiElement.Draw(""));
            Echo.Println("<i>After set element properties</i>");
            Echo.Println("<div style=\"width: 200px; height: 200px; position: relative;\">" + guiElement.Draw("") + "</div>");
            assert.patternEqual(guiElement.Draw(""),
                "<div id=\"starwars\" guiType=\"wraper\" class=\"TestClassName\" style=\"display: none; height: 200px;" +
                " href: I'm a tooltip; src: w3schools.jpg; title: http://www.w3schools.com; width: 100px;\"></div>");
        }

        public testDrawThree() : void {
            const guiElement : GuiElement = new GuiElement();
            // guiElement.Add(document.createElement("div"));
            guiElement.Add("<div></div>");
            guiElement.Add(new MockGuiCommons());
            assert.patternEqual(guiElement.Draw(""),
                "<div>" +
                "</div>\r\n<div class=\"ComWuiFrameworkGuiPrimitives\">\r\n" +
                "   <div id=\"GuiCommons*_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
                "      <div id=\"GuiCommons*\" class=\"GuiCommons\" style=\"display: none;\"></div>\r\n" +
                "   </div>\r\n" +
                "</div>");
        }

        public testToDOMElement() : void {
            const guiElement : GuiElement = new GuiElement();
            const element : HTMLElement = document.createElement("div");
            element.innerHTML = guiElement.Draw("div");
            assert.equal(guiElement.ToDOMElement("div"), <GuiElement>(<any>(<HTMLElement>element.childNodes[1])));
        }

        public testtoString() : void {
            const guiElement : GuiElement = new GuiElement();
            assert.equal(guiElement.toString(), "");
        }

        public testToString() : void {
            const guiElement : GuiElement = new GuiElement();
            const element : HTMLElement = document.createElement("div");
            assert.equal(guiElement.ToString("element"), "elementobject type of \'Com.Wui.Framework.Gui.Primitives.GuiElement\'");
        }

        public testitemToString() : void {
            const guiElement : GuiElement = new GuiElement();
            assert.patternEqual(guiElement.Draw(""), "");
        }

        public testToString20() : void {
            const guiElement : GuiElement = new GuiElement();
            assert.equal(guiElement.ToString(""), "object type of \'Com.Wui.Framework.Gui.Primitives.GuiElement\'");
        }

        public testToString30() : void {
            const guiElement : GuiElement = new GuiElement();
            assert.equal(guiElement.ToString("guiCommons"), "guiCommonsobject type of \'Com.Wui.Framework.Gui.Primitives.GuiElement\'");
        }

        public testToString40() : void {
            const guiElement : GuiElement = new GuiElement();
            const baseviewer : BaseViewer = new BaseViewer();
            assert.equal(guiElement.ToString("baseviewer"), "baseviewerobject type of \'Com.Wui.Framework.Gui.Primitives.GuiElement\'");
        }

        public testIdSecond() : void {
            Echo.Println("<div id=\"GuiElement888999\" type=\"text\">Multiple</div>");
            const element : GuiElement = new GuiElement();
            element.Id("GuiElement888999");
            assert.equal(ElementManager.Exists("GuiElement888999"), true);
        }

        public testGuiTypeTagSeoond() : void {
            Echo.Println("<div id=\"GuiElement888999\" guiType=\"GuiWraper\" >Multiple</div>");
            const element : GuiElement = new GuiElement();
            element.GuiTypeTag("GuiWraper");
            assert.equal(element.getGuiTypeTag(), "GuiWraper");
        }

        public testStyleClassNameSecond() : void {
            Echo.Println("<div id=\"GuiElement888999\" guiType=\"GuiWraper\" class=\"testClassName\"</div>");
            const element : GuiElement = new GuiElement();
            element.StyleClassName("testClassName");
            Echo.Println("<div id=\"GuiElement888999\" guiType=\"GuiWraper\" class=\"testClassName\"</div>");
        }

        public testWidthOfColumnNumber() : void {
            const element : GuiElement = new GuiElement();
            const gui : GuiCommons = new MockGuiCommons();
            const guiS : GuiCommons = new MockGuiCommons();
            element.addChildElement(gui);
            element.addChildElement(guiS);
            element.WidthOfColumn(1, true, true);
            assert.patternEqual(element.getWidthOfColumn().ToString("", false),
                "[IsPropagated] Object type: boolean. Return value: true\r\n" +
                "[Normalize] Object type: number. Return value: 1\r\n" +
                "*\r\n"
            );
            element.WidthOfColumn("5px", false);
            assert.patternEqual(element.getWidthOfColumn().ToString("", false),
                "[IsPropagated] Object type: boolean. Return value: false\r\n" +
                "[Normalize] Object type: number. Return value: 5\r\n" +
                "*\r\n"
            );
            //  assert.deepEqual(element.getWidthOfColumn(), {"displayUnitType": "px", "number": 5, "propagated": false});
            element.WidthOfColumn("3px", true);
            element.WidthOfColumn("2%", true);
            element.WidthOfColumn("1%", false);
            element.WidthOfColumn("sd1%", false);
            element.WidthOfColumn(40, UnitType.PCT, true);
            element.WidthOfColumn(20, UnitType.PX, true);
            element.WidthOfColumn(undefined, UnitType.PCT, true);
            element.WidthOfColumn(null, false, null);
            element.WidthOfColumn(null, true, null);
            element.WidthOfColumn("20px");
            assert.throws(() : void => {
                element.WidthOfColumn("7", true);
            }, /Unit type of value not supported : 7/);
        }

        public testWidthOfColumnString() : void {
            const element : GuiElement = new GuiElement();
            const gui : GuiCommons = new MockGuiCommons();
            const guiS : GuiCommons = new MockGuiCommons();
            element.addChildElement(gui);
            element.addChildElement(guiS);
            element.WidthOfColumn(15, true);
            assert.patternEqual(element.getWidthOfColumn().ToString("", false),
                "[IsPropagated] Object type: boolean. Return value: false\r\n" +
                "[Normalize] Object type: number. Return value: 15\r\n" +
                "*\r\n"
            );
            element.WidthOfColumn(15, undefined, true);
            assert.patternEqual(element.getWidthOfColumn().ToString("", false),
                "[IsPropagated] Object type: boolean. Return value: false\r\n" +
                "[Normalize] Object type: number. Return value: 15\r\n" +
                "*\r\n");
            element.WidthOfColumn(0, false, false);
            assert.patternEqual(element.getWidthOfColumn().ToString("", false),
                "[IsPropagated] Object type: boolean. Return value: false\r\n" +
                "[Normalize] Object type: number. Return value: 0\r\n" +
                "*\r\n");
            element.WidthOfColumn(null, null, null);
            element.WidthOfColumn(null, null);
            element.WidthOfColumn(null);
            element.WidthOfColumn(undefined);
        }

        public testHeightofRow() : void {
            const element : GuiElement = new GuiElement();
            const gui : GuiCommons = new MockGuiCommons();
            const guiS : GuiCommons = new MockGuiCommons();
            element.addChildElement(gui);
            element.addChildElement(guiS);
            element.HeightOfRow(1, true, true);
            assert.patternEqual(element.getHeightOfRow().ToString("", false),
                "[IsPropagated] Object type: boolean. Return value: true\r\n" +
                "[Normalize] Object type: number. Return value: 1\r\n" +
                "*\r\n"
            );
            element.HeightOfRow("5px", false);
            assert.patternEqual(element.getHeightOfRow().ToString("", false),
                "[IsPropagated] Object type: boolean. Return value: false\r\n" +
                "[Normalize] Object type: number. Return value: 5\r\n" +
                "*\r\n");
            element.HeightOfRow("3px", true);
            assert.equal(element.getHeightOfRow(), (<any>element).heightOfRow);
            element.HeightOfRow("2%", true);
            element.HeightOfRow("1%", false);
            element.HeightOfRow(40, UnitType.PCT, true);
            assert.patternEqual(element.getHeightOfRow().ToString("", false),
                "[IsPropagated] Object type: boolean. Return value: true\r\n" +
                "[Normalize] Object type: number. Return value: 40\r\n" +
                "*\r\n"
            );
            element.HeightOfRow(20, UnitType.PX, true);
            element.HeightOfRow(undefined, UnitType.PCT, true);
            element.HeightOfRow(null, false, null);
            element.HeightOfRow(null, true, null);
            element.HeightOfRow("20px");
            assert.throws(() : void => {
                element.HeightOfRow("7", true);
            }, /Unit type of value not supported : 7/);

            element.HeightOfRow(15, true);
            element.HeightOfRow(15, undefined, true);
            element.HeightOfRow(0, false, false);
            element.HeightOfRow(null, null, null);
            element.HeightOfRow(null, null);
            element.HeightOfRow(null);
            element.HeightOfRow(undefined);
            assert.patternEqual(element.getHeightOfRow().ToString("", false),
                "[IsPropagated] Object type: boolean. Return value: false\r\n" +
                "[Normalize] Object type: number. Return value: -1\r\n" +
                "*\r\n"
            );
        }

        public testVisibleSecond() : void {
            const element : GuiElement = new GuiElement();
            element.Visible(true);
            Echo.Println("<div id=\"GuiElement888999\" guiType=\"GuiWraper\" class=\"testClassName\"</div>");
            assert.equal(ElementManager.IsVisible("GuiElement888999"), true);
        }

        public testWidthSecond() : void {
            const element : GuiElement = new GuiElement();
            element.Width(50);
            Echo.Println("<div id=\"GuiElement888999\" guiType=\"GuiWraper\" class=\"testClassName\" style=\"width: \"50\";\"</div>");
            assert.equal(ElementManager.getElement("GuiElement888999").offsetWidth, 0);
        }

        public testHeightSecond() : void {
            const element : GuiElement = new GuiElement();
            element.Height(100);
            Echo.Println("<div id=\"GuiElement888999\" guiType=\"GuiWraper\" class=\"testClassName\" style=\"width: 50px; height: 100px\"" +
                "</div>");
            assert.equal(ElementManager.getElement("GuiElement888999").offsetHeight, 0);
        }

        public testsetAttributeSecond() : void {
            const element : GuiElement = new GuiElement();
            element.setAttribute("display", "none");
            element.setAttribute("color", "blue");
            assert.equal(element.getAttributes().getFirst(), "none", "validate that attribute exists on first position");
        }

        public testgetChildElementSecond() : void {
            Echo.Println("<div id=\"GuiElement888999\" guiType=\"GuiWraper\" class=\"testClassName\" style=\"width: 50px; height: 100px\"" +
                "</div>");
            const element : GuiElement = new GuiElement();
            const elementChild : GuiElement = new GuiElement();
            const gui : GuiCommons = new MockGuiCommons();
            const viewer : BasePanelViewer = new BasePanelViewer();
            element.Add(elementChild);
            element.Add(gui);
            element.Add(viewer);
            element.getChildElement(1);
            assert.deepEqual(element.getChildElement(0), elementChild, "validate, that child exists");
        }

        protected tearDown() : void {
            this.initSendBox();
            document.documentElement.innerHTML = "";
            this.registerElement("Content");
        }
    }
}
