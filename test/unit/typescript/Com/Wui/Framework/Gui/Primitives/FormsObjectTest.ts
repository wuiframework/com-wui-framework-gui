/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Primitives {
    "use strict";
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import IGuiCommonsArg = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommonsArg;
    import GuiCommonsArgType = Com.Wui.Framework.Gui.Enums.GuiCommonsArgType;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ElementEventsManager = Com.Wui.Framework.Gui.Events.ElementEventsManager;
    import INotification = Com.Wui.Framework.Gui.Interfaces.Components.INotification;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import GuiOptionType = Com.Wui.Framework.Gui.Enums.GuiOptionType;
    import StaticPageContentManager = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import IToolTip = Com.Wui.Framework.Gui.Interfaces.Components.IToolTip;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import GeneralEventOwner = Com.Wui.Framework.Gui.Enums.Events.GeneralEventOwner;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import EventsManager = Com.Wui.Framework.Gui.Events.EventsManager;
    import PersistenceType = Com.Wui.Framework.Gui.Enums.PersistenceType;

    class MockFormsObject extends FormsObject {
        public testCSS() : string {
            return this.statusCss();
        }

        public testerror() : string {
            return this.errorCssName();
        }

        public testData() : string[] {
            return this.excludeSerializationData();
        }

        public testCacheData() : string[] {

            return this.excludeCacheData();
        }

        protected getTitleClass() : any {
            return MockTitle;
        }
    }

    class MockSubmitFormsObject extends FormsObject {
        private value : any;

        public Value($value? : any) : any {
            if (ObjectValidator.IsSet($value)) {
                this.value = $value;
                this.setChanged();
            }
            return this.value;
        }

        protected getTitleClass() : any {
            return MockTitle;
        }
    }

    class MockGuiCommons extends GuiCommons {
    }

    class MockBaseViewer extends BaseViewer {
    }

    class MockTitle extends GuiCommons implements IToolTip {
        private text : string;
        private guiType : any;

        public GuiType($toolTipType? : any) : any {
            return this.guiType = Property.String(this.guiType, $toolTipType);
        }

        public Text($value? : string) : string {
            return this.text = Property.String(this.text, $value);
        }

    }

    class MockFormsObject2 extends MockFormsObject {
        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_Enabled").StyleClassName(GeneralCssNames.OFF);
        }
    }

    class MockFormsObject3 extends MockFormsObject {
        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_Enabled").StyleClassName(GeneralCssNames.ON);
        }
    }

    export class FormsObjectTest extends UnitTestRunner {

        public __IgnoretestTurnOn() : IUnitTestRunnerPromise {
            const form : FormsObject = new MockFormsObject2();
            form.InstanceOwner(new MockBaseViewer());
            StaticPageContentManager.BodyAppend(form.Draw());
            StaticPageContentManager.Draw();
            form.Enabled(false);
            return ($done : () => void) : void => {
                form.getEvents().setOnComplete(($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    assert.equal(ElementManager.getClassName(form.Id() + "_Enabled"), GeneralCssNames.OFF);
                    $manager.setActive(form, false);
                    FormsObject.TurnOn(form, $manager, $reflection);
                    assert.equal(ElementManager.getClassName(form.Id() + "_Enabled"), GeneralCssNames.ON);
                    $manager.setActive(form, true);
                    FormsObject.TurnOn(form, $manager, $reflection);
                    assert.equal(ElementManager.getClassName(form.Id() + "_Enabled"), GeneralCssNames.ON);
                    $done();
                });
                form.Visible(true);
            };
        }

        public __IgnoretestTurnOff() : IUnitTestRunnerPromise {
            const form : FormsObject = new MockFormsObject3();
            form.InstanceOwner(new MockBaseViewer());
            StaticPageContentManager.BodyAppend(form.Draw());
            StaticPageContentManager.Draw();
            form.Enabled(false);
            return ($done : () => void) : void => {
                form.getEvents().setOnComplete(($eventArgs : EventArgs, $manager : GuiObjectManager, $reflection : Reflection) : void => {
                    assert.equal(ElementManager.getClassName(form.Id() + "_Enabled"), GeneralCssNames.ON);
                    $manager.setActive(form, false);
                    FormsObject.TurnOff(form, $manager, $reflection);
                    assert.equal(ElementManager.getClassName(form.Id() + "_Enabled"), GeneralCssNames.OFF);
                    $manager.setActive(form, true);
                    FormsObject.TurnOff(form, $manager, $reflection);
                    assert.equal(ElementManager.getClassName(form.Id() + "_Enabled"), GeneralCssNames.OFF);
                    $done();
                });
                form.Visible(true);
            };
        }

        public testActive() : void {
            const form : FormsObject = new MockFormsObject2();
            form.InstanceOwner(new MockBaseViewer());
            StaticPageContentManager.BodyAppend(form.Draw());
            StaticPageContentManager.Draw();
            form.Enabled(true);
            form.getEvents().setOnComplete(() : void => {
                const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                const reflection : Reflection = new Reflection();
                FormsObject.TurnActive(form, manager, reflection);
                assert.equal(ElementManager.getClassName(form.Id() + "_Enabled"), GeneralCssNames.ACTIVE);
            });
        }

        public testBlur() : void {
            const form : FormsObject = new MockFormsObject("id44");
            form.InstanceOwner(new MockBaseViewer());
            StaticPageContentManager.BodyAppend(form.Draw());
            StaticPageContentManager.Draw();
            form.Enabled(true);
            form.getEvents().setOnComplete(() : void => {
                const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                const eventArgs : EventArgs = new EventArgs();
                FormsObject.Blur();
                assert.equal(manager.IsActive(form), false);
                EventsManager.getInstanceSingleton().FireEvent(form.getClassName(), EventType.ON_BLUR, eventArgs);
            });
        }

        public testCollectValues() : void {
            const form : FormsObject = new MockFormsObject("333");
            const form20 : FormsObject = new MockFormsObject("222");
            const form30 : FormsObject = new MockFormsObject("555");
            form.getChildElements().Add(form20);
            form.getChildElements().Add(form30);
            assert.deepEqual(FormsObject.CollectValues(form).getAll(), []);
        }

        public testCollectValues20() : void {
            const manager : GuiObjectManager = new GuiObjectManager();
            assert.deepEqual(FormsObject.CollectValues().getAll(), []);
            manager.Clear();
        }

        public testCollectValues30() : void {
            const form : FormsObject = new MockFormsObject("333");
            const guicommons : GuiCommons = new MockGuiCommons("54789");
            form.getChildElements().Add(guicommons);
            assert.deepEqual(FormsObject.CollectValues(form).getAll(), []);
        }

        public testNotification() : void {
            const form : FormsObject = new MockFormsObject("333");
            (<any>FormsObject).notificationClass = INotification;
            assert.equal(form.Notification(), (<any>FormsObject).notification);
            const formsobject : FormsObject = new MockFormsObject();
            assert.equal(formsobject.Notification(), (<any>FormsObject).notification);
        }

        public testEnabled() : void {
            const form : FormsObject = new MockFormsObject("111");
            assert.equal(form.Enabled(true), true);
            assert.equal(form.Enabled(false), false);
            assert.equal(form.Enabled(null), false);
            const formsobject : FormsObject = new MockFormsObject();
            assert.equal(formsobject.Enabled(), true, "validate enabled");
            formsobject.getGuiOptions().Add(GuiOptionType.DISABLE);
            const formsobject2 : FormsObject = new MockFormsObject();
            ElementManager.Enabled("111", false);
            formsobject2.Visible(false);
            assert.equal(formsobject2.Enabled(false), false);
            assert.equal(form.Enabled(), false);
        }

        public __IgnoretestFormEnabled() : IUnitTestRunnerPromise {
            const form : FormsObject = new MockFormsObject("222");
            form.InstanceOwner(new MockBaseViewer());
            StaticPageContentManager.BodyAppend(form.Draw());
            StaticPageContentManager.Draw();
            form.Enabled(true);
            assert.equal(form.Enabled(), true);

            return ($done : () => void) : void => {
                form.getEvents().setOnComplete(
                    () : void => {
                        form.Enabled(true);
                        $done();
                    });
                form.Visible(true);
            };
        }

        public __IgnoretestFormError() : IUnitTestRunnerPromise {
            const form : FormsObject = new MockFormsObject("222");
            form.InstanceOwner(new MockBaseViewer());
            StaticPageContentManager.BodyAppend(form.Draw());
            StaticPageContentManager.Draw();
            form.Error(true);
            assert.equal(form.Error(), true);

            return ($done : () => void) : void => {
                form.getEvents().setOnComplete(() : void => {
                    form.Error(true);
                    $done();
                });
                form.Visible(true);
            };
        }

        public __IgnoretestError() : IUnitTestRunnerPromise {
            const form2 : FormsObject = new MockFormsObject();
            form2.InstanceOwner(new MockBaseViewer());
            StaticPageContentManager.BodyAppend(form2.Draw());
            StaticPageContentManager.Draw();
            assert.equal(form2.Error(), false);

            return ($done : () => void) : void => {
                form2.getEvents().setOnComplete(() : void => {
                    form2.Error(false);
                    $done();
                });
                form2.Visible(false);
            };
        }

        public TabIndex() : void {
            const form : FormsObject = new MockFormsObject();
            assert.equal(form.TabIndex(10), (<any>FormsObject).tabIndex);
        }

        public testsetArg() : void {
            PersistenceFactory.getPersistence(PersistenceType.ERROR_FLAGS).Clear();
            const form : FormsObject = new MockFormsObject();
            form.setArg(<IGuiCommonsArg>{
                name : "arguments",
                type : GuiCommonsArgType.TEXT,
                value: "test"
            }, true);
            assert.patternEqual(JSON.stringify(form.getArgs()),
                "[{\"name\":\"Id\",\"type\":\"Text\",\"value\":\"FormsObject*\"}," +
                "{\"name\":\"StyleClassName\",\"type\":\"Text\",\"value\":\"\"}," +
                "{\"name\":\"Enabled\",\"type\":\"Bool\",\"value\":true}," +
                "{\"name\":\"Visible\",\"type\":\"Bool\",\"value\":true}," +
                "{\"name\":\"Width\",\"type\":\"Number\",\"value\":0}," +
                "{\"name\":\"Height\",\"type\":\"Number\",\"value\":0}," +
                "{\"name\":\"Top\",\"type\":\"Number\",\"value\":0}," +
                "{\"name\":\"Left\",\"type\":\"Number\",\"value\":0}," +
                "{\"name\":\"Title\",\"type\":\"Text\",\"value\":\"\"}," +
                "{\"name\":\"Value\",\"type\":\"Text\",\"value\":null}," +
                "{\"name\":\"Error\",\"type\":\"Bool\",\"value\":false}," +
                "{\"name\":\"TabIndex\",\"type\":\"Number\",\"value\":null}]");
        }

        public testsetArg20() : void {
            const form : FormsObject = new MockFormsObject();
            form.setArg(<IGuiCommonsArg>{
                name : "Error",
                type : GuiCommonsArgType.BOOLEAN,
                value: false
            }, false);
        }

        public testsetArg30() : void {
            const form : FormsObject = new MockFormsObject("id8");
            form.setArg(<IGuiCommonsArg>{
                name : "TabIndex",
                type : GuiCommonsArgType.NUMBER,
                value: 0
            }, false);
        }

        public testFocus() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const form : FormsObject = new MockFormsObject("id66");
                form.DisableAsynchronousDraw();
                Echo.Print(form.Draw());
                const manager : GuiObjectManager = new GuiObjectManager();
                FormsObject.Focus(form);
                assert.patternEqual(form.Draw(),
                    "\r\n<div class=\"ComWuiFrameworkGuiPrimitives\">\r\n" +
                    "   <div id=\"id66_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
                    "      <div id=\"id66\" class=\"FormsObject\" style=\"display: block;\"></div>\r\n" +
                    "   </div>\r\n</div>");
                $done();
            };
        }

        public testBlurSecond() : void {
            const form : FormsObject = new MockFormsObject();
            form.InstanceOwner(new MockBaseViewer());
            StaticPageContentManager.BodyAppend(form.Draw());
            StaticPageContentManager.Draw();
            form.DisableAsynchronousDraw();
            form.Enabled(true);
            form.getEvents().setOnComplete(() : void => {
                FormsObject.Blur();
            });
        }

        public testBlurThird() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                const elements : ArrayList<IGuiCommons> = manager.getActive();
                const form : FormsObject = new MockFormsObject("id12");
                const form2 : FormsObject = new MockFormsObject("id13");
                const form3 : FormsObject = new MockFormsObject("id14");
                const form4 : FormsObject = new MockFormsObject("id15");
                elements.Add(form);
                elements.Add(form2);
                elements.Add(form3);
                elements.Add(form4);
                form.DisableAsynchronousDraw();
                Echo.Print(form4.Draw());
                FormsObject.Blur();
                assert.patternEqual(form4.Draw(),
                    "\r\n<div class=\"ComWuiFrameworkGuiPrimitives\">\r\n" +
                    "   <div id=\"id15_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
                    "      <div id=\"id15\" class=\"FormsObject\" style=\"display: none;\"></div>\r\n" +
                    "   </div>\r\n</div>");
                $done();
            };
        }

        public testgetSelectorEvents() : void {
            const form : FormsObject = new MockFormsObject("37");
            const gui : GuiCommons = new MockGuiCommons("444");
            (<any>FormsObject).selectorEvents = null;
            this.resetCounters();
            assert.equal(form.getSelectorEvents().ToString("__", false), "__Registered events list:\r\n");
            const elementManager : ElementEventsManager = new ElementEventsManager(gui, "444");
            (<any>FormsObject).selectorEvents = elementManager;
            this.resetCounters();
            assert.deepEqual(form.getSelectorEvents().ToString("__", false), "__Registered events list:\r\n");
            const formsobject : FormsObject = new MockFormsObject();
            Echo.PrintCode(JSON.stringify(formsobject));
            const handler : any = () : void => {
                // test event handler
            };
            formsobject.getEvents().setEvent("test", handler);
            assert.equal(formsobject.getEvents().Exists("test"), true);
        }

        public testTabIndex() : void {
            const formsobject : FormsObject = new MockFormsObject();
            assert.equal(formsobject.TabIndex(5), 5);
        }

        public testIsPersistent() : void {
            const form : FormsObject = new MockFormsObject("333");
            form.InstanceOwner(new MockBaseViewer());
            StaticPageContentManager.BodyAppend(form.Draw());
            StaticPageContentManager.Draw();
            form.IsPersistent(true);
            assert.equal(form.IsPersistent(), true);
        }

        public __IgnoretestIsNotPersistent() : IUnitTestRunnerPromise {
            const form : FormsObject = new MockFormsObject("333");
            form.InstanceOwner(new MockBaseViewer());
            StaticPageContentManager.BodyAppend(form.Draw());
            StaticPageContentManager.Draw();
            form.IsPersistent();
            assert.equal(form.IsPersistent(), false);
            return ($done : () => void) : void => {
                form.getEvents().setOnComplete(() : void => {
                    assert.equal(form.IsPersistent(true), true);
                    $done();
                });
                form.Visible(true);
            };
        }

        public teststatusCSS() : void {
            PersistenceFactory.getPersistence(PersistenceType.ERROR_FLAGS).Clear();
            const form : MockFormsObject = new MockFormsObject();
            form.Enabled(true);
            assert.equal(form.testCSS(), "");
            form.Error(true);
            assert.equal(form.testCSS(), GeneralCssNames.ERROR);
            form.Enabled(false);
            assert.equal(form.testCSS(), GeneralCssNames.DISABLE);
            form.Error(false);
            assert.equal(form.testCSS(), "Disable");
        }

        public testerrorCssName() : void {
            PersistenceFactory.getPersistence(PersistenceType.ERROR_FLAGS).Clear();
            const form : MockFormsObject = new MockFormsObject();
            assert.equal(form.testerror(), "");
            form.Error(true);
            assert.equal(form.testCSS(), GeneralCssNames.ERROR);
            form.Error(false);
            assert.equal(form.testerror(), "");
        }

        public testexcludeSerializationData() : void {
            const form : MockFormsObject = new MockFormsObject("80");
            assert.deepEqual(form.testData(), [
                    "objectNamespace", "objectClassName", "options", "availableOptionsList",
                    "parent", "owner", "guiPath", "visible", "enabled", "prepared", "completed",
                    "interfaceClassName", "styleClassName", "containerClassName", "loaded",
                    "asyncDrawEnabled", "contentLoaded", "waitFor", "outputEndOfLine", "innerHtmlMap",
                    "events", "changed", "error", "tabIndex", "autofillPersistence",
                    "valuesPersistence", "errorFlags", "isPersistent"
                ]
            );
        }

        public testexcludeCacheData() : void {
            const form : MockFormsObject = new MockFormsObject("90");
            assert.deepEqual(form.testCacheData(), [
                "options", "availableOptionsList", "events", "childElements", "waitFor",
                "cached", "prepared", "completed", "parent", "owner", "guiPath", "interfaceClassName",
                "styleClassName", "containerClassName", "innerHtmlMap", "loaded", "title",
                "changed", "notification", "selectorEvents", "tabIndex"
            ]);
        }

        public __IgnoretestSubmit() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                let args : AsyncRequestEventArgs;
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.WINDOW, EventType.ON_ASYNC_REQUEST,
                    ($eventArgs : AsyncRequestEventArgs) : void => {
                        args = $eventArgs;
                        EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.WINDOW, EventType.ON_ASYNC_REQUEST);
                    });

                const form : FormsObject = new MockSubmitFormsObject();
                form.Value("test");
                assert.onGuiComplete(form,
                    () : void => {
                        FormsObject.Submit(form.Id(), "HomeBridgeControllerLink");
                    },
                    () : void => {
                        this.initSendBox();
                        $done();
                    }, form);
            };
        }

        protected tearDown() : void {
            this.initSendBox();
            document.documentElement.innerHTML = "";
            this.registerElement("Content");
        }
    }
}
