/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Primitives {
    "use strict";
    import BaseViewerLayerType = Com.Wui.Framework.Gui.Enums.BaseViewerLayerType;

    export class BaseViewerTest extends UnitTestRunner {
        public testCallbackLink() : void {
            assert.equal(BaseViewer.CallbackLink(false),
                "#/com-wui-framework-builder/web/Com/Wui/Framework/Gui/Primitives/BaseViewer");
            assert.equal(BaseViewer.CallbackLink(true), "" +
                "#/com-wui-framework-builder/web/Com/Wui/Framework/Gui/Primitives" +
                "/BaseViewer/TestMode");
        }

        public testConstructor() : void {
            const viewArgs : BaseViewerArgs = new BaseViewerArgs();
            const view : BaseViewer = new BaseViewer(viewArgs);
            assert.equal(view.ToString(""), "Com.Wui.Framework.Gui.Primitives.BaseViewer (instance NOT DEFINED)");
        }

        public testInstanceOwner() : void {
            const view : BaseViewer = new BaseViewer();
            assert.equal(view.InstanceOwner(), null);
        }

        public testIsCached() : void {
            const view : BaseViewer = new BaseViewer();
            assert.equal(view.IsCached(true), true);
        }

        public testIsPrinted() : void {
            const view : BaseViewer = new BaseViewer();
            assert.equal(view.IsPrinted(), false);
        }

        public testTestModeEnabled() : void {
            const view : BaseViewer = new BaseViewer();
            assert.equal(view.TestModeEnabled(false), false);
        }

        public testViewerArgs() : void {
            const viewArgs : BaseViewerArgs = new BaseViewerArgs();
            const view : BaseViewer = new BaseViewer();
            assert.equal(view.ViewerArgs(viewArgs), viewArgs);
        }

        public testgetLayersType() : void {
            const view : BaseViewer = new BaseViewer();
            assert.equal(view.getLayersType(), "Com.Wui.Framework.Gui.Enums.BaseViewerLayerType");
        }

        public testShow() : void {
            const view : BaseViewer = new BaseViewer();
            assert.equal(view.Show(), "");
        }

        public testSerializationData() : void {
            const view : BaseViewer = new BaseViewer();
            assert.deepEqual(view.SerializationData(), {isCached: false, instance: null});
        }

        public testToString() : void {
            const view : BaseViewer = new BaseViewer();
            assert.equal(view.ToString(""), "Com.Wui.Framework.Gui.Primitives.BaseViewer (instance NOT DEFINED)");
        }

        public testtoString() : void {
            const view : BaseViewer = new BaseViewer();
            assert.equal(view.toString(), "Com.Wui.Framework.Gui.Primitives.BaseViewer (instance NOT DEFINED)");
        }

        public testsetLayer() : void {
            const layertype : BaseViewerLayerType = new BaseViewerLayerType();
            const view : BaseViewer = new BaseViewer();
            view.setLayer(layertype);
            assert.equal((<any>BaseViewer).layer, undefined);
        }
    }
}
