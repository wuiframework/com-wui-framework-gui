/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Primitives {
    "use strict";
    import PanelContentType = Com.Wui.Framework.Gui.Enums.PanelContentType;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;
    import GeneralEventOwner = Com.Wui.Framework.Gui.Enums.Events.GeneralEventOwner;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import EventsManager = Com.Wui.Framework.Gui.Events.EventsManager;

    class MockBasePanelViewer extends BasePanelViewer {
        constructor($args? : BasePanelViewerArgs) {
            super($args);
            this.setInstance(new BasePanel("id2"));
        }
    }

    class MockViewer extends BasePanelViewer {
    }

    class MockGuiCommons extends GuiCommons {
    }

    export class BasePanelViewerTest extends UnitTestRunner {

        public testConstructor() : void {
            const panelviewer : BasePanelViewerArgs = new BasePanelViewerArgs();
            const panel : BasePanelViewer = new BasePanelViewer(panelviewer);
            assert.equal(panel.getLayersType(), "Com.Wui.Framework.Gui.Enums.BaseViewerLayerType");
        }

        public testShow() : void {
            const panelviewer : BasePanelViewer = new MockBasePanelViewer();
            const panel : BasePanel = panelviewer.getInstance();

            assert.equal(panelviewer.Show(panel.Id()),
                "id2" +
                "<div id=\"id2_PanelEnvelop\" class=\"Panel\" style=\"display: block;\">id2" +
                "    <div class=\"ComWuiFrameworkGuiPrimitives\">id2" +
                "       <div id=\"id2_GuiWrapper\" guiType=\"GuiWrapper\">id2" +
                "          <div id=\"id2\" class=\"BasePanel\" style=\"display: none;\"></div>id2" +
                "       </div>id2" +
                "    </div>id2" +
                "</div>");
            this.initSendBox();
        }

        public testPrepareImplementation() : void {
            const panelviewer : BasePanelViewerArgs = new BasePanelViewerArgs();
            panelviewer.AsyncEnabled(true);
            panelviewer.Visible(true);
            assert.equal(panelviewer.AsyncEnabled(), true);
            assert.equal(panelviewer.Visible(), true);
        }

        public testgetChildElement() : void {
            const panelViewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
            panelViewerArgs.AsyncEnabled(true);
            panelViewerArgs.Visible(true);
            const viewer : BasePanelViewer = new MockBasePanelViewer(panelViewerArgs);
            const viewer2 : BasePanelViewer = new MockBasePanelViewer(null);
            const panel : BasePanel = viewer.getInstance();

            panel.ContentType(PanelContentType.WITH_ELEMENT_WRAPPER);
            panel.getChildPanelList().Add(viewer);
            panel.getChildPanelList().Add(viewer2);
            panel.IsPrepared();

            assert.deepEqual(panel.getChildPanelList().getItem(0), viewer);
            assert.deepEqual(panel.getChildPanelList().getItem(0).InstanceOwner(), null);
            assert.deepEqual(panel.getChildPanelList().getLast(), viewer2);
            this.initSendBox();
        }

        public __IgnoretestPrepareImplementationEvent() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const panelViewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
                panelViewerArgs.AsyncEnabled(true);
                const viewer : BasePanelViewer = new MockBasePanelViewer(panelViewerArgs);
                const panel : BasePanel = new BasePanel();
                const viewerManager : ViewerManager = new ViewerManager("viewer", panel, panelViewerArgs);
                panel.getChildPanelList().Add(viewer);
                panel.ContentType(PanelContentType.ASYNC_LOADER);
                Reflection.getInstance();

                const data : ArrayList<any> = new ArrayList<any>();
                data.Add("value2", "key2");
                data.Add("value3", "key3");
                data.Add(panel.Id(), HttpRequestConstants.ELEMENT_INSTANCE);
                const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);

                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_COMPLETE,
                    ($eventArgs : AsyncRequestEventArgs) : void => {
                        assert.equal(args.POST().KeyExists(HttpRequestConstants.ELEMENT_INSTANCE), true);
                        assert.equal(args.POST().KeyExists(HttpRequestConstants.ELEMENT_INSTANCE), true);
                        this.initSendBox();
                        ExceptionsManager.Clear();
                        $done();
                    });
                viewerManager.IncludeChildren(true);
                viewerManager.Process();
                viewer.PrepareImplementation();
                EventsManager.getInstanceSingleton().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_COMPLETE);
            };
        }

        public testT() : void {
            const panelViewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
            panelViewerArgs.AsyncEnabled(true);
            const viewer : BasePanelViewer = new MockBasePanelViewer(panelViewerArgs);
            const panel : BasePanel = <BasePanel>BasePanel.getInstance();
            panel.getChildPanelList().Add(viewer);
            Reflection.getInstance();
            viewer.PrepareImplementation();
            this.initSendBox();
        }

        public __IgnoretestPrepareImplementation3() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const panelViewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
                const viewer : BasePanelViewer = new BasePanelViewer(panelViewerArgs);
                const panel : BasePanel = new BasePanel("id65");
                Reflection.getInstance();
                panel.getChildPanelList().Add(viewer);
                assert.onGuiComplete(panel,
                    () : void => {
                        viewer.PrepareImplementation();
                    },
                    $done, viewer);
            };
        }

        public __IgnoretestPrepareImplementation4() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const viewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
                viewerArgs.AsyncEnabled();
                const viewer : BasePanelViewer = new MockBasePanelViewer(viewerArgs);

                const panel : BasePanel = new BasePanel("id75");
                panel.getChildPanelList().Add(viewer);

                Reflection.getInstance();
                assert.onGuiComplete(panel,
                    () : void => {
                        viewer.PrepareImplementation();
                    },
                    $done, viewer);
            };
        }

        public __IgnoretestgetInstance() : void {
            const args : BasePanelViewerArgs = new BasePanelViewerArgs();
            const panel : BasePanel = new BasePanel();
            const viewer : BasePanelViewer = new BasePanelViewer(args);
            panel.ContentType(PanelContentType.HIDDEN);
            viewer.PrepareImplementation();
        }

        public __IgnoretestShowSecond() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const viewer : BasePanelViewer = new MockBasePanelViewer(new BasePanelViewerArgs());
                const panel : BasePanel = viewer.getInstance();
                const gui : GuiElement = new GuiElement();
                panel.Visible(true);
                panel.Scrollable(true);
                Reflection.getInstance();
                ElementManager.setSize("id22", panel.Width(500), panel.Height(500));
                panel.ContentType(PanelContentType.WITHOUT_ELEMENT_WRAPPER);
                panel.getChildPanelList().Add(viewer);
                panel.getEvents().setOnComplete(() : void => {
                    //  assert.equal(panel.Draw().toString(), "");
                    $done();
                });
                viewer.Show();
                this.initSendBox();
            };
        }

        protected tearDown() : void {
            this.initSendBox();
            document.documentElement.innerHTML = "";
            this.registerElement("Content");
        }
    }
}
