/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Utils {
    "use strict";
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import GeneralCssNames = Com.Wui.Framework.Gui.Enums.GeneralCssNames;
    import ElementOffset = Com.Wui.Framework.Gui.Structures.ElementOffset;
    import DirectionType = Com.Wui.Framework.Gui.Enums.DirectionType;
    import HttpRequestParser = Com.Wui.Framework.Commons.HttpProcessor.HttpRequestParser;
    import HttpManager = Com.Wui.Framework.Gui.HttpProcessor.HttpManager;
    import BrowserType = Com.Wui.Framework.Commons.Enums.BrowserType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import ValueProgressEventArgs = Com.Wui.Framework.Gui.Events.Args.ValueProgressEventArgs;
    import ProgressType = Com.Wui.Framework.Gui.Enums.ProgressType;
    import OpacityEventType = Com.Wui.Framework.Gui.Enums.Events.OpacityEventType;
    import EventsManager = Com.Wui.Framework.Gui.Events.EventsManager;

    class MockGuiCommons extends GuiCommons {
    }

    export class ElementManagerTest extends UnitTestRunner {

        public testgetElement() : void {
            const element : HTMLElement = document.createElement("div");
            this.registerElement("id123");
            const gui : GuiCommons = new MockGuiCommons("101");
            this.registerElement("101");
            const element2 : HTMLElement = document.getElementById("id123");
            assert.equal(ElementManager.getElement(element, true), element);
            assert.equal(ElementManager.getElement(null, true), null);
            assert.equal(ElementManager.getElement("id123", true), element2);
            assert.equal(ElementManager.getElement("54321", false), null);
        }

        public testCleanElementCache() : void {
            this.registerElement("test1", "p");
            this.registerElement("test2", "p");
            this.registerElement("test3", "p");
            this.registerElement("element1", "p");

            assert.ok(ElementManager.getElement("test1", false) !== null);
            assert.ok(ElementManager.getElement("test2", false) !== null);
            assert.ok(ElementManager.getElement("test3", false) !== null);
            assert.ok(ElementManager.getElement("element1", false) !== null);

            ElementManager.getElement("test1", false);
            ElementManager.getElement("test2", false);
            ElementManager.getElement("test3", false);
            ElementManager.getElement("element1", false);

            assert.equal((<any>ElementManager).domCache.KeyExists("test1"), true);
            assert.equal((<any>ElementManager).domCache.KeyExists("element1"), true);
            ElementManager.CleanElementCache("element1");
            ElementManager.CleanElementCache("test*");
            assert.equal((<any>ElementManager).domCache.KeyExists("test1"), false);
            assert.equal((<any>ElementManager).domCache.KeyExists("element1"), false);
        }

        public testExists() : void {
            const element2 : HTMLElement = document.createElement("div");
            assert.equal(ElementManager.Exists(element2), true);
        }

        public testsetClassName() : void {
            const element : HTMLElement = document.createElement("div");
            ElementManager.setClassName(element, "HTMLElement");
            assert.equal(ElementManager.getClassName(element), "HTMLElement");

            const gui : GuiCommons = new MockGuiCommons("id5");
            ElementManager.setClassName((<IGuiCommons>gui).Id(), "Gui");
            assert.equal(ElementManager.getClassName((<IGuiCommons>gui).Id()), null);
        }

        public testgetClassName() : void {
            const element : HTMLElement = document.createElement("div");
            ElementManager.setClassName(element, "HTMLElement");
            assert.equal(ElementManager.getClassName(element), "HTMLElement");
            assert.equal(ElementManager.getClassName(null), null);
        }

        public testsetCssPropertywithBrowser() : void {
            const element50 : any = document.createElement("div");
            const gui : GuiCommons = new MockGuiCommons("555");
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js?sessionid=6");
            this.setUserAgent(BrowserType.INTERNET_EXPLORER);
            (<any>HttpManager).request = HttpRequestParser.getInstance("unit");
            /// TODO: validate also other BrowserType use case

            ElementManager.setCssProperty(gui, "background-position", "see individual properties");
            ElementManager.setCssProperty("444", "background-color", 44);
            ElementManager.setCssProperty(element50, "background-position",
                "bottom = bottom center = center bottom = 50% 100%");
            ElementManager.setCssProperty(element50, "background-image", "url(/images/foo.gif");
            ElementManager.setCssProperty(element50, "text-decoration", "none");
            ElementManager.setCssProperty(element50, "text-transform", "capitalize");
            ElementManager.setCssProperty(element50, "text-align", "center");
            ElementManager.setCssProperty(element50, "CLASS-NAME", "mystyle");
            ElementManager.setClassName(element50, "mystyle");

            ElementManager.setCssProperty("555", "line-height:", "200%");
            ElementManager.setCssProperty(gui, "width", "40px");
            ElementManager.setCssProperty("555", "height", "40px");
            ElementManager.setCssProperty(gui, "border", "thin dotted #800080");
            ElementManager.setCssProperty(gui, "BGCOLOR", "RED");
            ElementManager.setCssProperty(gui, "background-color", "red");
            ElementManager.setCssProperty(null, null, null);
            ElementManager.setCssProperty(element50, "float", "right");
            ElementManager.setCssProperty(element50, "styleFloat", "right");

            assert.equal(ElementManager.getCssValue(gui, "background-color"), null);
            assert.equal(ElementManager.getCssValue("", null), null);
            assert.equal(ElementManager.getCssValue("555", "height"), null);
            assert.equal(ElementManager.getCssValue("GuiElement", "background-color"), null);
            assert.equal(ElementManager.getCssValue("element", "float"), null);
            assert.equal(ElementManager.getCssValue("element", "styleFloat"), null);

            ElementManager.ClearCssProperty(element50, "background-image");
            ElementManager.ClearCssProperty(element50, "text-align");
            ElementManager.ClearCssProperty(gui, "BGCOLOR");
            ElementManager.ClearCssProperty(gui, "width");
            ElementManager.ClearCssProperty(gui, "");
            ElementManager.ClearCssProperty(gui, null);
            this.initSendBox();
        }

        public testsetCssPropertywithoutBrowser() : void {
            const element50 : any = document.createElement("span");
            /// TODO: missing asserts
            this.registerElement("id707", "span");
            const gui : GuiCommons = new MockGuiCommons("555");
            ElementManager.setCssProperty(gui, "background-position", "see individual properties");
            ElementManager.setCssProperty("444", "background-color", 44);
            ElementManager.setCssProperty("id707", "background-position",
                "bottom = bottom center = center bottom = 50% 100%");
            ElementManager.setCssProperty("id707", "background-image", "url(/images/foo.gif");
            ElementManager.setCssProperty("id707", "text-decoration", "none");
            ElementManager.setCssProperty("id707", "text-transform", "capitalize");
            ElementManager.setCssProperty("id707", "text-align", "center");
            ElementManager.setCssProperty("id707", "CLASS-NAME", "mystyle");
            ElementManager.setClassName("id707", "mystyle");
            ElementManager.setCssProperty("555", "line-height:", "200%");
            ElementManager.setCssProperty(gui, "width", "40px");
            ElementManager.setCssProperty("555", "height", "40px");
            ElementManager.setCssProperty(gui, "border", "thin dotted #800080");
            ElementManager.setCssProperty(gui, "BGCOLOR", "RED");
            ElementManager.setCssProperty(gui, "background-color", "red");
            ElementManager.setCssProperty(null, null, null);
            ElementManager.setCssProperty("id707", "float", "right");
            ElementManager.setCssProperty("id707", "styleFloat", "right");

            ElementManager.ClearCssProperty("id707", "background-image");
            ElementManager.ClearCssProperty("id707", "text-align");
            ElementManager.ClearCssProperty(gui, "BGCOLOR");
            ElementManager.ClearCssProperty(gui, "width");
            ElementManager.ClearCssProperty(gui, "");
            ElementManager.ClearCssProperty(gui, null);
        }

        public testsetCssClassName() : void {
            const element51 : any = document.createElement("div");
            this.registerElement("id0004", "div");
            // ElementManager.setClassName("id0004", "mystyle");
            ElementManager.setCssProperty("id0004", "className", "TestClass");
            // assert.equal(ElementManager.getCssValue("id0004", "className"), "TestClass");

            const element30 : any = document.createElement("div");
            this.registerElement("id0008", "div");
            ElementManager.setClassName("id0008", "mystyle");
            assert.equal(ElementManager.getClassName("id0008"), "mystyle");
            ElementManager.ClearCssProperty("id0008", element30.styleClassName);

            const element40 : any = document.createElement("div");
            this.registerElement("id0007", "div");
            ElementManager.setClassName("id0007", "#class");
            assert.equal(ElementManager.getClassName("id0007"), "#class");
            ElementManager.ClearCssProperty("id0007", "CLASS-NAME");

            Echo.PrintCode("<div id=\"test\" class=\"maindivhover\"></div>");
            assert.equal(ElementManager.getClassName("test"), null);
            assert.equal(ElementManager.getCssValue("test", "className"), null);
            ElementManager.ClearCssProperty("test", "class-name");
        }

        public testgetCssValue() : void {
            const gui2 : GuiCommons = new MockGuiCommons("444");
            const gui3 : GuiCommons = new MockGuiCommons("666");

            ElementManager.setClassName(gui2, "GuiElement");
            ElementManager.setCssProperty(gui2, "background-color", "yello");
            ElementManager.setCssProperty(gui2, "float", "left");
            ElementManager.setCssProperty(gui3, "styleFloat", "right");
            assert.equal(ElementManager.getCssValue(gui2, "background-color"), null);
            assert.equal(ElementManager.getCssValue("", null), null);
            assert.equal(ElementManager.getCssValue("GuiElement", "background-color"), null);
            assert.equal(ElementManager.getCssValue(gui2, "float"), null);
            assert.equal(ElementManager.getCssValue(gui3, "styleFloat"), null);
        }

        public testgetCssInteegerValue() : void {
            const gui : GuiCommons = new MockGuiCommons("555");
            ElementManager.setCssProperty(gui, "background-position", 65);
            assert.equal(ElementManager.getCssIntegerValue(gui, "background-position"), null);
            Echo.Println("<div id=\"testId6\" style=\"display: none; position: absolute; z-index: 4; right: 52px;" +
                " height: 44px;\">...element...</div>");
            assert.equal(ElementManager.getCssIntegerValue("testId6", "z-index"), 4);
        }

        public testClearCssProperty() : void {
            ElementManager.setCssProperty("444", "background-color", 44);
            ElementManager.ClearCssProperty("444", "background-color");
            assert.equal(ElementManager.Exists("444", true), false);
            ElementManager.setCssProperty("444", null, null);
            ElementManager.ClearCssProperty("444", null);
            assert.equal(ElementManager.Exists("444"), false);
        }

        public testsetInnerHtml() : void {
            const element : HTMLElement = document.createElement("div");
            ElementManager.setInnerHtml(element, "My content text");
            assert.equal(ElementManager.getInnerHtml(element), "My content text");
            ElementManager.setInnerHtml(null, "My content text");
            assert.equal(ElementManager.getInnerHtml(null), null);
        }

        public testAppendHtml() : void {
            const element : HTMLElement = document.createElement("div");
            ElementManager.AppendHtml(element, "Content of element");
            assert.equal(ElementManager.getElement(element, true), element);
            ElementManager.AppendHtml("", "");
            assert.equal(ElementManager.getElement("", false), null);
        }

        public testIsVisible() : void {
            const gui : GuiCommons = new MockGuiCommons("989");
            assert.equal(ElementManager.IsVisible(gui), false);
        }

        public testToggleVisibility() : void {
            ElementManager.ToggleVisibility("989");
            const gui2 : GuiCommons = new MockGuiCommons("878");
            ElementManager.ToggleVisibility(gui2);
        }

        public testToggleOnOff() : void {
            const element : HTMLElement = document.createElement("div");
            ElementManager.setClassName(element, "name");
            assert.equal(ElementManager.getClassName(element), "name");
            ElementManager.ToggleOnOff(element);
            assert.ok(element.className = GeneralCssNames.ON);
            const element5 : HTMLElement = document.createElement("div");
            ElementManager.setClassName(element5, "logo");
            ElementManager.ToggleOnOff(element);
            assert.ok(element.className = GeneralCssNames.OFF);
            ElementManager.ToggleOnOff(null);
        }

        public testTopMost() : void {
            this.registerElement("id44", "p");
            ElementManager.setCssProperty("id44", "position", "fixed");
            ElementManager.TopMost("id44");
            assert.equal(ElementManager.getCssValue("id44", "position"), "fixed");

            const gui10 : GuiCommons = new MockGuiCommons("656");
            ElementManager.TopMost(gui10);
            assert.equal(ElementManager.getCssValue("656", "position"), null);

            const gui20 : GuiCommons = new MockGuiCommons("606");
            ElementManager.setCssProperty(gui20, "position", "fixed");
            ElementManager.TopMost(gui20);
            assert.equal(ElementManager.getCssValue(gui20, "position"), null);
        }

        public testAppendHtmlSecond() : void {
            const element : HTMLElement = document.createElement("span");
            this.registerElement("id001", "span");
            ElementManager.AppendHtml("id001", "Hello");
            ElementManager.AppendHtml("id001", "Hello");
            ElementManager.AppendHtml("id001", "HelloHelloDear");
            ElementManager.AppendHtml("id001", "HelloDearGirlandBoy");
        }

        public testAppendHtmlThird() : void {
            const basegui : GuiCommons = new MockGuiCommons("id002");
            const baseguiNext : GuiCommons = new MockGuiCommons("id003");
            StaticPageContentManager.BodyAppend(baseguiNext.Draw());
            StaticPageContentManager.Draw();
            baseguiNext.Visible(true);

            this.registerElement("id003", "span");
            this.registerElement("id002", "span");
            StaticPageContentManager.BodyAppend(basegui.Draw());
            StaticPageContentManager.Draw();
            basegui.Visible(true);
            ElementManager.AppendHtml("id003", "HelloSubstring");
            assert.equal(ElementManager.getElement(baseguiNext, true).outerHTML,
                "<div id=\"id003\" class=\"GuiCommons\" style=\"display: none;\">" +
                "<span guitype=\"HtmlAppender\">HelloSubstring</span></div>");
            assert.equal(basegui.Draw(),
                "\r\n<div class=\"ComWuiFrameworkGuiPrimitives\">\r\n   " +
                "<div id=\"id002_GuiWrapper\" guiType=\"GuiWrapper\">\r\n      " +
                "<div id=\"id002\" class=\"GuiCommons\" style=\"display: none;\"></div>\r\n   " +
                "</div>\r\n</div>");
            ElementManager.AppendHtml("id002", "Hello");
            basegui.DisableAsynchronousDraw();
            assert.equal(ElementManager.getElement(basegui, true).outerHTML,
                "<div id=\"id002\" class=\"GuiCommons\" style=\"display: none;\">" +
                "<span guitype=\"HtmlAppender\">Hello</span></div>");
            ElementManager.AppendHtml("id002", "Hello");
            basegui.DisableAsynchronousDraw();
            assert.equal(ElementManager.getElement(basegui, true).outerHTML,
                "<div id=\"id002\" class=\"GuiCommons\" style=\"display: none;\"><span guitype=\"HtmlAppender\">Hello</span>" +
                "<span guitype=\"HtmlAppender\">Hello</span></div>");
            baseguiNext.Parent(basegui);
            ElementManager.AppendHtml("id0003", "HelloDear");
            assert.equal(ElementManager.getElement(baseguiNext, true).outerHTML,
                "<div id=\"id003\" class=\"GuiCommons\" style=\"display: none;\">" +
                "<span guitype=\"HtmlAppender\">HelloSubstring</span></div>");
        }

        public testPrependHtml() : void {
            this.registerElement("id009", "span");
            ElementManager.PrependHtml("id009__", "test");
            ElementManager.PrependHtml("id009", "test");
            assert.equal(ElementManager.getElement("id009", true).outerHTML,
                "<span id=\"id009\"><span guitype=\"HtmlAppender\">test</span></span>");
            ElementManager.PrependHtml("id009", "test2");
            assert.equal(ElementManager.getElement("id009", true).outerHTML,
                "<span id=\"id009\"><span guitype=\"HtmlAppender\">test2</span><span guitype=\"HtmlAppender\">test</span></span>");
        }

        public testBringToFront() : void {
            this.registerElement("id33", "span");
            ElementManager.setCssProperty("id33", "position", "relative");
            ElementManager.BringToFront("id33");
            assert.equal(ElementManager.getCssValue("id44", "position"), "fixed");

            const gui10 : GuiCommons = new MockGuiCommons("898");
            ElementManager.BringToFront(gui10);
            assert.equal(ElementManager.getCssValue("898", "position"), null);

            const gui20 : GuiCommons = new MockGuiCommons("808");
            ElementManager.setCssProperty(gui20, "position", "relative");
            ElementManager.BringToFront(gui20);
            assert.equal(ElementManager.getCssValue(gui20, "position"), null);
        }

        public testSendToBack() : void {
            this.registerElement("id22", "div");
            ElementManager.setCssProperty("id22", "position", "relative");
            ElementManager.SendToBack("id22");
            assert.equal(ElementManager.getCssValue("id44", "position"), "fixed");

            const gui20 : GuiCommons = new MockGuiCommons("888");
            ElementManager.SendToBack(gui20);
            assert.equal(ElementManager.getCssValue("888", "position"), null);
        }

        public testsetOpacity() : void {
            ElementManager.setOpacity("id3", 0.98);
            // assert.equal(ElementManager.getCssValue("id3", "opacity"), null);

            this.registerElement("id1", "div");
            ElementManager.setOpacity("id1", 0.98);
            assert.equal(ElementManager.getCssValue("id1", "opacity"), "0.98");
            assert.equal(ElementManager.getCssValue("id1", "-moz-opacity"), null);
            // assert.equal(ElementManager.getCssValue("id1", "filter"), "alpha(opacity=98)");
            // assert.equal(ElementManager.getCssValue("id1", "-ms-filter"),
            //    "progid:DXImageTransform.Microsoft.Alpha(Opacity=98)");
            assert.equal(ElementManager.getCssValue("id1", "-khtml-opacity"), null);

            this.registerElement("id4", "div");
            ElementManager.setOpacity("id4", 120);
            assert.equal(ElementManager.getCssValue("id4", "opacity"), "1");

            this.registerElement("id2", "div");
            ElementManager.setOpacity("id2", -1);
            assert.equal(ElementManager.getCssValue("id2", "opacity"), "0");
        }

        public testStopOpacityChange() : void {
            this.registerElement("id1", "div");
            ElementManager.setOpacity("id1", 0.98);
            ElementManager.ChangeOpacity("id1", DirectionType.DOWN, 0.48);
            assert.equal(ElementManager.getCssValue("id1", "opacity"), "0.98");
        }

        public testChangeOpacityThird() : void {
            this.registerElement("id4", "div");
            ElementManager.setOpacity("id4", 0.60);
            ElementManager.ChangeOpacity("id4", DirectionType.DOWN, 0.70);
            assert.equal(ElementManager.getCssValue("id4", "opacity"), "0.6");
        }

        public testChangeOpacityFourth() : void {
            const gui : GuiCommons = new MockGuiCommons("id5");
            ElementManager.setOpacity(gui.Id(), 0.50);
            assert.equal(ElementManager.getCssValue(gui.Id(), "opacity"), null);
            ElementManager.ChangeOpacity(gui.Id(), DirectionType.UP, 30);
            ElementManager.StopOpacityChange(gui.Id());
        }

        public __IgnoretestChangeOpacitySecond() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const gui : GuiCommons = new MockGuiCommons("id6");
                ElementManager.setOpacity(gui.Id(), 0.50);
                const args : ValueProgressEventArgs = new ValueProgressEventArgs("666");
                args.DirectionType(DirectionType.DOWN);
                args.ProgressType(ProgressType.LINEAR);
                ValueProgressManager.Execute(args);
                EventsManager.getInstanceSingleton().setEvent(gui.Id(), OpacityEventType.CHANGE,
                    ($eventArgs : ValueProgressEventArgs) : void => {
                        assert.deepEqual(args.DirectionType(OpacityEventType.CHANGE), "Down");
                        ElementManager.StopOpacityChange(gui.Id());
                        $done();
                    });
                ElementManager.ChangeOpacity(gui.Id(), DirectionType.DOWN, 30);
            };
        }

        public testEnabled() : void {
            const element : HTMLElement = document.createElement("div");
            ElementManager.Enabled(element, true);
            assert.equal(ElementManager.Exists(element), true);
            ElementManager.Enabled("", true);
            assert.equal(ElementManager.Exists(""), false);
            const element2 : HTMLElement = document.createElement("div");
            ElementManager.Enabled(element2, false);
            assert.equal(ElementManager.Exists(element2), true);
        }

        public testIsEnabled() : void {
            const element : HTMLElement = document.createElement("div");
            ElementManager.Enabled(element, true);
            assert.equal(ElementManager.IsEnabled(element), false);
        }

        public testsetSize() : void {
            const gui60 : GuiCommons = new MockGuiCommons("484");
            ElementManager.setSize(gui60, 70, 90);
        }

        public testsetPosition() : void {
            const gui30 : GuiCommons = new MockGuiCommons("787");
            const offset : ElementOffset = new ElementOffset(99, 66);
            ElementManager.setPosition(gui30, offset);
        }

        public testgetAbsoluteOffset() : void {
            const gui40 : GuiCommons = new MockGuiCommons("656");
            assert.deepEqual(ElementManager.getAbsoluteOffset(gui40), new ElementOffset(0, 0));

            const element30 : any = document.createElement("div");
            element30.setAttribute("height", "100px");
            element30.setAttribute("width", "200px");
            element30.setAttribute("top", "20px");

            this.setUrl("http://localhost:8888/UnitTestEnvironment.js?sessionid=6");
            this.setUserAgent(window.navigator.userAgent + " com-wui-framework-jre");
            (<any>HttpManager).request = HttpRequestParser.getInstance("unit");
            const element50 : any = document.createElement("div");
        }

        public testsetZoom() : void {
            const element120 : HTMLElement = document.createElement("div");
            ElementManager.setClassName(element120, "id666");
            ElementManager.setZoom("id666", 99);
            const element130 : HTMLElement = document.createElement("div");
            ElementManager.setClassName(element130, "id888");
            ElementManager.setZoom("id888", 0.99);
        }

        public testsetWidth() : void {
            ElementManager.setWidth("321", 600);
        }

        public testsetheight() : void {
            ElementManager.setHeight("321", 800);
        }

        public testgetScreenPosition() : void {
            const element50 : any = document.createElement("body");
            this.registerElement("id123", "body");
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js?sessionid=6");
            this.setUserAgent(window.navigator.userAgent);
            (<any>HttpManager).request = HttpRequestParser.getInstance("unit");
            this.setUserAgent(BrowserType.INTERNET_EXPLORER);
            ElementManager.setCssProperty("id123", "border-left-width", 40);
            ElementManager.setCssProperty("id123", "border-top-width", 20);
            assert.deepEqual(ElementManager.getScreenPosition(element50), new ElementOffset(0, 0));
            const offset : ElementOffset = new ElementOffset(400, 200);
            ElementManager.setPosition(element50, offset);
        }

        public testgetScreenPositionNext() : void {
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js?sessionid=6");
            this.setUserAgent(window.navigator.userAgent);
            (<any>HttpManager).request = HttpRequestParser.getInstance("unit");
            this.setUserAgent(BrowserType.FIREFOX);
            this.registerElement("id123", "body");
            const gui : GuiCommons = new MockGuiCommons("555");
            const gui2 : GuiCommons = new MockGuiCommons("222");

            const offset2 : ElementOffset = new ElementOffset(300, 100);
            ElementManager.setPosition(gui2, offset2);
            gui.getChildElements().Add(gui2);

            this.setUrl("http://localhost:8888/UnitTestEnvironment.js?sessionid=6");
            this.setUserAgent(window.navigator.userAgent);
            (<any>HttpManager).request = HttpRequestParser.getInstance("unit");
            this.setUserAgent(BrowserType.FIREFOX);
            ElementManager.setCssProperty("id123", "border-left-width", 40);
            ElementManager.setCssProperty("id123", "border-top-width", 20);
            assert.deepEqual(ElementManager.getScreenPosition(gui), new ElementOffset(0, 0));
            const offset : ElementOffset = new ElementOffset(400, 200);
            ElementManager.setPosition(gui, offset);
        }

        public testgetScreenPositionSecond() : void {
            this.registerElement("id123", "span");
            const offset : ElementOffset = new ElementOffset();
            offset.Top(20);
            offset.Left(10);
            ElementManager.setPosition("id123", offset);
            //   assert.patternEqual(ElementManager.getScreenPosition("id123").toString(), offset.toString());

            Echo.PrintCode("<script type=\"text/tcl\">proc my_onload {} {. . .}set win " +
                "[window open \"some/other/URI\"]if {$win != \"\"} {$win onload my_onload} </script>" +
                "input<div class=\"ComWuiFrameworkGuiPrimitives\">" +
                "<h5 id=\"HEOLProjectsIndex\"><span>EOL Projects Index</span></h5>" +
                "input   <div id=\"BaseGuiObject*_GuiWrapper\" guiType=\"GuiWrapper\">" +
                "input      <div id=\"BaseGuiObject*\" class=\"BaseGuiObject\" style=\"position: relative;\">" +
                "<div id=\"BaseGuiObject*\" class=\"\" style=\"position: absolute !important; visibility: hidden !important\">" +
                "</div>\"</div>" +
                "<p>Hi<b>Hi &lt;Some text=\"\"&gt; and other text</b></b>" +
                "input   </div>" +
                "input</div>");

            this.resetCounters();
            assert.patternEqual(ElementManager.getScreenPosition("HEOLProjectsIndex").toString(),
                "[Top] <i>Object type:</i> number. <i>Return value:</i> 0<br/>" +
                "[Left] <i>Object type:</i> number. <i>Return value:</i> 0<br/>" +
                "[getClassName] Com.Wui.Framework.Gui.Structures.ElementOffset<br/>" +
                "[getNamespaceName] Com.Wui.Framework.Gui.Structures<br/>" +
                "[getClassNameWithoutNamespace] ElementOffset<br/>" +
                "[IsMemberOf] <i>Object type:</i> boolean. <i>Return value:</i> false<br/>" +
                "[Implements] <i>Object type:</i> boolean. <i>Return value:</i> false<br/>" +
                "[getHash] <i>Object type:</i> number. <i>Return value:</i> *<br/>" +
                "[excludeSerializationData] <i>Object type:</i> array. <i>Return values:</i><br/><i>Array object</i> " +
                "<span onclick=\"" +
                "document.getElementById('ContentBlock_0').style.display=" +
                "document.getElementById('ContentBlock_0').style.display==='block'?'none':'block';" +
                "\" style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000;" +
                " FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black;" +
                " display: none;\">" +
                "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;objectNamespace<br/>" +
                "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;objectClassName<br/></span><br/>" +
                "[excludeIdentityHashData] <i>Object type:</i> array. <i>Return values:</i><br/>" +
                "<i>Array object</i> " +
                "<span onclick=\"" +
                "document.getElementById('ContentBlock_1').style.display=" +
                "document.getElementById('ContentBlock_1').style.display==='block'?'none':'block';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span><br/>");
        }

        public testScreenPosition5() : void {
            Echo.PrintCode("<div id=\"precontent\" data-region=\"precontent\" data-id=\"19\" data-m=\"" +
                "{&quot;i&quot;:19,&quot;n&quot;:&quot;precontent&quot;,&quot;y&quot;:6}\"> " +
                "<div id=\"refreshbar\" class=\"custombanner\" title=\"refresh page\" data-id=\"20\" data-m=\"" +
                "{&quot;i&quot;:20,&quot;n&quot;:&quot;UIPR_render&quot;,&quot;y&quot;:8}\" data-aop=\"uipr_bar\"> " +
                "<div class=\"refreshbody\"> <a href=\"https://www.msn.com:443/?pc=SK216&amp;ocid=SK216DHP&amp;osmkt=en-us\" " +
                "data-id=\"21\" data-m=\"{&quot;i&quot;:21,&quot;p&quot;:20,&quot;n&quot;:&quot;UIPR_button&quot;,&quot;y&quot;:11," +
                "&quot;o&quot;:1}\"><span class=\"icon reset\"></span><span class=\"text\" aria-hidden=\"true\">" +
                "New content is available. <span class=\"underline\">Click here to refresh the page.</span></span>" +
                "<span class=\"button\" role=\"button\">REFRESH</span></a><a href=\"#\" class=\"icon delete\" data-id=\"22\" data-m=\"" +
                "{&quot;i&quot;:22,&quot;p&quot;:20,&quot;n&quot;:&quot;UIPR_close&quot;,&quot;y&quot;:11,&quot;o&quot;:2}\"" +
                " title=\"close\"><div id=\"BaseGuiObject\" class=\"\" style=\"position: absolute; top: 10px\"></div></a></div>" +
                "</div><div id=\"mestripebg\"></div></div>");

            assert.patternEqual(ElementManager.getScreenPosition("BaseGuiObject*")
                , "[Top] <i>Object type:</i> number. <i>Return value:</i> 0<br/>" +
                "[Left] <i>Object type:</i> number. <i>Return value:</i> 0<br/>" +
                "[getClassName] Com.Wui.Framework.Gui.Structures.ElementOffset<br/>*");
        }

        public testScreenPosition6() : void {
            Echo.PrintCode("<div id=\"precontent\" data-region=\"precontent\" data-id=\"19\" data-m=\"" +
                "{&quot;i&quot;:19,&quot;n&quot;:&quot;precontent&quot;,&quot;y&quot;:6}\"> " +
                "<div id=\"refreshbar\" class=\"custombanner\" title=\"refresh page\" data-id=\"20\" data-m=\"" +
                "{&quot;i&quot;:20,&quot;n&quot;:&quot;UIPR_render&quot;,&quot;y&quot;:8}\" data-aop=\"uipr_bar\"> " +
                "<div class=\"refreshbody\"> <a href=\"https://www.msn.com:443/?pc=SK216&amp;ocid=SK216DHP&amp;osmkt=en-us\" " +
                "data-id=\"21\" data-m=\"{&quot;i&quot;:21,&quot;p&quot;:20,&quot;n&quot;:&quot;UIPR_button&quot;,&quot;y&quot;:11," +
                "&quot;o&quot;:1}\"><div id=\"BaseGui\" class=\"class\" style=\"position: absolute; border-left-width: 2em;\"></div>" +
                "<span class=\"icon reset\"></span><span class=\"text\" aria-hidden=\"true\">" +
                "New content is available. <span class=\"underline\">Click here to refresh the page.</span></span>" +
                "<span class=\"button\" role=\"button\">REFRESH</span></a><a href=\"#\" class=\"icon delete\" data-id=\"22\" data-m=\"" +
                "{&quot;i&quot;:22,&quot;p&quot;:20,&quot;n&quot;:&quot;UIPR_close&quot;,&quot;y&quot;:11,&quot;o&quot;:2}\"" +
                " title=\"close\"><div id=\"BaseGuiObject\" class=\"\" style=\"position: absolute; border-left-width: thick;\"></div>" +
                "</a></div>" +
                "</div><div id=\"mestripebg\"></div></div>");
        }

        public testTurnOff() : void {
            const gui : GuiCommons = new MockGuiCommons();
            gui.Visible(true);
            StaticPageContentManager.BodyAppend(gui.Draw());
            StaticPageContentManager.Draw();
            gui.Enabled(true);
            ElementManager.TurnOn(gui);
            ElementManager.TurnOff(gui);
            assert.equal(ElementManager.getClassName(gui.Id()), GeneralCssNames.OFF);
        }

        public testTurnOn() : void {
            const gui : GuiCommons = new MockGuiCommons("id6");
            gui.Visible(true);
            gui.Enabled(true);
            StaticPageContentManager.BodyAppend(gui.Draw());
            StaticPageContentManager.Draw();
            gui.DisableAsynchronousDraw();
            ElementManager.TurnOn("id6");
            assert.equal(ElementManager.getClassName("id6"), GeneralCssNames.ON);
        }

        public testTurnActive() : void {
            const gui : GuiCommons = new MockGuiCommons("id7");
            gui.Visible(true);
            gui.Enabled(true);
            StaticPageContentManager.BodyAppend(gui.Draw());
            StaticPageContentManager.Draw();
            gui.DisableAsynchronousDraw();
            ElementManager.TurnOn("id7");
            ElementManager.TurnActive("id7");
            assert.equal(ElementManager.getClassName("id7"), GeneralCssNames.ACTIVE);
        }

        public testTuggleVisibility() : void {
            Echo.Println("<div id=\"testId11\">test element</div>");
            ElementManager.ToggleVisibility("testId11");
            assert.equal(ElementManager.getCssValue(
                "testId11", "display"), "none", "validate that element is not visible");
        }

        public __IgnoretestgetCssClassText() : void {
            const element : GuiCommons = new MockGuiCommons("id300");
            element.Width(100);
            element.Height(200);
            element.Visible(true);
            element.Enabled(true);
            element.DisableAsynchronousDraw();
            assert.patternEqual(element.Draw(),
                "\r\n<div class=\"ComWuiFrameworkGuiPrimitives\">\r\n" +
                "   <div id=\"id300_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
                "      <div id=\"id300\" class=\"GuiCommons\" style=\"display: block;\"></div>\r\n" +
                "   </div>\r\n" +
                "</div>");

            assert.equal(WindowManager.ViewHTMLCode(), "<div class=\"ViewHtmlCode\"><pre><div class=\"Line  Odd\">" +
                "<span class=\"Number\">1</span><span class=\"Tag\"><span class=\"Tag\">&lt;head&gt;</span><span class=\"Tag\">" +
                "&lt;/head&gt;</span><span class=\"Tag\">&lt;body&gt;</span><span class=\"Tag\">&lt;div <span class=\"Attribute\">" +
                "id=</span><span class=\"String\">\"Content\"</span>&gt;</span><span class=\"Tag\">&lt;/div&gt;</span>" +
                "<span class=\"Tag\">&lt;/body&gt;</span></span></div><div class=\"Line  Even\">" +
                "<span class=\"Number\">2</span></div></pre></div>");
            ElementManager.setCssProperty(element.Id(), "float", "left");
            assert.equal(ElementManager.getCssValue(element.Id(), "float"), "left");
        }

        protected tearDown() : void {
            this.initSendBox();
            document.documentElement.innerHTML = "";
            this.registerElement("Content");
        }
    }
}
