/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Gui.Utils {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class TextSelectionManagerTest extends UnitTestRunner {
        public testSelectAll() : void {
            const manager : TextSelectionManager = new TextSelectionManager();
            const element : HTMLElement = document.createElement("div");
            const elementinput : HTMLInputElement = document.createElement("input");
            TextSelectionManager.SelectAll(element);

            assert.equal(manager.ToString(""), "object type of \'Com.Wui.Framework.Gui.Utils.TextSelectionManager\'");
            const inputelement : HTMLInputElement = document.createElement("input");
            TextSelectionManager.SelectAll(inputelement);
            assert.equal(TextSelectionManager.getCurrentPosition(inputelement), 0);
            TextSelectionManager.SelectAll("dialog");
            assert.equal(manager.ToString(""), "object type of \'Com.Wui.Framework.Gui.Utils.TextSelectionManager\'");
            TextSelectionManager.SelectAll(elementinput);
        }

        public testgetSelectedTextSecond() : void {
            const inputelement : HTMLInputElement = document.createElement("input");
            assert.equal(TextSelectionManager.getSelectedText(inputelement), "");
            assert.equal(TextSelectionManager.getSelectedText(), "");
        }

        public testClear() : void {
            assert.equal(TextSelectionManager.Clear(), false);
            const inputelement : HTMLInputElement = document.createElement("input");
            assert.equal(TextSelectionManager.Clear(inputelement), false);
        }

        public __IgnoretestgetSelectedTextThird() : void {
            Echo.Println("<label><input id=\"testId\" type=\"text\">Multiple</label><br>");
            const elementinput : HTMLInputElement = document.createElement("input");
            assert.equal(TextSelectionManager.getSelectedText(<HTMLInputElement>ElementManager.getElement("testId")).toString(), "");
            TextSelectionManager.SelectAll(<HTMLInputElement>ElementManager.getElement("testId"));
            assert.equal(TextSelectionManager.getSelectedText(<HTMLInputElement>ElementManager.getElement("testId")), "Multiple");
            TextSelectionManager.SelectAll(<HTMLInputElement>ElementManager.getElement("testId"));
        }

        public __IgnoretestsetCurrentPositionSecond() : void {
            const elementinput : HTMLInputElement = document.createElement("input");
            Echo.Println("<label><input id=\"testId\" type=\"text\">Multiple</label><br>");
            assert.equal(TextSelectionManager.setCurrentPosition(<HTMLInputElement>ElementManager.getElement("testId", true), 5), "");
            TextSelectionManager.SelectAll(<HTMLInputElement>ElementManager.getElement("testId"));
            assert.equal(TextSelectionManager.getCurrentPosition(<HTMLInputElement>ElementManager.getElement("testId", true)), 5);
        }

        public __IgnoretestgetCurrentPositionSecond() : void {
            const elementinput : HTMLInputElement = document.createElement("input");
            Echo.Println("<label><input id=\"testId\" type=\"text\">Multiple</label><br>");
            TextSelectionManager.setCurrentPosition(<HTMLInputElement>ElementManager.getElement("testId"), 4);
            TextSelectionManager.SelectAll(<HTMLInputElement>ElementManager.getElement("testId"));
            assert.equal(TextSelectionManager.getCurrentPosition(<HTMLInputElement>ElementManager.getElement("testId")), 4);
        }

        public __IgnoretestPasteTextAtCurrentPositionSecond() : void {
            const elementinput2 : HTMLInputElement = document.createElement("input");
            Echo.Println("<label><input id=\"testId\" type=\"text\">Multiple</label><br>");
            TextSelectionManager.setCurrentPosition(<HTMLInputElement>ElementManager.getElement("testId"), 4);
            TextSelectionManager.PasteTextAtCurrentPosition("new");
            assert.equal(TextSelectionManager.getSelectedText(), "Multnewiple");
        }

        public __IgnoretestClearSecond() : void {
            const elementinput : HTMLInputElement = document.createElement("input");
            TextSelectionManager.Clear(elementinput);
            assert.equal(TextSelectionManager.Clear(), true, "");
        }
    }
}
